package explore.arrays101;

public class ValidMountainArray {
    public static boolean validMountainArray(int[] A) {
        if (A == null || A.length < 3) {
            return false;
        }
        int i = 0;
        int j = A.length - 1;
        while (i < A.length - 1 && A[i] < A[i + 1]) {
            i++;
        }
        while (j > 0 && A[j - 1] > A[j]) {
            j--;
        }
        return i < A.length - 1 && i == j && j > 0;
    }
}
