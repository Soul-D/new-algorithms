package explore.arrays101;

public class ThirdMaximumNumber {
    public static int thirdMax(int[] nums) {
        long min = Long.MIN_VALUE;
        long first, second, third;

        first = second = third = min;
        for (int num : nums) {
            if (num == first || num == second || num == third) {
                continue;
            }
            if (first == min || first < num) {
                third = second;
                second = first;
                first = num;
            } else if (second == min || second < num) {
                third = second;
                second = num;
            } else if (third == min || third < num) {
                third = num;
            }
        }

        return third == min ? (int) first : (int) third;
    }
}
