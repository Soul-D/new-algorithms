package explore.arrays101;

public class FindNumbersWithEvenNumberOfDigits {
    public static int findNumbers(int[] nums) {
        int count = 0;
        for (int num : nums) {
            if (isEvenLength(num)) {
                count++;
            }
        }
        return count;
    }

    private static boolean isEvenLength(int n) {
        if (n < 100_000) {
            if (n < 10_000) {
                if (n < 1000) {
                    if (n < 100) {
                        return n >= 10;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
}
