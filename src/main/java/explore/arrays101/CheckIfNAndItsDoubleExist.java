package explore.arrays101;

public class CheckIfNAndItsDoubleExist {
    public static boolean checkIfExist(int[] arr) {
        int[] cache = new int[2001];
        boolean firstZero = false;
        for (int value : arr) {
            if (value == 0) {
                if (firstZero) {
                    return true;
                }
                firstZero = true;
                continue;
            }
            if (-500 <= value && value <= 500) {
                if (cache[value * 2 + 1000] != 0) {
                    return true;
                }
            }
            if (value % 2 == 0) {
                if (cache[(value / 2) + 1000] != 0) {
                    return true;
                }
            }
            cache[value + 1000] = 1;
        }
        return false;
    }
}
