package explore.arrays101;

public class SquaresOfASortedArray {
    public static int[] sortedSquares(int[] A) {
        int start = 0;
        int end = A.length - 1;
        int index = end;
        int[] result = new int[A.length];
        while (start <= end) {
            if (Math.abs(A[start]) <= Math.abs(A[end])) {
                result[index--] = A[end] * A[end];
                end--;
            } else {
                result[index--] = A[start] * A[start];
                start++;
            }
        }

        return result;
    }
}
