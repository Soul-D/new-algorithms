package explore.arrays101;

public class MoveZeroes {
    public static void moveZeroes(int[] nums) {
        for (int current = 0, lastZero = 0; current < nums.length; current++) {
            if (nums[current] != 0) {
                swap(nums, lastZero++, current);
            }
        }
    }

    private static void swap(int[] ints, int x, int y) {
        int temp = ints[x];
        ints[x] = ints[y];
        ints[y] = temp;
    }
}
