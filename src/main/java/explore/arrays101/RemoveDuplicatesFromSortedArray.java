package explore.arrays101;

public class RemoveDuplicatesFromSortedArray {
    public static int removeDuplicates(int[] nums) {
        if (nums.length == 0 || nums.length == 1) {
            return nums.length;
        }
        int size = 1;
        int lastIndex = 1;
        int currentEl = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == currentEl) {
                continue;
            }
            nums[lastIndex++] = nums[i];
            currentEl = nums[i];
            size++;
        }
        return size;
    }
}
