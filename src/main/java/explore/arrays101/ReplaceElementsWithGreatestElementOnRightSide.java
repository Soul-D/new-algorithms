package explore.arrays101;

public class ReplaceElementsWithGreatestElementOnRightSide {
    public static int[] replaceElements(int[] arr) {
        int max = arr[arr.length - 1];
        arr[arr.length - 1] = -1;
        int temp;
        for (int i = arr.length - 2; i >= 0; i--) {
            if (arr[i] > max) {
                temp = max;
                max = arr[i];
                arr[i] = temp;
            } else {
                arr[i] = max;
            }
        }

        return arr;
    }

    public static int[] replaceElementsSlow(int[] arr) {
        if (arr.length == 1) {
            arr[0] = -1;
            return arr;
        }
        int currMax = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] >= currMax) {
                currMax = getGreaterOnRight(arr, i + 1);
            }
            arr[i] = currMax;
        }
        arr[arr.length - 1] = -1;
        return arr;
    }

    private static int getGreaterOnRight(int[] arr, int index) {
        int max = arr[index];
        for (int i = index + 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }
}
