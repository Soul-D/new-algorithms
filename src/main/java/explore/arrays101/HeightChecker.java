package explore.arrays101;

public class HeightChecker {
    public static int heightChecker(int[] heights) {
        int[] frequencies = new int[101];

        for (int height : heights) {
            frequencies[height]++;
        }

        int result = 0;

        for (int i = 0, current = 0; i < heights.length; i++) {
            while (frequencies[current] == 0) {
                current++;
            }

            if (current != heights[i]) {
                result++;
            }
            frequencies[current]--;
        }

        return result;
    }
}
