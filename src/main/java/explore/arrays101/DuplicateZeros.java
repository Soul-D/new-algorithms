package explore.arrays101;

public class DuplicateZeros {
    public static void duplicateZeros(int[] arr) {
        int shift = 0;
        int size = arr.length - 1;
        for (int i = 0; i <= size - shift; i++) {
            if (arr[i] != 0) {
                continue;
            }
            if (i == size - shift) {
                arr[size] = 0;
                size--;
                break;
            }
            shift++;
        }

        int last = size - shift;

        for (int i = last; i >= 0; i--) {
            if (arr[i] == 0) {
                arr[i + shift] = 0;
                shift--;
                arr[i + shift] = 0;
            } else {
                arr[i + shift] = arr[i];
            }
        }
    }
}
