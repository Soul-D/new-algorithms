package explore.arrays101;

public class MaxConsecutiveOnesII {
    public static int findMaxConsecutiveOnes(int[] nums) {
        int max = 0;
        int currentWindow = 0;
        int prevWindow = 0;
        for (int num : nums) {
            currentWindow++;
            if (num == 0) {
                prevWindow = currentWindow;
                currentWindow = 0;
            }
            max = Math.max(max, currentWindow + prevWindow);
        }
        return max;
    }
}
