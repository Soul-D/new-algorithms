package explore.arrays101;

public class SortArrayByParity {
    public static int[] sortArrayByParity(int[] A) {
        int left = 0;
        int right = A.length - 1;
        while (left <= right) {
            while (left < A.length && A[left] % 2 == 0) {
                left++;
            }
            while (right >= 0 && A[right] % 2 != 0) {
                right--;
            }
            if (left > right) {
                break;
            }
            swap(A, left++, right--);
        }
        return A;
    }

    private static void swap(int[] ints, int x, int y) {
        int temp = ints[x];
        ints[x] = ints[y];
        ints[y] = temp;
    }
}
