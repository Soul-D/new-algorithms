package explore.queue_stack;

public class DesignCircularQueue {
    public static class MyCircularQueue {
        private final int[] array;
        private int tail;
        private int num;

        /**
         * Initialize your data structure here. Set the size of the queue to be k.
         */
        public MyCircularQueue(int k) {
            this.array = new int[k];
            this.tail = -1;
            this.num = 0;
        }

        /**
         * Insert an element into the circular queue. Return true if the operation is successful.
         */
        public boolean enQueue(int value) {
            if (isFull()) {
                return false;
            }
            tail = tailAfterEnqueue();
            array[tail] = value;
            num++;
            return true;
        }

        /**
         * Delete an element from the circular queue. Return true if the operation is successful.
         */
        public boolean deQueue() {
            if (isEmpty()) {
                return false;
            }
            num--;
            return true;
        }

        /**
         * Get the front item from the queue.
         */
        public int Front() {
            if (isEmpty()) {
                return -1;
            }
            return array[head()];
        }

        /**
         * Get the last item from the queue.
         */
        public int Rear() {
            if (isEmpty()) {
                return -1;
            }
            return array[tail];
        }

        /**
         * Checks whether the circular queue is empty or not.
         */
        public boolean isEmpty() {
            return num == 0;
        }

        /**
         * Checks whether the circular queue is full or not.
         */
        public boolean isFull() {
            return num == array.length;
        }

        private int head() {
            return (array.length + (tail - num + 1)) % array.length;
        }

        private int tailAfterEnqueue() {
            return (tail + 1) % array.length;
        }
    }
}
