package explore.queue_stack;

import java.util.*;

public class OpenTheLock {
    public static int openLock(String[] deadends, String target) {
        Set<String> deads = new HashSet<>(Arrays.asList(deadends));
        if (deads.contains("0000")) {
            return -1;
        }
        if ("0000".equals(target)) {
            return 0;
        }
        Set<String> v = new HashSet<>();
        Queue<String> queue = new LinkedList<>();
        queue.add("0000");
        for (int turns = 1; !queue.isEmpty(); turns++) {
            for (int n = queue.size(); n > 0; n--) {
                String cur = queue.poll();
                for (int i = 0; i < 4; i++) {
                    for (int dif = 1; dif <= 9; dif += 8) {
                        char[] ca = cur.toCharArray();
                        ca[i] = (char) ((ca[i] - '0' + dif) % 10 + '0');
                        String s = new String(ca);
                        if (target.equals(s)) {
                            return turns;
                        }
                        if (!deads.contains(s) && !v.contains(s)) {
                            queue.add(s);
                        }
                        v.add(s);
                    }
                }
            }
        }
        return -1;
    }
}
