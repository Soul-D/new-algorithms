package explore.queue_stack;

import java.util.LinkedList;
import java.util.Queue;

public class NumberOfIslands {
    public static int numIslands(char[][] grid) {
        int m = grid.length;
        int n = m != 0 ? grid[0].length : 0;
        int islands = 0;
        int[] offsets = {0, 1, 0, -1, 0};
        for (int row = 0; row < m; row++) {
            for (int col = 0; col < n; col++) {
                if (grid[row][col] == '1') {
                    islands++;
                    fillIslands(grid, m, n, offsets, row, col);
                }
            }
        }
        return islands;
    }

    private static void fillIslands(char[][] grid, int m, int n, int[] offsets, int row, int col) {
        grid[row][col] = '0';
        Queue<Point> todo = new LinkedList<>();
        todo.offer(new Point(row, col));
        while (!todo.isEmpty()) {
            Point p = todo.poll();
            for (int k = 0; k < 4; k++) {
                int r = p.first + offsets[k];
                int c = p.second + offsets[k + 1];
                if (r >= 0 && r < m && c >= 0 && c < n && grid[r][c] == '1') {
                    grid[r][c] = '0';
                    todo.offer(new Point(r, c));
                }
            }
        }
    }

    private static class Point {
        int first;
        int second;

        public Point(int first, int second) {
            this.first = first;
            this.second = second;
        }
    }
}
