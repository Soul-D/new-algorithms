package explore.binary_tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinaryTreeLevelOrderTraversal {
    public static List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Queue<TreeNode> level = new LinkedList<>();
        level.add(root);
        while (!level.isEmpty()) {
            List<Integer> integers = new ArrayList<>();
            int size = level.size();
            for (int i = 0; i < size; i++) {
                TreeNode treeNode = level.poll();
                integers.add(treeNode.val);
                if (treeNode.left != null) {
                    level.add(treeNode.left);
                }
                if (treeNode.right != null) {
                    level.add(treeNode.right);
                }
            }
            result.add(integers);
        }
        return result;
    }
}
