package explore.binary_tree;

import java.util.*;

// Runtime: 12 ms
// Memory Usage: 49.6 MB
public class SerializeAndDeserializeBinaryTree {
    private static final String SPLITTER = ",";
    private static final String NULL = "null";
    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        StringBuilder builder = new StringBuilder();
        build(root, builder);
        return builder.toString();
    }

    private void build(TreeNode root, StringBuilder builder) {
        if (root == null) {
            builder.append(NULL).append(SPLITTER);
        } else {
            builder.append(root.val).append(SPLITTER);
            build(root.left, builder);
            build(root.right, builder);
        }
    }

    private String getString(TreeNode node) {
        return node == null ? NULL : String.valueOf(node.val);
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        Deque<String> deque = new LinkedList<>(Arrays.asList(data.split(SPLITTER)));
        return build(deque);
    }

    private TreeNode build(Deque<String> deque) {
        String remove = deque.remove();
        if (remove.endsWith(NULL)) {
            return null;
        }
        TreeNode root = new TreeNode(Integer.parseInt(remove));
        root.left = build(deque);
        root.right = build(deque);
        return root;
    }
}
