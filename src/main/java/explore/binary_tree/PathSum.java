package explore.binary_tree;

public class PathSum {
    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        int rest = sum - root.val;
        if (rest == 0 && root.right == null && root.left == null) {
            return true;
        }
        return hasPathSum(root.left, rest) || hasPathSum(root.right, rest);
    }
}
