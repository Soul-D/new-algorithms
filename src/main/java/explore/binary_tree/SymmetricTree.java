package explore.binary_tree;

import java.util.*;

public class SymmetricTree {

    public boolean isSymmetricIterative(TreeNode root) {
        if (root == null) {
            return true;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            List<Integer> elements = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll == null) {
                    elements.add(null);
                } else {
                    elements.add(poll.val);
                    queue.add(poll.left);
                    queue.add(poll.right);
                }
            }
            int low = 0;
            int high = elements.size() - 1;
            while (low <= high) {
                if (!Objects.equals(elements.get(low++), elements.get(high--))) {
                    return false;
                }
            }
        }

        return true;
    }

    public boolean isSymmetricRecursive(TreeNode root) {
        return root == null || isSymmetric(root.left, root.right);
    }

    private boolean isSymmetric(TreeNode left, TreeNode right) {
        if (left == null && right == null) {
            return true;
        }
        if ((left != null && right == null) || (left == null && right != null)) {
            return false;
        }
        if (left.val != right.val) {
            return false;
        }
        return isSymmetric(left.left, right.right) && isSymmetric(left.right, right.left);
    }
}
