package explore.binary_tree;

import java.util.*;

public class LowestCommonAncestorOfABinaryTree {
    private TreeNode ans;

    // Runtime: 9 ms
    // Memory Usage: 45.6 MB
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        find(root, p, q);
        return ans;
    }

    public boolean find(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) {
            return false;
        }

        int left = find(root.left, p, q) ? 1 : 0;
        int right = find(root.right, p, q) ? 1 : 0;
        int middle = (root == p || root == q) ? 1 : 0;

        if (left + middle + right >= 2) {
            ans = root;
        }

        return (left + middle + right) > 0;
    }

    // Runtime: 24 ms
    // Memory Usage: 46 MB
    public TreeNode lowestCommonAncestorIterative(TreeNode root, TreeNode p, TreeNode q) {
        Stack<TreeNode> stack = new Stack<>();
        Map<TreeNode, TreeNode> parents = new HashMap<>();
        stack.push(root);
        parents.put(root, null);

        while (!(parents.containsKey(p) && parents.containsKey(q))) {
            root = stack.pop();
            if (root.left != null) {
                stack.push(root.left);
                parents.put(root.left, root);
            }
            if (root.right != null) {
                stack.push(root.right);
                parents.put(root.right, root);
            }
        }

        Set<TreeNode> ancestors = new HashSet<>();
        while (p != null) {
            ancestors.add(p);
            p = parents.get(p);
        }
        while (!ancestors.contains(q)) {
            q = parents.get(q);
        }

        return q;
    }
}
