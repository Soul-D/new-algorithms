package explore.binary_tree;

import java.util.HashMap;
import java.util.Map;

public class ConstructBinaryTreeFromPreorderAndInorderTraversal {
    public static TreeNode buildTreeHashMap(int[] preorder, int[] inorder) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < inorder.length; i++) {
            map.put(inorder[i], i);
        }

        return build(0, 0, inorder.length - 1, preorder, map);
    }

    private static TreeNode build(int preStart, int inStart, int inEnd, int[] preorder, Map<Integer, Integer> map) {
        if (preStart >= preorder.length || inStart > inEnd) {
            return null;
        }
        TreeNode root = new TreeNode(preorder[preStart]);
        int index = map.get(preorder[preStart]);
        root.left = build(preStart + 1, inStart, index - 1, preorder, map);
        root.right = build(preStart + index - inStart + 1, index + 1, inEnd, preorder, map);
        return root;
    }

    public static TreeNode buildTreeIterate(int[] preorder, int[] inorder) {
        return build(0, 0, inorder.length - 1, preorder, inorder);
    }

    private static TreeNode build(int preStart, int inStart, int inEnd, int[] preorder, int[] inorder) {
        if (preStart >= preorder.length || inStart > inEnd) {
            return null;
        }
        TreeNode root = new TreeNode(preorder[preStart]);
        int index = 0;
        for (int i = inStart; i <= inEnd; i++) {
            if (inorder[i] == preorder[preStart]) {
                index = i;
                break;
            }
        }
        root.left = build(preStart + 1, inStart, index - 1, preorder, inorder);
        root.right = build(preStart + index - inStart + 1, index + 1, inEnd, preorder, inorder);
        return root;
    }
}
