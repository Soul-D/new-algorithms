package explore.binary_tree;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreePreorderTraversal {
    public static List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        traverse(root, result);
        return result;
    }

    private static void traverse(TreeNode root, List<Integer> acc) {
        if (root == null) {
            return;
        }
        acc.add(root.val);
        traverse(root.left, acc);
        traverse(root.right, acc);
    }
}
