package explore.binary_tree;

public class PopulatingNextRightPointersInEachNode {
    public static Node connect(Node root) {
        Node start = root;
        while (start != null) {
            Node current = start;
            while (current != null) {
                if (current.left != null) {
                    current.left.next = current.right;
                }
                if (current.right != null && current.next != null) {
                    current.right.next = current.next.left;
                }

                current = current.next;
            }
            start = start.left;
        }
        return root;
    }
}
