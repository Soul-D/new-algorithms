package explore.binary_tree;

import java.util.HashMap;
import java.util.Map;

public class ConstructBinaryTreeFromInorderAndPostorderTraversal {
    public static TreeNode buildTree(int[] inorder, int[] postorder) {
        if (inorder == null || postorder == null || inorder.length != postorder.length) {
            return null;
        }
        Map<Integer, Integer> hm = new HashMap<>();
        for (int i = 0; i < inorder.length; ++i) {
            hm.put(inorder[i], i);
        }
        return buildTree(0, inorder.length - 1, postorder, 0, postorder.length - 1, hm);
    }

    private static TreeNode buildTree(int inStart, int inEnd, int[] postorder, int postStart, int postEnd, Map<Integer, Integer> hm) {
        if (postStart > postEnd || inStart > inEnd) return null;
        TreeNode root = new TreeNode(postorder[postEnd]);
        int ri = hm.get(postorder[postEnd]);
        root.left = buildTree(inStart, ri - 1, postorder, postStart, postStart + ri - inStart - 1, hm);
        root.right = buildTree(ri + 1, inEnd, postorder, postStart + ri - inStart, postEnd - 1, hm);
        return root;
    }
}
