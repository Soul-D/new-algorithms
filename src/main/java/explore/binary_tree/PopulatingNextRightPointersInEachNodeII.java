package explore.binary_tree;

public class PopulatingNextRightPointersInEachNodeII {
    public Node connect(Node root) {
        Node start = root;
        while (start != null) {
            Node temp = new Node();
            Node current = temp;
            while (start != null) {
                if (start.left != null) {
                    current.next = start.left;
                    current = current.next;
                }
                if (start.right != null) {
                    current.next = start.right;
                    current = current.next;
                }
                start = start.next;
            }
            start = temp.next;
        }
        return root;
    }
}
