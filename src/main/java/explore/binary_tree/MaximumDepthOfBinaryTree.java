package explore.binary_tree;

public class MaximumDepthOfBinaryTree {
    private int answer;
    public int maxDepthMaybeFaster(TreeNode root) {
        if (root == null) {
            return 0;
        }
        answer = 0;
        doWork(root, 1);
        return answer;
    }

    private void doWork(TreeNode root, int depth) {
        if (root == null) {
            return;
        }
        if (root.left == null && root.right == null) {
            answer = Math.max(answer, depth);
        }
        doWork(root.left, depth + 1);
        doWork(root.right, depth + 1);
    }

    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
    }
}
