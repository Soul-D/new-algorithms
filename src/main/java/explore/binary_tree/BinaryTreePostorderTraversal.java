package explore.binary_tree;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreePostorderTraversal {
    public static List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        traverse(root, result);
        return result;
    }

    private static void traverse(TreeNode root, List<Integer> acc) {
        if (root == null) {
            return;
        }
        traverse(root.left, acc);
        traverse(root.right, acc);
        acc.add(root.val);
    }
}
