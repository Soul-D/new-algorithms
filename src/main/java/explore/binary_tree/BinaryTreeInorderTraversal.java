package explore.binary_tree;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreeInorderTraversal {
    public static List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        traverse(root, result);
        return result;
    }

    private static void traverse(TreeNode root, List<Integer> acc) {
        if (root == null) {
            return;
        }
        traverse(root.left, acc);
        acc.add(root.val);
        traverse(root.right, acc);
    }
}
