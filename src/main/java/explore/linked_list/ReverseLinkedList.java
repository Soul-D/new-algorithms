package explore.linked_list;

public class ReverseLinkedList {
    public static ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode newHead = head;
        while (head.next != null) {
            ListNode tmp = head.next;
            head.next = head.next.next;
            tmp.next = newHead;
            newHead = tmp;
        }
        return newHead;
    }
}
