package explore.linked_list;

public class MergeTwoSortedLists {
    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null) {
            return null;
        }
        ListNode current, head;
        if (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                current = head = new ListNode(l1.val);
                l1 = l1.next;
            } else {
                current = head = new ListNode(l2.val);
                l2 = l2.next;
            }
        } else {
            if (l1 != null) {
                current = head = new ListNode(l1.val);
                l1 = l1.next;
            } else {
                current = head = new ListNode(l2.val);
                l2 = l2.next;
            }
        }

        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                ListNode node = new ListNode(l1.val);
                current.next = node;
                current = node;
                l1 = l1.next;
            } else {
                ListNode node = new ListNode(l2.val);
                current.next = node;
                current = node;
                l2 = l2.next;
            }
        }

        while (l1 != null) {
            ListNode node = new ListNode(l1.val);
            current.next = node;
            current = node;
            l1 = l1.next;
        }

        while (l2 != null) {
            ListNode node = new ListNode(l2.val);
            current.next = node;
            current = node;
            l2 = l2.next;
        }
        return head;
    }
}
