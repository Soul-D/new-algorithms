package explore.linked_list;

public class FlattenAMultilevelDoublyLinkedList {
    public static Node flatten(Node head) {
        if (head != null) {
            flattenChildren(head);
        }
        return head;
    }

    private static Node flattenChildren(Node head) {
        Node current = head;
        while (current.next != null || current.child != null) {
            Node next = current.next;
            if (current.child != null) {
                current.next = current.child;
                current.child.prev = current;
                Node childrenTail = flattenChildren(current.child);
                current.child = null;
                current = childrenTail;
            }

            if (next != null) {
                current.next = next;
                next.prev = current;
                current = next;
            }
        }
        return current;
    }

    private static class Node {
        public int val;
        public Node prev;
        public Node next;
        public Node child;

        public Node(int i) {
            this.val = i;
        }

        public static Node build(int... ints) {
            Node head = new Node(ints[0]);
            Node curr = head;
            for (int i = 1; i < ints.length; i++) {
                Node next = new Node(ints[i]);
                curr.next = next;
                next.prev = curr;
                curr = next;
            }
            return head;
        }
    }
}
