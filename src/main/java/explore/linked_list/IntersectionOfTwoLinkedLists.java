package explore.linked_list;

public class IntersectionOfTwoLinkedLists {
    public static ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if (headA == null || headB == null) {
            return null;
        }
        ListNode a = headA;
        ListNode b = headB;
        while (!(a == null && b == null)) {
            if (a == null) {
                a = headB;
                continue;
            }
            if (b == null) {
                b = headA;
            }

            if (a == b) {
                return a;
            }
            a = a.next;
            b = b.next;
        }
        return null;
    }
}
