package explore.linked_list;

import java.util.HashMap;
import java.util.Map;

public class CopyListWithRandomPointer {
    public Node copyRandomList(Node head) {
        Node fake = new Node(0);
        Node newCurrent = fake;
        Map<Node, Node> mapper = new HashMap<>();
        Node pointer = head;
        while (pointer != null) {
            Node node = new Node(pointer.val);
            node.next = pointer.next;
            newCurrent.next = node;
            newCurrent = node;
            mapper.put(pointer, node);
            pointer = pointer.next;
        }
        fake = newCurrent = fake.next;
        pointer = head;
        while (pointer != null) {
            Node random = pointer.random;
            newCurrent.random = mapper.get(random);
            pointer = pointer.next;
            newCurrent = newCurrent.next;
        }

        return fake;
    }

    private static class Node {
        int val;
        Node next;
        Node random;

        public Node(int val) {
            this.val = val;
            this.next = null;
            this.random = null;
        }
    }
}
