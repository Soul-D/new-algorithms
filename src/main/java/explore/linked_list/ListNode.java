package explore.linked_list;

public class ListNode {
    int val;
    ListNode next;

    public ListNode() {
    }

    public ListNode(int x) {
        val = x;
        next = null;
    }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    public static ListNode build(int... ints) {
        ListNode head = new ListNode(ints[0]);
        ListNode curr = head;
        for (int i = 1; i < ints.length; i++) {
            ListNode next = new ListNode(ints[i]);
            curr.next = next;
            curr = next;
        }
        return head;
    }

    public String buildString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ListNode");
        ListNode node = this;

        builder.append(": ").append(node.val);
        node = node.next;


        while (node != null) {
            builder.append(" => ").append(node.val);
            node = node.next;
        }

        return builder.toString();
    }
}