package explore.linked_list;

public class PalindromeLinkedList {
    public static boolean isPalindrome(ListNode head) {
        int length = length(head);
        if (length < 2) {
            return true;
        }
        int half = length / 2;
        if (length % 2 != 0) {
            half++;
        }
        ListNode halfHead = head;
        while (half != 0) {
            halfHead = halfHead.next;
            half--;
        }
        ListNode reverse = reverse(halfHead);
        while (reverse != null) {
            if (reverse.val != head.val) {
                return false;
            }
            reverse = reverse.next;
            head = head.next;
        }
        return true;
    }

    private static int length(ListNode head) {
        int count = 0;
        while (head != null) {
            count++;
            head = head.next;
        }
        return count;
    }

    private static ListNode reverse(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode newHead = head;
        while (head.next != null) {
            ListNode tmp = head.next;
            head.next = head.next.next;
            tmp.next = newHead;
            newHead = tmp;
        }
        return newHead;
    }
}
