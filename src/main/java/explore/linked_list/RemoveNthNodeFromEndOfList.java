package explore.linked_list;

public class RemoveNthNodeFromEndOfList {
    public static ListNode removeNthFromEnd(ListNode head, int n) {
        if (head == null || n == 0) {
            return head;
        }
        ListNode curr = head, last = head;
        while (n > 0 && last != null) {
            last = last.next;
            n--;
        }

        if (last == null && n != 0) {
            return head;
        }

        if (last == null) {
            return head.next;
        }

        while (last.next != null) {
            last = last.next;
            curr = curr.next;
        }
        curr.next = curr.next.next;
        return head;
    }
}
