package explore.linked_list;


public class MyDoublyLinkedList {

    private Node head;
    private Node tail;
    private int size;

    /**
     * Initialize your data structure here.
     */
    public MyDoublyLinkedList() {

    }

    /**
     * Get the value of the index-th node in the linked list. If the index is invalid, return -1.
     */
    public int get(int index) {
        if (size == 0 || index < 0 || index >= size) {
            return -1;
        } else {
            Node node = getNodeByIndex(index);
            return node.val;
        }
    }

    private Node getNodeByIndex(int index) {
        int middle = size / 2;
        Node cur;
        int shiftCount = 0;
        if (index < middle) {
            cur = head;
            while (shiftCount < index) {
                cur = cur.next;
                shiftCount++;
            }
        } else {
            cur = tail;
            while (shiftCount < size - index - 1) {
                cur = cur.prev;
                shiftCount++;
            }
        }
        return cur;
    }

    private boolean isEmpty() {
        return size == 0;
    }

    /**
     * Add a node of value val before the first element of the linked list. After the insertion, the
     * new node will be the first node of the linked list.
     */
    public void addAtHead(int val) {
        Node newHead = new Node(val);
        if (isEmpty()) {
            head = tail = newHead;
        } else {
            head.prev = newHead;
            newHead.next = head;
            head = newHead;
        }
        size++;
    }

    /**
     * Append a node of value val to the last element of the linked list.
     */
    public void addAtTail(int val) {
        Node newTail = new Node(val);
        if (isEmpty()) {
            head = tail = newTail;
        } else {
            newTail.prev = tail;
            tail.next = newTail;
            tail = newTail;
        }
        size++;
    }

    /**
     * Add a node of value val before the index-th node in the linked list. If index equals to the
     * length of linked list, the node will be appended to the end of linked list. If index is greater
     * than the length, the node will not be inserted.
     */
    public void addAtIndex(int index, int val) {
        if (index > size) {
            return;
        }

        if (index == 0) {
            addAtHead(val);
        } else if (index == size) {
            addAtTail(val);
        } else {

            Node newNode = new Node(val);

            Node curNode = getNodeByIndex(index);
            Node preNode = curNode.prev;

            newNode.prev = preNode;
            newNode.next = curNode;
            preNode.next = newNode;
            curNode.prev = newNode;
            size++;
        }

    }

    /**
     * Delete the index-th node in the linked list, if the index is valid.
     */
    public void deleteAtIndex(int index) {
        if (index >= size || index < 0) {
            return;
        }
        if (size == 1) {
            head = null;
            tail = null;
        } else {
            if (index == 0) {
                head = head.next;
                head.prev = null;
            } else if (index == size - 1) {
                tail = tail.prev;
                tail.next = null;
            } else {
                Node curNode = getNodeByIndex(index);


                Node prevNode = curNode.prev;
                Node nextNode = curNode.next;

                prevNode.next = nextNode;
                nextNode.prev = prevNode;

            }
        }
        size--;
    }

    private static class Node {
        Node next;
        Node prev;
        int val;

        Node(int val) {
            this.val = val;
        }
    }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("MyLinkedList");
            Node node = head;
            if (node != null) {
                builder.append(": ").append(node.val);
                node = node.next;
            }

            while (node != null) {
                builder.append(" => ").append(node.val);
                node = node.next;
            }

            return builder.toString();
        }
}