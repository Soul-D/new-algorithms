package explore.linked_list;

public class OddEvenLinkedList {
    public static ListNode oddEvenList(ListNode head) {
        if (head == null || head.next == null || head.next.next == null) {
            return head;
        }
        int count = 1;
        ListNode odds = head;
        ListNode oddsLast = head;
        ListNode evens = head.next;
        ListNode evensLast = head.next;
        head = head.next.next;
        while (head != null) {
            if (count % 2 == 0) {
                evensLast.next = head;
                evensLast = head;
                head = head.next;
                evensLast.next = null;
            } else {
                oddsLast.next = head;
                oddsLast = head;
                head = head.next;
                oddsLast.next = null;
            }
            count++;
        }
        evensLast.next = null;
        oddsLast.next = evens;
        return odds;
    }
}
