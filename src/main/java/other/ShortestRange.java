package other;

import java.util.Arrays;
import java.util.List;

public class ShortestRange {
    public static void main(String[] args) {
        System.out.println(findSmallestRange(Arrays.asList(
                Arrays.asList(4, 7, 9, 12, 15),
                Arrays.asList(0, 8, 10, 14, 20),
                Arrays.asList(6, 12, 16, 30, 50)
        ), 3));
    }

    public static List<Integer> findSmallestRange(List<List<Integer>> arr, int k) {
        int[] indexes = new int[arr.size()];

        int minEl = 0;
        int maxEl = 0;
        int minRange = Integer.MAX_VALUE;

        while (true) {
            int minInd = -1;
            int minVal = Integer.MAX_VALUE;
            int maxVal = Integer.MIN_VALUE;
            boolean isFound = false;

            for (int i = 0; i < k; i++) {
                if (indexes[i] == arr.get(i).size()) {
                    isFound = true;
                    break;
                }

                if (arr.get(i).get(indexes[i]) < minVal) {
                    minInd = i;
                    minVal = arr.get(i).get(indexes[i]);
                }

                if (arr.get(i).get(indexes[i]) > maxVal) {
                    maxVal = arr.get(i).get(indexes[i]);
                }
            }

            if (isFound) {
                break;
            }

            indexes[minInd]++;

            if ((maxVal - minVal) < minRange) {
                minEl = minVal;
                maxEl = maxVal;
                minRange = maxEl - minEl;
            }
        }
        return Arrays.asList(minEl, maxEl);
    }
}
