package other;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class CountColors {
    public static Map<String, Integer> countOne(String[] colors) {
        Map<String, Integer> result = new HashMap<>();
        for (String color : colors) {
            Integer current = result.getOrDefault(color, 0);
            result.put(color, current + 1);
        }
        return result;
    }

    public static Map<String, Integer> countTwo(String[] colors) {
        return Arrays.stream(colors)
                .collect(Collectors.groupingBy(x -> x))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().size()));
    }
}
