package hackerrank.datastructures.easy.trees;

import hackerrank.datastructures.model.Node;

/**
 * https://www.hackerrank.com/challenges/tree-preorder-traversal/problem
 */
public class TreePreorderTraversal {
    public static void preOrder(Node root) {
        System.out.print(root.data + " ");
        if (root.left != null) {
            preOrder(root.left);
        }
        if (root.right != null) {
            preOrder(root.right);
        }
    }
}
