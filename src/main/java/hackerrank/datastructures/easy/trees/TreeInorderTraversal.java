package hackerrank.datastructures.easy.trees;

import hackerrank.datastructures.model.Node;

/**
 * https://www.hackerrank.com/challenges/tree-inorder-traversal/problem
 */
public class TreeInorderTraversal {
    public static void inOrder(Node root) {
        if (root.left != null) {
            inOrder(root.left);
        }
        System.out.print(root.data + " ");
        if (root.right != null) {
            inOrder(root.right);
        }
    }
}
