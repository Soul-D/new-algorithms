package hackerrank.datastructures.easy.trees;

import hackerrank.datastructures.model.Node;

/**
 * https://www.hackerrank.com/challenges/binary-search-tree-lowest-common-ancestor/problem
 */
public class BinarySearchTreeLowestCommonAncestor {
    public static Node lca(Node root, int v1, int v2) {
        int left, right;
        if (v1 < v2) {
            left = v1;
            right = v2;
        } else {
            left = v2;
            right = v1;
        }
        while (root != null) {
            if (left <= root.data && right >= root.data) {
                break;
            }
            if (root.data < left) {
                root = root.right;
            } else {
                root = root.left;
            }
        }
        return root;
    }
}
