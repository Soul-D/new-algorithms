package hackerrank.datastructures.easy.trees;

import hackerrank.datastructures.model.Node;

import java.util.LinkedList;
import java.util.Queue;

/**
 * https://www.hackerrank.com/challenges/tree-level-order-traversal/problem
 */
public class TreeLevelOrderTraversal {
    public static void levelOrder(Node root) {
        Queue<Node> nodes = new LinkedList<>();
        if (root != null) {
            nodes.add(root);
        }
        while (!nodes.isEmpty()) {
            root = nodes.poll();
            System.out.print(root.data + " ");
            if (root.left != null) {
                nodes.add(root.left);
            }
            if (root.right != null) {
                nodes.add(root.right);
            }
        }
    }
}
