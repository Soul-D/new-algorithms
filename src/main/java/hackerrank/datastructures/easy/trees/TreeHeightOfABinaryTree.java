package hackerrank.datastructures.easy.trees;

import hackerrank.datastructures.model.Node;

/**
 * https://www.hackerrank.com/challenges/tree-height-of-a-binary-tree/problem
 */
public class TreeHeightOfABinaryTree {
    public static int height(Node root) {
        int left = 0;
        int right = 0;
        if (root.left != null) {
            left = 1 + height(root.left);
        }
        if (root.right != null) {
            right = 1 + height(root.right);
        }
        return Math.max(left, right);
    }
}
