package hackerrank.datastructures.easy.trees;

import hackerrank.datastructures.model.Node;

/**
 * https://www.hackerrank.com/challenges/binary-search-tree-insertion/problem
 */
public class BinarySearchTreeInsertion {
    public static Node insert(Node root, int data) {
        Node insert = new Node(data);
        if (root == null) {
            return insert;
        }
        Node result = root;

        while (true) {
            if (root.data == data) {
                return result;
            }
            if (root.data > data) {
                if (root.left == null) {
                    root.left = insert;
                    break;
                }
                root = root.left;
            } else {
                if (root.right == null) {
                    root.right = insert;
                    break;
                }
                root = root.right;
            }
        }
        return result;
    }
}
