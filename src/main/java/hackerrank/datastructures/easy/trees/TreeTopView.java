package hackerrank.datastructures.easy.trees;

import hackerrank.datastructures.model.Node;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * https://www.hackerrank.com/challenges/tree-top-view/problem
 */
public class TreeTopView {
    public static void topView1(Node root) {
        if (root.left != null) {
            printLeft(root.left);
        }
        System.out.print(root.data + " ");
        if (root.right != null) {
            printRight(root.right);
        }
    }

    private static void printLeft(Node node) {
        if (node.left != null) {
            printLeft(node.left);
        }
        System.out.print(node.data + " ");
    }

    private static void printRight(Node node) {
        if (node.right != null) {
            printRight(node.right);
        }
        System.out.print(node.data + " ");
    }

    // not my solution
    public static void topView(Node root) {
        int MAX = 500;    // Otherwise, MAX= getNodeCount(root);
        int[] top = new int[MAX * 2];
        Queue<Object[]> queue = new ArrayDeque<>();
        queue.add(new Object[]{root, MAX});
        while (!queue.isEmpty()) {
            Object[] array = queue.remove();
            Node node = (Node) array[0];
            Integer order = (Integer) array[1];
            if (node == null) continue;

            if (top[order] == 0) top[order] = node.data;
            queue.add(new Object[]{node.left, order - 1});
            queue.add(new Object[]{node.right, order + 1});
        }

        for (int data : top) if (data != 0) System.out.print(data + " ");
    }
}
