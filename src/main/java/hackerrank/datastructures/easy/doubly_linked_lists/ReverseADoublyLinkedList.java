package hackerrank.datastructures.easy.doubly_linked_lists;

import hackerrank.datastructures.model.DoublyLinkedListNode;

/**
 * https://www.hackerrank.com/challenges/reverse-a-doubly-linked-list/problem
 */
public class ReverseADoublyLinkedList {
    static DoublyLinkedListNode reverse(DoublyLinkedListNode head) {
        DoublyLinkedListNode newHead = head;
        while (head != null) {
            DoublyLinkedListNode prev = head.prev;
            head.prev = head.next;
            head.next = prev;
            newHead = head;
            head = head.prev;
        }
        return newHead;
    }
}
