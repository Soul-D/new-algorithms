package hackerrank.datastructures.easy.doubly_linked_lists;

import hackerrank.datastructures.model.DoublyLinkedListNode;

/**
 * https://www.hackerrank.com/challenges/insert-a-node-into-a-sorted-doubly-linked-list/problem
 */
public class InsertingANodeIntoASortedDoublyLinkedList {
    static DoublyLinkedListNode sortedInsert(DoublyLinkedListNode head, int data) {
        DoublyLinkedListNode insert = new DoublyLinkedListNode(data);
        if (head == null) {
            return insert;
        }
        if (head.data >= data) {
            insert.next = head;
            head.prev = insert;
            return insert;
        }

        DoublyLinkedListNode tail = sortedInsert(head.next, data);
        head.next = tail;
        tail.prev = head;
        return head;
    }
}
