package hackerrank.datastructures.easy.singly_linked_lists;

import hackerrank.datastructures.model.SinglyLinkedListNode;

/**
 * https://www.hackerrank.com/challenges/reverse-a-linked-list/problem
 */
public class ReverseALinkedList {
    static SinglyLinkedListNode reverse(SinglyLinkedListNode head) {
        SinglyLinkedListNode node = null;
        while (head != null) {
            SinglyLinkedListNode prev = head;
            head = head.next;
            prev.next = node;
            node = prev;
        }
        return node;
    }
}
