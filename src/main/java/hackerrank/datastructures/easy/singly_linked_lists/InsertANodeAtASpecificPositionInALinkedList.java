package hackerrank.datastructures.easy.singly_linked_lists;

import hackerrank.datastructures.model.SinglyLinkedListNode;

/**
 * https://www.hackerrank.com/challenges/insert-a-node-at-a-specific-position-in-a-linked-list/problem
 */
public class InsertANodeAtASpecificPositionInALinkedList {
    static SinglyLinkedListNode insertNodeAtPosition(SinglyLinkedListNode head, int data, int position) {
        SinglyLinkedListNode insert = new SinglyLinkedListNode(data);
        if (position == 0) {
            insert.next = head;
            return insert;
        }
        SinglyLinkedListNode prev = head;
        while (--position > 0) {
            prev = prev.next;
        }
        insert.next = prev.next;
        prev.next = insert;
        return head;
    }
}
