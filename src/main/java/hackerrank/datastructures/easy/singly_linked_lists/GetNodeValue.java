package hackerrank.datastructures.easy.singly_linked_lists;

import hackerrank.datastructures.model.SinglyLinkedListNode;

/**
 * https://www.hackerrank.com/challenges/get-the-value-of-the-node-at-a-specific-position-from-the-tail/problem
 */
public class GetNodeValue {
    static int getNode(SinglyLinkedListNode head, int positionFromTail) {
        SinglyLinkedListNode pointer = head;
        while (positionFromTail-- != 0) {
            pointer = pointer.next;
        }
        SinglyLinkedListNode result = head;
        while (pointer.next != null) {
            result = result.next;
            pointer = pointer.next;
        }
        return result.data;
    }
}
