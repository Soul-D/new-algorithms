package hackerrank.datastructures.easy.singly_linked_lists;

import hackerrank.datastructures.model.SinglyLinkedListNode;

/**
 * https://www.hackerrank.com/challenges/delete-a-node-from-a-linked-list/problem
 */
public class DeleteANode {
    static SinglyLinkedListNode deleteNode(SinglyLinkedListNode head, int position) {
        if (position == 0) {
            return head.next;
        }
        SinglyLinkedListNode next = head;
        SinglyLinkedListNode curr = head;
        while (position-- > 0) {
            curr = next;
            next = next.next;
        }
        curr.next = next.next;
        return head;
    }
}
