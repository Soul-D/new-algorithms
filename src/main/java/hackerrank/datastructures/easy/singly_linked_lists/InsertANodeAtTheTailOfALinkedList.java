package hackerrank.datastructures.easy.singly_linked_lists;

import hackerrank.datastructures.model.SinglyLinkedListNode;

public class InsertANodeAtTheTailOfALinkedList {
    static SinglyLinkedListNode insertNodeAtTail(SinglyLinkedListNode head, int data) {
        SinglyLinkedListNode tail = new SinglyLinkedListNode(data);
        if (head == null) {
            return tail;
        }
        SinglyLinkedListNode res = head;

        while (head.next != null) {
            head = head.next;
        }
        head.next = tail;

        return res;
    }
}
