package hackerrank.datastructures.easy.singly_linked_lists;

import hackerrank.datastructures.model.SinglyLinkedListNode;

/**
 * https://www.hackerrank.com/challenges/find-the-merge-point-of-two-joined-linked-lists/problem
 */
public class FindMergePointOfTwoLists {
    static int findMergeNode(SinglyLinkedListNode head1, SinglyLinkedListNode head2) {
        SinglyLinkedListNode current1 = head1;
        SinglyLinkedListNode current2 = head2;

        while (current1 != current2) {
            if (current1.next == null) {
                current1 = head2;
            } else {
                current1 = current1.next;
            }
            if (current2.next == null) {
                current2 = head1;
            } else {
                current2 = current2.next;
            }
        }
        return current2.data;

    }
}
