package hackerrank.datastructures.easy.singly_linked_lists;

import hackerrank.datastructures.model.SinglyLinkedListNode;

import java.util.LinkedList;

/**
 * https://www.hackerrank.com/challenges/print-the-elements-of-a-linked-list-in-reverse/problem
 */
public class PrintInReverse {
    static void reversePrint(SinglyLinkedListNode head) {
        LinkedList<Integer> list = new LinkedList<>();
        while (head != null) {
            list.addLast(head.data);
            head = head.next;
        }
        while (!list.isEmpty()) {
            System.out.println(list.removeLast());
        }
    }
}
