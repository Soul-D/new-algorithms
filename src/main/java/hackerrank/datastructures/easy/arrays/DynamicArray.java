package hackerrank.datastructures.easy.arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * https://www.hackerrank.com/challenges/dynamic-array/problem
 */
public class DynamicArray {
    public static List<Integer> dynamicArray(int n, List<List<Integer>> queries) {
        int lastAnswer = 0;
        List<Integer> result = new ArrayList<>();
        List<List<Integer>> seqList = IntStream.range(0, n)
                .mapToObj(x -> new ArrayList<Integer>())
                .collect(Collectors.toList());
        for (List<Integer> query : queries) {
            int type = query.get(0);
            int x = query.get(1);
            int y = query.get(2);

            int index = (x ^ lastAnswer) % n;
            List<Integer> seq = seqList.get(index);

            if (type == 1) {
                seq.add(y);
            } else {
                int size = seq.size();
                if (size == 0) {
                    continue;
                }

                lastAnswer = seq.get(y % size);
                result.add(lastAnswer);
            }
        }


        return result;
    }
}
