package hackerrank.datastructures.easy.arrays;

/**
 * https://www.hackerrank.com/challenges/2d-array/problem
 */
public class TwoDArrayDS {
    static int hourglassSum(int[][] arr) {
        int max = Integer.MIN_VALUE;
        for (int row = 1; row <= 4; row++) {
            for (int col = 1; col <= 4; col++) {
                int candidate = sumLine(col, arr[row - 1]) + arr[row][col] + sumLine(col, arr[row + 1]);
                if (candidate > max) {
                    max = candidate;
                }
            }
        }
        return max;
    }

    private static int sumLine(int col, int[] line) {
        return line[col - 1] + line[col] + line[col + 1];
    }
}
