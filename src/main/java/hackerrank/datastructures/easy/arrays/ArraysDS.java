package hackerrank.datastructures.easy.arrays;

/**
 * https://www.hackerrank.com/challenges/arrays-ds/problem
 */
public class ArraysDS {
    static int[] reverseArray(int[] a) {
        int[] result = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            result[i] = a[a.length - 1 - i];
        }
        return result;
    }
}
