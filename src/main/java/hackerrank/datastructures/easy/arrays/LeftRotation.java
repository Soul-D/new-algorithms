package hackerrank.datastructures.easy.arrays;

/**
 * https://www.hackerrank.com/challenges/array-left-rotation/problem
 */
public class LeftRotation {
    static void leftRotation(int d, int[] nums) {
        int[] h = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            h[getIndex(d, nums.length, i)] = nums[i];
        }

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < nums.length; i++) {
            builder.append(h[i]).append(" ");
        }
        System.out.println(builder);
    }

    private static int getIndex(int d, int length, int ind) {
        int candidate = ind - d;
        if (candidate < 0) {
            return length + candidate;
        }
        return candidate;
    }
}
