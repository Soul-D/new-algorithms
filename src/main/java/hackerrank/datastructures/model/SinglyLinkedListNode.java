package hackerrank.datastructures.model;

public class SinglyLinkedListNode {
    public int data;
    public SinglyLinkedListNode next;

    public SinglyLinkedListNode(int x) {
        data = x;
    }

    public SinglyLinkedListNode(int data, SinglyLinkedListNode next) {
        this.data = data;
        this.next = next;
    }

    public static SinglyLinkedListNode of(int... values) {
        SinglyLinkedListNode head = new SinglyLinkedListNode(values[0]);
        SinglyLinkedListNode prev = head;
        for (int i = 1; i < values.length; i++) {
            SinglyLinkedListNode curr = new SinglyLinkedListNode(values[i]);
            prev.next = curr;
            prev = curr;
        }

        return head;
    }

    public static void printNode(SinglyLinkedListNode head) {
        StringBuilder builder = new StringBuilder();
        while (head != null) {
            builder.append(head.data);
            if (head.next != null) {
                builder.append("->");
            }
            head = head.next;
        }
        System.out.println(builder.toString());
    }

    @Override
    public String toString() {
        return "ListNode{" +
                "data=" + data +
                ", next=" + next +
                '}';
    }
}

