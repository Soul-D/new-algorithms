package hackerrank.algorithms.medium.implementation;

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/bomber-man/problem
 */
public class TheBombermanGame {

    public static String[] bomberMan(int n, String[] grid) {
        if (n == 1) {
            return grid;
        }

        if (n % 2 == 0) {
            return getFull(grid);
        }

        if (n % 4 == 3) {
            return getOpposite(grid);
        }

        return getOpposite(getOpposite(grid));
    }

    private static String[] getFull(String[] grid) {
        int length = grid[0].length();
        String[] result = new String[grid.length];
        char[] chars = new char[length];
        Arrays.fill(chars, 'O');
        String full = new String(chars);
        Arrays.fill(result, full);
        return result;
    }

    private static String[] getOpposite(String[] grid) {
        int length = grid[0].length();
        String[] result = new String[grid.length];
        for (int row = 0; row < grid.length; row++) {
            char[] chars = new char[length];
            for (int col = 0; col < chars.length; col++) {
                if (grid[row].charAt(col) == 'O') {
                    chars[col] = '.';
                } else {
                    chars[col] = getCell(row, col, length, grid);
                }
            }
            result[row] = new String(chars);
        }
        return result;
    }

    private static char getCell(int row, int col, int length, String[] grid) {
        boolean left = col == 0 || grid[row].charAt(col - 1) == '.';
        boolean right = col == length - 1 || grid[row].charAt(col + 1) == '.';
        boolean up = row == 0 || grid[row - 1].charAt(col) == '.';
        boolean down = row == grid.length - 1 || grid[row + 1].charAt(col) == '.';

        if (left && right && up && down) {
            return 'O';
        }
        return '.';
    }
}
