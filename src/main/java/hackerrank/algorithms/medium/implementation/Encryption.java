package hackerrank.algorithms.medium.implementation;

/**
 * https://www.hackerrank.com/challenges/encryption/problem
 */
public class Encryption {
    public static String encryption(String s) {
        // TODO: can be faster if replace with iteration
        String all = s.replaceAll(" ", "");
        int cols = (int) Math.ceil(Math.sqrt(all.length()));
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < cols; i++) {
            int n = i;
            while (n < all.length()) {
                builder.append(all.charAt(n));
                n += cols;
            }
            if (i < cols - 1) {
                builder.append(" ");
            }
        }
        return builder.toString();
    }
}
