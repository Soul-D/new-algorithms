package hackerrank.algorithms.medium.implementation;

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/organizing-containers-of-balls/problem
 */
public class OrganizingContainersOfBalls {
    public static String organizingContainers(int[][] container) {
        int n = container[0].length;
        int[] capacities = new int[n];
        int[] types = new int[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int x = container[i][j];
                capacities[i] += x;
                types[j] += x;
            }
        }
        Arrays.sort(capacities);
        Arrays.sort(types);
        if (Arrays.equals(capacities, types)) {
            return "Possible";
        } else {
            return "Impossible";
        }
    }
}
