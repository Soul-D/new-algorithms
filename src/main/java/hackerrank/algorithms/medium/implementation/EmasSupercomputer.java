package hackerrank.algorithms.medium.implementation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * https://www.hackerrank.com/challenges/two-pluses/problem
 */
public class EmasSupercomputer {
    public static int twoPluses(String[] grid) {

        List<Set<String>> list = new ArrayList<>();

        int rows = grid.length;
        int cols = grid[0].length();
        for (int row = 1; row < rows - 1; row++) {
            for (int col = 1; col < cols - 1; col++) {
                if (grid[row].charAt(col) == 'B') {
                    continue;
                }
                list.addAll(getMaxPlus(row, col, rows, cols, grid));
            }
        }

        int max = 0;
        for (int i = 0; i < list.size(); i++) {
            Set<String> p1 = list.get(i);

            for (Set<String> p2 : list) {
                int prod = p1.size() * p2.size();
                if (prod > max && !overlaps(p1, p2)) {
                    max = prod;
                }
            }
        }

        return max;

    }

    private static boolean overlaps(Set<String> p1, Set<String> p2) {
        for (String s : p2) {
            if (p1.contains(s)) {
                return true;
            }
        }
        return false;
    }

    private static List<Set<String>> getMaxPlus(int row, int col, int rows, int cols, String[] grid) {
        List<Set<String>> result = new ArrayList<>();
        Set<String> set = new HashSet<>();
        set.add("" + row + col);

        int left = col - 1;
        int right = col + 1;

        int top = row - 1;
        int down = row + 1;

        result.add(set);
        while (true) {

            if (left < 0 || right >= cols || top < 0 || down >= rows) {
                break;
            }

            if (grid[row].charAt(left) == 'B' || grid[row].charAt(right) == 'B'
                    || grid[top].charAt(col) == 'B' || grid[down].charAt(col) == 'B') {
                break;
            }

            set = new HashSet<>();
            result.add(set);


            for (Set<String> s1 : result) {
                set.addAll(s1);
            }

            set.add("" + row + left);
            set.add("" + row + right);
            set.add("" + top + col);
            set.add("" + down + col);


            left = left - 1;
            right = right + 1;
            top = top - 1;
            down = down + 1;

        }
        return result;
    }
}
