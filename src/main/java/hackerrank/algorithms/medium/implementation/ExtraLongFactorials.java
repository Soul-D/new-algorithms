package hackerrank.algorithms.medium.implementation;

import java.math.BigInteger;

/**
 * https://www.hackerrank.com/challenges/extra-long-factorials/problem
 */
public class ExtraLongFactorials {
    public static void extraLongFactorials(int n) {
        BigInteger result = new BigInteger(String.valueOf(n--));
        while (n > 0) {
            result = result.multiply(new BigInteger(String.valueOf(n--)));
        }
        System.out.println(result);
    }
}
