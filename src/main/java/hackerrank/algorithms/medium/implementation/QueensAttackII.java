package hackerrank.algorithms.medium.implementation;

/**
 * https://www.hackerrank.com/challenges/queens-attack-2/problem
 */
public class QueensAttackII {

    public static int queensAttack(int n, int k, int r_q, int c_q, int[][] obstacles) {
        int up = n - r_q;
        int down = r_q - 1;
        int right = n - c_q;
        int left = c_q - 1;
        int upRight = Math.min(up, right);
        int downRight = Math.min(right, down);
        int upLeft = Math.min(left, up);
        int downLeft = Math.min(left, down);

        for (int[] o : obstacles) {
            if (o[1] == c_q) {
                if (o[0] < r_q) {
                    down = Math.min(down, r_q - 1 - o[0]);
                } else {
                    up = Math.min(up, o[0] - r_q - 1);
                }
            } else if (o[0] == r_q) {
                if (o[1] < c_q) {
                    left = Math.min(left, c_q - 1 - o[1]);
                } else {
                    right = Math.min(right, o[1] - c_q - 1);
                }
            } else if (Math.abs(o[0] - r_q) == Math.abs(o[1] - c_q)) {
                if (o[1] > c_q) {
                    if (o[0] > r_q) {
                        upRight = Math.min(upRight, o[1] - c_q - 1);
                    } else {
                        downRight = Math.min(downRight, o[1] - c_q - 1);
                    }
                } else {
                    if (o[0] > r_q) {
                        upLeft = Math.min(upLeft, c_q - 1 - o[1]);
                    } else {
                        downLeft = Math.min(downLeft, c_q - 1 - o[1]);
                    }
                }
            }
        }
        return up + down + right + left + upRight + downRight + upLeft + downLeft;
    }

    // fall with OOM on huge inputs
    public static int queensAttack1(int n, int k, int r_q, int c_q, int[][] obstacles) {
        int[][] board = new int[n + 1][];
        for (int i = 0; i < n + 1; i++) {
            board[i] = new int[n + 1];
        }
        for (int i = 0; i < k; i++) {
            int row = obstacles[i][0];
            int col = obstacles[i][1];
            board[row][col] = 1;
        }
        return up(r_q, c_q, board) + down(r_q, c_q, board) + left(r_q, c_q, board) + right(r_q, c_q, board)
                + upLeft(r_q, c_q, board) + upRight(r_q, c_q, board) + downLeft(r_q, c_q, board) + downRight(r_q, c_q, board);
    }

    private static int up(int r_q, int c_q, int[][] board) {
        int count = 0;
        for (int i = r_q + 1; i < board.length; i++) {
            if (board[i][c_q] == 1) {
                break;
            }
            count++;
        }
        return count;
    }

    private static int right(int r_q, int c_q, int[][] board) {
        int count = 0;
        for (int i = c_q + 1; i < board.length; i++) {
            if (board[r_q][i] == 1) {
                break;
            }
            count++;
        }
        return count;
    }

    private static int down(int r_q, int c_q, int[][] board) {
        int count = 0;
        for (int i = r_q - 1; i > 0; i--) {
            if (board[i][c_q] == 1) {
                break;
            }
            count++;
        }
        return count;
    }

    private static int left(int r_q, int c_q, int[][] board) {
        int count = 0;
        for (int i = c_q - 1; i > 0; i--) {
            if (board[r_q][i] == 1) {
                break;
            }
            count++;
        }
        return count;
    }

    private static int upRight(int r_q, int c_q, int[][] board) {
        int count = 0;
        for (int i = r_q + 1, j = c_q + 1; i < board.length && j < board.length; i++, j++) {
            if (board[i][j] == 1) {
                break;
            }
            count++;
        }
        return count;
    }

    private static int upLeft(int r_q, int c_q, int[][] board) {
        int count = 0;
        for (int i = r_q + 1, j = c_q - 1; i < board.length && j > 0; i++, j--) {
            if (board[i][j] == 1) {
                break;
            }
            count++;
        }
        return count;
    }

    private static int downRight(int r_q, int c_q, int[][] board) {
        int count = 0;
        for (int i = r_q - 1, j = c_q + 1; i > 0 && j < board.length; i--, j++) {
            if (board[i][j] == 1) {
                break;
            }
            count++;
        }
        return count;
    }

    private static int downLeft(int r_q, int c_q, int[][] board) {
        int count = 0;
        for (int i = r_q - 1, j = c_q - 1; i > 0 && j > 0; i--, j--) {
            if (board[i][j] == 1) {
                break;
            }
            count++;
        }
        return count;
    }
}
