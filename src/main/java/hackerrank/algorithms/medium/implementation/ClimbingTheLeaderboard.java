package hackerrank.algorithms.medium.implementation;

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/climbing-the-leaderboard/problem
 */
public class ClimbingTheLeaderboard {
    public static int[] climbingLeaderboard(int[] scores, int[] alice) {
        int scoreCount = 1;
        for (int i = 1; i < scores.length; i++) {
            if (scores[i] != scores[i - 1]) {
                scoreCount++;
            }
        }
        int[] cache = new int[scoreCount];
        cache[cache.length - 1] = scores[0];
        int ind = cache.length - 2;
        for (int i = 1; i < scores.length; i++) {
            if (scores[i] != scores[i - 1]) {
                cache[ind--] = scores[i];
            }
        }

        int[] result = new int[alice.length];
        for (int i = 0; i < alice.length; i++) {
            int score = Arrays.binarySearch(cache, alice[i]);
            if (score >= 0) {
                result[i] = cache.length - score;
            } else {
                result[i] = cache.length - (-(score + 1)) + 1;
            }
        }
        return result;
    }
}
