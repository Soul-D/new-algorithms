package hackerrank.algorithms.medium.implementation;

import java.util.List;

/**
 * https://www.hackerrank.com/challenges/non-divisible-subset/problem
 */
public class NonDivisibleSubset {
    public static int nonDivisibleSubset(int k, List<Integer> s) {
        int[] counts = new int[k];
        for (Integer integer : s) {
            counts[integer % k]++;
        }
        int count = Math.min(counts[0], 1);
        for (int i = 1; i <= k / 2; i++) {
            if (i == k - i) {
                count++;
            } else {
                count += Math.max(counts[i], counts[k - i]);
            }
        }
        return count;
    }
}
