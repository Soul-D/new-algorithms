package hackerrank.algorithms.medium.implementation;

/**
 * https://www.hackerrank.com/challenges/larrys-array/problem
 */
public class LarrysArray {
    public static String larrysArray(int[] A) {
        int inversion = 0;
        for (int i = 0; i < A.length; i++) {
            for (int j = i + 1; j < A.length; j++) {
                if (A[i] > A[j])
                    inversion++;
            }
        }
        return inversion % 2 == 0 ? "YES" : "NO";

    }
}
