package hackerrank.algorithms.medium.implementation;

/**
 * https://www.hackerrank.com/challenges/absolute-permutation/problem
 */
public class AbsolutePermutation {
    public static int[] absolutePermutation(int n, int k) {
        if (k != 0 && n % (2 * k) != 0) {
            return new int[]{-1};
        }
        int[] res = new int[n];

        int temp = k;
        if (k == 0) {
            for (int i = 1; i <= n; i++) {
                res[i - 1] = i;
            }
        } else {
            for (int i = 1; i <= n; i++) {
                res[i - 1] = i + temp;
                if (i % k == 0) {
                    temp *= -1;
                }
            }
        }

        return res;
    }
}
