package hackerrank.algorithms.medium.implementation;

/**
 * https://www.hackerrank.com/challenges/3d-surface-area/problem
 */
public class ThreeDSurfaceArea {
    public static int surfaceArea(int[][] A) {
        int area = 2 * A.length * A[0].length;
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[0].length; j++) {
                area += Math.max(0, A[i][j] - (i == 0 ? 0 : A[i - 1][j]));
                area += Math.max(0, A[i][j] - (i == A.length - 1 ? 0 : A[i + 1][j]));
                area += Math.max(0, A[i][j] - (j == 0 ? 0 : A[i][j - 1]));
                area += Math.max(0, A[i][j] - (j == A[0].length - 1 ? 0 : A[i][j + 1]));
            }
        }
        return area;
    }
}
