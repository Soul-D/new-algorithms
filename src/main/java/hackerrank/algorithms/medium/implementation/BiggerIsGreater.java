package hackerrank.algorithms.medium.implementation;

/**
 * https://www.hackerrank.com/challenges/bigger-is-greater/problem
 */
public class BiggerIsGreater {
    public static String biggerIsGreater(String w) {
        char[] arr = w.toCharArray();
        int i = arr.length - 1;
        for (; i > 0; i--) {
            if (arr[i - 1] < arr[i]) {
                break;
            }
        }

        if (i == 0) {
            return "no answer";
        }
        int j = arr.length - 1;

        for (; ; j--) {
            if (arr[j] > arr[i - 1]) {
                break;
            }
        }

        char temp = arr[i - 1];
        arr[i - 1] = arr[j];
        arr[j] = temp;
        j = arr.length - 1;
        while (i < j) {
            char tem = arr[i];
            arr[i] = arr[j];
            arr[j] = tem;
            j--;
            i++;
        }
        return new String(arr);
    }
}


