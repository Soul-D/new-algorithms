package hackerrank.algorithms.medium.implementation;

/**
 * https://www.hackerrank.com/challenges/almost-sorted/problem
 */
public class AlmostSorted {
    public static void almostSorted(int[] arr) {
        if (isSorted(arr)) {
            System.out.println("yes");
        }

        int start, end;
        for (start = 0; start < arr.length - 1 && arr[start] < arr[start + 1]; start++) ;
        for (end = arr.length - 1; end > 0 && arr[end - 1] < arr[end]; end--) ;

        swap(arr, start, end);
        if (isSorted(arr)) {
            System.out.println("yes\nswap " + (start + 1) + " " + (end + 1));
            return;
        }

        reverse(arr, start + 1, end - 1);
        if (isSorted(arr)) {
            System.out.println("yes\nreverse " + (start + 1) + " " + (end + 1));
            return;
        }
        System.out.println("no");
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    private static void reverse(int[] nums, int i, int j) {
        int tmp;
        while (i < j) {
            tmp = nums[i];
            nums[i] = nums[j];
            nums[j] = tmp;
            i++;
            j--;
        }
    }

    private static boolean isSorted(int[] a) {
        for (int i = 1; i < a.length; i++) {
            if (a[i] < a[i - 1]) {
                return false;
            }
        }
        return true;
    }
}
