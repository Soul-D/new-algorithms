package hackerrank.algorithms.medium.implementation;

/**
 * https://www.hackerrank.com/challenges/the-grid-search/problem
 */
public class TheGridSearch {
    public static String gridSearch(String[] G, String[] P) {
        if (G.length == 0 || P.length == 0) {
            return "NO";
        }
        if (G.length == 1) {
            if (G[0].contains(P[0])) {
                return "YES";
            } else {
                return "NO";
            }
        }
        for (int i = 0; i <= G.length - P.length; i++) {
            int index = G[i].indexOf(P[0]);
            while (index != -1) {
                boolean found = true;
                for (int j = 1; j < P.length; j++) {
                    if (!G[i + j].startsWith(P[j], index)) {
                        index = G[i].indexOf(P[0], index + 1);
                        found = false;
                        break;
                    }
                }
                if (found) {
                    return "YES";
                }
            }
        }

        return "NO";
    }
}
