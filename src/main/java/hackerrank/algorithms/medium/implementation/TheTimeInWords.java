package hackerrank.algorithms.medium.implementation;

import java.util.HashMap;
import java.util.Map;

/**
 * https://www.hackerrank.com/challenges/the-time-in-words/problem
 */
public class TheTimeInWords {
    public static void main(String[] args) {
        System.out.println(timeInWords(5, 33));
    }

    private static final Map<Integer, String> numbers = new HashMap<>();

    static {
        numbers.put(1, "one");
        numbers.put(2, "two");
        numbers.put(3, "three");
        numbers.put(4, "four");
        numbers.put(5, "five");
        numbers.put(6, "six");
        numbers.put(7, "seven");
        numbers.put(8, "eight");
        numbers.put(9, "nine");
        numbers.put(10, "ten");
        numbers.put(11, "eleven");
        numbers.put(12, "twelve");
        numbers.put(13, "thirteen");
        numbers.put(14, "fourteen");
        numbers.put(15, "quarter");
        numbers.put(16, "sixteen");
        numbers.put(17, "seventeen");
        numbers.put(18, "eighteen");
        numbers.put(19, "nineteen");
        numbers.put(20, "twenty");
    }

    public static String timeInWords(int h, int m) {
        if (m == 0) {
            return numbers.get(h) + " o' clock";
        }

        if (m == 1) {
            return numbers.get(m) + " minute past " + numbers.get(h);
        }

        if (m == 15) {
            return numbers.get(15) + " past " + numbers.get(h);
        }

        if (m <= 20) {
            return numbers.get(m) + " minutes past " + numbers.get(h);
        }

        if (m < 30) {
            return numbers.get(20) + " " + numbers.get(m % 20) + " minutes past " + numbers.get(h);
        }

        if (m == 30) {
            return "half" + " past " + numbers.get(h);
        }

        if (m == 45) {
            return numbers.get(15) + " to " + getNexHour(h);
        }

        m = 60 - m;
        if (m > 20) {
            return numbers.get(20) + " " + numbers.get(m % 20) + " minutes to " + getNexHour(h);
        }

        if (m == 1) {
            return numbers.get(m) + " minute to " + numbers.get(h);
        }

        return numbers.get(m) + " minutes to " + getNexHour(h);
    }

    private static String getNexHour(int h) {
        h = h + 1;
        if (h > 12) {
            h = h % 12;
        }
        return numbers.get(h);
    }
}
