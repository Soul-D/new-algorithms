package hackerrank.algorithms.medium.implementation;

/**
 * https://www.hackerrank.com/challenges/magic-square-forming/problem
 */
public class FormingAMagicSquare {
    private static final int[][][] etalons = new int[][][]{
            {{8, 1, 6}, {3, 5, 7}, {4, 9, 2}},
            {{6, 1, 8}, {7, 5, 3}, {2, 9, 4}},
            {{4, 9, 2}, {3, 5, 7}, {8, 1, 6}},
            {{2, 9, 4}, {7, 5, 3}, {6, 1, 8}},
            {{8, 3, 4}, {1, 5, 9}, {6, 7, 2}},
            {{4, 3, 8}, {9, 5, 1}, {2, 7, 6}},
            {{6, 7, 2}, {1, 5, 9}, {8, 3, 4}},
            {{2, 7, 6}, {9, 5, 1}, {4, 3, 8}}
    };

    public static int formingMagicSquare(int[][] s) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < 8; i++) {
            int candidate = cost(etalons[i], s);
            min = Math.min(min, candidate);
        }
        return min;
    }

    private static int cost(int[][] etalon, int[][] target) {
        int cost = 0;
        for (int i = 0; i < etalon.length; i++) {
            for (int j = 0; j < etalon[0].length; j++) {
                cost += Math.abs(etalon[i][j] - target[i][j]);
            }
        }

        return cost;
    }
}
