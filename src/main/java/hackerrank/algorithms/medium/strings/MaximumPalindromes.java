package hackerrank.algorithms.medium.strings;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * https://www.hackerrank.com/challenges/maximum-palindromes/problem
 */
public class MaximumPalindromes {
    private static BigInteger div = BigInteger.valueOf(1000000007);
    private static List<BigInteger> rems = new ArrayList<>();
    private static List<BigInteger> ints = new ArrayList<>();
    private static List<int[]> chars = new ArrayList<>();

    // TODO: NOT MINE
    public static void initialize(String s) {
        rems.add(BigInteger.valueOf(1));
        ints.add(BigInteger.valueOf(1));
        for (int i = 1; i < s.length(); ++i) {
            rems.add(rems.get(i - 1).multiply(BigInteger.valueOf(i)).remainder(div));
            ints.add(rems.get(i).modPow(div.subtract(BigInteger.valueOf(2)), div));
        }
        int[] arrA = new int[26];
        chars.add(new int[26]);
        for (int i = 0; i < s.length(); ++i) {
            arrA[s.charAt(i) - 'a']++;
            int[] arrB = Arrays.copyOf(arrA, 26);
            chars.add(arrB);
        }
    }

    public static int answerQuery(int l, int r) {
        int[] array = new int[26];
        for (int i = 0; i < 26; ++i) {
            array[i] = chars.get(r)[i] - chars.get(l - 1)[i];
        }
        int odd = 0;
        int sum = 0;
        BigInteger divider = rems.get(0);
        for (int a : array) {
            if (a % 2 != 0) {
                ++odd;
            }
            sum += a / 2;
            divider = divider.multiply(ints.get(a / 2));
            divider = divider.remainder(div);
        }
        odd = odd > 0 ? odd : 1;
        BigInteger result = rems.get(sum).multiply(divider).multiply(BigInteger.valueOf(odd));
        return (result.remainder(div)).intValue();
    }
}
