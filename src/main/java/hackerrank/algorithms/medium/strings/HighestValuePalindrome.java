package hackerrank.algorithms.medium.strings;

/**
 * https://www.hackerrank.com/challenges/richie-rich/problem
 */
public class HighestValuePalindrome {
    public static String highestValuePalindrome(String s, int n, int k) {
        if (n == 0) {
            return "-1";
        }
        if (n == 1) {
            if (k > 0) {
                return "9";
            } else {
                return s;
            }
        }
        int middle = n / 2;
        int diffs = 0;
        for (int i = 0; i < middle; i++) {
            if (s.charAt(i) != s.charAt(n - 1 - i)) {
                diffs++;
            }
        }
        if (diffs > k) {
            return "-1";
        }

        char[] chars = new char[n];
        for (int i = 0; i < middle; i++) {
            if (s.charAt(i) == s.charAt(n - 1 - i)) {
                if (s.charAt(i) == '9') {
                    chars[i] = chars[n - 1 - i] = s.charAt(i);
                } else if (k - 2 >= diffs) {
                    chars[i] = '9';
                    chars[n - 1 - i] = '9';
                    k -= 2;
                } else {
                    chars[i] = chars[n - 1 - i] = s.charAt(i);
                }
            } else {
                if (s.charAt(i) != '9' && s.charAt(n - 1 - i) != '9' && k - 1 >= diffs) {
                    chars[i] = '9';
                    chars[n - 1 - i] = '9';
                    k -= 2;
                } else {
                    int val = Math.max(s.charAt(i), s.charAt(n - 1 - i));
                    chars[i] = (char) val;
                    chars[n - 1 - i] = (char) val;
                    k -= 1;
                }
                diffs--;
            }
        }

        if (n % 2 == 1) {
            if (k >= 1) {
                chars[middle] = '9';
            } else {
                chars[middle] = s.charAt(middle);
            }
        }
        return new String(chars);
    }
}
