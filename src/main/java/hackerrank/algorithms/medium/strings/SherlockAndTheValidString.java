package hackerrank.algorithms.medium.strings;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * https://www.hackerrank.com/challenges/sherlock-and-valid-string/problem
 */
public class SherlockAndTheValidString {
    public static String isValid(String s) {
        int[] counts = new int[26];
        for (int i = 0; i < s.length(); i++) {
            counts[s.charAt(i) - 'a']++;
        }
        Map<Integer, Integer> map = new HashMap<>();
        for (int count : counts) {
            map.merge(count, 1, Integer::sum);
        }
        map.remove(0);
        if (map.size() == 1) {
            return "YES";
        }
        if (map.size() > 2) {
            return "NO";
        }

        TreeMap<Integer, Integer> sorted = new TreeMap<>(map);
        if (sorted.firstKey() == 1 && sorted.firstEntry().getValue() == 1) {
            return "YES";
        }
        if (sorted.lastEntry().getValue() == 1 && sorted.lastKey() - sorted.firstKey() == 1) {
            return "YES";
        }
        return "NO";
    }
}
