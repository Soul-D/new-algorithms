package hackerrank.algorithms.medium.sorting;

/**
 * https://www.hackerrank.com/challenges/quicksort3/problem
 */
public class QuicksortInPlace {

    static void quickSort(int[] ar) {
        quickSort(ar, 0, ar.length - 1);
    }

    private static int partition(int[] ar, int minpos, int maxpos) {
        int pivot = ar[maxpos];
        for (int i = minpos; i < maxpos; i++) {
            if (ar[i] < pivot) {
                swap(ar, i, minpos);
                minpos++;
            }
        }

        swap(ar, minpos, maxpos);
        printArray(ar);
        return minpos;
    }

    private static void swap(int[] ar, int a, int b) {
        int temp = ar[a];
        ar[a] = ar[b];
        ar[b] = temp;
    }

    private static void quickSort(int[] ar, int minpos, int maxpos) {
        if (minpos >= maxpos) {
            return;
        }

        int pos = partition(ar, minpos, maxpos);

        quickSort(ar, minpos, pos - 1);
        quickSort(ar, pos + 1, maxpos);
    }

    private static void printArray(int[] arr) {
        for (int value : arr) {
            System.out.print(value + " ");
        }
        System.out.println();
    }
}



