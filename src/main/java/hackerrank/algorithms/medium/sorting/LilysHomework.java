package hackerrank.algorithms.medium.sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * https://www.hackerrank.com/challenges/lilys-homework/problem
 */
public class LilysHomework {
    public static int lilysHomework(int[] arr) {
        int[] reversedArr = new int[arr.length];
        List<Integer> sorted = new ArrayList<>(arr.length);
        for (int i = 0; i < arr.length; i++) {
            sorted.add(arr[i]);
            reversedArr[arr.length - 1 - i] = arr[i];
        }
        Collections.sort(sorted);

        return Math.min(findSwapCount(arr, sorted), findSwapCount(reversedArr, sorted));
    }

    private static int findSwapCount(int[] arr, List<Integer> sorted) {
        int count = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            map.put(arr[i], i);
        }

        for (int i = 0; i < arr.length; i++) {
            int cv = arr[i];
            Integer scv = sorted.get(i);
            if (cv != scv) {
                count++;
                Integer index = map.get(scv);
                swap(arr, i, index);
                map.put(cv, index);
            }
        }

        return count;
    }

    private static void swap(int[] arr, int a, int b) {
        int tmp = arr[a];
        arr[a] = arr[b];
        arr[b] = tmp;
    }
}
