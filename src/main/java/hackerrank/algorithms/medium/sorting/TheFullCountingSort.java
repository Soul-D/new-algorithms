package hackerrank.algorithms.medium.sorting;

/**
 * https://www.hackerrank.com/challenges/countingsort4/problem
 */
public class TheFullCountingSort {
    static void countSort(String[][] arr) {
        int half = arr.length / 2;
        StringBuilder[] result = new StringBuilder[100];
        for (int i = 0; i < result.length; i++) {
            result[i] = new StringBuilder();
        }
        for (int i = 0; i < arr.length; i++) {
            int num = Integer.parseInt(arr[i][0]);
            String str = arr[i][1];
            if (i < half) {
                result[num] = result[num].append("- ");
            } else {
                result[num] = result[num].append(str).append(" ");
            }
        }

        for (StringBuilder s : result) {
            System.out.print(s);
        }
    }
}
