package hackerrank.algorithms.medium.sorting;

import java.util.LinkedList;
import java.util.Queue;

/**
 * https://www.hackerrank.com/challenges/fraudulent-activity-notifications/problem
 */
public class FraudulentActivityNotifications {
    public static int activityNotifications(int[] expenditure, int d) {
        int count = 0;
        int[] freq = new int[201];
        Queue<Integer> q = new LinkedList<>();

        for (int i = 0; i < expenditure.length; i++) {
            while (i < d) {
                q.add(expenditure[i]);
                freq[expenditure[i]]++;
                i++;
            }

            int suspiciousSum = suspiciousSum(freq, d);

            if (expenditure[i] >= suspiciousSum) {
                count++;
            }

            int elem = q.remove();
            freq[elem]--;

            q.add(expenditure[i]);
            freq[expenditure[i]]++;
        }

        return count;
    }

    private static int suspiciousSum(int[] freq, int d) {
        int center = 0;
        if (d % 2 == 1) {
            int i = 0;
            for (; i < freq.length; i++) {
                center += freq[i];
                if (center > d / 2) {
                    break;
                }
            }
            return i * 2;
        } else {
            int first = -1;
            int second = -1;
            for (int i = 0; i < freq.length; i++) {
                center += freq[i];

                if (center == d / 2) {
                    first = i;
                } else if (center > d / 2) {
                    if (first < 0) {
                        first = i;
                    }
                    second = i;
                    break;
                }
            }
            return first + second;
        }
    }


    // fails due timeout.
    public static int activityNotificationsBad(int[] expenditure, int d) {
        if (d >= expenditure.length) {
            return 0;
        }
        int count = 0;
        for (int i = d; i < expenditure.length; i++) {
            if (expenditure[i] >= suspiciousSum(expenditure, i, d)) {
                count++;
            }
        }
        return count;
    }

    private static int suspiciousSum(int[] arr, int curr, int d) {
        int[] copy = copySorted(arr, curr, d);
        if (d % 2 == 1) {
            return copy[d / 2] * 2;
        } else {
            return copy[d / 2] + copy[d / 2 - 1];
        }
    }

    private static int[] copySorted(int[] arr, int curr, int d) {
        int[] counts = new int[201];
        for (int i = curr - d; i < curr; i++) {
            counts[arr[i]]++;
        }
        int[] copy = new int[d];
        int index = 0;
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] != 0) {
                for (int j = 0; j < counts[i]; j++) {
                    copy[index++] = i;
                }
            }
        }
        return copy;
    }
}
