package hackerrank.algorithms.easy.warmup;

import java.util.List;

/**
 * https://www.hackerrank.com/challenges/diagonal-difference/problem
 */
public class DiagonalDifference {
    public static int diagonalDifference(List<List<Integer>> arr) {
        int topLeft = 0;
        int topRight = 0;
        int length = arr.size();
        for (int i = 0; i < length; i++) {
            List<Integer> currLine = arr.get(i);
            topLeft += currLine.get(i);
            topRight += currLine.get(length - 1 - i);
        }

        return Math.abs(topLeft - topRight);
    }

}
