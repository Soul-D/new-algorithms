package hackerrank.algorithms.easy.warmup;

import java.util.Arrays;
import java.util.List;

/**
 * https://www.hackerrank.com/challenges/compare-the-triplets/problem
 */
public class CompareTheTriplets {
    static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        int ar = 0;
        int br = 0;
        for (int i = 0; i < 3; i++) {
            if (a.get(i) > b.get(i)) {
                ar++;
            } else if (a.get(i) < b.get(i)) {
                br++;
            }
        }
        return Arrays.asList(ar, br);
    }
}
