package hackerrank.algorithms.easy.warmup;

/**
 * https://www.hackerrank.com/challenges/birthday-cake-candles/problem
 */
public class BirthdayCakeCandles {
    // Complete the birthdayCakeCandles function below.
    static int birthdayCakeCandles(int[] ar) {
        int max = ar[0];
        for (int i = 1; i < ar.length; i++) {
            if (ar[i] > max) {
                max = ar[i];
            }
        }

        int count = 0;
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] == max) {
                count++;
            }
        }

        return count;
    }
}
