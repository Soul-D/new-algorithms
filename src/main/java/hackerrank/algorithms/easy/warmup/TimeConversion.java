package hackerrank.algorithms.easy.warmup;

/**
 * https://www.hackerrank.com/challenges/time-conversion/problem
 */
public class TimeConversion {
    static String timeConversion(String s) {
        String suffix = s.substring(s.length() - 2);
        String time = s.substring(0, s.length() - 2);
        String head = time.substring(0, 2);
        String tail = time.substring(2);

        if ("PM".equals(suffix)) {
            if (!head.equals("12")) {
                head = String.valueOf((Integer.parseInt(head) + 12));
            }
        } else {
            if (head.equals("12")) {
                head = "00";
            }
        }
        return head + tail;
    }
}
