package hackerrank.algorithms.easy.warmup;

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/mini-max-sum/problem
 */
public class MiniMaxSum {
    // Complete the miniMaxSum function below.
    static void miniMaxSum(int[] arr) {
        Arrays.sort(arr);
        long min = 0;
        long max = 0;
        for (int i = 0; i < arr.length; i++) {
            int curr = arr[i];
            if (i == 0) {
                min += curr;
            } else if (i == arr.length - 1) {
                max += curr;
            } else {
                min += curr;
                max += curr;
            }
        }

        System.out.println(min + " " + max);
    }
}
