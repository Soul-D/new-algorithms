package hackerrank.algorithms.easy.warmup;

/**
 * https://www.hackerrank.com/challenges/plus-minus/problem
 */
public class PlusMinus {
    // Complete the plusMinus function below.
    static void plusMinus(int[] arr) {
        int pos = 0;
        int neg = 0;
        int zero = 0;
        int length = arr.length;
        for (int value : arr) {
            if (value < 0) {
                neg++;
            } else if (value > 0) {
                pos++;
            } else {
                zero++;
            }
        }

        System.out.println(String.format("%06f", (double) pos / length));
        System.out.println(String.format("%06f", (double) neg / length));
        System.out.println(String.format("%06f", (double) zero / length));
    }
}
