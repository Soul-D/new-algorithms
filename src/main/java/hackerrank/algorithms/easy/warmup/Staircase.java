package hackerrank.algorithms.easy.warmup;

/**
 * https://www.hackerrank.com/challenges/staircase/problem
 */
public class Staircase {
    // Complete the staircase function below.
    static void staircase(int n) {
        for (int i = 1; i <= n; i++) {
            int length = n;

            while (length - i != 0) {
                System.out.print(" ");
                length--;
            }
            while (length != 0) {
                System.out.print("#");
                length--;
            }
            System.out.print("\n");
        }
    }
}
