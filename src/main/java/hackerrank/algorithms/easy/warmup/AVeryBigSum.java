package hackerrank.algorithms.easy.warmup;

/**
 * https://www.hackerrank.com/challenges/a-very-big-sum/problem
 */
public class AVeryBigSum {
    static long aVeryBigSum(long[] ar) {
        long result = 0L;
        for (long i : ar) {
            result += i;
        }
        return result;
    }
}
