package hackerrank.algorithms.easy.greedy;

/**
 * https://www.hackerrank.com/challenges/beautiful-pairs/problem
 */
public class BeautifulPairs {
    static int beautifulPairs(int[] A, int[] B) {
        int[] storage = new int[1001];
        int beauty = 0;
        for (int i : A) {
            storage[i]++;
        }
        for (int i : B) {
            if (storage[i] > 0) {
                storage[i]--;
                beauty++;
            }
        }

        if (beauty == A.length) {
            beauty--;
        } else {
            beauty++;
        }
        return beauty;
    }
}
