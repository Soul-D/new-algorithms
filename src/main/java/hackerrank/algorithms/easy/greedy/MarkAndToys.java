package hackerrank.algorithms.easy.greedy;

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/mark-and-toys/problem
 */
public class MarkAndToys {
    static int maximumToys(int[] prices, int k) {
        Arrays.sort(prices);
        int count = 0;
        for (int price : prices) {
            if (price <= k) {
                count++;
                k -= price;
                continue;
            }
            break;
        }
        return count;
    }
}
