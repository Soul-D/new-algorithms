package hackerrank.algorithms.easy.greedy;

import java.util.Arrays;
import java.util.Comparator;

/**
 * https://www.hackerrank.com/challenges/maximum-perimeter-triangle/problem
 */
public class MaximumPerimeterTriangle {

    static int[] maximumPerimeterTriangle(int[] sticks) {
        Arrays.sort(sticks);
        int[][] triangles = new int[sticks.length - 2][];
        for (int i = 0, ind = 0; i < sticks.length - 2; i++) {
            if (!isArray(sticks[i], sticks[i + 1], sticks[i + 2])) {
                continue;
            }
            triangles[ind++] =
                    new int[]{sticks[i], sticks[i + 1], sticks[i + 2], sticks[i] + sticks[i + 1] + sticks[i + 2]};
        }

        Comparator<int[]> c = (o1, o2) -> {
            if (o1[3] != o2[3]) {
                return o2[3] - o1[3];
            } else if (o1[2] != o2[2]) {
                return o2[2] - o1[2];
            } else if (o1[0] != o2[0]) {
                return o2[0] - o1[0];
            } else {
                return 0;
            }
        };

        Comparator<int[]> nullsLast = Comparator.nullsLast(c);
        Arrays.sort(triangles, nullsLast);
        int[] triangle = triangles[0];
        if (triangle == null) {
            return new int[]{-1};
        }

        return new int[]{triangle[0], triangle[1], triangle[2]};
    }

    private static boolean isArray(int a, int b, int c) {
        return a < (b + c) && b < (c + a) && c < (a + b);
    }
}
