package hackerrank.algorithms.easy.greedy;

/**
 * https://www.hackerrank.com/challenges/sherlock-and-the-beast/problem
 */
public class SherlockAndTheBeast {

    static void decentNumber(int n) {
        int five = n;
        while (five >= 0) {
            if (five % 3 == 0) {
                int three = n - five;
                if (three % 5 == 0) {
                    StringBuilder builder = new StringBuilder();
                    while (five > 0) {
                        builder.append(5);
                        five--;
                    }

                    while (three > 0) {
                        builder.append(3);
                        three--;
                    }
                    System.out.println(builder);
                    return;
                }
            }
            five--;
        }

        System.out.println(-1);
    }
}
