package hackerrank.algorithms.easy.greedy;

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/largest-permutation/problem
 */
public class LargestPermutation {
    static int[] largestPermutation(int k, int[] arr) {
        int[] copy = Arrays.copyOf(arr, arr.length);
        Arrays.sort(copy);
        for (int i = 0; i < arr.length && k != 0; i++) {
            int currMax = copy[copy.length - 1 - i];
            if (currMax == arr[i]) {
                continue;
            }
            int index = findIndex(currMax, arr, i);
            arr[index] = arr[i];
            arr[i] = currMax;
            k--;
        }
        return arr;
    }

    private static int findIndex(int target, int[] arr, int fromIndex) {
        for (int i = fromIndex; i < arr.length; i++) {
            if (arr[i] == target) {
                return i;
            }
        }
        return -1;
    }

}
