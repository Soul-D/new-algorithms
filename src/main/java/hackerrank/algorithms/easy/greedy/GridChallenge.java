package hackerrank.algorithms.easy.greedy;

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/grid-challenge/problem
 */
public class GridChallenge {
    static String gridChallenge(String[] grid) {
        for (int i = 0; i < grid.length; i++) {
            grid[i] = sort(grid[i]);
        }
        if (isSorted(grid)) {
            return "YES";
        }
        return "NO";
    }

    private static String sort(String string) {
        char[] chars = string.toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }

    private static boolean isSorted(String[] grid) {
        for (int i = 0; i < grid[0].length(); i++) {
            for (int j = 0; j < grid.length - 1; j++) {
                if (grid[j].charAt(i) > grid[j + 1].charAt(i)) {
                    return false;
                }
            }
        }
        return true;
    }
}
