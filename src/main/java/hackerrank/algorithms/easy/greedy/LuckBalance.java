package hackerrank.algorithms.easy.greedy;

import java.util.Arrays;
import java.util.Comparator;

/**
 * https://www.hackerrank.com/challenges/luck-balance/problem
 */
public class LuckBalance {

    static int luckBalance(int k, int[][] contests) {
        Comparator<int[]> c = (o1, o2) -> {
            if (o1[1] > o2[1]) {
                return -1;
            } else if (o1[1] < o2[1]) {
                return 1;
            } else {
                return o2[0] - o1[0];
            }
        };

        Arrays.sort(contests, c);

        int luck = 0;
        for (int[] contest : contests) {
            int importance = contest[1];
            if (importance == 1) {
                if (k > 0) {
                    luck += contest[0];
                    k--;
                } else {
                    luck -= contest[0];
                }
            } else {
                luck += contest[0];
            }
        }

        return luck;
    }
}
