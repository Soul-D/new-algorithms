package hackerrank.algorithms.easy.greedy;

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/priyanka-and-toys/problem
 */
public class PriyankaAndToys {
    static int toys(int[] w) {
        Arrays.sort(w);
        int containers = 1;
        int high = w[0] + 4;
        for (int i = 1; i < w.length; i++) {
            if (w[i] > high) {
                containers++;
                high = w[i] + 4;
            }
        }
        return containers;
    }
}
