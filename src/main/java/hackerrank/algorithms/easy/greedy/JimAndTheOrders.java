package hackerrank.algorithms.easy.greedy;

import java.util.Arrays;
import java.util.Comparator;

/**
 * https://www.hackerrank.com/challenges/jim-and-the-orders/problem
 */
public class JimAndTheOrders {
    static int[] jimOrders(int[][] orders) {
        for (int i = 0; i < orders.length; i++) {
            orders[i] = new int[]{i, orders[i][0], orders[i][1]};
        }
        Comparator<int[]> c = (o1, o2) -> {
            int weight1 = o1[1] + o1[2];
            int weight2 = o2[1] + o2[2];
            if (weight1 != weight2) {
                return weight1 - weight2;
            } else {
                return o1[0] - o2[0];
            }
        };

        Arrays.sort(orders, c);
        int[] result = new int[orders.length];
        for (int i = 0; i < orders.length; i++) {
            result[i] = orders[i][0] + 1;
        }
        return result;
    }
}
