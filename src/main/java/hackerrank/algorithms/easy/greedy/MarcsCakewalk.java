package hackerrank.algorithms.easy.greedy;

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/marcs-cakewalk/problem
 */
public class MarcsCakewalk {
    static long marcsCakewalk(int[] calorie) {
        Arrays.sort(calorie);
        long miles = 0L;
        for (int i = 0; i < calorie.length; i++) {
            miles += (Math.pow(2, i) * calorie[calorie.length - 1 - i]);
        }
        return miles;
    }
}
