package hackerrank.algorithms.easy.sorting;

/**
 * https://www.hackerrank.com/challenges/quicksort4/problem
 */
public class RunningTimeOfQuicksort {
    // Complete the runningTime function below.
    static int quicks = 0;

    static int runningTime(int[] arr) {
        quicks = 0;
        int[] copy = new int[arr.length];
        System.arraycopy(arr, 0, copy, 0, arr.length);
        quickSort(copy);
        return insert(arr) - quicks;
    }

    static void quickSort(int[] ar) {
        quickSort(ar, 0, ar.length - 1);
    }

    private static int partition(int[] ar, int minpos, int maxpos) {
        int pivot = ar[maxpos];
        for (int i = minpos; i < maxpos; i++) {
            if (ar[i] < pivot) {
                swap(ar, i, minpos);
                minpos++;
            }
        }

        swap(ar, minpos, maxpos);
        return minpos;
    }

    private static void swap(int[] ar, int a, int b) {
        int temp = ar[a];
        ar[a] = ar[b];
        ar[b] = temp;
        quicks++;
    }

    private static void quickSort(int[] ar, int minpos, int maxpos) {
        if (minpos >= maxpos) {
            return;
        }

        int pos = partition(ar, minpos, maxpos);

        quickSort(ar, minpos, pos - 1);
        quickSort(ar, pos + 1, maxpos);
    }


    private static int insert(int[] arr) {
        int count = 0;
        for (int i = 1; i < arr.length; i++) {
            int value = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > value) {
                arr[j + 1] = arr[j];
                j = j - 1;
                count++;
            }
            arr[j + 1] = value;
        }
        return count;
    }
}
