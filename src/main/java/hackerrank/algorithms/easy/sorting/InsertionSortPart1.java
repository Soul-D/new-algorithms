package hackerrank.algorithms.easy.sorting;

/**
 * https://www.hackerrank.com/challenges/insertionsort1/problem
 */
public class InsertionSortPart1 {

    static void insertionSort1(int n, int[] arr) {
        int tail = arr[arr.length - 1];
        for (int i = arr.length - 1; i >= 0; i--) {
            if (i == 0) {
                arr[i] = tail;
                print(arr);
                break;
            }
            if (arr[i - 1] > tail) {
                arr[i] = arr[i - 1];
                print(arr);
            } else {
                arr[i] = tail;
                print(arr);
                break;
            }
        }
    }

    private static void print(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println(arr[arr.length - 1]);
    }
}
