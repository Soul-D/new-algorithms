package hackerrank.algorithms.easy.sorting;

/**
 * https://www.hackerrank.com/challenges/countingsort3/problem
 */
public class CountingSort3 {

    static int[] prepare(String[] strings) {
        int[] store = new int[100];
        int[] result = new int[100];
        for (String string : strings) {
            store[Integer.parseInt(string.split(" ")[0])]++;
        }

        for (int i = 0, acc = 0; i < 100; i++) {
            acc += store[i];
            result[i] = acc;
        }
        return result;
    }
}
