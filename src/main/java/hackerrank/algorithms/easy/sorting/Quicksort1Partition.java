package hackerrank.algorithms.easy.sorting;

/**
 * https://www.hackerrank.com/challenges/quicksort1/problem
 */
public class Quicksort1Partition {
    static int[] quickSort(int[] arr) {
        int[] result = new int[arr.length];
        int index = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < arr[0]) {
                result[index++] = arr[i];
            }
        }
        result[index++] = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > arr[0]) {
                result[index++] = arr[i];
            }
        }
        return result;
    }
}
