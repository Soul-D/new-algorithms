package hackerrank.algorithms.easy.sorting;

import java.util.Arrays;
import java.util.Comparator;

/**
 * https://www.hackerrank.com/challenges/big-sorting/problem
 */
public class BigSorting {

    static String[] bigSorting(String[] unsorted) {
        Comparator<String> comparator = (o1, o2) -> {
            if (o1.length() > o2.length()) {
                return 1;
            } else if (o1.length() < o2.length()) {
                return -1;
            } else {
                for (int i = 0; i < o1.length(); i++) {
                    if (o1.charAt(i) != o2.charAt(i)) {
                        return o1.charAt(i) - o2.charAt(i);
                    }
                }
                return 0;
            }
        };

        Arrays.sort(unsorted, comparator);
        return unsorted;
    }
}
