package hackerrank.algorithms.easy.sorting;

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/closest-numbers/problem
 */
public class ClosestNumbers {

    static int[] closestNumbers(int[] arr) {
        Arrays.sort(arr);
        int min = Integer.MAX_VALUE;
        int[] diffs = new int[arr.length - 1];
        for (int i = 0; i < arr.length - 1; i++) {
            diffs[i] = arr[i + 1] - arr[i];
            if (diffs[i] < min) {
                min = diffs[i];
            }
        }

        int minCount = 0;
        for (int diff : diffs) {
            if (diff == min) {
                minCount++;
            }
        }

        int[] result = new int[minCount * 2];
        int ind = 0;
        for (int i = 0; i < diffs.length; i++) {
            if (diffs[i] == min) {
                result[ind++] = arr[i];
                result[ind++] = arr[i + 1];
            }
        }
        return result;
    }
}
