package hackerrank.algorithms.easy.sorting;

import java.util.ArrayList;
import java.util.List;

/**
 * https://www.hackerrank.com/challenges/quicksort2/problem
 */
public class Quicksort2Sorting {

    static void quickSort(int[] ar) {
        quickSort(ar, 0, ar.length - 1);
    }

    private static int partition(int[] ar, int minpos, int maxpos) {
        int p = ar[minpos];
        List<Integer> leftlist = new ArrayList<>();
        List<Integer> rightlist = new ArrayList<>();

        for (int i = minpos + 1; i <= maxpos; i++) {
            if (ar[i] > p)
                rightlist.add(ar[i]);
            else
                leftlist.add(ar[i]);
        }
        copy(leftlist, ar, minpos);
        int ppos = leftlist.size() + minpos;
        ar[ppos] = p;
        copy(rightlist, ar, ppos + 1);

        return minpos + leftlist.size();
    }

    private static void copy(List<Integer> list, int[] ar, int startIdx) {
        for (int num : list) {
            ar[startIdx++] = num;
        }
    }

    private static void quickSort(int[] ar, int minpos, int maxpos) {
        if (minpos >= maxpos)
            return;

        int pos = partition(ar, minpos, maxpos);

        quickSort(ar, minpos, pos - 1);
        quickSort(ar, pos + 1, maxpos);

        printArray(ar, minpos, maxpos);
    }

    private static void printArray(int[] arr, int minpos, int maxpos) {
        for (int i = minpos; i <= maxpos; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}
