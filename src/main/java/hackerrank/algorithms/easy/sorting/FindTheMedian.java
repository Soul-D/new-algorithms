package hackerrank.algorithms.easy.sorting;

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/find-the-median/problem
 */
public class FindTheMedian {
    static int findMedian(int[] arr) {
        Arrays.sort(arr);
        return arr[arr.length / 2];
    }
}
