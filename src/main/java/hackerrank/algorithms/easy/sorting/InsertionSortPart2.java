package hackerrank.algorithms.easy.sorting;

/**
 * https://www.hackerrank.com/challenges/insertionsort2/problem
 */
public class InsertionSortPart2 {

    static void insertionSort2(int n, int[] arr) {
        for (int i = 1; i < n; i++) {
            for (int k = i; k > 0; k--) {
                if (arr[k] < arr[k - 1]) {
                    int temp = arr[k];
                    arr[k] = arr[k - 1];
                    arr[k - 1] = temp;
                }
            }
            print(arr);
        }
    }

    private static void print(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println(arr[arr.length - 1]);
    }
}
