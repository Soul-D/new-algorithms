package hackerrank.algorithms.easy.sorting;

/**
 * https://www.hackerrank.com/challenges/tutorial-intro/problem
 */
public class IntroToTutorialChallenges {
    static int introTutorial(int V, int[] arr) {
        int start = 0;
        int end = arr.length - 1;
        while (start <= end) {
            int middle = (end + start) / 2;
            if (arr[middle] == V) {
                return middle;
            }
            if (arr[middle] > V) {
                end = middle - 1;
            } else {
                start = middle + 1;
            }
        }
        return -(start + 1);
    }
}
