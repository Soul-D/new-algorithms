package hackerrank.algorithms.easy.sorting;

/**
 * https://www.hackerrank.com/challenges/countingsort1/problem
 */
public class CountingSort1 {
    static int[] countingSort(int[] arr) {
        int[] result = new int[100];
        for (int i : arr) {
            result[i]++;
        }
        return result;
    }
}
