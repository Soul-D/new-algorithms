package hackerrank.algorithms.easy.sorting;

/**
 * https://www.hackerrank.com/challenges/countingsort2/problem
 */
public class CountingSort2 {

    static int[] countingSort(int[] arr) {
        int[] result = new int[100];
        for (int i : arr) {
            result[i]++;
        }

        for (int i = 0, currIndex = 0; i < result.length; i++) {
            for (int j = 0; j < result[i]; j++) {
                arr[currIndex++] = i;
            }
        }
        return arr;
    }
}
