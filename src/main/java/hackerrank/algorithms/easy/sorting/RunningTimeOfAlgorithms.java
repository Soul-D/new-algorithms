package hackerrank.algorithms.easy.sorting;

/**
 * https://www.hackerrank.com/challenges/runningtime/problem
 */
public class RunningTimeOfAlgorithms {
    static int runningTime(int[] arr) {
        int count = 0;
        for (int i = 1; i < arr.length; i++) {
            int value = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > value) {
                arr[j + 1] = arr[j];
                j = j - 1;
                count++;
            }
            arr[j + 1] = value;
        }
        return count;
    }
}
