package hackerrank.algorithms.easy.game_theory;

/**
 * https://www.hackerrank.com/challenges/game-of-stones-1/problem
 */
public class GameOfStones {
    static String gameOfStones(int n) {
        int rest = n % 7;
        if (rest == 0 || rest == 1) {
            return "Second";
        }
        return "First";
    }
}
