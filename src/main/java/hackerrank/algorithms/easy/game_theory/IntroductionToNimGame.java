package hackerrank.algorithms.easy.game_theory;

/**
 * https://www.hackerrank.com/challenges/nim-game-1/problem
 */
public class IntroductionToNimGame {
    static String nimGame(int[] pile) {
        int xor = 0;
        for (int i : pile) {
            xor ^= i;
        }
        if (xor == 0) {
            return "Second";
        }
        return "First";
    }
}
