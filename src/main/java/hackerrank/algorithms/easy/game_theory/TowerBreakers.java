package hackerrank.algorithms.easy.game_theory;

/**
 * https://www.hackerrank.com/challenges/tower-breakers-1/problem
 */
public class TowerBreakers {
    static int towerBreakers(int n, int m) {
        return (m == 1 || n % 2 == 0) ? 2 : 1;
    }
}
