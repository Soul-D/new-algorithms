package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/making-anagrams/problem
 */
public class MakingAnagrams {
    static int makingAnagrams(String s1, String s2) {
        int count = 0;

        int[] holder = new int[26];
        for (int i = 0; i < s1.length(); i++) {
            holder[s1.charAt(i) - 97]++;
        }
        for (int i = 0; i < s2.length(); i++) {
            holder[s2.charAt(i) - 97]--;
        }
        for (int i : holder) {

            count += Math.abs(i);

        }
        return count;
    }
}
