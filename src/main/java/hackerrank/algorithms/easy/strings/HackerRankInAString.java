package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/hackerrank-in-a-string/problem
 */
public class HackerRankInAString {

    static String hackerrankInString(String s) {
        String etalon = "hackerrank";
        int index = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == etalon.charAt(index)) {
                if (index == etalon.length() - 1) {
                    return "YES";
                }
                index++;
            }
        }
        return "NO";
    }

}
