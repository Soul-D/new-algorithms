package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/alternating-characters/problem
 */
public class AlternatingCharacters {

    static int alternatingCharacters(String s) {
        if (s.length() == 1) {
            return 0;
        }
        int count = 0;
        char ch = s.charAt(0);
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) == ch) {
                count++;
            } else {
                ch = s.charAt(i);
            }
        }
        return count;
    }
}
