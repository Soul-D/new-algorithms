package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/beautiful-binary-string/problem
 */
public class BeautifulBinaryString {
    static int beautifulBinaryString(String b) {
        return (b.length() - b.replaceAll("010", "").length()) / 3;
    }
}
