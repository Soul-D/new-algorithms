package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/palindrome-index/problem
 */
public class PalindromeIndex {

    static int palindromeIndex(String s) {
        int res = -1;
        String r = new StringBuilder(s).reverse().toString();
        for (int i = 0; i < s.length() - 1; i++) {
            char sCh = s.charAt(i);
            char rCh = r.charAt(i);
            if (sCh != rCh) {
                if (isPalindrom(s, i)) {
                    res = i;
                } else {
                    res = s.length() - 1 - i;
                }

                break;
            }
        }

        return res;
    }

    private static boolean isPalindrom(String s, int ind) {
        String newStr = s.substring(0, ind) + s.substring(ind + 1);
        String r = new StringBuilder(newStr).reverse().toString();
        return newStr.equals(r);
    }
}
