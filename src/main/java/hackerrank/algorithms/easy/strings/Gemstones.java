package hackerrank.algorithms.easy.strings;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * https://www.hackerrank.com/challenges/gem-stones/problem
 */
public class Gemstones {

    static int gemstones(String[] arr) {
        Set<Integer> chars = Arrays.stream(arr)
                .flatMap(str -> str.chars().boxed())
                .collect(Collectors.toSet());

        Set<Set<Integer>> strings = Arrays.stream(arr)
                .map(str -> str.chars().boxed().collect(Collectors.toSet()))
                .collect(Collectors.toSet());

        int count = 0;
        outer:
        for (Integer ch : chars) {
            for (Set<Integer> string : strings) {
                if (!string.contains(ch)) {
                    continue outer;
                }
            }
            count++;
        }
        return count;
    }
}
