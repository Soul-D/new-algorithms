package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/reduced-string/problem
 */
public class SuperReducedString {

    static String superReducedString(String s) {
        char[] holder = new char[s.length()];
        int currInd = -1;
        for (int i = 0; i < s.length(); i++) {
            if (currInd != -1 && s.charAt(i) == holder[currInd]) {
                holder[currInd--] = 0;
                continue;
            }
            if (i != s.length() - 1 && s.charAt(i) == s.charAt(i + 1)) {
                i++;
                continue;
            }
            holder[++currInd] = s.charAt(i);
        }

        if (currInd == -1) {
            return "Empty String";
        }
        char[] toStr = new char[currInd + 1];
        System.arraycopy(holder, 0, toStr, 0, currInd + 1);
        return new String(toStr);
    }
}
