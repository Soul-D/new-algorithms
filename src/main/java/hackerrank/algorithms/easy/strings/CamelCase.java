package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/camelcase/problem
 */
public class CamelCase {

    static int camelcase(String s) {
        int count = 1;
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) < 91) {
                count++;
            }
        }
        return count;
    }
}
