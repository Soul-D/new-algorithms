package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/strong-password/problem
 */
public class StrongPassword {

    static int minimumNumber(int n, String password) {
        boolean hasLower = false;
        boolean hasNumber = false;
        boolean hasSpecial = false;
        boolean hasUpper = false;

        for (int i = 0; i < n; i++) {
            char current = password.charAt(i);
            if (isLower(current)) {
                hasLower = true;
                continue;
            }

            if (isNumber(current)) {
                hasNumber = true;
                continue;
            }

            if (isSpecial(current)) {
                hasSpecial = true;
                continue;
            }

            if (isUpper(current)) {
                hasUpper = true;
            }
        }

        int toAdd = 0;
        if (!hasLower) {
            toAdd++;
        }

        if (!hasUpper) {
            toAdd++;
        }

        if (!hasSpecial) {
            toAdd++;
        }

        if (!hasNumber) {
            toAdd++;
        }

        if (toAdd + password.length() >= 6) {
            return toAdd;
        } else {
            return 6 - password.length();
        }
    }

    private static boolean isNumber(char ch) {
        return 48 <= ch && ch <= 57;
    }

    private static boolean isSpecial(char ch) {
        return (33 <= ch && ch <= 38) || (40 <= ch && ch <= 43) || ch == 45 || ch == 64 || ch == 94;
    }

    private static boolean isLower(char ch) {
        return 97 <= ch && ch <= 122;
    }

    private static boolean isUpper(char ch) {
        return 65 <= ch && ch <= 90;
    }
}
