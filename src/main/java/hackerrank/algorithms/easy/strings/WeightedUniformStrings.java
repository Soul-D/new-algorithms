package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/weighted-uniform-string/problem
 */
public class WeightedUniformStrings {

    static String[] weightedUniformStrings(String s, int[] queries) {
        boolean[] holder = new boolean[10_000_001];
        String[] answers = new String[queries.length];
        char prev = '0';
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if (prev != s.charAt(i)) {
                prev = s.charAt(i);
                count = 0;
            }
            count++;
            int index = count * (prev - 96);
            if (index < holder.length) {
                holder[index] = true;
            }
        }

        for (int i = 0; i < queries.length; i++) {
            if (holder[queries[i]]) {
                answers[i] = "Yes";
            } else {
                answers[i] = "No";
            }
        }
        return answers;
    }
}
