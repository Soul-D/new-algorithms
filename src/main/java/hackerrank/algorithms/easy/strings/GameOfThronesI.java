package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/game-of-thrones/problem
 */
public class GameOfThronesI {
    static String gameOfThrones(String s) {
        int[] holder = new int[26];
        for (int i = 0; i < s.length(); i++) {
            holder[s.charAt(i) - 97]++;
        }
        boolean isOddMet = false;
        for (int i : holder) {
            if (i == 0) {
                continue;
            }
            if (i % 2 == 1) {
                if (isOddMet) {
                    return "NO";
                } else {
                    isOddMet = true;
                }
            }
        }
        return "YES";
    }
}
