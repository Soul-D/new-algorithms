package hackerrank.algorithms.easy.strings;

import java.util.HashSet;
import java.util.Set;

/**
 * https://www.hackerrank.com/challenges/pangrams/problem
 */
public class Pangrams {

    static String pangrams(String s) {
        int count = 0;
        Set<Character> holder = new HashSet<>();

        String toLower = s.toLowerCase();
        String toWork = toLower.replaceAll(" ", "");
        for (int i = 0; i < toWork.length(); i++) {
            if (holder.add(toWork.charAt(i))) {
                count++;
            }
            if (count == 26) {
                return "pangram";
            }
        }
        return "not pangram";
    }
}
