package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/caesar-cipher-1/problem
 */
public class CaesarCipher {

    static String caesarCipher(String s, int k) {
        char[] chars = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            chars[i] = rotate(s.charAt(i), k);
        }
        return new String(chars);
    }

    private static char rotate(char ch, int n) {
        if (97 <= ch && ch <= 122) {
            ch += n;
            while (!(97 <= ch && ch <= 122)) {
                ch -= 26;
            }
        }
        if (65 <= ch && ch <= 90) {
            ch += n;
            while (!(65 <= ch && ch <= 90)) {
                ch -= 26;
            }
        }

        return ch;
    }
}
