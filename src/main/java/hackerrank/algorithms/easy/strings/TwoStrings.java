package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/two-strings/problem
 */
public class TwoStrings {

    static String twoStrings(String s1, String s2) {
        if (s1.replaceAll(toRegex(s2), "").length() != s1.length()
                || s2.replaceAll(toRegex(s1), "").length() != s2.length()) {
            return "YES";
        }
        return "NO";
    }

    private static String toRegex(String s) {
        if (s.length() == 0) {
            return "[]";
        }
        char[] chars = new char[(s.length() * 2) + 1];
        chars[0] = '[';
        for (int i = 0; i < s.length() - 1; i++) {
            chars[i * 2 + 1] = s.charAt(i);
            chars[(i + 1) * 2] = ',';
        }

        chars[chars.length - 2] = s.charAt(s.length() - 1);
        chars[chars.length - 1] = ']';
        return new String(chars);
    }
}
