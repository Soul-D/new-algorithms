package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/the-love-letter-mystery/problem
 */
public class TheLoveLetterMystery {

    static int theLoveLetterMystery(String s) {
        String r = new StringBuilder(s).reverse().toString();
        int count = 0;

        for (int i = 0; i < s.length() / 2; i++) {
            count += Math.abs(s.charAt(i) - r.charAt(i));
        }

        return count;
    }
}
