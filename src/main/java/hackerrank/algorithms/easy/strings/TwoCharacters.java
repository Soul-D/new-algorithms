package hackerrank.algorithms.easy.strings;

import java.util.List;
import java.util.stream.Collectors;

/**
 * https://www.hackerrank.com/challenges/two-characters/problem
 */
public class TwoCharacters {

    static int alternate(String s) {
        int max = 0;
        List<Character> characters = s.chars().mapToObj(x -> (char) x).distinct().collect(Collectors.toList());
        String tmp;
        for (int i = 0; i < characters.size(); i++) {
            for (int j = i + 1; j < characters.size(); j++) {
                tmp = s.replaceAll("[^" + characters.get(i) + characters.get(j) + "]", "");
                if (tmp.length() > max && validate(tmp)) {
                    max = tmp.length();
                }
            }
        }

        return max;
    }

    private static boolean validate(String s) {
        if (s.length() < 2) {
            return false;
        }
        for (int i = 0; i < s.length() - 1; i++) {
            if (s.charAt(i) == s.charAt(i + 1)) {
                return false;
            }
        }
        return true;
    }
}
