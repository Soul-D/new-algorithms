package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/separate-the-numbers/problem
 */
public class SeparateTheNumbers {

    static void separateNumbers(String s) {
        if (s.length() < 2) {
            System.out.println("NO");
            return;
        }
        for (int i = 1; i <= s.length() / 2; i++) {
            StringBuilder builder = new StringBuilder();
            long parseInt, next;
            parseInt = next = Long.parseLong(s.substring(0, i));
            builder.append(parseInt);
            do {
                next = next + 1;
                builder.append(next);
            } while (builder.length() < s.length());

            if (builder.toString().equals(s)) {
                System.out.println("YES " + parseInt);
                return;
            }
        }

        System.out.println("NO");
    }
}
