package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/funny-string/problem
 */
public class FunnyString {

    static String funnyString(String s) {
        String reversed = new StringBuilder(s).reverse().toString();
        for (int i = 0; i < s.length() - 1; i++) {
            int or = Math.abs(s.charAt(i + 1) - s.charAt(i));
            int rev = Math.abs(reversed.charAt(i + 1) - reversed.charAt(i));
            if (or != rev) {
                return "Not Funny";
            }
        }
        return "Funny";
    }
}
