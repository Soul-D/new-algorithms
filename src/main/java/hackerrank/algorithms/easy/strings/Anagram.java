package hackerrank.algorithms.easy.strings;

/**
 * https://www.hackerrank.com/challenges/anagram/problem
 */
public class Anagram {

    static int anagram(String s) {
        if (s.length() % 2 == 1) {
            return -1;
        }
        int count = 0;
        String s1 = s.substring(0, s.length() / 2);
        String s2 = s.substring(s.length() / 2);

        int[] holder = new int[26];
        for (int i = 0; i < s.length() / 2; i++) {
            holder[s1.charAt(i) - 97]++;
            holder[s2.charAt(i) - 97]--;
        }
        for (int i : holder) {
            if (i > 0) {
                count += i;
            }
        }
        return count;
    }
}
