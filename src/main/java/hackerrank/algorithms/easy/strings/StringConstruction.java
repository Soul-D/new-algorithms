package hackerrank.algorithms.easy.strings;

import java.util.HashSet;
import java.util.Set;

/**
 * https://www.hackerrank.com/challenges/string-construction/problem
 */
public class StringConstruction {
    static int stringConstruction(String s) {
        Set<Character> set = new HashSet<>();
        for (int i = 0; i < s.length(); i++) {
            set.add(s.charAt(i));
        }
        return set.size();
    }
}
