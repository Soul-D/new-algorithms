package hackerrank.algorithms.easy.implementaion;

import java.util.ArrayList;
import java.util.List;

/**
 * https://www.hackerrank.com/challenges/grading/problem
 */
public class GradingStudents {
    public static List<Integer> gradingStudents(List<Integer> grades) {

        List<Integer> result = new ArrayList<>(grades.size());
        for (int candidate : grades) {
            if (candidate < 38) {
                result.add(candidate);
            } else {
                int mod = candidate % 5;
                if (mod < 3) {
                    result.add(candidate);
                } else {
                    result.add(candidate - mod + 5);
                }
            }
        }
        return result;
    }
}
