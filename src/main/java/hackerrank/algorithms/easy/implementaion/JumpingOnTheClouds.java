package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/jumping-on-the-clouds/problem
 */
public class JumpingOnTheClouds {

    static int jumpingOnClouds(int[] c) {
        int jumps = 0;
        for (int i = 0; i < c.length - 1; ) {
            if (i == c.length - 2) {
                jumps++;
                break;
            }
            if (c[i + 2] != 1) {
                i += 2;
                jumps++;
            } else {
                i += 1;
                jumps++;
            }
        }

        return jumps;
    }
}
