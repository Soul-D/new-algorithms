package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/repeated-string/problem
 */
public class RepeatedString {

    static long repeatedString(String s, long n) {
        if (s.length() > n) {
            return getCountInSubstring(s.substring(0, (int) n));
        }
        long fullStrCount = n / s.length();
        long reminder = n % s.length();
        return getCountInSubstring(s) * fullStrCount + getCountInSubstring(s.substring(0, (int) reminder));
    }

    private static long getCountInSubstring(String sub) {
        long count = 0;
        for (int i = 0; i < sub.length(); i++) {
            if (sub.charAt(i) == 'a') {
                count++;
            }
        }
        return count;
    }
}
