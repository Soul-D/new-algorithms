package hackerrank.algorithms.easy.implementaion;

import java.util.Collections;
import java.util.List;

/**
 * https://www.hackerrank.com/challenges/picking-numbers/problem
 */
public class PickingNumbers {

    public static int pickingNumbers(List<Integer> a) {
        int[] store = new int[100];

        for (int i : a) {
            store[i]++;
        }

        Collections.sort(a);
        int max = store[a.get(0)];
        for (int i = 1; i < a.size(); i++) {
            if (store[i] > max) {
                max = store[i];
            }
            if (!a.get(i - 1).equals(a.get(i)) && a.get(i) - a.get(i - 1) <= 1) {
                int candidate = store[a.get(i - 1)] + store[a.get(i)];
                if (candidate > max) {
                    max = candidate;
                }
            }
        }

        return max;
    }
}
