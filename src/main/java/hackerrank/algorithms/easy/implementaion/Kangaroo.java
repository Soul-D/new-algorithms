package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/kangaroo/problem
 */
public class Kangaroo {

    static String kangaroo(int x1, int v1, int x2, int v2) {
        if (x1 == x2) {
            return "YES";
        }

        if (v1 == v2) {
            return "NO";
        }

        int prevDistance;
        int nextDistance = Math.abs(x1 - x2);
        do {
            prevDistance = nextDistance;
            x1 += v1;
            x2 += v2;
            if (x1 == x2) {
                return "YES";
            }
            nextDistance = Math.abs(x1 - x2);
        } while (!(nextDistance > prevDistance));
        return "NO";
    }
}
