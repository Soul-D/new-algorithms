package hackerrank.algorithms.easy.implementaion;

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/flatland-space-stations/problem
 */
public class FlatlandSpaceStations {
    static int flatlandSpaceStations(int n, int[] c) {
        if (n == c.length) {
            return 0;
        }
        int max = 0;
        Arrays.sort(c);
        for (int i = 0; i < c.length - 1; i++) {
            int candidate = c[i + 1] - c[i];
            if (candidate > max) {
                max = candidate;
            }
        }

        if (max % 2 == 1) {
            max = max / 2;
        } else {
            max = max / 2 + max % 2;
        }

        if (c[0] > max) {
            max = c[0];
        }
        if (n - 1 - c[c.length - 1] > max) {
            max = n - 1 - c[c.length - 1];
        }
        return max;
    }
}
