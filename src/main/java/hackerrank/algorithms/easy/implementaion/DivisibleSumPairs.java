package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/divisible-sum-pairs/problem
 */
public class DivisibleSumPairs {

    static int divisibleSumPairs(int n, int k, int[] ar) {
        int[] mod = new int[k];
        for (int i = 0; i < n; i++) {
            mod[ar[i] % k]++;
        }
        int count = 0;
        count += ((mod[0] * (mod[0] - 1)) / 2);
        for (int i = 1; i <= k / 2 && i != k - i; i++) {
            count += mod[i] * mod[k - i];
        }

        if (k % 2 == 0) {
            count += ((mod[k / 2] * (mod[k / 2] - 1)) / 2);
        }
        return count;
    }

    static int divisibleSumPairs1(int n, int k, int[] ar) {
        int count = 0;
        for (int i = 0; i < ar.length - 1; i++) {
            for (int j = i + 1; j < ar.length; j++) {
                if ((ar[i] + ar[j]) % k == 0) {
                    count++;
                }
            }
        }

        return count;
    }
}
