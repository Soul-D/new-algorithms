package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/circular-array-rotation/problem
 */
public class CircularArrayRotation {

    static int[] circularArrayRotation(int[] a, int k, int[] queries) {
        int[] result = new int[queries.length];
        k = k % a.length;
        for (int i = 0; i < queries.length; i++) {
            result[i] = a[getIndex(k, a.length, queries[i])];
        }

        return result;
    }

    private static int getIndex(int k, int length, int ind) {
        int candidate = ind - k;
        if (candidate < 0) {
            return length + candidate;
        }
        return candidate;
    }
}
