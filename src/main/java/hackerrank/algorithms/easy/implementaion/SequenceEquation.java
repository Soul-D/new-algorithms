package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/permutation-equation/problem
 */
public class SequenceEquation {

    static int[] permutationEquation(int[] p) {
        int[] temp = new int[p.length];

        for (int i = 1; i <= p.length; i++) {
            temp[p[i - 1] - 1] = i;
        }

        int[] store = new int[temp.length];
        for (int i = 0; i < temp.length; i++) {
            store[i] = temp[temp[i] - 1];
        }

        return store;
    }

}
