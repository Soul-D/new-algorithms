package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/jumping-on-the-clouds-revisited/problem
 */
public class JumpingOnTheCloudsRevisited {

    static int jumpingOnClouds(int[] c, int k) {
        int energy = 100; //initial energy
        int i = k % c.length; //initial jump from 0
        energy -= c[i] * 2 + 1; //initial energy loss
        while (i != 0) {
            i = (i + k) % c.length;
            energy -= c[i] * 2 + 1;
        }

        return energy;
    }
}
