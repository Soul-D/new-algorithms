package hackerrank.algorithms.easy.implementaion;

import java.util.List;

/**
 * https://www.hackerrank.com/challenges/the-birthday-bar/problem
 */
public class BirthdayChocolate {
    static int birthday(List<Integer> s, int d, int m) {
        int result = 0;
        for (int i = 0; i <= s.size() - m; i++) {
            int month = 0;
            int day = 0;

            for (int j = i; j < s.size(); j++) {
                month++;
                day += s.get(j);
                if (month == m && day == d) {
                    result++;
                    break;
                }
                if (month > 12 || day > 31) {
                    break;
                }
            }
        }
        return result;
    }
}
