package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/strange-code/problem
 */
public class StrangeCounter {

    static long strangeCounter(long t) {
        long start = 3;
        while (t > start) {
            t -= start;
            start *= 2;
        }

        return start - t + 1;
    }
}
