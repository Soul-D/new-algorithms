package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/fair-rations/problem
 */
public class FairRations {
    static Object fairRations(int[] ints) {
        int count = 0;
        for (int i = 0; i < ints.length; i++) {
            if (ints[i] % 2 == 0) {
                continue;
            }
            if (i == ints.length - 1) {
                return "NO";
            }
            ints[i]++;
            ints[i + 1]++;
            count += 2;
        }

        return count;
    }
}
