package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/sherlock-and-squares/problem
 */
public class SherlockAndSquares {

    static int squares(int a, int b) {
        int count = 0;
        int start = findStart(a, b);
        if (start == -1) {
            return count;
        }

        int pow = start * start;
        while (pow >= a && pow <= b) {
            count++;
            start += 1;
            pow = start * start;
        }
        return count;
    }

    private static int findStart(int a, int b) {
        int candidate = (int) Math.floor(Math.sqrt(a)) - 1;
        int pow = candidate * candidate;
        while (pow < a) {
            candidate++;
            pow = candidate * candidate;
        }

        if (pow > b) {
            return -1;
        }
        return candidate;
    }
}
