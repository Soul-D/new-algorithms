package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/kaprekar-numbers/problem
 */
public class ModifiedKaprekarNumbers {

    static void kaprekarNumbers(int p, int q) {
        StringBuilder builder = new StringBuilder();
        for (int i = p; i <= q; i++) {
            String square = String.valueOf(i * (long) i);
            if (!preCondition(i, square)) {
                continue;
            }
            if (isGood(i, square)) {
                builder.append(i).append(" ");
            }
        }
        if (builder.length() == 0) {
            System.out.println("INVALID RANGE");
        } else {
            builder.deleteCharAt(builder.length() - 1);
            System.out.println(builder.toString());
        }

    }

    private static boolean preCondition(int num, String square) {
        String numStr = String.valueOf(num);
        return square.length() / numStr.length() >= 1;
    }

    private static boolean isGood(int num, String square) {
        String numStr = String.valueOf(num);
        String left = square.substring(0, square.length() - numStr.length());
        String right = square.substring(square.length() - numStr.length());

        int leftNum = left.isEmpty() ? 0 : Integer.parseInt(left);
        int rightNum = right.isEmpty() ? 0 : Integer.parseInt(right);

        return leftNum + rightNum == num;
    }

}
