package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/beautiful-days-at-the-movies/problem
 */
public class BeautifulDaysAtTheMovies {

    static int beautifulDays(int i, int j, int k) {
        int count = 0;
        for (int a = i; a <= j; a++) {
            if (isBeautiful(a, k)) {
                count++;
            }
        }
        return count;
    }

    private static boolean isBeautiful(int a, int k) {
        int reverse = Integer.parseInt(new StringBuilder().append(a).reverse().toString());
        return (a - reverse) % k == 0;
    }
}
