package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/apple-and-orange/problem
 */
public class AppleAndOrange {
    static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {
        countFruits(s, t, a, apples);
        countFruits(s, t, b, oranges);
    }

    private static void countFruits(int s, int t, int tree, int[] fruits) {
        int count = 0;

        for (int app : fruits) {
            int aLoc = tree + app;
            if (aLoc >= s && aLoc <= t) {
                count++;
            }
        }

        System.out.println(count);
    }
}
