package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/drawing-book/problem
 */
public class DrawingBook {
    static int pageCount(int n, int p) {
        return Math.min(p / 2, n / 2 - p / 2);
    }
}
