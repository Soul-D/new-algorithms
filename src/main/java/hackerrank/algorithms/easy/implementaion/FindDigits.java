package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/find-digits/problem
 */
public class FindDigits {

    static int findDigits(int n) {
        int copy = n;
        int count = 0;
        while (n != 0) {
            int reminder = n % 10;
            n = n / 10;
            if (reminder != 0 && copy % reminder == 0) {
                count++;
            }
        }

        return count;
    }
}
