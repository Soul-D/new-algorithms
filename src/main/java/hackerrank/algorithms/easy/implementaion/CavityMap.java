package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/cavity-map/problem
 */
public class CavityMap {
    static String[] cavityMap(String[] grid) {
        int length = grid.length;
        if (length < 3) {
            return grid;
        }
        String[] result = new String[length];
        result[0] = grid[0];
        result[length - 1] = grid[length - 1];
        for (int row = 1; row < length - 1; row++) {
            StringBuilder substitute = new StringBuilder();
            substitute.append(grid[row].charAt(0));
            for (int col = 1; col < length - 1; col++) {
                if (isCavity(row, col, grid)) {
                    substitute.append('X');
                } else {
                    substitute.append(grid[row].charAt(col));
                }
            }
            substitute.append(grid[row].charAt(length - 1));
            result[row] = substitute.toString();
        }

        return result;
    }

    private static boolean isCavity(int row, int col, String[] grid) {
        if (grid[row - 1].charAt(col) == 'X') {
            return false;
        }
        int cell = Character.getNumericValue(grid[row].charAt(col));
        int left = Character.getNumericValue(grid[row].charAt(col - 1));
        int right = Character.getNumericValue(grid[row].charAt(col + 1));
        int top = Character.getNumericValue(grid[row - 1].charAt(col));
        int bottom = Character.getNumericValue(grid[row + 1].charAt(col));
        return cell > left && cell > right && cell > top && cell > bottom;
    }
}
