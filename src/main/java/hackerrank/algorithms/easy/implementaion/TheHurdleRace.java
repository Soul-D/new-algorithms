package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/the-hurdle-race/problem
 */
public class TheHurdleRace {
    static int hurdleRace(int k, int[] height) {
        int max = 0;
        for (int i : height) {
            if (i > max) {
                max = i;
            }
            if (max == 100) {
                break;
            }
        }
        if (max > k) {
            return max - k;
        }
        return 0;
    }
}
