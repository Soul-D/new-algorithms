package hackerrank.algorithms.easy.implementaion;

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/cut-the-sticks/problem
 */
public class CutTheSticks {

    static int[] cutTheSticks(int[] arr) {
        int diffNumbers = 1;

        Arrays.sort(arr);
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] != arr[i - 1]) {
                diffNumbers++;
            }
        }

        int cutted = 0;
        int round = 0;
        int prev = -1;
        int[] result = new int[diffNumbers];
        for (int value : arr) {
            if (value != prev) {
                result[round] = arr.length - cutted;
                prev = value;
                round++;
            }
            cutted++;
        }

        return result;
    }
}
