package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/strange-advertising/problem
 */
public class ViralAdvertising {
    static int viralAdvertising(int n) {
        int acc = 0;
        int shared = 5;
        while (n > 0) {
            acc += (shared / 2);
            shared = 3 * (shared / 2);
            n--;
        }

        return acc;
    }
}
