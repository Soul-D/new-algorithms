package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/magic-square-forming/problem
 */
public class CatsAndAMouse {
    static String catAndMouse(int x, int y, int z) {
        int xDist = Math.abs(z - x);
        int yDist = Math.abs(z - y);
        if (xDist < yDist) {
            return "Cat A";
        } else if (xDist > yDist) {
            return "Cat B";
        } else {
            return "Mouse C";
        }
    }
}
