package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/halloween-sale/problem
 */
public class HalloweenSale {

    static int howManyGames(int p, int d, int m, int s) {
        int count = 0;
        while (p > m) {
            if (p > s) {
                break;
            }
            count++;
            s -= p;
            p -= d;
        }

        if (p < m) {
            p = m;
        }

        return count + s / p;
    }
}
