package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/utopian-tree/problem
 */
public class UtopianTree {

    static int utopianTree(int n) {
        int start = 1;
        int count = 0;

        while (count < n) {
            if (++count % 2 != 0) {
                start = 2 * start;
            } else {
                start = start + 1;
            }
        }
        return start;
    }
}
