package hackerrank.algorithms.easy.implementaion;

import java.util.List;

/**
 * https://www.hackerrank.com/challenges/bon-appetit/problem
 */
public class BonAppetit {

    static void bonAppetit(List<Integer> bill, int k, int b) {
        int sum = 0;
        int ind = -1;
        for (int i : bill) {
            ind++;
            if (ind == k) {
                continue;
            }
            sum += i;
        }

        int half = sum / 2;
        if (half == b) {
            System.out.println("Bon Appetit");
        } else {
            System.out.println(b - half);
        }
    }
}
