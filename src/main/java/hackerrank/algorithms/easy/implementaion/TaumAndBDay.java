package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/taum-and-bday/problem
 */
public class TaumAndBDay {

    public static long taumBday(int b, int w, int bc, int wc, int z) {
        if (!hasSense(bc, wc, z)) {
            return b * (long) bc + w * (long) wc;
        }

        if (bc < wc) {
            return (b + w) * (long) bc + w * (long) z;
        } else {
            return (b + w) * (long) wc + b * (long) z;
        }
    }

    private static boolean hasSense(int bc, int wc, int z) {
        return (bc + z) < wc || (wc + z) < bc;
    }
}
