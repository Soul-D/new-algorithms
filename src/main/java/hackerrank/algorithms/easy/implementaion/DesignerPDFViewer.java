package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/designer-pdf-viewer/problem
 */
public class DesignerPDFViewer {
    static int designerPdfViewer(int[] h, String word) {
        int offset = 97;

        int max = h[word.charAt(0) - offset];
        for (int i = 1; i < word.length(); i++) {
            int curr = h[word.charAt(i) - offset];
            if (curr > max) {
                max = curr;
            }
        }
        return max * word.length();
    }
}
