package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/acm-icpc-team/problem
 */
public class AcmIcpcTeam {
    static int[] acmTeam(String[] topic) {
        int count = 0;
        int max = 0;
        for (int i = 0; i < topic.length - 1; i++) {
            for (int j = i + 1; j < topic.length; j++) {
                int candidate = count(topic[i], topic[j]);
                if (candidate > max) {
                    max = candidate;
                    count = 1;
                    continue;
                }
                if (candidate == max) {
                    count++;
                }
            }
        }
        return new int[]{max, count};
    }

    static int count(String s1, String s2) {
        int count = 0;
        for (int i = 0; i < s1.length(); i++) {
            if (s1.charAt(i) == '1' || s2.charAt(i) == '1') {
                count++;
            }
        }
        return count;
    }
}
