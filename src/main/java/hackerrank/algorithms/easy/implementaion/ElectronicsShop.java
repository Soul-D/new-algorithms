package hackerrank.algorithms.easy.implementaion;

import java.util.Arrays;

/**
 * https://www.hackerrank.com/challenges/electronics-shop/problem
 */
public class ElectronicsShop {

    static int getMoneySpentBrut(int[] keyboards, int[] drives, int b) {
        int sum = -1;
        for (int i = keyboards.length - 1; i > 0; i--) {
            for (int j = drives.length - 1; j > 0; j--) {
                int set = keyboards[i] + drives[i];
                if (set <= b && set > sum) {
                    sum = set;
                }
            }
        }

        return sum;
    }


    static int getMoneySpent(int[] keyboards, int[] drives, int s) {
        Arrays.sort(keyboards);
        Arrays.sort(drives);

        if (keyboards[0] + drives[0] > s) {
            return -1;
        }

        int smallestBoard = 0;
        int largestDrive = drives.length - 1;

        while (keyboards[smallestBoard] + drives[largestDrive] > s && largestDrive >= 0) {
            largestDrive--;
        }

        int min = s - keyboards[smallestBoard] - drives[largestDrive];

        int diff;
        while (smallestBoard < keyboards.length && largestDrive >= 0) {
            diff = s - keyboards[smallestBoard] - drives[largestDrive];
            if (diff >= 0) {
                if (diff < min) {
                    min = diff;
                }
                smallestBoard++;
            } else {
                largestDrive--;
            }
        }
        return s - min;
    }
}
