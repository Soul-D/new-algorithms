package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/save-the-prisoner/problem
 */
public class SaveThePrisoner {
    static int saveThePrisoner(int n, int m, int s) {
        int candidate = (m + s - 1) % n;

        if (candidate == 0) {
            return n;
        } else {
            return candidate;
        }
    }
}
