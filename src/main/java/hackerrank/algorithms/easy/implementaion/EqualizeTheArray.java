package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/equality-in-a-array/problem
 */
public class EqualizeTheArray {

    static int equalizeArray(int[] arr) {
        int[] counter = new int[101];

        for (int i : arr) {
            counter[i]++;
        }
        int maxCount = -1;
        for (int i = 1; i < counter.length; i++) {
            if (counter[i] > maxCount) {
                maxCount = counter[i];
            }
        }

        return arr.length - maxCount;
    }
}
