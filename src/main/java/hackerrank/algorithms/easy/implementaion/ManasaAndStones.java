package hackerrank.algorithms.easy.implementaion;

import java.util.Set;
import java.util.TreeSet;

/**
 * https://www.hackerrank.com/challenges/manasa-and-stones/problem
 */
public class ManasaAndStones {
    static int[] stones(int n, int a, int b) {
        Set<Integer> set = new TreeSet<>();
        for (int i = 0; i < n; i++) {
            int aStart = i * a + (n - i - 1) * b;
            int bStart = i * b + (n - i - 1) * a;
            set.add(aStart);
            set.add(bStart);
        }
        int[] res = new int[set.size()];
        int ind = 0;
        for (Integer integer : set) {
            res[ind++] = integer;
        }

        return res;
    }
}
