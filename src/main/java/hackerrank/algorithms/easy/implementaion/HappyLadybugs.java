package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/happy-ladybugs/problem
 */
public class HappyLadybugs {
    static String happyLadybugs(String b) {
        if (b.length() == 0) {
            return "YES";
        }
        int[] chars = new int[26];
        int spaces = 0;
        for (int i = 0; i < b.length(); i++) {
            if (b.charAt(i) == '_') {
                spaces++;
            } else {
                chars[(int) b.charAt(i) - 65]++;
            }
        }

        if (spaces == b.length()) {
            return "YES";
        }
        if (spaces == 0) {
            if (b.length() == 1 || b.charAt(0) != b.charAt(1) || b.charAt(b.length() - 2) != b.charAt(b.length() - 1)) {
                return "NO";
            }
            for (int i = 1; i < b.length() - 1; i++) {
                if (!(b.charAt(i) == b.charAt(i - 1) || b.charAt(i) == b.charAt(i + 1))) {
                    return "NO";
                }
            }
        }

        for (int ch : chars) {
            if (ch == 1) {
                return "NO";
            }
        }

        return "YES";
    }
}
