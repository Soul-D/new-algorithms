package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/lisa-workbook/problem
 */
public class LisasWorkbook {

    static int workbook(int n, int k, int[] arr) {
        int page = 1;
        int count = 0;
        for (int a : arr) {
            for (int i = 1; i <= a; i++) {
                if (i == page) {
                    count++;
                }
                if (i % k == 0) {
                    page++;
                }
            }
            if (a % k != 0) {
                page++;
            }
        }
        return count;
    }
}
