package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/day-of-the-programmer/problem
 */
public class DayOfTheProgrammer {

    static String dayOfProgrammer(int year) {
        String notLeap = "13.09.";
        String leap = "12.09.";
        String transition = "26.09.";

        String prefix;
        if (year == 1918) {
            prefix = transition;
        } else if (isLeapYear(year)) {
            prefix = leap;
        } else {
            prefix = notLeap;
        }

        return prefix + year;
    }

    private static boolean isLeapYear(int year) {
        if (year < 1918) {
            return year % 4 == 0;
        }

        if (year % 400 == 0) {
            return true;
        }

        return year % 4 == 0 && year % 100 != 0;
    }
}
