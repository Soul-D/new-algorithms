package hackerrank.algorithms.easy.implementaion;

import java.util.List;

/**
 * https://www.hackerrank.com/challenges/migratory-birds/problem
 */
public class MigratoryBirds {
    static int migratoryBirds(List<Integer> arr) {
        int[] accumulator = new int[5];
        for (int i : arr) {
            accumulator[i - 1]++;
        }

        int max = Integer.MIN_VALUE;
        int id = 0;
        for (int i = 5; i > 0; i--) {
            if (accumulator[i - 1] >= max) {
                max = accumulator[i - 1];
                id = i;
            }
        }

        return id;
    }
}
