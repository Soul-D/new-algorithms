package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/append-and-delete/problem
 */
public class AppendAndDelete {

    static String appendAndDelete(String s, String t, int k) {
        int commonLength = findSimilarSuffixLength(s, t);
        int notCommonLength = s.length() + t.length() - 2 * commonLength;

        if (notCommonLength > k) {
            return "No";
        } else if (notCommonLength % 2 == k % 2) {
            return "Yes";
        } else if (s.length() + t.length() < k) {
            return "Yes";
        } else {
            return "No";
        }
    }

    private static int findSimilarSuffixLength(String s, String t) {
        int length = 0;
        int maxLength = Math.min(s.length(), t.length());

        for (int i = 0; i < maxLength; i++) {
            if (s.charAt(i) == t.charAt(i)) {
                length++;
            } else {
                break;
            }
        }
        return length;
    }
}
