package hackerrank.algorithms.easy.implementaion;

import java.util.HashSet;
import java.util.Set;

/**
 * https://www.hackerrank.com/challenges/sock-merchant/problem
 */
public class SockMerchant {

    static int sockMerchant(int n, int[] ar) {
        Set<Integer> stock = new HashSet<>();
        int count = 0;
        for (int i : ar) {
            if (!stock.add(i)) {
                count++;
                stock.remove(i);
            }
        }

        return count;
    }

    static int sockMerchant1(int n, int[] ar) {
        int[] acc = new int[101];
        for (int i : ar) {
            acc[i]++;
        }

        int count = 0;
        for (int i : acc) {
            count += (i / 2);
        }

        return count;
    }
}
