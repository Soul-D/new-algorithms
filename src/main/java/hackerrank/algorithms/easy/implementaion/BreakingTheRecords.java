package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/breaking-best-and-worst-records/problem
 */
public class BreakingTheRecords {
    static int[] breakingRecords(int[] scores) {
        int min = scores[0];
        int max = scores[0];
        int brMin = 0;
        int brMax = 0;

        for (int i = 1; i < scores.length; i++) {
            int curr = scores[i];
            if (curr > max) {
                max = curr;
                brMax++;
            } else if (curr < min) {
                min = curr;
                brMin++;
            }
        }

        return new int[]{brMax, brMin};
    }
}
