package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/chocolate-feast/problem
 */
public class ChocolateFeast {
    static int chocolateFeast(int n, int c, int m) {
        int count;
        int wrappers;
        count = wrappers = n / c;
        do {
            int candies = wrappers / m;
            count += candies;
            wrappers = wrappers % m + candies;
        } while (wrappers >= m);
        return count;
    }
}
