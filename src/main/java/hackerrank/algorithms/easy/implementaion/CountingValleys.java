package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/counting-valleys/problem
 */
public class CountingValleys {

    static int countingValleys(int n, String s) {
        int valleys = 0;
        int level = 0;

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'D') {
                level--;
            } else {
                level++;
            }

            if (level == 0 && s.charAt(i) == 'U') {
                valleys++;
            }
        }

        return valleys;
    }
}
