package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/minimum-distances/problem
 */
public class MinimumDistances {

    static int minimumDistances(int[] a) {
        int min = Integer.MAX_VALUE;
        if (a.length == 1) {
            return -1;
        }
        int first = a[0];

        int[] storage = new int[100_001];
        for (int i = 1; i < a.length; i++) {
            int prevInd = storage[a[i]];
            if (prevInd == 0 && a[i] != first) {
                storage[a[i]] = i;
            } else {
                int candidate = i - prevInd;
                if (candidate < min) {
                    min = candidate;
                }
                storage[a[i]] = i;
            }
        }

        if (min != Integer.MAX_VALUE) {
            return min;
        } else {
            return -1;
        }
    }
}
