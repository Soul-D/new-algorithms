package hackerrank.algorithms.easy.implementaion;

/**
 * https://www.hackerrank.com/challenges/service-lane/problem
 */

public class ServiceLane {
    static int[] serviceLane(int n, int[][] cases, int[] width) {
        int[] result = new int[cases.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = min(cases[i][0], cases[i][1], width);
        }
        return result;
    }

    private static int min(int start, int end, int[] ints) {
        int min = Integer.MAX_VALUE;
        for (int i = start; i <= end; i++) {
            if (ints[i] < min) {
                min = ints[i];
            }
        }
        return min;
    }
}
