package hackerrank.algorithms.easy.implementaion;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * https://www.hackerrank.com/challenges/between-two-sets/problem
 */
public class BetweenTwoSets {
    public static int getTotalX(List<Integer> a, List<Integer> b) {
        Set<Integer> candidates = new HashSet<>();
        for (int i = Collections.max(a); i <= Collections.min(b); i++) {
            if (isCommonDivider(i, a)) {
                candidates.add(i);
            }
        }

        int count = 0;
        for (Integer i : candidates) {
            if (isCommonFactor(i, b)) {
                count++;
            }
        }

        return count;
    }

    private static boolean isCommonDivider(Integer a, List<Integer> nums) {
        for (Integer i : nums) {
            if (a % i != 0) {
                return false;
            }
        }
        return true;
    }

    private static boolean isCommonFactor(Integer a, List<Integer> nums) {
        for (Integer i : nums) {
            if (i % a != 0) {
                return false;
            }
        }
        return true;
    }
}
