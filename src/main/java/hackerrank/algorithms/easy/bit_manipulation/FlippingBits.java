package hackerrank.algorithms.easy.bit_manipulation;

/**
 * https://www.hackerrank.com/challenges/flipping-bits/problem
 */
public class FlippingBits {
    static long flippingBits(long n) {
        return 4294967295L ^ n;
    }
}
