package hackerrank.algorithms.easy.bit_manipulation;

/**
 * https://www.hackerrank.com/challenges/lonely-integer/problem
 */
public class LonelyInteger {
    static int lonelyinteger(int[] a) {
        int res = 0;
        for (int i : a) {
            res ^= i;
        }
        return res;
    }
}
