package hackerrank.algorithms.easy.bit_manipulation;

/**
 * https://www.hackerrank.com/challenges/maximizing-xor/problem
 */
public class MaximizingXOR {
    static int maximizingXor(int l, int r) {
        int xor = l ^ r;
        int significantBit = 31 - Integer.numberOfLeadingZeros(xor);
        return (1 << (significantBit + 1)) - 1;
    }
}
