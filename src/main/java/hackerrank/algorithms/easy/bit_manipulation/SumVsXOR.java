package hackerrank.algorithms.easy.bit_manipulation;

/**
 * https://www.hackerrank.com/challenges/sum-vs-xor/problem
 */
public class SumVsXOR {
    static long sumXor(long n) {
        long c = 0;
        while (n != 0) {
            c += n % 2 == 0 ? 1 : 0;
            n /= 2;
        }
        return (long) Math.pow(2, c);
    }
}
