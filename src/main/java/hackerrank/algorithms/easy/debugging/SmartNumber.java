package hackerrank.algorithms.easy.debugging;

/**
 * https://www.hackerrank.com/challenges/smart-number/problem
 */
public class SmartNumber {
    public static boolean isSmartNumber(int num) {
        int val = (int) Math.sqrt(num);
        if (num == (int) Math.pow(val, 2))
            return true;
        return false;
    }
}
