package hackerrank.algorithms.easy.search;

/**
 * https://www.hackerrank.com/challenges/icecream-parlor/problem
 */
public class IceCreamParlor {
    static int[] icecreamParlor(int m, int[] arr) {
        int[] rest = new int[1001];
        for (int i = 0; i < arr.length; i++) {
            int r = m - arr[i];
            if (r < 1) {
                continue;
            }
            if (rest[arr[i]] != 0) {
                return new int[]{rest[arr[i]], i + 1};
            }
            rest[r] = i + 1;
        }
        throw new RuntimeException();
    }
}
