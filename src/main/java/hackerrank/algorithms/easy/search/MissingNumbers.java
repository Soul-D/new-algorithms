package hackerrank.algorithms.easy.search;

/**
 * https://www.hackerrank.com/challenges/missing-numbers/problem
 */
public class MissingNumbers {

    static int[] missingNumbers(int[] arr, int[] brr) {
        int min = Integer.MAX_VALUE;
        for (int i : brr) {
            if (i < min) {
                min = i;
            }
        }

        int[] counts = new int[101];
        for (int i : brr) {
            counts[i - min]++;
        }
        for (int i : arr) {
            counts[i - min]--;
        }

        int count = 0;
        for (int i : counts) {
            if (i != 0) {
                System.out.println(i);
                count++;
            }
        }
        int[] result = new int[count];
        int ind = 0;
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] != 0) {
                result[ind++] = i + min;
            }
        }
        return result;
    }
}
