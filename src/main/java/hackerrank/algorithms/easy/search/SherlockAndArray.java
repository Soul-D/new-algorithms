package hackerrank.algorithms.easy.search;

import java.util.List;

public class SherlockAndArray {
    static String balancedSums(List<Integer> arr) {
        int sum = 0;
        for (Integer integer : arr) {
            sum += integer;
        }
        int left = 0;
        for (Integer integer : arr) {
            int right = sum - left - integer;
            if (left == right) {
                return "YES";
            }
            left += integer;
        }
        return "NO";
    }
}
