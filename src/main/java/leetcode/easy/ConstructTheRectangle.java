package leetcode.easy;

/**
 * 492. Construct the Rectangle
 *
 * @link https://leetcode.com/problems/construct-the-rectangle/
 */
public class ConstructTheRectangle {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Construct the Rectangle.
     * Memory Usage: 33.7 MB, less than 8.33% of Java online submissions for Construct the Rectangle.
     */
    public static int[] constructRectangleFaster(int area) {
        int candidate = (int) Math.sqrt(area);
        while (area % candidate != 0) {
            candidate--;
        }
        return new int[]{area / candidate, candidate};
    }

    /**
     * Runtime: 52 ms, faster than 19.80% of Java online submissions for Construct the Rectangle.
     * Memory Usage: 33.8 MB, less than 8.33% of Java online submissions for Construct the Rectangle.
     */
    public static int[] constructRectangle(int area) {
        int candidate = (int) Math.ceil(Math.sqrt(area));
        while (area % candidate != 0) {
            candidate++;
        }
        return new int[]{candidate, area / candidate};
    }
}
