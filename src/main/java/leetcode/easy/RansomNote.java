package leetcode.easy;

/**
 * 383. Ransom Note
 *
 * @link https://leetcode.com/problems/ransom-note/
 * @tags String.
 */
public class RansomNote {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Ransom Note.
     * Memory Usage: 38 MB, less than 100.00% of Java online submissions for Ransom Note.
     */
    public boolean canConstructFaster(String ransomNote, String magazine) {
        if (ransomNote.length() > magazine.length()) {
            return false;
        }

        int[] counts = new int[26];

        for (char ch : ransomNote.toCharArray()) {
            int index = magazine.indexOf(ch, counts[ch - 'a']);
            if (index < 0) {
                return false;
            } else {
                counts[ch - 'a'] = index + 1;
            }
        }

        return true;
    }

    /**
     * Runtime: 4 ms, faster than 73.19% of Java online submissions for Ransom Note.
     * Memory Usage: 37.9 MB, less than 100.00% of Java online submissions for Ransom Note.
     */
    public static boolean canConstruct(String ransomNote, String magazine) {
        int[] counts = new int[26];
        for (int i = 0; i < ransomNote.length(); i++) {
            counts[ransomNote.charAt(i) - 'a']--;
        }
        for (int i = 0; i < magazine.length(); i++) {
            counts[magazine.charAt(i) - 'a']++;
        }
        for (int charCounter : counts) {
            if (charCounter < 0) {
                return false;
            }
        }
        return true;
    }
}
