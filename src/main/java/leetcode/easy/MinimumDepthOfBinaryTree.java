package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 111. Minimum Depth of Binary Tree
 *
 * @link https://leetcode.com/problems/minimum-depth-of-binary-tree/description/
 * @tags Tree, Depth-first search, Breadth-first search.
 */
public class MinimumDepthOfBinaryTree {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Minimum Depth of Binary Tree.
     * Memory Usage: 38.8 MB, less than 98.44% of Java online submissions for Minimum Depth of Binary Tree.
     */
    public static int minDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return depth(root);
    }

    private static int depth(TreeNode root) {
        if (root.left == null && root.right == null) {
            return 1;
        }
        if (root.left != null && root.right != null) {
            return 1 + Math.min(depth(root.left), depth(root.right));
        }
        if (root.left != null) {
            return 1 + depth(root.left);
        } else {
            return 1 + depth(root.right);
        }
    }
}
