package leetcode.easy;

/**
 * 7. Reverse Integer
 *
 * @link https://leetcode.com/problems/reverse-integer/description/
 * @tags Math
 */
public class ReverseInteger {
    /**
     * 100%
     * Runtime: 1 ms
     * Memory Usage: 33.6 MB
     */
    public int reverse(int x) {
        long candidate;
        if (x > 0) {
            candidate = reverseAbs(x);
            return candidate > Integer.MAX_VALUE ? 0 : (int) candidate;
        } else {
            candidate = reverseAbs(-x);
            return candidate > Integer.MAX_VALUE ? 0 : -(int) candidate;
        }
    }

    private long reverseAbs(int x) {
        long reversed = 0;
        while (x > 0) {
            int last = x % 10;
            reversed = reversed * 10 + last;
            x = x / 10;
        }
        return reversed;
    }
}
