package leetcode.easy;

/**
 * 344. Reverse String
 *
 * @link https://leetcode.com/problems/reverse-string/
 * @tags Two Pointers, String.
 */
public class ReverseString {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Reverse String.
     * Memory Usage: 52.3 MB, less than 7.10% of Java online submissions for Reverse String.
     */
    public static void reverseString(char[] s) {
        int middle = s.length / 2;
        for (int i = 0; i < middle; i++) {
            char temp = s[i];
            s[i] = s[s.length - 1 - i];
            s[s.length - 1 - i] = temp;
        }
    }
}
