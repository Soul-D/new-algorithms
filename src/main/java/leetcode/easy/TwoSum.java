package leetcode.easy;

import java.util.HashMap;
import java.util.Map;

/**
 * 1. Two Sum
 *
 * @link https://leetcode.com/problems/two-sum/description/
 * @tags Array, Hash Table
 */
public class TwoSum {
    /**
     * 98, 95%
     * Runtime: 2 ms
     * Memory Usage: 38.3 MB
     */
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            Integer curr = nums[i];
            Integer addition = map.get(curr);
            if (addition != null) {
                return new int[]{addition, i};
            }
            map.put(target - curr, i);
        }
        throw new RuntimeException();
    }
}
