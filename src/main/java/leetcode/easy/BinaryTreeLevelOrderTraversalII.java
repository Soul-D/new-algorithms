package leetcode.easy;

import leetcode.model.TreeNode;

import java.util.*;

/**
 * 107. Binary Tree Level Order Traversal II
 *
 * @link https://leetcode.com/problems/binary-tree-level-order-traversal-ii/
 * @tags Tree, Breadth-first search.
 */
public class BinaryTreeLevelOrderTraversalII {
    /**
     * Runtime: 1 ms, faster than 98.32% of Java online submissions for Binary Tree Level Order Traversal II.
     * Memory Usage: 36.3 MB, less than 100.00% of Java online submissions for Binary Tree Level Order Traversal II.
     */
    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        LinkedList<List<Integer>> result = new LinkedList<>();
        if (root == null) {
            return result;
        }
        Queue<List<TreeNode>> levels = new LinkedList<>();
        levels.add(Collections.singletonList(root));
        while (!levels.isEmpty()) {
            List<TreeNode> level = levels.poll();
            List<Integer> toRes = new ArrayList<>(level.size());
            List<TreeNode> nextLevel = new ArrayList<>();
            for (TreeNode treeNode : level) {
                toRes.add(treeNode.val);
                if (treeNode.left != null) {
                    nextLevel.add(treeNode.left);
                }
                if (treeNode.right != null) {
                    nextLevel.add(treeNode.right);
                }
            }
            result.addFirst(toRes);
            if (nextLevel.isEmpty()) {
                break;
            }
            levels.add(nextLevel);
        }

        return result;
    }
}
