package leetcode.easy;

/**
 * 414. Third Maximum Number
 *
 * @link https://leetcode.com/problems/third-maximum-number/
 * @tags Array.
 */
public class ThirdMaximumNumber {
    /**
     * Runtime: 1 ms, faster than 91.26% of Java online submissions for Third Maximum Number.
     * Memory Usage: 35.8 MB, less than 100.00% of Java online submissions for Third Maximum Number.
     */
    public static int thirdMax(int[] nums) {
        long min = Long.MIN_VALUE;

        long first = min, second = min, third = min;
        for (int num : nums) {
            if (first == num || second == num || third == num) {
                continue;
            }
            if (first == min || first < num) {
                third = second;
                second = first;
                first = num;
            } else if (second == min || second < num) {
                third = second;
                second = num;
            } else if (third == min || third < num) {
                third = num;
            }
        }
        return third == min ? (int) first : (int) third;
    }
}
