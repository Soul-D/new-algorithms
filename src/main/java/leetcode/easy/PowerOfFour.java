package leetcode.easy;

/**
 * 342. Power of Four
 *
 * @link https://leetcode.com/problems/power-of-four/
 * @tags Bit Manipulation.
 */
public class PowerOfFour {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Power of Four.
     * Memory Usage: 33.6 MB, less than 6.67% of Java online submissions for Power of Four.
     * Not mine solution :(
     */
    public static boolean isPowerOfFour(int num) {
        return num != 0 && ((num & (num - 1)) == 0) && (num & 0xAAAAAAAA) == 0;
    }
}
