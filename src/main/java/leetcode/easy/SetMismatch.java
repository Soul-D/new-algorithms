package leetcode.easy;

/**
 * 645. Set Mismatch
 *
 * @link https://leetcode.com/problems/set-mismatch/
 * @tags Hash Table, Math.
 */
public class SetMismatch {
    /**
     * Runtime: 2 ms, faster than 97.19% of Java online submissions for Set Mismatch.
     * Memory Usage: 49.8 MB, less than 14.29% of Java online submissions for Set Mismatch.
     */
    public static int[] findErrorNumsFast(int[] nums) {
        int[] copy = new int[nums.length + 1];
        int dup = 0, missed = 0;
        for (int i = 0; i < nums.length; i++) {
            if (copy[nums[i]] == 0) {
                copy[nums[i]]++;
            } else {
                dup = nums[i];
            }
        }
        for (int i = 1; i < copy.length; i++) {
            if (copy[i] == 0) {
                missed = i;
                break;
            }

        }
        return new int[]{dup, missed};
    }

    /**
     * Runtime: 3 ms, faster than 67.57% of Java online submissions for Set Mismatch.
     * Memory Usage: 48.3 MB, less than 14.29% of Java online submissions for Set Mismatch.
     */
    public static int[] findErrorNums(int[] nums) {
        int n = nums.length;
        int sum = 0;
        int shouldBeSum = (1 + n) * (n / 2) + ((n / 2) + 1) * (n % 2);
        int[] copy = new int[n];
        for (int num : nums) {
            sum += num;
            copy[(num - 1) % n] = num + n;
        }
        int missed = 0;
        for (int i = 0; i < n; i++) {
            if (copy[i] / n < 1) {
                missed = i + 1;
            }
        }

        return new int[]{
                sum + missed - shouldBeSum, missed
        };
    }
}
