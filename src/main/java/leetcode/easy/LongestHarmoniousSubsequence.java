package leetcode.easy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 594. Longest Harmonious Subsequence
 *
 * @link https://leetcode.com/problems/longest-harmonious-subsequence/
 * @tags Hash Table.
 */
public class LongestHarmoniousSubsequence {
    /**
     * Runtime: 30 ms, faster than 67.86% of Java online submissions for Longest Harmonious Subsequence.
     * Memory Usage: 40.2 MB, less than 94.74% of Java online submissions for Longest Harmonious Subsequence.
     */
    public static int findLHSHash(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        int result = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            Integer candidate = map.get(entry.getKey() + 1);
            if (candidate != null) {
                result = Math.max(result, candidate + entry.getValue());
            }
        }
        return result;
    }

    /**
     * Runtime: 15 ms, faster than 100.00% of Java online submissions for Longest Harmonious Subsequence.
     * Memory Usage: 42.1 MB, less than 31.58% of Java online submissions for Longest Harmonious Subsequence.
     */
    public static int findLHS(int[] nums) {
        Arrays.sort(nums);
        int count = 0;
        int prevs = 0;
        int currs = 1;
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == nums[i + 1]) {
                currs++;
            } else {
                if (prevs != 0)
                    count = Math.max(count, prevs + currs);
                if (nums[i] + 1 == nums[i + 1]) {
                    prevs = currs;
                } else {
                    prevs = 0;
                }
                currs = 1;
            }
        }
        if (prevs != 0) {
            count = Math.max(count, prevs + currs);
        }
        return count;
    }
}
