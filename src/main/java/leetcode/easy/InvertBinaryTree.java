package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 226. Invert Binary Tree
 *
 * @link https://leetcode.com/problems/invert-binary-tree/
 * @tags Tree.
 */
public class InvertBinaryTree {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Invert Binary Tree.
     * Memory Usage: 34.3 MB, less than 100.00% of Java online submissions for Invert Binary Tree.
     */
    public static TreeNode invertTree(TreeNode root) {
        if (root != null) {
            TreeNode temp = root.left;
            root.left = root.right;
            root.right = temp;
            root.left = invertTree(root.left);
            root.right = invertTree(root.right);
        }
        return root;
    }
}
