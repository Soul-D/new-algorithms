package leetcode.easy;

/**
 * 9. Palindrome Number
 *
 * @link https://leetcode.com/problems/palindrome-number/description/
 * @tags Math
 */
public class PalindromeNumber {
    /**
     * 100%
     * Runtime: 6 ms
     * Memory Usage: 36.3 MB
     */
    public static boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        int copy = x;
        int reversed = 0;
        while (copy > 0) {
            int last = copy % 10;
            reversed = reversed * 10 + last;
            copy = copy / 10;
        }
        return reversed == x;
    }
}
