package leetcode.easy;

import java.util.Arrays;

/**
 * 977. Squares of a Sorted Array
 *
 * @link https://leetcode.com/problems/squares-of-a-sorted-array/
 * @tags Array, Two Pointers.
 */
public class SquaresOfASortedArray {
    /**
     * Runtime: 2 ms, faster than 60.79% of Java online submissions for Squares of a Sorted Array.
     * Memory Usage: 42.5 MB, less than 42.07% of Java online submissions for Squares of a Sorted Array.
     */
    public static int[] sortedSquares(int[] A) {
        for (int i = 0; i < A.length; i++) {
            A[i] = A[i] * A[i];
        }

        Arrays.sort(A);
        return A;
    }

    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Squares of a Sorted Array.
     * Memory Usage: 42.6 MB, less than 39.63% of Java online submissions for Squares of a Sorted Array.
     */
    public static int[] sortedSquaresFast(int[] A) {
        int[] result = new int[A.length];
        int left = 0;
        int right = A.length - 1;
        int index = A.length - 1;
        while (left <= right) {
            if (Math.abs(A[left]) < Math.abs(A[right])) {
                result[index] = A[right] * A[right];
                right--;
            } else {
                result[index] = A[left] * A[left];
                left++;
            }
            index--;
        }
        return result;
    }

}
