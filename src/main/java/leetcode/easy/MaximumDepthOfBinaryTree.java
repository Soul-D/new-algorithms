package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 104. Maximum Depth of Binary Tree
 *
 * @link https://leetcode.com/problems/maximum-depth-of-binary-tree/description/
 * @tags Tree, Depth-first search.
 */
public class MaximumDepthOfBinaryTree {
    /**
     * Runtime: 0 ms
     * Memory Usage: 38.5 MB
     */
    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return 1 + Math.max(maxDepth(root.left), maxDepth(root.right));
    }
}
