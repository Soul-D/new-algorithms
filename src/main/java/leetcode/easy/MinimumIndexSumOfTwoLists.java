package leetcode.easy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 599. Minimum Index Sum of Two Lists
 *
 * @link https://leetcode.com/problems/minimum-index-sum-of-two-lists/
 * @tags Hash Table.
 */
public class MinimumIndexSumOfTwoLists {
    /**
     * Runtime: 24 ms, faster than 13.49% of Java online submissions for Minimum Index Sum of Two Lists.
     * Memory Usage: 39.3 MB, less than 93.55% of Java online submissions for Minimum Index Sum of Two Lists.
     */
    public static String[] findRestaurant(String[] list1, String[] list2) {
        Map<String, Integer> map = new HashMap<>();
        Map<String, Boolean> occurrence = new HashMap<>();
        List<String> strings = new ArrayList<>();
        int minSum = Integer.MAX_VALUE;
        int length = Math.max(list1.length, list2.length);
        for (int i = 0; i < length; i++) {
            if (i < list1.length) {
                map.merge(list1[i], i, Integer::sum);
                occurrence.merge(list1[i], false, (a, b) -> true);
            }
            if (i < list2.length) {
                map.merge(list2[i], i, Integer::sum);
                occurrence.merge(list2[i], false, (a, b) -> true);
            }
        }
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (occurrence.get(entry.getKey())) {
                int c = entry.getValue();
                if (c < minSum) {
                    minSum = c;
                    strings.clear();
                    strings.add(entry.getKey());
                } else if (c == minSum) {
                    strings.add(entry.getKey());
                }
            }
        }

        return strings.toArray(new String[0]);
    }
}
