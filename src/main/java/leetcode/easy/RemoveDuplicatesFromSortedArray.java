package leetcode.easy;

/**
 * 26. Remove Duplicates from Sorted Array
 *
 * @link https://leetcode.com/problems/remove-duplicates-from-sorted-array/description/
 * @tags Array, Two Pointer
 */
public class RemoveDuplicatesFromSortedArray {
    /**
     * 97,64%
     * Runtime: 1 ms
     * Memory Usage: 38.8 MB
     */
    public static int removeDuplicates(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int length = 1;
        int prev = nums[0];
        int index = 0;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != prev) {
                nums[++index] = nums[i];
                length++;
                prev = nums[i];
            }
        }
        return length;
    }
}
