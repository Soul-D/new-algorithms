package leetcode.easy;

/**
 * 485. Max Consecutive Ones
 *
 * @link https://leetcode.com/problems/max-consecutive-ones/
 * @tags Array.
 */
public class MaxConsecutiveOnes {
    /**
     * Runtime: 2 ms, faster than 99.73% of Java online submissions for Max Consecutive Ones.
     * Memory Usage: 38.8 MB, less than 100.00% of Java online submissions for Max Consecutive Ones.
     */
    public static int findMaxConsecutiveOnes(int[] nums) {
        int max = 0;
        int count = 0;
        for (int num : nums) {
            if (num == 0) {
                max = Math.max(count, max);
                count = 0;
            } else {
                count++;
            }
        }
        return Math.max(max, count);
    }
}
