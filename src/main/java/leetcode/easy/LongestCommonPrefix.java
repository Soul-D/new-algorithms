package leetcode.easy;

/**
 * 14. Longest Common Prefix
 *
 * @link https://leetcode.com/problems/longest-common-prefix/description/
 * @tags String
 */
public class LongestCommonPrefix {
    /**
     * 74,44%
     * Runtime: 1 ms
     * Memory Usage: 37.9 MB
     */
    public static String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) {
            return "";
        }
        StringBuilder pref = new StringBuilder();
        int length = length(strs);
        out:
        for (int i = 0; i < length; i++) {
            char curr = strs[0].charAt(i);
            for (int j = 1; j < strs.length; j++) {
                if (curr != strs[j].charAt(i)) {
                    break out;
                }
            }
            pref.append(curr);
        }
        return pref.toString();
    }

    private static int length(String[] strs) {
        int min = Integer.MAX_VALUE;
        for (String str : strs) {
            if (str.length() < min) {
                min = str.length();
            }
        }
        return min;
    }
}
