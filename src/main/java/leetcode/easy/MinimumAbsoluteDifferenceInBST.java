package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 530. Minimum Absolute Difference in BST
 *
 * @link https://leetcode.com/problems/minimum-absolute-difference-in-bst/
 * @tags Tree.
 */
public class MinimumAbsoluteDifferenceInBST {
    private int min = Integer.MAX_VALUE;
    private TreeNode prev;

    /**
     * Runtime: 1 ms, faster than 97.20% of Java online submissions for Minimum Absolute Difference in BST.
     * Memory Usage: 38.9 MB, less than 96.15% of Java online submissions for Minimum Absolute Difference in BST.
     */
    public int getMinimumDifference(TreeNode root) {
        inorder(root);
        return min;
    }

    private void inorder(TreeNode root) {
        if (root == null) {
            return;
        }
        inorder(root.left);
        if (prev != null) {
            min = Math.min(min, root.val - prev.val);
        }
        prev = root;
        inorder(root.right);
    }


}
