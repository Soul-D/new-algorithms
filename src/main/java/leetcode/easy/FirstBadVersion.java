package leetcode.easy;

/**
 * 278. First Bad Version
 *
 * @link https://leetcode.com/problems/first-bad-version/
 * @tags Binary Search.
 */
public class FirstBadVersion {
    /**
     * Runtime: 10 ms, faster than 99.81% of Java online submissions for First Bad Version.
     * Memory Usage: 32.9 MB, less than 100.00% of Java online submissions for First Bad Version.
     */
    public int firstBadVersion(int n) {
        int start = 1;
        int end = n;
        while (start < end) {
            int mid = start + (end - start) / 2;
            if (!isBadVersion(mid)) {
                start = mid + 1;
            } else {
                end = mid;
            }
        }
        return start;
    }

    private boolean isBadVersion(int version) {
        return version > 3;
    }
}
