package leetcode.easy;

/**
 * 189. Rotate Array
 *
 * @link https://leetcode.com/problems/rotate-array/
 * @tags Array.
 */
public class RotateArray {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Rotate Array.
     * Memory Usage: 37.7 MB, less than 100.00% of Java online submissions for Rotate Array.
     */
    public static void rotate(int[] nums, int k) {

        if (nums == null || nums.length < 2) {
            return;
        }

        k = k % nums.length;
        reverse(nums, 0, nums.length - k - 1);
        reverse(nums, nums.length - k, nums.length - 1);
        reverse(nums, 0, nums.length - 1);

    }

    private static void reverse(int[] nums, int i, int j) {
        int tmp;
        while (i < j) {
            tmp = nums[i];
            nums[i] = nums[j];
            nums[j] = tmp;
            i++;
            j--;
        }
    }
}
