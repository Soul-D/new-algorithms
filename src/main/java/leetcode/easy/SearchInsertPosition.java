package leetcode.easy;

/**
 * 35. Search Insert Position
 *
 * @link https://leetcode.com/problems/search-insert-position/description/
 * @tags Array, Binary Search.
 */
public class SearchInsertPosition {
    /**
     * Runtime: 0 ms
     * Memory Usage: 39.1 MB
     */
    public static int searchInsert(int[] nums, int target) {
        int start = 0;
        int end = nums.length - 1;
        while (start <= end) {
            int middle = (start + end) / 2;
            int num = nums[middle];
            if (num == target) {
                return middle;
            }
            if (num < target) {
                start = middle + 1;
            } else {
                end = middle - 1;
            }
        }

        return start;
    }
}
