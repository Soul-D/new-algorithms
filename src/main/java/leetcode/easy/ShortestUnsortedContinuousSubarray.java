package leetcode.easy;

import java.util.Arrays;

/**
 * 581. Shortest Unsorted Continuous Subarray
 *
 * @link https://leetcode.com/problems/shortest-unsorted-continuous-subarray/
 * @tags Array.
 */
public class ShortestUnsortedContinuousSubarray {
    /**
     * Runtime: 2 ms, faster than 90.30% of Java online submissions for Shortest Unsorted Continuous Subarray.
     * Memory Usage: 39.8 MB, less than 100.00% of Java online submissions for Shortest Unsorted Continuous Subarray.
     * Not mine.
     */
    public static int findUnsortedSubarrayFaster(int[] nums) {
        int first = 0;
        int last = -1;

        int max = Integer.MIN_VALUE;
        for (int left = 0; left < nums.length; left++) {
            max = Math.max(max, nums[left]);
            if (nums[left] != max) {
                last = left;
            }
        }

        int min = Integer.MAX_VALUE;
        for (int right = nums.length - 1; right >= 0; right--) {
            min = Math.min(min, nums[right]);
            if (nums[right] != min) {
                first = right;
            }
        }

        return (last - first + 1);
    }

    /**
     * Runtime: 8 ms, faster than 42.02% of Java online submissions for Shortest Unsorted Continuous Subarray.
     * Memory Usage: 40 MB, less than 100.00% of Java online submissions for Shortest Unsorted Continuous Subarray.
     */
    public static int findUnsortedSubarray(int[] nums) {
        int[] copy = Arrays.copyOf(nums, nums.length);
        Arrays.sort(copy);
        int start = -1;
        int end = -1;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != copy[i]) {
                if (start == -1) {
                    start = i;
                }
                end = -1;
            } else {
                if (end == -1) {
                    end = i;
                }
            }
        }
        if (start == -1) {
            return 0;
        } else if (end == -1) {
            return nums.length - start;
        } else {
            return end - start;
        }
    }
}
