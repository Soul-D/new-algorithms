package leetcode.easy;

/**
 * 389. Find the Difference
 *
 * @link https://leetcode.com/problems/find-the-difference/
 * @tags Hash Table, Bit Manipulation.
 */
public class FindTheDifference {
    /**
     * Runtime: 1 ms, faster than 98.51% of Java online submissions for Find the Difference.
     * Memory Usage: 35.7 MB, less than 100.00% of Java online submissions for Find the Difference.
     */
    public static char findTheDifferenceBit(String s, String t) {
        int xor = 0;
        for (int i = 0; i < s.length(); i++) {
            xor ^= (s.charAt(i) - 'a');
            xor ^= (t.charAt(i) - 'a');
        }
        xor ^= (t.charAt(t.length() - 1) - 'a');
        return (char) (xor + 'a');
    }

    /**
     * Runtime: 1 ms, faster than 98.51% of Java online submissions for Find the Difference.
     * Memory Usage: 35.9 MB, less than 100.00% of Java online submissions for Find the Difference.
     */
    public char findTheDifference(String s, String t) {
        int[] counts = new int[26];
        for (int i = 0; i < s.length(); i++) {
            counts[s.charAt(i) - 'a']++;
            counts[t.charAt(i) - 'a']--;
        }
        counts[t.charAt(t.length() - 1) - 'a']--;
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] != 0) {
                return (char) (i + 'a');
            }
        }
        return 'a';
    }
}
