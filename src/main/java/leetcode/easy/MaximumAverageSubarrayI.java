package leetcode.easy;

/**
 * 643. Maximum Average Subarray I
 *
 * @link https://leetcode.com/problems/maximum-average-subarray-i/
 * @tags Array.
 */
public class MaximumAverageSubarrayI {
    /**
     * Runtime: 5 ms, faster than 49.36% of Java online submissions for Maximum Average Subarray I.
     * Memory Usage: 50.2 MB, less than 7.69% of Java online submissions for Maximum Average Subarray I.
     */
    public static double findMaxAverage(int[] nums, int k) {
        int curr = 0;
        int first = 0;
        for (int i = 0; i < k; i++) {
            curr += nums[i];
        }
        double max = curr;
        for (int i = k; i < nums.length; i++) {
            curr = curr - nums[first++] + nums[i];
            max = Math.max(curr, max);
        }
        return max / k;
    }
}
