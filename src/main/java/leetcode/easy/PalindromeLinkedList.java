package leetcode.easy;

import leetcode.model.ListNode;

/**
 * 234. Palindrome Linked List
 *
 * @link https://leetcode.com/problems/palindrome-linked-list/
 * @tags Linked List, Two Pointers.
 */
public class PalindromeLinkedList {
    /**
     * Runtime: 1 ms, faster than 99.46% of Java online submissions for Palindrome Linked List.
     * Memory Usage: 41.5 MB, less than 41.46% of Java online submissions for Palindrome Linked List.
     */
    public static boolean isPalindrome(ListNode head) {
        ListNode fast = head, slow = head;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        if (fast != null) {
            slow = slow.next;
        }
        slow = reverse(slow);
        fast = head;

        while (slow != null) {
            if (fast.val != slow.val) {
                return false;
            }
            fast = fast.next;
            slow = slow.next;
        }
        return true;
    }

    private static ListNode reverse(ListNode head) {
        ListNode prev = null;
        while (head != null) {
            ListNode next = head.next;
            head.next = prev;
            prev = head;
            head = next;
        }
        return prev;
    }
}
