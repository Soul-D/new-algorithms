package leetcode.easy;

/**
 * 541. Reverse String II
 *
 * @link https://leetcode.com/problems/reverse-string-ii/
 * @tags String.
 */
public class ReverseStringII {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Reverse String II.
     * Memory Usage: 35.9 MB, less than 100.00% of Java online submissions for Reverse String II.
     */
    public static String reverseStrFaster(String s, int k) {
        char[] chars = s.toCharArray();
        int i = 0;
        int n = chars.length;
        while (i < n) {
            int j = Math.min(i + k - 1, n - 1);
            reverseString(chars, i, j);
            i += 2 * k;
        }
        return String.valueOf(chars);
    }

    private static void reverseString(char[] chars, int start, int end) {
        while (start < end) {
            char temp = chars[start];
            chars[start] = chars[end];
            chars[end] = temp;
            start++;
            end--;
        }
    }

    /**
     * Runtime: 1 ms, faster than 70.49% of Java online submissions for Reverse String II.
     * Memory Usage: 37 MB, less than 100.00% of Java online submissions for Reverse String II.
     */
    public static String reverseStr(String s, int k) {
        char[] chars = s.toCharArray();
        for (int i = 0; i < s.length(); ) {
            int bound = Math.min(i + k, s.length());
            int start = i;
            int end = bound - 1;
            while (start < end) {
                char temp = chars[start];
                chars[start] = chars[end];
                chars[end] = temp;
                start++;
                end--;
            }
            i = bound + k;
        }

        return new String(chars);
    }
}
