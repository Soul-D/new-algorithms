package leetcode.easy;

/**
 * 168. Excel Sheet Column Title
 *
 * @link https://leetcode.com/problems/excel-sheet-column-title/
 * @tags Math.
 */
public class ExcelSheetColumnTitle {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Excel Sheet Column Title.
     * Memory Usage: 33.9 MB, less than 100.00% of Java online submissions for Excel Sheet Column Title.
     */
    public static String convertToTitle(int n) {
        StringBuilder result = new StringBuilder();
        while (n > 0) {
            n--;
            result.append((char) ('A' + n % 26));
            n = n / 26;
        }

        return result.reverse().toString();
    }
}
