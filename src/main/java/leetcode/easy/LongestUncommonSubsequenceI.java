package leetcode.easy;

/**
 * 521. Longest Uncommon Subsequence I
 *
 * @link https://leetcode.com/problems/longest-uncommon-subsequence-i/
 * @tags String.
 */
public class LongestUncommonSubsequenceI {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Longest Uncommon Subsequence I .
     * Memory Usage: 34 MB, less than 100.00% of Java online submissions for Longest Uncommon Subsequence I .
     */
    public int findLUSlength(String a, String b) {
        if (a.length() != b.length()) {
            return Math.max(a.length(), b.length());
        }
        if (!a.equals(b)) {
            return a.length();
        }
        return -1;
    }
}
