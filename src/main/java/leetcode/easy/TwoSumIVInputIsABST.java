package leetcode.easy;

import leetcode.model.TreeNode;

import java.util.HashSet;
import java.util.Set;

/**
 * 653. Two Sum IV - Input is a BST
 *
 * @link https://leetcode.com/problems/two-sum-iv-input-is-a-bst/
 * @tags Tree.
 */
public class TwoSumIVInputIsABST {
    /**
     * Runtime: 4 ms, faster than 58.86% of Java online submissions for Two Sum IV - Input is a BST.
     * Memory Usage: 51.1 MB, less than 5.36% of Java online submissions for Two Sum IV - Input is a BST.
     */
    public static boolean findTarget(TreeNode root, int k) {
        Set<Integer> set = new HashSet<>();
        return find(root, set, k);
    }

    public static boolean find(TreeNode root, Set<Integer> set, int k) {
        if (root == null) {
            return false;
        }
        if (set.contains(k - root.val)) {
            return true;
        } else {
            set.add(root.val);
        }
        return find(root.left, set, k) || find(root.right, set, k);
    }
}
