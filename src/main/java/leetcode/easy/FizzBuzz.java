package leetcode.easy;

import java.util.ArrayList;
import java.util.List;

/**
 * 412. Fizz Buzz
 *
 * @link https://leetcode.com/problems/fizz-buzz/
 */
public class FizzBuzz {
    /**
     * Runtime: 1 ms, faster than 99.81% of Java online submissions for Fizz Buzz.
     * Memory Usage: 37.2 MB, less than 100.00% of Java online submissions for Fizz Buzz.
     */
    public static List<String> fizzBuzz(int n) {
        List<String> result = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            if (i % 15 == 0) {
                result.add("FizzBuzz");
            } else if (i % 5 == 0) {
                result.add("Buzz");
            } else if (i % 3 == 0) {
                result.add("Fizz");
            } else {
                result.add(String.valueOf(i));
            }
        }
        return result;
    }
}
