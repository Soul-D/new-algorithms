package leetcode.easy;

/**
 * 476. Number Complement
 *
 * @link https://leetcode.com/problems/number-complement/
 * @tags Bit Manipulation.
 */
public class NumberComplement {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Number Complement.
     * Memory Usage: 33.2 MB, less than 8.70% of Java online submissions for Number Complement.
     */
    public static int findComplement(int num) {
        int n = 0;
        while (n < num) {
            n = (n << 1) | 1;
        }
        return n - num;
    }
}
