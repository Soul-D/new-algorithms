package leetcode.easy;

/**
 * 258. Add Digits
 *
 * @link https://leetcode.com/problems/add-digits/
 * @tags Math.
 */
public class AddDigits {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Add Digits.
     * Memory Usage: 33.5 MB, less than 6.67% of Java online submissions for Add Digits.
     *
     * @see https://en.wikipedia.org/wiki/Digital_root#Congruence_formula
     */
    public static int addDigitsMath(int num) {
        return 1 + (num - 1) % 9;
    }

    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Add Digits.
     * Memory Usage: 33.6 MB, less than 6.67% of Java online submissions for Add Digits.
     */
    public static int addDigits(int num) {
        while (num / 10 != 0) {
            num = flatten(num);
        }
        return num;
    }

    private static int flatten(int num) {
        int result = 0;
        while (num > 0) {
            result += (num % 10);
            num = num / 10;
        }
        return result;
    }
}
