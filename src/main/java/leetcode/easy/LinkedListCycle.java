package leetcode.easy;

import leetcode.model.ListNode;

/**
 * 141. Linked List Cycle
 *
 * @link https://leetcode.com/problems/linked-list-cycle/
 * @tags Linked List, Two Pointers.
 */
public class LinkedListCycle {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Linked List Cycle.
     * Memory Usage: 36.9 MB, less than 100.00% of Java online submissions for Linked List Cycle.
     */
    public boolean hasCycle(ListNode head) {
        if (head == null || head.next == null) {
            return false;
        }
        ListNode slow = head;
        ListNode fast = head.next;
        while (fast != null && fast.next != null) {
            if (slow == fast) {
                return true;
            }
            slow = slow.next;
            fast = fast.next.next;
        }
        return false;
    }
}
