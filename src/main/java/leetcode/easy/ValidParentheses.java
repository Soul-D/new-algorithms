package leetcode.easy;

import java.util.Stack;

/**
 * 20. Valid Parentheses
 *
 * @link https://leetcode.com/problems/valid-parentheses/description/
 * @tags String, Stack
 */
public class ValidParentheses {
    /**
     * 98,82%
     * Runtime: 1 ms
     * Memory Usage: 34.4 MB
     */
    public static boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch == '(' || ch == '{' || ch == '[') {
                stack.push(ch);
                continue;
            }
            if (ch == ')' || ch == '}' || ch == ']') {
                if (stack.isEmpty()) {
                    return false;
                }
                Character top = stack.pop();
                if ((ch == ')' && top != '(')
                        || (ch == '}' && top != '{')
                        || (ch == ']' && top != '[')) {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}
