package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 572. Subtree of Another Tree
 *
 * @link https://leetcode.com/problems/subtree-of-another-tree/
 * @tags Tree.
 */
public class SubtreeOfAnotherTree {
    /**
     * Runtime: 1 ms, faster than 99.88% of Java online submissions for Subtree of Another Tree.
     * Memory Usage: 40 MB, less than 97.78% of Java online submissions for Subtree of Another Tree.
     */
    public boolean isSubtreeFaster(TreeNode s, TreeNode t) {
        if (s == null && t == null) return true;
        if (s == null || t == null) return false;

        return ((s.val == t.val) && isSubtreeFaster(s.left, t.left) && isSubtreeFaster(t.right, s.right))
                || isSubtreeFaster(s.left, t) || isSubtreeFaster(s.right, t);
    }

    /**
     * Runtime: 6 ms, faster than 83.23% of Java online submissions for Subtree of Another Tree.
     * Memory Usage: 40 MB, less than 97.78% of Java online submissions for Subtree of Another Tree.
     */
    public boolean isSubtree(TreeNode s, TreeNode t) {
        if (s == null && t != null) {
            return false;
        }
        if (isEqual(s, t)) {
            return true;
        }
        return isSubtree(s.left, t) || isSubtree(s.right, t);
    }

    private boolean isEqual(TreeNode s, TreeNode t) {
        if (s == null && t == null) {
            return true;
        }
        if (s == null || t == null) {
            return false;
        }
        return s.val == t.val && isEqual(s.left, t.left) && isEqual(s.right, t.right);
    }
}
