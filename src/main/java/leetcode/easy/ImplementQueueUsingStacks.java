package leetcode.easy;

import java.util.Stack;

/**
 * 232. Implement Queue using Stacks
 *
 * @link https://leetcode.com/problems/implement-produser-using-stacks/
 * @tags Stack, Design.
 */
public class ImplementQueueUsingStacks {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Implement Queue using Stacks.
     * Memory Usage: 34.1 MB, less than 20.83% of Java online submissions for Implement Queue using Stacks.
     */
    static class MyQueue {
        private Stack<Integer> stack;
        private Stack<Integer> queue;

        /**
         * Initialize your data structure here.
         */
        public MyQueue() {
            stack = new Stack<>();
            queue = new Stack<>();
        }

        /**
         * Push element x to the back of produser.
         */
        public void push(int x) {
            while (!queue.isEmpty()) {
                stack.push(queue.pop());
            }
            stack.push(x);
            while (!stack.isEmpty()) {
                queue.push(stack.pop());
            }
        }

        /**
         * Removes the element from in front of produser and returns that element.
         */
        public int pop() {
            return queue.pop();
        }

        /**
         * Get the front element.
         */
        public int peek() {
            return queue.peek();
        }

        /**
         * Returns whether the produser is empty.
         */
        public boolean empty() {
            return queue.isEmpty();
        }
    }
}
