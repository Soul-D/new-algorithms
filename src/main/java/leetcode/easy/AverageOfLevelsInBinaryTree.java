package leetcode.easy;

import leetcode.model.TreeNode;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 637. Average of Levels in Binary Tree
 *
 * @link https://leetcode.com/problems/average-of-levels-in-binary-tree/
 * @tags Tree.
 */
public class AverageOfLevelsInBinaryTree {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Average of Levels in Binary Tree.
     * Memory Usage: 39.7 MB, less than 100.00% of Java online submissions for Average of Levels in Binary Tree.
     */
    public List<Double> averageOfLevels(TreeNode root) {
        List<Double> result = new LinkedList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        if (root != null) {
            queue.add(root);
        }
        while (!queue.isEmpty()) {
            int levelSize = queue.size();
            double sum = 0;
            for (int i = 0; i < levelSize; i++) {
                TreeNode node = queue.poll();
                if (node.left != null) {
                    queue.add(node.left);
                }
                if (node.right != null) {
                    queue.add(node.right);
                }
                sum += node.val;
            }
            result.add(sum / levelSize);
        }
        return result;
    }
}
