package leetcode.easy;

/**
 * 204. Count Primes
 *
 * @link https://leetcode.com/problems/count-primes/
 * @tags Hash Table, Math.
 */
public class CountPrimes {
    /**
     * Runtime: 12 ms, faster than 70.98% of Java online submissions for Count Primes.
     * Memory Usage: 34.2 MB, less than 9.43% of Java online submissions for Count Primes.
     */
    public static int countPrimesFaster(int n) {
        boolean[] notPrime = new boolean[n];
        int count = 0;
        for (int i = 2; i < n; i++) {
            if (!notPrime[i]) {
                count++;
                for (int j = 2; i * j < n; j++) {
                    notPrime[i * j] = true;
                }
            }
        }

        return count;
    }

    /**
     * Runtime: 291 ms, faster than 18.92% of Java online submissions for Count Primes.
     * Memory Usage: 33.2 MB, less than 20.75% of Java online submissions for Count Primes.
     */
    public static int countPrimes(int n) {
        if (n < 3) {
            return 0;
        }
        int count = 1;
        for (int i = 3; i < n; i++) {
            if (isPrime(i)) {
                count++;
            }
        }
        return count;
    }

    private static boolean isPrime(int n) {
        if (n % 2 == 0) {
            return false;
        }
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
