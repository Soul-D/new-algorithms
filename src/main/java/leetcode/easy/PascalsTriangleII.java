package leetcode.easy;

import java.util.ArrayList;
import java.util.List;

/**
 * 119. Pascal's Triangle II
 *
 * @link https://leetcode.com/problems/pascals-triangle-ii/
 * @tags Array.
 */
public class PascalsTriangleII {
    /**
     * Runtime: 1 ms, faster than 90.13% of Java online submissions for Pascal's Triangle II.
     * Memory Usage: 33.7 MB, less than 6.17% of Java online submissions for Pascal's Triangle II.
     */
    public static List<Integer> getRow(int rowIndex) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i <= rowIndex; i++) {
            result.add(0, 1);
            for (int j = 1; j < result.size() - 1; j++) {
                result.set(j, result.get(j) + result.get(j + 1));
            }
        }

        return result;
    }
}
