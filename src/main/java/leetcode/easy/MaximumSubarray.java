package leetcode.easy;

/**
 * 53. Maximum Subarray
 *
 * @link https://leetcode.com/problems/maximum-subarray/description/
 * @tags Array, Divide and Conquer, Dynamic Programming.
 */
public class MaximumSubarray {
    /**
     * 86,14%
     * Runtime: 1 ms
     * Memory Usage: 37.1 MB
     */
    public static int maxSubArray(int[] nums) {
        int result = nums[0], sum = 0;
        for (int i = 0; i < nums.length; ++i) {
            sum += nums[i];
            result = Math.max(result, sum);
            sum = Math.max(sum, 0);
        }
        return result;
    }
}
