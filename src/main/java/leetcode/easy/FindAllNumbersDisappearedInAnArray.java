package leetcode.easy;

import java.util.ArrayList;
import java.util.List;

/**
 * 448. Find All Numbers Disappeared in an Array
 *
 * @link https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/
 * @tags Array.
 */
public class FindAllNumbersDisappearedInAnArray {
    /**
     * Runtime: 6 ms, faster than 80.89% of Java online submissions for Find All Numbers Disappeared in an Array.
     * Memory Usage: 47 MB, less than 94.34% of Java online submissions for Find All Numbers Disappeared in an Array.
     */
    public static List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> res = new ArrayList<>();
        int n = nums.length;
        for (int i = 0; i < n; i++) {
            nums[(nums[i] - 1) % n] += n;
        }
        for (int i = 0; i < n; i++) {
            if (nums[i] <= n) {
                res.add(i + 1);
            }
        }
        return res;
    }
}
