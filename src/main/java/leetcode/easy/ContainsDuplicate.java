package leetcode.easy;

import java.util.HashSet;
import java.util.Set;

/**
 * 217. Contains Duplicate
 *
 * @link https://leetcode.com/problems/contains-duplicate/
 * @tags Array, Hash Table.
 */
public class ContainsDuplicate {
    /**
     * Runtime: 6 ms, faster than 82.68% of Java online submissions for Contains Duplicate.
     * Memory Usage: 44.2 MB, less than 61.21% of Java online submissions for Contains Duplicate.
     */
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            if (!set.add(num)) {
                return true;
            }
        }
        return false;
    }
}
