package leetcode.easy;

/**
 * 299. Bulls and Cows
 *
 * @link https://leetcode.com/problems/bulls-and-cows/
 * @tags Hash Table.
 */
public class BullsAndCows {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Bulls and Cows.
     * Memory Usage: 37.5 MB, less than 97.37% of Java online submissions for Bulls and Cows.
     */
    public String getHintFast(String secret, String guess) {
        int bulls = 0;
        int cows = 0;

        int[] nums = new int[10];
        for (int i = 0; i < guess.length(); i++) {
            if (secret.charAt(i) == guess.charAt(i)) {
                bulls++;
                continue;
            }
            if (nums[secret.charAt(i) - '0']++ < 0) {
                cows++;
            }
            if (nums[guess.charAt(i) - '0']-- > 0) {
                cows++;
            }
        }

        return bulls + "A" + cows + "B";
    }

    /**
     * Runtime: 2 ms, faster than 56.62% of Java online submissions for Bulls and Cows.
     * Memory Usage: 37.8 MB, less than 68.42% of Java online submissions for Bulls and Cows.
     */
    public String getHint(String secret, String guess) {
        int[] digits = new int[10];
        boolean[] bulls = new boolean[secret.length()];
        int bullsCount = 0;
        int cowsCount = 0;
        for (int i = 0; i < secret.length(); i++) {
            if (secret.charAt(i) == guess.charAt(i)) {
                bullsCount++;
                bulls[i] = true;
            } else {
                digits[Character.getNumericValue(secret.charAt(i))]++;
            }
        }
        for (int i = 0; i < guess.length(); i++) {
            if (bulls[i]) {
                continue;
            }
            if (digits[Character.getNumericValue(guess.charAt(i))] != 0) {
                digits[Character.getNumericValue(guess.charAt(i))]--;
                cowsCount++;
            }
        }
        return bullsCount + "A" + cowsCount + "B";
    }
}
