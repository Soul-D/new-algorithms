package leetcode.easy;

/**
 * 509. Fibonacci Number
 *
 * @link https://leetcode.com/problems/fibonacci-number/submissions/
 * @tags Array.
 */
public class FibonacciNumber {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Fibonacci Number.
     * Memory Usage: 33 MB, less than 5.51% of Java online submissions for Fibonacci Number.
     */
    public static int fibArr(int n) {
        int[] arr = new int[]{0, 1, 1};
        for (int i = 3; i <= n; i++) {
            arr[i % 3] = arr[(i + 1) % 3] + arr[(i + 2) % 3];
        }

        return arr[n % 3];
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Fibonacci Number.
     * Memory Usage: 33 MB, less than 5.51% of Java online submissions for Fibonacci Number.
     */
    public int fib(int n) {
        int first = 0;
        int second = 1;
        if (n == 0) {
            return first;
        }
        if (n == 1) {
            return second;
        }
        while (n > 2) {
            int temp = second + first;
            first = second;
            second = temp;
            n--;
        }
        return first + second;
    }
}
