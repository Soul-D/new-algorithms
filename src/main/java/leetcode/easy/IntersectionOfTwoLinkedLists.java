package leetcode.easy;

import leetcode.model.ListNode;

/**
 * 160. Intersection of Two Linked Lists
 *
 * @link https://leetcode.com/problems/intersection-of-two-linked-lists/
 * @tags Linked List,
 */
public class IntersectionOfTwoLinkedLists {
    /**
     * Runtime: 1 ms, faster than 99.08% of Java online submissions for Intersection of Two Linked Lists.
     * Memory Usage: 37.5 MB, less than 55.71% of Java online submissions for Intersection of Two Linked Lists.
     */
    public static ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        ListNode result = null;
        ListNode a = headA;
        ListNode b = headB;
        while (a != null && b != null) {
            if (a == b) {
                result = a;
                break;
            }
            a = a.next;
            b = b.next;
            if (a == null && b != null) {
                a = headB;
            }
            if (b == null && a != null) {
                b = headA;
            }
        }

        return result;
    }
}
