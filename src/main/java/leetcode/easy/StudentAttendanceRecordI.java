package leetcode.easy;

/**
 * 551. Student Attendance Record I
 *
 * @link https://leetcode.com/problems/student-attendance-record-i/
 * @tags String.
 */
public class StudentAttendanceRecordI {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Student Attendance Record I.
     * Memory Usage: 34.3 MB, less than 100.00% of Java online submissions for Student Attendance Record I.
     */
    public static boolean checkRecord(String s) {
        int as = s.charAt(0) == 'A' ? 1 : 0;
        int ls = s.charAt(0) == 'L' ? 1 : 0;
        boolean isPrevL = s.charAt(0) == 'L';

        for (int i = 1; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch == 'L') {
                ls++;
                if (isPrevL && ls > 2) {
                    return false;
                } else {
                    isPrevL = true;
                }
                continue;
            } else {
                isPrevL = false;
                ls = 0;
            }

            if (ch == 'A') {
                as++;
            }
            if (as > 1) {
                return false;
            }
        }
        return true;
    }
}
