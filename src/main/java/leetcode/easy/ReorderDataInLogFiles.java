package leetcode.easy;

import java.util.Arrays;
import java.util.Comparator;

/**
 * 937. Reorder Data in Log Files
 *
 * @link https://leetcode.com/problems/reorder-data-in-log-files/description/
 * @tags String.
 */
public class ReorderDataInLogFiles {
    /**
     * Runtime: 2 ms, faster than 99.69% of Java online submissions for Reorder Data in Log Files.
     * Memory Usage: 38.5 MB, less than 73.53% of Java online submissions for Reorder Data in Log Files.
     */
    public static String[] reorderLogFilesFaster(String[] logs) {
        Comparator<String> c = (o1, o2) -> {
            int o1Ind = o1.indexOf(' ') + 1;
            int o2Ind = o2.indexOf(' ') + 1;
            int candidate = (o1.substring(o1Ind)).compareTo(o2.substring(o2Ind));
            if (candidate != 0) {
                return candidate;
            } else {
                return (o1.substring(0, o1Ind - 2)).compareTo(o2.substring(0, o2Ind - 2));
            }

        };


        String[] res = new String[logs.length];
        int digs = 0;
        for (int i = logs.length - 1; i >= 0; i--) {
            if (isDig(logs[i])) {
                res[logs.length - 1 - digs++] = logs[i];
            }
        }

        for (int i = 0, j = 0; i < logs.length; i++) {
            if (!isDig(logs[i])) {
                res[j++] = logs[i];
            }
        }

        Arrays.sort(res, 0, res.length - digs, c);

        return res;
    }

    private static boolean isDig(String s) {
        int o1Ind = s.indexOf(' ') + 1;
        return Character.isDigit(s.charAt(o1Ind));
    }

    /**
     * Runtime: 3 ms, faster than 82.81% of Java online submissions for Reorder Data in Log Files.
     * Memory Usage: 38.8 MB, less than 70.59% of Java online submissions for Reorder Data in Log Files.
     */
    public static String[] reorderLogFiles(String[] logs) {
        Comparator<String> c = (o1, o2) -> {
            int o1Ind = o1.indexOf(' ') + 1;
            int o2Ind = o2.indexOf(' ') + 1;
            boolean o1Dig = Character.isDigit(o1.charAt(o1Ind));
            boolean o2Dig = Character.isDigit(o2.charAt(o2Ind));
            if (o1Dig && o2Dig) {
                return 0;
            } else if (!o1Dig && o2Dig) {
                return -1;
            } else if (o1Dig && !o2Dig) {
                return 1;
            } else {
                int candidate = (o1.substring(o1Ind)).compareTo(o2.substring(o2Ind));
                if (candidate != 0) {
                    return candidate;
                } else {
                    return (o1.substring(0, o1Ind - 2)).compareTo(o2.substring(0, o2Ind - 2));
                }
            }
        };

        Arrays.sort(logs, c);
        return logs;
    }
}
