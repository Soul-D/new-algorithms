package leetcode.easy;

/**
 * 69. Sqrt(x)
 *
 * @link https://leetcode.com/problems/sqrtx/description/
 * @tags Math, Binary Search.
 */
public class SqrtX {
    /**
     * 100%
     * Runtime: 1 ms
     * Memory Usage: 33.7 MB
     */
    public static int mySqrtBinary(int x) {
        if (x == 0 || x == 1) {
            return x;
        }
        int low = 0;
        int high = x / 2;
        while (low <= high) {
            int middle = (low + high) / 2;
            long candidate = middle * (long) middle;
            if (candidate == x) {
                return middle;
            }
            if (candidate > x) {
                high = middle - 1;
            } else {
                low = middle + 1;
            }
        }
        return low - 1;
    }

    /**
     * 8,44%
     * Runtime: 16 ms
     * Memory Usage: 33.7 MB
     */
    public static int mySqrt(int x) {
        if (x == 0 || x == 1) {
            return x;
        }
        int prev = 1;

        for (int i = 2; i <= x / 2; i++) {
            long candidate = i * (long) i;
            if (candidate == x) {
                return i;
            }
            if (x < candidate) {
                return prev;
            } else {
                prev = i;
            }
        }
        return prev;
    }
}
