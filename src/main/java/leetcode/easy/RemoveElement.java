package leetcode.easy;

/**
 * 27. Remove Element
 *
 * @link https://leetcode.com/problems/remove-element/description/
 * @tags Array, Two pointers.
 */
public class RemoveElement {
    /**
     * 100% by memory. By time: sorry. We do not have enough accepted submissions to show distribution chart.
     * Runtime: 0 ms
     * Memory Usage: 36 MB
     */
    public static int removeElement(int[] nums, int val) {
        int length = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[length++] = nums[i];
            }
        }
        return length;
    }
}
