package leetcode.easy;

import java.util.Arrays;

/**
 * 349. Intersection of Two Arrays
 *
 * @link https://leetcode.com/problems/intersection-of-two-arrays/
 * @tags Hash Table, Binary Search, Sort, Two Pointers.
 */
public class IntersectionOfTwoArrays {
    /**
     * Runtime: 1 ms, faster than 99.70% of Java online submissions for Intersection of Two Arrays.
     * Memory Usage: 37.6 MB, less than 58.11% of Java online submissions for Intersection of Two Arrays.
     */
    public static int[] intersection(int[] nums1, int[] nums2) {
        int minLength = nums1.length < nums2.length ? nums1.length : nums2.length;

        Arrays.sort(nums1);
        Arrays.sort(nums2);
        int count = 0;
        int[] storage = new int[minLength];
        for (int i = 0; i < nums1.length; i++) {
            if ((i == 0 || nums1[i] != nums1[i - 1])
                    && Arrays.binarySearch(nums2, nums1[i]) >= 0) {
                storage[count++] = nums1[i];
            }
        }

        return Arrays.copyOf(storage, count);
    }
}
