package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 108. Convert Sorted Array to Binary Search Tree
 *
 * @link https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/
 * @tags Tree, Depth-first search.
 */
public class ConvertSortedArrayToBinarySearchTree {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Convert Sorted Array to Binary Search Tree.
     * Memory Usage: 37.8 MB, less than 43.30% of Java online submissions for Convert Sorted Array to Binary Search Tree.
     */
    public static TreeNode sortedArrayToBST(int[] nums) {
        if (nums == null) {
            return null;
        }
        return build(nums, 0, nums.length - 1);
    }

    private static TreeNode build(int[] arr, int start, int end) {
        if (start > end) {
            return null;
        }

        int middle = (start + end) / 2;
        TreeNode node = new TreeNode(arr[middle]);
        node.left = build(arr, start, middle - 1);
        node.right = build(arr, middle + 1, end);
        return node;
    }
}
