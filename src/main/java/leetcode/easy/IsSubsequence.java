package leetcode.easy;

/**
 * 392. Is Subsequence
 *
 * @link https://leetcode.com/problems/is-subsequence/
 * @tags Binary Search, Dynamic Programming, Greedy.
 */
public class IsSubsequence {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Is Subsequence.
     * Memory Usage: 49.2 MB, less than 100.00% of Java online submissions for Is Subsequence.
     */
    public static boolean isSubsequenceFast(String s, String t) {
        if (t.length() < s.length()) {
            return false;
        }
        int prev = 0;
        for (int i = 0; i < s.length(); i++) {
            prev = t.indexOf(s.charAt(i), prev);
            if (prev == -1) {
                return false;
            }
            prev++;
        }

        return true;
    }

    /**
     * Runtime: 20 ms, faster than 27.31% of Java online submissions for Is Subsequence.
     * Memory Usage: 47.9 MB, less than 100.00% of Java online submissions for Is Subsequence.
     */
    public static boolean isSubsequence(String s, String t) {
        if (s.isEmpty()) {
            return true;
        }
        int currIndex = 0;
        for (int i = 0; i < t.length(); i++) {
            if (t.charAt(i) == s.charAt(currIndex)) {
                currIndex++;
                if (currIndex == s.length()) {
                    return true;
                }
            }
        }
        return false;
    }
}
