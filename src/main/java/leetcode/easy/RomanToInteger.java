package leetcode.easy;

/**
 * 13. Roman to Integer
 *
 * @link https://leetcode.com/problems/roman-to-integer/description/
 * @tags Math, String
 */
public class RomanToInteger {
    /**
     * 100%
     * Runtime: 3 ms
     * Memory Usage: 36.1 MB
     */
    public static int romanToInt(String s) {
        int result = 0;
        for (int i = 0; i < s.length(); i++) {
            char curr = s.charAt(i);
            switch (curr) {
                case 'I':
                    if (i == s.length() - 1 || (s.charAt(i + 1) != 'V' && s.charAt(i + 1) != 'X')) {
                        result += 1;
                    } else if (s.charAt(i + 1) == 'V') {
                        result += 4;
                        i++;
                    } else {
                        result += 9;
                        i++;
                    }
                    break;
                case 'X':
                    if (i == s.length() - 1 || (s.charAt(i + 1) != 'L' && s.charAt(i + 1) != 'C')) {
                        result += 10;
                    } else if (s.charAt(i + 1) == 'L') {
                        result += 40;
                        i++;
                    } else {
                        result += 90;
                        i++;
                    }
                    break;
                case 'C':
                    if (i == s.length() - 1 || (s.charAt(i + 1) != 'D' && s.charAt(i + 1) != 'M')) {
                        result += 100;
                    } else if (s.charAt(i + 1) == 'D') {
                        result += 400;
                        i++;
                    } else {
                        result += 900;
                        i++;
                    }
                    break;
                case 'L':
                    result += 50;
                    break;
                case 'V':
                    result += 5;
                    break;
                case 'D':
                    result += 500;
                    break;
                default:
                    result += 1000;
                    break;
            }
        }
        return result;
    }
}
