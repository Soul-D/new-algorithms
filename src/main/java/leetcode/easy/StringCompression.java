package leetcode.easy;

/**
 * 443. String Compression
 *
 * @link https://leetcode.com/problems/string-compression/
 * @tags String.
 */
public class StringCompression {
    /**
     * Runtime: 1 ms, faster than 97.10% of Java online submissions for String Compression.
     * Memory Usage: 36.3 MB, less than 100.00% of Java online submissions for String Compression.
     */
    public static int compress(char[] chars) {
        int charStart = 0;
        int cursor = 0;
        for (int i = 1; i <= chars.length; i++) {
            if (i == chars.length || chars[i] != chars[i - 1]) {
                chars[cursor++] = chars[i - 1];
                int count = i - charStart;
                if (count != 1) {
                    String str = String.valueOf(count);
                    for (int j = 0; j < str.length(); j++) {
                        chars[cursor++] = str.charAt(j);
                    }
                }
                charStart = i;
            }
            if (i == chars.length) {
                break;
            }
        }
        return cursor;
    }
}
