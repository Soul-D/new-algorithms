package leetcode.easy;

/**
 * 70. Climbing Stairs
 *
 * @link https://leetcode.com/problems/climbing-stairs/description/
 * @tags Dynamic Programming.
 */
public class ClimbingStairs {
    /**
     * Runtime: 0 ms
     * Memory Usage: 32.8 MB
     */
    public static int climbStairs(int n) {
        int[] cache = new int[2];
        cache[0] = 1;
        cache[1] = 2;

        for (int i = 2; i < n; i++) {
            cache[i % 2] = cache[0] + cache[1];
        }

        return cache[(n - 1) % 2];
    }

    // time limit exceeded
    public static int climbStairs1(int n) {
        if (n == 1) {
            return 1;
        }
        if (n == 2) {
            return 2;
        }
        return climbStairs(n - 1) + climbStairs(n - 2);
    }
}
