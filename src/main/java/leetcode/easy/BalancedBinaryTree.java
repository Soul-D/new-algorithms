package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 110. Balanced Binary Tree
 *
 * @link https://leetcode.com/problems/balanced-binary-tree/
 * @tags Tree, Depth-first search.
 */
public class BalancedBinaryTree {
    /**
     * 99,77%.
     * Runtime: 1 ms
     * Memory Usage: 36.8 MB
     */
    public static boolean isBalanced(TreeNode root) {
        if (root == null) {
            return true;
        }

        int left = depth(root.left);
        int right = depth(root.right);

        return Math.abs(left - right) <= 1
                && isBalanced(root.left)
                && isBalanced(root.right);
    }

    private static int depth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return 1 + Math.max(depth(root.left), depth(root.right));
    }
}
