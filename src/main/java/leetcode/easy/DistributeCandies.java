package leetcode.easy;

/**
 * 575. Distribute Candies
 *
 * @link https://leetcode.com/problems/distribute-candies/
 * @tags Hash Table.
 */
public class DistributeCandies {
    /**
     * Runtime: 25 ms, faster than 97.02% of Java online submissions for Distribute Candies.
     * Memory Usage: 40 MB, less than 100.00% of Java online submissions for Distribute Candies.
     */
    public int distributeCandies(int[] candies) {
        int[] storage = new int[200_001];
        for (int candy : candies) {
            storage[candy + 100_000]++;
        }
        int typesNum = 0;
        for (int value : storage) {
            if (value != 0) {
                typesNum++;
            }
        }
        if (typesNum < candies.length) {
            return typesNum;
        } else {
            return candies.length / 2;
        }
    }
}
