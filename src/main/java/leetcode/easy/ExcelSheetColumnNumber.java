package leetcode.easy;

/**
 * 171. Excel Sheet Column Number
 *
 * @link https://leetcode.com/problems/excel-sheet-column-number/
 * @tags Math.
 */
public class ExcelSheetColumnNumber {
    /**
     * Runtime: 1 ms, faster than 99.98% of Java online submissions for Excel Sheet Column Number.
     * Memory Usage: 36 MB, less than 100.00% of Java online submissions for Excel Sheet Column Number.
     */
    public static int titleToNumber(String s) {
        int result = 0;
        for (int i = 0; i < s.length(); i++) {
            result *= 26;
            result += (s.charAt(i) - 'A' + 1);
        }

        return result;
    }
}
