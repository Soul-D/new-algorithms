package leetcode.easy;

import java.util.HashSet;
import java.util.Set;

/**
 * 136. Single Number
 *
 * @link https://leetcode.com/problems/single-number/
 * @tags Hash Table, Bit Manipulation.
 */
public class SingleNumber {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Single Number.
     * Memory Usage: 39 MB, less than 98.52% of Java online submissions for Single Number.
     */
    public int singleNumberFaster(int[] nums) {
        int xor = 0;
        for (int num : nums) {
            xor ^= num;
        }
        return xor;
    }

    /**
     * Runtime: 7 ms, faster than 37.68% of Java online submissions for Single Number.
     * Memory Usage: 40 MB, less than 96.30% of Java online submissions for Single Number.
     */
    public int singleNumber(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            if (!set.add(num)) {
                set.remove(num);
            }
        }
        return set.iterator().next();
    }
}
