package leetcode.easy;

/**
 * 459. Repeated Substring Pattern
 *
 * @link https://leetcode.com/problems/repeated-substring-pattern/
 * @tags String.
 */
public class RepeatedSubstringPattern {
    /**
     * Runtime: 14 ms, faster than 73.45% of Java online submissions for Repeated Substring Pattern.
     * Memory Usage: 37.3 MB, less than 100.00% of Java online submissions for Repeated Substring Pattern.
     */
    public boolean repeatedSubstringPattern(String s) {
        int length = s.length();
        for (int i = length / 2; i >= 1; i--) {
            if (length % i == 0) {
                int count = length / i;
                String pattern = s.substring(0, i);
                StringBuilder builder = new StringBuilder();
                for (int j = 0; j < count; j++) {
                    builder.append(pattern);
                }
                if (builder.toString().equals(s)) {
                    return true;
                }
            }
        }
        return false;
    }
}
