package leetcode.easy;

/**
 * 242. Valid Anagram
 *
 * @link https://leetcode.com/problems/valid-anagram/
 * @tags Hash Table, Sort.
 */
public class ValidAnagram {
    /**
     * Runtime: 3 ms, faster than 93.52% of Java online submissions for Valid Anagram.
     * Memory Usage: 37.5 MB, less than 72.90% of Java online submissions for Valid Anagram.
     */
    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        int[] counts = new int[26];

        for (int i = 0; i < s.length(); i++) {
            counts[s.charAt(i) - 97]++;
            counts[t.charAt(i) - 97]--;
        }
        for (int count : counts) {
            if (count != 0) {
                return false;
            }
        }
        return true;
    }
}
