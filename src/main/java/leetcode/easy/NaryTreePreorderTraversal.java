package leetcode.easy;

import leetcode.model.n_ary_tree.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * 589. N-ary Tree Preorder Traversal
 *
 * @link https://leetcode.com/problems/n-ary-tree-preorder-traversal/
 * @tags Tree.
 */
public class NaryTreePreorderTraversal {
    private List<Integer> storage = new ArrayList<>();

    /**
     * Runtime: 1 ms, faster than 91.56% of Java online submissions for N-ary Tree Preorder Traversal.
     * Memory Usage: 39.1 MB, less than 100.00% of Java online submissions for N-ary Tree Preorder Traversal.
     */
    public List<Integer> preorder(Node root) {
        work(root);
        return storage;
    }

    private void work(Node root) {
        if (root != null) {
            storage.add(root.val);

            for (Node child : root.children) {
                work(child);
            }
        }
    }

    public List<Integer> preorderNoVal(Node root) {
        List<Integer> list = new ArrayList<>();
        parse(root, list);
        return list;
    }

    private void parse(Node root, List<Integer> list) {
        if (root == null) {
            return;
        }
        list.add(root.val);
        for (Node child : root.children) {
            parse(child, list);
        }
    }
}
