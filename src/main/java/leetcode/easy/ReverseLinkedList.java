package leetcode.easy;

import leetcode.model.ListNode;

/**
 * 206. Reverse Linked List
 *
 * @link https://leetcode.com/problems/reverse-linked-list/
 * @tags Linked List.
 */
public class ReverseLinkedList {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Reverse Linked List.
     * Memory Usage: 37.5 MB, less than 97.84% of Java online submissions for Reverse Linked List.
     */
    public static ListNode reverseList(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode prev = null;
        ListNode next;
        while (head.next != null) {
            next = head.next;
            head.next = prev;
            prev = head;
            head = next;
        }
        head.next = prev;
        return head;
    }
}
