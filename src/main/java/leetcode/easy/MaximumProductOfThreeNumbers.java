package leetcode.easy;

/**
 * 628. Maximum Product of Three Numbers
 *
 * @link https://leetcode.com/problems/maximum-product-of-three-numbers/
 * @tags Array, Math.
 */
public class MaximumProductOfThreeNumbers {
    /**
     * Runtime: 2 ms, faster than 99.31% of Java online submissions for Maximum Product of Three Numbers.
     * Memory Usage: 38.8 MB, less than 100.00% of Java online submissions for Maximum Product of Three Numbers.
     */
    public int maximumProduct(int[] nums) {
        int max1 = Integer.MIN_VALUE;
        int max2 = Integer.MIN_VALUE;
        int max3 = Integer.MIN_VALUE;
        int min1 = Integer.MAX_VALUE;
        int min2 = Integer.MAX_VALUE;
        for (int n : nums) {
            if (n > max1) {
                max3 = max2;
                max2 = max1;
                max1 = n;
            } else if (n > max2) {
                max3 = max2;
                max2 = n;
            } else if (n > max3) {
                max3 = n;
            }

            if (n < min1) {
                min2 = min1;
                min1 = n;
            } else if (n < min2) {
                min2 = n;
            }
        }
        return Math.max(max1 * max2 * max3, max1 * min1 * min2);
    }
}
