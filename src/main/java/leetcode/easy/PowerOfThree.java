package leetcode.easy;

/**
 * 326. Power of Three
 *
 * @link https://leetcode.com/problems/power-of-three/
 * @tags Math.
 */
public class PowerOfThree {
    /**
     * Runtime: 11 ms, faster than 84.39% of Java online submissions for Power of Three.
     * Memory Usage: 35.5 MB, less than 9.38% of Java online submissions for Power of Three.
     */
    public boolean isPowerOfThree(int n) {
        return n > 0 && 1162261467 % n == 0;
    }
}
