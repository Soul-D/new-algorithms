package leetcode.easy;

/**
 * 557. Reverse Words in a String III
 *
 * @link https://leetcode.com/problems/reverse-words-in-a-string-iii/
 * @tags String.
 */
public class ReverseWordsInAStringIII {
    /**
     * Runtime: 4 ms, faster than 83.45% of Java online submissions for Reverse Words in a String III.
     * Memory Usage: 37.7 MB, less than 100.00% of Java online submissions for Reverse Words in a String III.
     */
    public static String reverseWords(String s) {
        char[] chars = s.toCharArray();
        int start = 0;
        int end;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == ' ') {
                end = i - 1;
                reverse(chars, start, end);
                start = i + 1;
            }
        }

        if (start < s.length()) {
            reverse(chars, start, s.length() - 1);
        }

        return new String(chars);
    }

    private static void reverse(char[] chars, int start, int end) {
        while (start < end) {
            char tmp = chars[start];
            chars[start] = chars[end];
            chars[end] = tmp;
            start++;
            end--;
        }
    }
}
