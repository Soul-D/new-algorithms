package leetcode.easy;

/**
 * 283. Move Zeroes
 *
 * @link https://leetcode.com/problems/move-zeroes/
 * @tags Array, Two Pointers.
 */
public class MoveZeroes {

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Move Zeroes.
     * Memory Usage: 37.8 MB, less than 100.00% of Java online submissions for Move Zeroes.
     */
    public static void moveZeroes(int[] nums) {
        int zeroes = 0;
        int lastNonZero = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                nums[lastNonZero++] = nums[i];
                continue;
            }
            zeroes++;
        }
        for (int i = nums.length - zeroes; i < nums.length; i++) {
            nums[i] = 0;
        }
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Move Zeroes.
     * Memory Usage: 37.7 MB, less than 100.00% of Java online submissions for Move Zeroes.
     */
    public static void moveZeroesSwap(int[] nums) {
        int firstZero = -1;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                if (firstZero == -1) {
                    firstZero = i;
                }
            } else {
                if (firstZero != -1) {
                    nums[firstZero] = nums[i];
                    nums[i] = 0;
                    firstZero++;
                }
            }
        }

    }
}
