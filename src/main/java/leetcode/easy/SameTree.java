package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 100. Same Tree
 *
 * @link https://leetcode.com/problems/same-tree/description/
 * @tags Tree, Depth-first search.
 */
public class SameTree {
    /**
     * Runtime: 0 ms
     * Memory Usage: 34 MB
     */
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        }
        if ((p != null && q == null) || (p == null && q != null)) {
            return false;
        }

        return p.val == q.val && isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }
}
