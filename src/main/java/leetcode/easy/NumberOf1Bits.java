package leetcode.easy;

/**
 * 191. Number of 1 Bits
 *
 * @link https://leetcode.com/problems/number-of-1-bits/
 * @tags Bit Manipulation.
 */
public class NumberOf1Bits {
    /**
     * Runtime: 1 ms, faster than 57.57% of Java online submissions for Number of 1 Bits.
     * Memory Usage: 33.2 MB, less than 5.41% of Java online submissions for Number of 1 Bits.
     */
    public int hammingWeight(int n) {
        if (n == 0) return 0;

        int result = 0;
        for (int i = 0; i < 32; i++) {
            result += ((n >> i) & 1);
        }
        return result;
    }
}
