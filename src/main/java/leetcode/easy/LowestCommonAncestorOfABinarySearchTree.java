package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 235. Lowest Common Ancestor of a Binary Search Tree
 *
 * @link https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/
 * @tags Tree.
 */
public class LowestCommonAncestorOfABinarySearchTree {
    /**
     * Runtime: 4 ms, faster than 100.00% of Java online submissions for Lowest Common Ancestor of a Binary Search Tree.
     * Memory Usage: 35.8 MB, less than 5.10% of Java online submissions for Lowest Common Ancestor of a Binary Search Tree.
     */
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) {
            return null;
        }

        if (q.val < p.val) {
            TreeNode temp = q;
            q = p;
            p = temp;
        }

        if (p.val <= root.val && root.val <= q.val) {
            return root;
        }

        if (root.val > q.val) {
            return lowestCommonAncestor(root.left, p, q);
        } else {
            return lowestCommonAncestor(root.right, p, q);
        }
    }
}
