package leetcode.easy;

import java.util.HashMap;
import java.util.Map;

/**
 * 290. Word Pattern
 *
 * @link https://leetcode.com/problems/word-pattern/
 * @tags Hash Table.
 */
public class WordPattern {
    /**
     * Runtime: 1 ms, faster than 99.24% of Java online submissions for Word Pattern.
     * Memory Usage: 34.2 MB, less than 97.30% of Java online submissions for Word Pattern.
     */
    public static boolean wordPattern(String pattern, String str) {
        String[] tokens = str.split(" ");
        char[] chars = pattern.toCharArray();
        if (tokens.length != chars.length) {
            return false;
        }

        Map<String, Character> tokenMap = new HashMap<>();
        Map<Character, String> charMap = new HashMap<>();
        for (int i = 0; i < tokens.length; i++) {
            Character ch = tokenMap.put(tokens[i], chars[i]);
            String st = charMap.put(chars[i], tokens[i]);
            if (ch != null && ch != chars[i]) {
                return false;
            }

            if (st != null && !st.equals(tokens[i])) {
                return false;
            }
        }

        return true;
    }
}
