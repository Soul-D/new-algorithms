package leetcode.easy;

import java.util.Arrays;

/**
 * 455. Assign Cookies
 *
 * @link https://leetcode.com/problems/assign-cookies/
 * @tags Greedy.
 */
public class AssignCookies {
    /**
     * Runtime: 8 ms, faster than 98.78% of Java online submissions for Assign Cookies.
     * Memory Usage: 40.1 MB, less than 100.00% of Java online submissions for Assign Cookies.
     */
    public static int findContentChildren(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        int result = 0;
        for (int ch = 0, cs = 0; ch < g.length && cs < s.length; ) {
            int cookie = s[s.length - 1 - cs];
            int child = g[g.length - 1 - ch];
            if (cookie >= child) {
                result++;
                ch++;
                cs++;
            } else {
                ch++;
            }
        }
        return result;
    }
}
