package leetcode.easy;

/**
 * 709. To Lower Case
 *
 * @link https://leetcode.com/problems/to-lower-case/
 * @tags String.
 */
public class ToLowerCase {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for To Lower Case.
     * Memory Usage: 41.1 MB, less than 6.49% of Java online submissions for To Lower Case.
     */
    public static String toLowerCase(String str) {
        char[] chars = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {
            if (isUpper(str.charAt(i))) {
                chars[i] = (char) (str.charAt(i) + 32);
            } else {
                chars[i] = str.charAt(i);
            }
        }
        return new String(chars);
    }

    private static boolean isUpper(char ch) {
        return 65 <= ch && ch <= 90;
    }
}
