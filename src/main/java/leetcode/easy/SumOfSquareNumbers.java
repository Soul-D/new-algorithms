package leetcode.easy;

/**
 * 633. Sum of Square Numbers
 *
 * @link https://leetcode.com/problems/sum-of-square-numbers/
 * @tags Math.
 */
public class SumOfSquareNumbers {
    /**
     * Runtime: 2 ms, faster than 96.67% of Java online submissions for Sum of Square Numbers.
     * Memory Usage: 33.4 MB, less than 7.14% of Java online submissions for Sum of Square Numbers.
     */
    public boolean judgeSquareSum(int c) {
        int low = 0, high = (int) Math.sqrt(c);
        while (low <= high) {
            int candidate = low * low + high * high;
            if (candidate == c) {
                return true;
            } else if (candidate > c) {
                high--;
            } else {
                low++;
            }
        }
        return false;
    }
}
