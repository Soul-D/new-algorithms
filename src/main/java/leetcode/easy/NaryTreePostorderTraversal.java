package leetcode.easy;

import leetcode.model.n_ary_tree.Node;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * 590. N-ary Tree Postorder Traversal
 *
 * @link https://leetcode.com/problems/n-ary-tree-postorder-traversal/
 * @tags Tree.
 */
public class NaryTreePostorderTraversal {
    /**
     * Runtime: 4 ms, faster than 19.16% of Java online submissions for N-ary Tree Postorder Traversal.
     * Memory Usage: 39.9 MB, less than 100.00% of Java online submissions for N-ary Tree Postorder Traversal.
     */
    public List<Integer> postorderIterativeArrayList(Node root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Stack<Node> stack = new Stack<>();
        stack.add(root);
        while (!stack.isEmpty()) {
            root = stack.pop();
            for (Node child : root.children) {
                stack.push(child);
            }
            result.add(root.val);
        }

        Integer tmp;
        int size = result.size();
        for (int i = 0; i < size / 2; i++) {
            tmp = result.get(i);
            result.set(i, result.get(size - 1 - i));
            result.set(size - 1 - i, tmp);
        }
        return result;
    }

    /**
     * Runtime: 3 ms, faster than 34.30% of Java online submissions for N-ary Tree Postorder Traversal.
     * Memory Usage: 40.4 MB, less than 100.00% of Java online submissions for N-ary Tree Postorder Traversal.
     */
    public List<Integer> postorderIterativeLinkedList(Node root) {
        LinkedList<Integer> result = new LinkedList<>();
        if (root == null) {
            return result;
        }
        Stack<Node> stack = new Stack<>();
        stack.add(root);
        while (!stack.isEmpty()) {
            root = stack.pop();
            for (Node child : root.children) {
                stack.push(child);
            }
            result.addFirst(root.val);
        }
        return result;
    }

    /**
     * Runtime: 1 ms, faster than 91.34% of Java online submissions for N-ary Tree Postorder Traversal.
     * Memory Usage: 40.5 MB, less than 100.00% of Java online submissions for N-ary Tree Postorder Traversal.
     */
    public List<Integer> postorderRecursive(Node root) {
        List<Integer> result = new ArrayList<>();
        postorder(root, result);
        return result;
    }

    private void postorder(Node root, List<Integer> list) {
        if (root == null) {
            return;
        }
        for (Node child : root.children) {
            postorder(child, list);
        }
        list.add(root.val);
    }
}
