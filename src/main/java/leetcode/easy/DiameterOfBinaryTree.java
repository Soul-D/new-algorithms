package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 543. Diameter of Binary Tree
 *
 * @link https://leetcode.com/problems/diameter-of-binary-tree/
 * @tags Tree.
 */
public class DiameterOfBinaryTree {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Diameter of Binary Tree.
     * Memory Usage: 38.4 MB, less than 46.75% of Java online submissions for Diameter of Binary Tree.
     */
    public int diameterOfBinaryTreeFaster(TreeNode root) {
        int[] diameter = new int[1];
        height(root, diameter);
        return diameter[0];
    }

    private int height(TreeNode node, int[] diameter) {
        if (node == null) {
            return 0;
        }
        int lh = height(node.left, diameter);
        int rh = height(node.right, diameter);
        diameter[0] = Math.max(diameter[0], lh + rh);
        return 1 + Math.max(lh, rh);
    }


    /**
     * Runtime: 9 ms, faster than 13.39% of Java online submissions for Diameter of Binary Tree.
     * Memory Usage: 38.5 MB, less than 32.47% of Java online submissions for Diameter of Binary Tree.
     */
    public int diameterOfBinaryTree(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int diameter = depth(root.left) + depth(root.right);
        int leftDiameter = diameterOfBinaryTree(root.left);
        int rightDiameter = diameterOfBinaryTree(root.right);
        return Math.max(diameter, Math.max(leftDiameter, rightDiameter));

    }

    private int depth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return 1 + Math.max(depth(root.left), depth(root.right));
    }
}
