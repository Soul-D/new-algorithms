package leetcode.easy;

import java.util.Arrays;

/**
 * 561. Array Partition I
 *
 * @link https://leetcode.com/problems/array-partition-i/
 * @tags Array.
 */
public class ArrayPartitionI {
    /**
     * Runtime: 14 ms, faster than 93.45% of Java online submissions for Array Partition I.
     * Memory Usage: 38.8 MB, less than 100.00% of Java online submissions for Array Partition I.
     */
    public int arrayPairSum(int[] nums) {
        Arrays.sort(nums);
        int sum = 0;
        for (int i = 0; i < nums.length; i += 2) {
            sum += nums[i];
        }
        return sum;
    }
}
