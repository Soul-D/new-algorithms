package leetcode.easy;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 225. Implement Stack using Queues
 *
 * @link https://leetcode.com/problems/implement-stack-using-queues/
 * @tags Stack, Design.
 */
public class ImplementStackUsingQueues {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Implement Stack using Queues.
     * Memory Usage: 34.3 MB, less than 91.67% of Java online submissions for Implement Stack using Queues.
     */
    static class MyStack {
        private Queue<Integer> queue;
        private int size;

        /**
         * Initialize your data structure here.
         */
        public MyStack() {
            queue = new LinkedList<>();
        }

        /**
         * Push element x onto stack.
         */
        public void push(int x) {
            queue.add(x);
            int copySize = size++;
            while (copySize > 0) {
                queue.add(queue.poll());
                copySize--;
            }
        }

        /**
         * Removes the element on top of the stack and returns that element.
         */
        public int pop() {
            size--;
            return queue.poll();
        }

        /**
         * Get the top element.
         */
        public int top() {
            return queue.peek();
        }

        /**
         * Returns whether the stack is empty.
         */
        public boolean empty() {
            return size == 0;
        }
    }
}
