package leetcode.easy;

/**
 * 125. Valid Palindrome
 *
 * @link https://leetcode.com/problems/valid-palindrome/
 * @tags Two pointers, String.
 */
public class ValidPalindrome {
    /**
     * Runtime: 3 ms, faster than 96.08% of Java online submissions for Valid Palindrome.
     * Memory Usage: 37.7 MB, less than 98.21% of Java online submissions for Valid Palindrome.
     */
    public static boolean isPalindromeFaster(String s) {
        int start = 0;
        int end = s.length() - 1;
        while (start <= end) {
            char startCh = s.charAt(start);
            if (!Character.isLetterOrDigit(startCh)) {
                start++;
                continue;
            }
            char endCh = s.charAt(end);
            if (!Character.isLetterOrDigit(endCh)) {
                end--;
                continue;
            }
            if (Character.toLowerCase(startCh) != Character.toLowerCase(endCh)) {
                return false;
            }
            start++;
            end--;
        }
        return true;
    }

    /**
     * Runtime: 18 ms, faster than 22.82% of Java online submissions for Valid Palindrome.
     * Memory Usage: 38.2 MB, less than 78.57% of Java online submissions for Valid Palindrome.
     */
    public static boolean isPalindrome(String s) {
        s = s.toLowerCase().replaceAll("[^a-z0-9]", "");
        for (int i = 0; i < s.length() / 2; i++) {
            if (s.charAt(i) != s.charAt(s.length() - 1 - i)) {
                return false;
            }
        }
        return true;
    }
}
