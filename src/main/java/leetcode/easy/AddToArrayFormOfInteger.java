package leetcode.easy;

import java.util.LinkedList;
import java.util.List;

/**
 * 989. Add to Array-Form of Integer
 *
 * @link https://leetcode.com/problems/add-to-array-form-of-integer/
 * @tags Array.
 */
public class AddToArrayFormOfInteger {
    /**
     * Runtime: 2 ms, faster than 100.00% of Java online submissions for Add to Array-Form of Integer.
     * Memory Usage: 42.8 MB, less than 9.52% of Java online submissions for Add to Array-Form of Integer.
     */
    public static List<Integer> addToArrayForm(int[] A, int K) {
        int add = 0;
        int index = A.length - 1;
        LinkedList<Integer> result = new LinkedList<>();
        while (K > 0 && index >= 0) {
            int val = A[index--] + add + (K % 10);
            add = val / 10;
            result.addFirst(val % 10);
            K = K / 10;
        }

        while (K > 0) {
            int val = add + (K % 10);
            add = val / 10;
            result.addFirst(val % 10);
            K = K / 10;
        }

        while (index >= 0) {
            int val = A[index--] + add;
            add = val / 10;
            result.addFirst(val % 10);
        }
        if (add != 0) {
            result.addFirst(add);
        }

        return result;
    }
}
