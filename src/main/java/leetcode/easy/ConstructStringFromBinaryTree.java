package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 606. Construct String from Binary Tree
 *
 * @link https://leetcode.com/problems/construct-string-from-binary-tree/
 * @tags String, Tree.
 */
public class ConstructStringFromBinaryTree {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Construct String from Binary Tree.
     * Memory Usage: 38.3 MB, less than 94.74% of Java online submissions for Construct String from Binary Tree.
     */
    public static String tree2str(TreeNode t) {
        StringBuilder builder = new StringBuilder();
        if (t != null) {
            tree2str(t, builder);
        }
        return builder.toString();
    }

    private static void tree2str(TreeNode t, StringBuilder builder) {
        builder.append(t.val);
        if (t.left == null && t.right == null) {
            return;
        }
        if (t.left == null) {
            builder.append("()");
        } else {
            builder.append("(");
            tree2str(t.left, builder);
            builder.append(")");
        }

        if (t.right != null) {
            builder.append("(");
            tree2str(t.right, builder);
            builder.append(")");
        }
    }
}
