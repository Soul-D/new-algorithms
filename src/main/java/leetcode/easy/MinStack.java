package leetcode.easy;

import java.util.Stack;

/**
 * 155. Min Stack
 *
 * @link https://leetcode.com/problems/min-stack/
 * @tags Stack, Design.
 */
public class MinStack {
    /**
     * Runtime: 4 ms, faster than 99.93% of Java online submissions for Min Stack.
     * Memory Usage: 39.8 MB, less than 37.68% of Java online submissions for Min Stack.
     */
    private Stack<Integer> stack;
    private int min;

    public MinStack() {
        stack = new Stack<>();
        min = Integer.MAX_VALUE;
    }

    public void push(int x) {
        if (x <= min) {
            stack.push(min);
            min = x;
        }
        stack.push(x);
    }

    public void pop() {
        if (stack.pop() == min) {
            min = stack.pop();
        }
    }

    public int top() {
        return stack.peek();
    }

    public int getMin() {
        return min;
    }
}