package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 437. Path Sum III
 *
 * @link https://leetcode.com/problems/path-sum-iii/
 * @tags Tree.
 */
public class PathSumIII {
    /**
     * Runtime: 11 ms, faster than 52.23% of Java online submissions for Path Sum III.
     * Memory Usage: 41.5 MB, less than 47.73% of Java online submissions for Path Sum III.
     */
    public int pathSum(TreeNode root, int sum) {
        if (root == null) {
            return 0;
        }
        return pathSumFrom(root, sum) + pathSum(root.left, sum) + pathSum(root.right, sum);
    }

    private int pathSumFrom(TreeNode node, int sum) {
        if (node == null) {
            return 0;
        }
        return (node.val == sum ? 1 : 0)
                + pathSumFrom(node.left, sum - node.val)
                + pathSumFrom(node.right, sum - node.val);
    }
}
