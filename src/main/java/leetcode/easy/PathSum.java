package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 112. Path Sum
 *
 * @link https://leetcode.com/problems/path-sum/
 * @tags Tree, Depth-first search.
 */
public class PathSum {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Path Sum.
     * Memory Usage: 37.1 MB, less than 100.00% of Java online submissions for Path Sum.
     */
    public static boolean hasPathSum(TreeNode root, int sum) {
        return root != null && search(root, sum);
    }

    private static boolean search(TreeNode root, int sum) {
        int rest = sum - root.val;

        if (rest == 0 && root.left == null && root.right == null) {
            return true;
        }

        if (root.left == null && root.right == null) {
            return false;
        }

        return (root.left != null && search(root.left, rest)) || (root.right != null && search(root.right, rest));
    }
}
