package leetcode.easy;

import java.util.Arrays;

/**
 * 169. Majority Element
 *
 * @link https://leetcode.com/problems/majority-element/
 * @tags Array, Divide and Conquer, Bit Manipulation.
 */
public class MajorityElement {
    /**
     * Runtime: 1 ms, faster than 99.85% of Java online submissions for Majority Element.
     * Memory Usage: 42.7 MB, less than 50.73% of Java online submissions for Majority Element.
     * http://www.cs.utexas.edu/~moore/best-ideas/mjrty/
     */
    public int majorityElementFaster(int[] nums) {
        int res = nums[0], count = 1;
        for (int i = 1; i < nums.length; i++) {
            if (count == 0) {
                count++;
                res = nums[i];
            } else if (nums[i] == res) {
                count++;
            } else {
                count--;
            }
        }
        return res;
    }

    /**
     * Runtime: 2 ms, faster than 58.91% of Java online submissions for Majority Element.
     * Memory Usage: 42.7 MB, less than 53.68% of Java online submissions for Majority Element.
     */
    public int majorityElement(int[] nums) {
        Arrays.sort(nums);
        int limit = nums.length / 2;
        int prev = nums[0];
        int count = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == prev) {
                count++;
                if (count > limit) {
                    return nums[i];
                }
            } else {
                prev = nums[i];
                count = 1;
            }
        }
        return prev;
    }
}
