package leetcode.easy;

import leetcode.model.n_ary_tree.Node;

/**
 * 559. Maximum Depth of N-ary Tree
 *
 * @link https://leetcode.com/problems/maximum-depth-of-n-ary-tree/
 * @tags Tree, Depth-first search, Breadth-first search.
 */
public class MaximumDepthOfNaryTree {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Maximum Depth of N-ary Tree.
     * Memory Usage: 39.9 MB, less than 100.00% of Java online submissions for Maximum Depth of N-ary Tree.
     */
    public int maxDepth(Node root) {
        if (root == null) {
            return 0;
        }

        int max = 0;
        for (Node child : root.children) {
            max = Math.max(max, maxDepth(child));
        }
        return 1 + max;
    }
}
