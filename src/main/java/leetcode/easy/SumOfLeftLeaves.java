package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 404. Sum of Left Leaves
 *
 * @link https://leetcode.com/problems/sum-of-left-leaves/
 * @tags Tree.
 */
public class SumOfLeftLeaves {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Sum of Left Leaves.
     * Memory Usage: 35.6 MB, less than 100.00% of Java online submissions for Sum of Left Leaves.
     */
    public static int sumOfLeftLeaves(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return sumOfLeftLeaves(root, false);
    }

    private static int sumOfLeftLeaves(TreeNode root, boolean isLeft) {
        if (root.left == null && root.right == null) {
            if (isLeft) {
                return root.val;
            } else {
                return 0;
            }
        }
        int leftSum = 0, rightSum = 0;
        if (root.left != null) {
            leftSum = sumOfLeftLeaves(root.left, true);
        }
        if (root.right != null) {
            rightSum = sumOfLeftLeaves(root.right, false);
        }
        return leftSum + rightSum;
    }
}
