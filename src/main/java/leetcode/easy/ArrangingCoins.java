package leetcode.easy;

/**
 * 441. Arranging Coins
 *
 * @link https://leetcode.com/problems/arranging-coins/
 * @tags Math, Binary Search.
 */
public class ArrangingCoins {
    /**
     * Runtime: 2 ms, faster than 65.78% of Java online submissions for Arranging Coins.
     * Memory Usage: 33.7 MB, less than 5.26% of Java online submissions for Arranging Coins.
     * Not mine.
     */
    public static int arrangeCoinsBinary(int n) {
        // get sum 1+2+3+...k < n
        // find the largest k, the sum of 1+2+3+..+k = k(k+1)/2 < n
        // solve this equation
        // solve k^2 + k - 2n = 0
        // then (-1 + sqrt(1+8n))/2

        double res = (-1.0 + Math.sqrt(1 + 8 * (long) n)) / 2.0;
        return (int) Math.floor(res);
    }

    /**
     * Runtime: 9 ms, faster than 15.13% of Java online submissions for Arranging Coins.
     * Memory Usage: 33.6 MB, less than 5.26% of Java online submissions for Arranging Coins.
     */
    public static int arrangeCoins(int n) {
        int count = 0;
        int a = 1;
        while (n - a >= 0) {
            count++;
            n -= a++;
        }
        return count;
    }
}
