package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 101. Symmetric Tree
 *
 * @link https://leetcode.com/problems/symmetric-tree/description/
 * @tags Tree, Depth-first search, Breadth-first search.
 */
public class SymmetricTree {
    /**
     * Runtime: 0 ms
     * Memory Usage: 37.8 MB
     */
    public static boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        return isSymmetric(root.left, root.right);
    }

    private static boolean isSymmetric(TreeNode left, TreeNode right) {
        if (left == null && right == null) {
            return true;
        }
        if (left == null || right == null) {
            return false;
        }
        return left.val == right.val && isSymmetric(left.left, right.right) && isSymmetric(left.right, right.left);
    }
}
