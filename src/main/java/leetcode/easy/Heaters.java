package leetcode.easy;

import java.util.Arrays;

/**
 * 475. Heaters
 *
 * @link https://leetcode.com/problems/heaters/
 * @tags Binary Search.
 */
public class Heaters {
    /**
     * Runtime: 8 ms, faster than 99.45% of Java online submissions for Heaters.
     * Memory Usage: 38.3 MB, less than 100.00% of Java online submissions for Heaters.
     * Not mine.
     */
    public static int findRadiusFast(int[] houses, int[] heaters) {
        Arrays.sort(houses);
        Arrays.sort(heaters);
        int i = 0, j = 0;
        long left = Integer.MIN_VALUE;
        int radius = 0;
        while (i < houses.length && j < heaters.length) {
            if (houses[i] > heaters[j]) {
                left = heaters[j];
                j++;
            } else {
                int temp = (int) Math.min(houses[i] - left, (long) heaters[j] - houses[i]);
                radius = Math.max(temp, radius);
                i++;
            }
        }
        if (j >= heaters.length) {
            radius = Math.max(radius, Math.abs(houses[houses.length - 1] - heaters[heaters.length - 1]));
        }
        return radius;
    }

    /**
     * Runtime: 13 ms, faster than 47.27% of Java online submissions for Heaters.
     * Memory Usage: 39.1 MB, less than 100.00% of Java online submissions for Heaters.
     */
    public static int findRadius(int[] houses, int[] heaters) {
        Arrays.sort(heaters);
        int[] warmed = new int[houses.length];
        for (int i = 0; i < houses.length; i++) {
            warmed[i] = Arrays.binarySearch(heaters, houses[i]);
        }
        int min = 0;
        for (int i = 0; i < warmed.length; i++) {
            int w = warmed[i];
            if (w < 0) {
                int ind = -(w + 1);
                int candidate;
                if (ind == 0) {
                    candidate = Math.abs(houses[i] - heaters[ind]);
                } else if (ind > heaters.length - 1) {
                    candidate = Math.abs(houses[i] - heaters[ind - 1]);
                } else {
                    candidate = Math.min(Math.abs(houses[i] - heaters[ind - 1]), Math.abs(heaters[ind] - houses[i]));
                }
                if (candidate > min) {
                    min = candidate;
                }
            }
        }
        return min;
    }
}
