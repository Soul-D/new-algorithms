package leetcode.easy;

/**
 * 292. Nim Game
 *
 * @link https://leetcode.com/problems/nim-game/
 * @tags Brainteaser, Minimax.
 */
public class NimGame {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Nim Game.
     * Memory Usage: 32.9 MB, less than 7.69% of Java online submissions for Nim Game.
     */
    public boolean canWinNim(int n) {
        return n <= 3 || n % 4 != 0;
    }
}
