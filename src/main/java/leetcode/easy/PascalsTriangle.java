package leetcode.easy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 118. Pascal's Triangle
 *
 * @link https://leetcode.com/problems/pascals-triangle/
 * @tags Array.
 */
public class PascalsTriangle {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Pascal's Triangle.
     * Memory Usage: 33.7 MB, less than 7.23% of Java online submissions for Pascal's Triangle.
     */
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> result = new ArrayList<>();
        if (numRows == 0) {
            return result;
        }
        result.add(Collections.singletonList(1));
        for (int i = 2; i <= numRows; i++) {
            List<Integer> current = new ArrayList<>(i);
            current.add(1);
            for (int j = 1; j < i - 1; j++) {
                current.add(result.get(i - 2).get(j - 1) + result.get(i - 2).get(j));
            }
            current.add(1);
            result.add(current);
        }

        return result;
    }
}
