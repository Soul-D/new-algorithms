package leetcode.easy;

import leetcode.model.TreeNode;

import java.util.*;

/**
 * 501. Find Mode in Binary Search Tree
 *
 * @link https://leetcode.com/problems/find-mode-in-binary-search-tree/
 * @tags Tree
 */
public class FindModeInBinarySearchTree {
    /**
     * Runtime: 7 ms, faster than 34.40% of Java online submissions for Find Mode in Binary Search Tree.
     * Memory Usage: 39.3 MB, less than 28.57% of Java online submissions for Find Mode in Binary Search Tree.
     */
    public static int[] findMode(TreeNode root) {
        if (root == null) {
            return new int[0];
        }
        Set<Integer> set = new HashSet<>();
        Map<Integer, Integer> map = new HashMap<>();
        int max = walk(root, map);
        map.forEach((k, v) -> {
            if (v == max) {
                set.add(k);
            }
        });

        int[] res = new int[set.size()];
        int ind = 0;
        for (Integer integer : set) {
            res[ind++] = integer;
        }
        return res;
    }

    private static int walk(TreeNode node, Map<Integer, Integer> map) {
        Integer current = map.merge(node.val, 1, Integer::sum);
        int left = node.left == null ? 0 : walk(node.left, map);
        int right = node.right == null ? 0 : walk(node.right, map);
        return Math.max(current, Math.max(left, right));
    }


    private int count = 0;
    private int maxCount = -1;
    private TreeNode prev = null;

    public int[] findModefast(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        inorder(root, list);

        int[] res = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            res[i] = list.get(i);
        }
        return res;
    }

    private void inorder(TreeNode root, List<Integer> list) {
        if (root == null) {
            return;
        }

        inorder(root.left, list);

        if (prev != null && root.val == prev.val) {
            count++;
        } else {
            count = 0;
        }

        if (count > maxCount) {
            list.clear();
            list.add(root.val);
            maxCount = count;
        } else if (count == maxCount) {
            list.add(root.val);
        }

        prev = root;
        inorder(root.right, list);
    }
}
