package leetcode.easy;

/**
 * 507. Perfect Number
 *
 * @link https://leetcode.com/problems/perfect-number/
 * @tags Math.
 */
public class PerfectNumber {
    /**
     * Runtime: 1324 ms, faster than 11.79% of Java online submissions for Perfect Number.
     * Memory Usage: 33.1 MB, less than 5.88% of Java online submissions for Perfect Number.
     */
    public static boolean checkPerfectNumber(int num) {
        if (num == 1) {
            return false;
        }
        int sum = 1;
        for (int i = 2; i <= num / 2; i++) {
            if (num % i == 0) {
                sum += i;
            }
        }
        return sum == num;
    }

    /**
     * Runtime: 1 ms, faster than 95.28% of Java online submissions for Perfect Number.
     * Memory Usage: 33.3 MB, less than 5.88% of Java online submissions for Perfect Number.
     */
    public static boolean checkPerfectNumberFast(int num) {
        if (num == 1) {
            return false;
        }
        int sum = 1;
        double sqrt = Math.sqrt(num);
        for (int i = 2; i <= sqrt; i++) {
            if (num % i == 0) {
                sum += i;
                sum += (num / i);
            }
        }
        return sum == num;
    }
}
