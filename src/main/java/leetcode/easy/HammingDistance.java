package leetcode.easy;

/**
 * 461. Hamming Distance
 *
 * @link https://leetcode.com/problems/hamming-distance/
 * @tags Bit Manipulation.
 */
public class HammingDistance {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Hamming Distance.
     * Memory Usage: 33.3 MB, less than 5.09% of Java online submissions for Hamming Distance.
     */
    public static int hammingDistance(int x, int y) {
        int result = 0;
        int n = 31;
        while (n > 0) {
            result += (x & 1) ^ (y & 1);
            x = x >>> 1;
            y = y >>> 1;
            n--;
        }
        return result;
    }
}
