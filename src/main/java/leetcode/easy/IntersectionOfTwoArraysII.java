package leetcode.easy;

import java.util.Arrays;

/**
 * 350. Intersection of Two Arrays II
 *
 * @link https://leetcode.com/problems/intersection-of-two-arrays-ii/
 * @tags Hash Table, Binary Search, Sort, Two Pointers.
 */
public class IntersectionOfTwoArraysII {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Intersection of Two Arrays II.
     * Memory Usage: 36.2 MB, less than 83.87% of Java online submissions for Intersection of Two Arrays II.
     */
    public static int[] intersect(int[] nums1, int[] nums2) {
        int minLength = nums1.length < nums2.length ? nums1.length : nums2.length;

        Arrays.sort(nums1);
        Arrays.sort(nums2);
        int count = 0;
        int[] storage = new int[minLength];

        for (int a = 0, b = 0; a < nums1.length && b < nums2.length; ) {
            if (nums1[a] < nums2[b]) {
                a++;
            } else if (nums1[a] > nums2[b]) {
                b++;
            } else {
                storage[count++] = nums1[a++];
                b++;
            }
        }

        return Arrays.copyOf(storage, count);
    }
}
