package leetcode.easy;

/**
 * 520. Detect Capital
 *
 * @link https://leetcode.com/problems/detect-capital/
 * @tags String.
 */
public class DetectCapital {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Detect Capital.
     * Memory Usage: 34.7 MB, less than 100.00% of Java online submissions for Detect Capital.
     */
    public static boolean detectCapitalUse(String word) {
        if (word.length() < 2) {
            return true;
        }
        int low = 0;
        int high = word.length() - 1;
        int uppers = 0;
        while (low < high) {
            if (Character.isUpperCase(word.charAt(low++))) {
                uppers++;
            }
            if (Character.isUpperCase(word.charAt(high--))) {
                uppers++;
            }
        }
        if (word.length() % 2 != 0 && Character.isUpperCase(word.charAt(low))) {
            uppers++;
        }
        if (uppers == 0 || uppers == word.length()) {
            return true;
        }
        return uppers == 1 && Character.isUpperCase(word.charAt(0));
    }
}
