package leetcode.easy;

/**
 * 303. Range Sum Query - Immutable
 *
 * @link https://leetcode.com/problems/range-sum-query-immutable/
 * @tags Dynamic Programming.
 */
public class RangeSumQueryImmutable {
    /**
     * Runtime: 9 ms, faster than 97.93% of Java online submissions for Range Sum Query - Immutable.
     * Memory Usage: 42.1 MB, less than 60.98% of Java online submissions for Range Sum Query - Immutable.
     */
    static class NumArray {
        private int[] nums;

        public NumArray(int[] nums) {
            for (int i = 1; i < nums.length; i++) {
                nums[i] = nums[i - 1] + nums[i];
            }
            this.nums = nums;
        }

        public int sumRange(int i, int j) {
            return nums[j] - (i == 0 ? 0 : nums[i - 1]);
        }
    }
}
