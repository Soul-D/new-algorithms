package leetcode.easy;

/**
 * 367. Valid Perfect Square
 *
 * @link https://leetcode.com/problems/valid-perfect-square/
 * @tags Math, Binary Search.
 */
public class ValidPerfectSquare {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Valid Perfect Square.
     * Memory Usage: 32.8 MB, less than 6.12% of Java online submissions for Valid Perfect Square.
     */
    public static boolean isPerfectSquareNewton(int num) {
        long x = num;
        while (x * x > num) {
            x = (x + num / x) >> 1;
        }
        return x * x == num;
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Valid Perfect Square.
     * Memory Usage: 32.9 MB, less than 6.12% of Java online submissions for Valid Perfect Square.
     */
    public static boolean isPerfectSquareBinary(int num) {
        int low = 1, high = num;
        while (low <= high) {
            long mid = (low + high) >>> 1;
            if (mid * mid == num) {
                return true;
            } else if (mid * mid < num) {
                low = (int) mid + 1;
            } else {
                high = (int) mid - 1;
            }
        }
        return false;
    }
}
