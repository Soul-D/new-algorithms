package leetcode.easy;

/**
 * 38. Count and Say
 *
 * @link https://leetcode.com/problems/count-and-say/description/
 * @tags String
 */
public class CountAndSay {
    /**
     * 99,96%
     * Runtime: 1 ms
     * Memory Usage: 34 MB
     */
    public static String countAndSay(int n) {
        if (n == 0) {
            return "";
        }
        if (n == 1) {
            return "1";
        }
        String s = countAndSay(n - 1);
        StringBuilder sb = new StringBuilder();
        char[] c = s.toCharArray();
        int count = 1;
        for (int i = 1; i < c.length; i++) {
            if (c[i] == c[i - 1])
                count++;
            else {
                sb.append(count).append(c[i - 1]);
                count = 1;
            }
        }
        sb.append(count).append(c[c.length - 1]);
        return sb.toString();
    }
}
