package leetcode.easy;

/**
 * 122. Best Time to Buy and Sell Stock II
 *
 * @link https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/
 * @tags Array, Greedy.
 */
public class BestTimeToBuyAndSellStockII {
    /**
     * Runtime: 1 ms, faster than 92.76% of Java online submissions for Best Time to Buy and Sell Stock II.
     * Memory Usage: 37.4 MB, less than 100.00% of Java online submissions for Best Time to Buy and Sell Stock II.
     */
    public static int maxProfit(int[] prices) {
        int profit = 0;
        for (int i = 0; i < prices.length - 1; i++) {
            if (prices[i + 1] > prices[i]) {
                profit += prices[i + 1] - prices[i];
            }
        }

        return profit;
    }
}
