package leetcode.easy;

/**
 * 661. Image Smoother
 *
 * @link https://leetcode.com/problems/image-smoother/
 * @tags Array.
 */
public class ImageSmoother {
    /**
     * Runtime: 4 ms, faster than 99.56% of Java online submissions for Image Smoother.
     * Memory Usage: 66.8 MB, less than 6.25% of Java online submissions for Image Smoother.
     */
    public static int[][] imageSmoother(int[][] M) {
        int[][] result = new int[M.length][M[0].length];
        for (int col = 0; col < M.length; col++) {
            for (int row = 0; row < M[0].length; row++) {
                result[col][row] = get(M, col, row);
            }
        }
        return result;
    }

    private static int get(int[][] m, int col, int row) {
        double sum = 0;
        int count = 0;
        int up = col == 0 ? 0 : col - 1;
        int down = col == m.length - 1 ? col : col + 1;
        int left = row == 0 ? row : row - 1;
        int right = row == m[col].length - 1 ? row : row + 1;
        for (int i = up; i <= down; i++) {
            for (int j = left; j <= right; j++) {
                count++;
                sum += m[i][j];
            }
        }
        return (int) (sum / count);
    }
}
