package leetcode.easy;

/**
 * 463. Island Perimeter
 *
 * @link https://leetcode.com/problems/island-perimeter/
 * @tags Hash Table.
 */
public class IslandPerimeter {
    /**
     * Runtime: 6 ms, faster than 99.75% of Java online submissions for Island Perimeter.
     * Memory Usage: 58.9 MB, less than 97.92% of Java online submissions for Island Perimeter.
     */
    public static int islandPerimeterFast(int[][] grid) {
        int result = 0;
        for (int row = 0; row < grid.length; row++) {
            for (int col = 0; col < grid[0].length; col++) {
                if (grid[row][col] == 1) {
                    result += 4;
                    if (row > 0 && grid[row - 1][col] == 1) {
                        result -= 2;
                    }
                    if (col > 0 && grid[row][col - 1] == 1) {
                        result -= 2;
                    }
                }
            }
        }

        return result;
    }

    /**
     * Runtime: 8 ms, faster than 47.19% of Java online submissions for Island Perimeter.
     * Memory Usage: 59 MB, less than 97.92% of Java online submissions for Island Perimeter.
     */
    public static int islandPerimeter(int[][] grid) {
        int res = 0;
        for (int row = 0; row < grid.length; row++) {
            for (int col = 0; col < grid[row].length; col++) {
                if (grid[row][col] == 1) {
                    res += cell(row, col, grid);
                }
            }
        }
        return res;
    }

    private static int cell(int row, int col, int[][] grid) {
        int res = 0;
        res += (row == 0 ? 1 : (grid[row - 1][col] == 1 ? 0 : 1));
        res += (row == grid.length - 1 ? 1 : (grid[row + 1][col] == 1 ? 0 : 1));
        res += (col == 0 ? 1 : (grid[row][col - 1] == 1 ? 0 : 1));
        res += (col == grid[row].length - 1 ? 1 : (grid[row][col + 1] == 1 ? 0 : 1));

        return res;
    }
}
