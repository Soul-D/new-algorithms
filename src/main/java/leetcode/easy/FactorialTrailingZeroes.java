package leetcode.easy;

/**
 * 172. Factorial Trailing Zeroes
 *
 * @link https://leetcode.com/problems/factorial-trailing-zeroes/
 * @tags Math.
 */
public class FactorialTrailingZeroes {
    /**
     * Runtime: 1 ms, faster than 48.28% of Java online submissions for Factorial Trailing Zeroes.
     * Memory Usage: 33.1 MB, less than 7.69% of Java online submissions for Factorial Trailing Zeroes.
     */
    public static int trailingZeroes(int n) {
        int count = 0;
        long divider = 5;

        while ((n / divider) > 0) {
            count += n / divider;
            divider *= 5;
        }
        return count;
    }
}
