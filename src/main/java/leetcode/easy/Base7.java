package leetcode.easy;

/**
 * 504. Base 7
 *
 * @link https://leetcode.com/problems/base-7/
 */
public class Base7 {
    /**
     * Runtime: 1 ms, faster than 97.74% of Java online submissions for Base 7.
     * Memory Usage: 34.1 MB, less than 100.00% of Java online submissions for Base 7.
     */
    public static String convertToBase7(int num) {
        if (num == 0) {
            return "0";
        }
        boolean isNegative = num < 0;
        StringBuilder builder = new StringBuilder();
        num = Math.abs(num);
        while (num != 0) {
            builder.append(num % 7);
            num = num / 7;
        }
        if (isNegative) {
            builder.append('-');
        }
        return builder.reverse().toString();
    }
}
