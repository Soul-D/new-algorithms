package leetcode.easy;

/**
 * 268. Missing Number
 *
 * @link https://leetcode.com/problems/missing-number/
 * @tags Array, Math, Bit Manipulation.
 */
public class MissingNumber {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Missing Number.
     * Memory Usage: 38.9 MB, less than 100.00% of Java online submissions for Missing Number.
     */
    public static int missingNumberBit(int[] nums) {
        int xor, i;
        for (i = 0, xor = 0; i < nums.length; i++) {
            xor = xor ^ i ^ nums[i];
        }
        return xor ^ i;
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Missing Number.
     * Memory Usage: 39.1 MB, less than 100.00% of Java online submissions for Missing Number.
     */
    public static int missingNumber(int[] nums) {
        int length = nums.length;
        long sum = (1 + length) * (length / 2);
        if (length % 2 != 0) {
            sum = sum + length / 2 + 1;
        }
        for (int num : nums) {
            sum -= num;
        }
        return (int) sum;
    }
}
