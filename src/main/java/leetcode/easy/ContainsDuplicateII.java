package leetcode.easy;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 219. Contains Duplicate II
 *
 * @link https://leetcode.com/problems/contains-duplicate-ii/
 * @tags Array, Hash Table.
 */
public class ContainsDuplicateII {
    /**
     * Runtime: 7 ms, faster than 91.78% of Java online submissions for Contains Duplicate II.
     * Memory Usage: 44.3 MB, less than 21.05% of Java online submissions for Contains Duplicate II.
     */
    public static boolean containsNearbyDuplicate(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            Integer ind = map.put(nums[i], i);
            if (ind != null) {
                int distance = i - ind;
                if (distance <= k) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Runtime: 7 ms, faster than 91.78% of Java online submissions for Contains Duplicate II.
     * Memory Usage: 42.2 MB, less than 92.11% of Java online submissions for Contains Duplicate II.
     */
    public static boolean containsNearbyDuplicateWithWindow(int[] nums, int k) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            if (i > k) {
                set.remove(nums[i - k - 1]);
            }
            if (!set.add(nums[i])) {
                return true;
            }
        }
        return false;
    }
}
