package leetcode.easy;

/**
 * 387. First Unique Character in a String
 *
 * @link https://leetcode.com/problems/first-unique-character-in-a-string/
 * @tags Hash Table, String.
 */
public class FirstUniqueCharacterInAString {
    /**
     * Runtime: 9 ms, faster than 82.51% of Java online submissions for First Unique Character in a String.
     * Memory Usage: 36.9 MB, less than 100.00% of Java online submissions for First Unique Character in a String.
     */
    public int firstUniqChar(String s) {
        int[] counts = new int[26];
        for (int i = 0; i < s.length(); i++) {
            counts[s.charAt(i) - 'a']++;
        }
        for (int i = 0; i < s.length(); i++) {
            if (counts[s.charAt(i) - 'a'] == 1) {
                return i;
            }
        }
        return -1;
    }
}
