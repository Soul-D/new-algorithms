package leetcode.easy;

import leetcode.model.ListNode;

/**
 * 21. Merge Two Sorted Lists
 *
 * @link https://leetcode.com/problems/merge-two-sorted-lists/description/
 * @tags Linked List
 */
public class MergeTwoSortedLists {
    /**
     * 16,84% by memory.
     * Runtime: 0 ms
     * Memory Usage: 39.5 MB
     */
    public static ListNode mergeTwoLists1(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        }
        if (l2 == null) {
            return l1;
        }

        ListNode result;
        if (l1.val < l2.val) {
            result = l1;
            l1 = l1.next;
        } else {
            result = l2;
            l2 = l2.next;
        }
        ListNode prev = result;
        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                prev.next = l1;
                l1 = l1.next;
                prev = prev.next;
                ;
            } else {
                prev.next = l2;
                l2 = l2.next;
                prev = prev.next;
            }
        }

        if (l1 != null) {
            prev.next = l1;
        }
        if (l2 != null) {
            prev.next = l2;
        }
        return result;
    }

    /**
     * 15,15% by memory, but it seems higher results were obtained in the old system.
     * Runtime: 0 ms
     * Memory Usage: 40.9 MB
     */
    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        }
        if (l2 == null) {
            return l1;
        }
        if (l1.val < l2.val) {
            l1.next = mergeTwoLists(l1.next, l2);
            return l1;
        } else {
            l2.next = mergeTwoLists(l1, l2.next);
            return l2;
        }
    }
}
