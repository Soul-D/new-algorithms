package leetcode.easy;

/**
 * 434. Number of Segments in a String
 *
 * @link https://leetcode.com/problems/number-of-segments-in-a-string/
 * @tags String.
 */
public class NumberOfSegmentsInAString {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Number of Segments in a String.
     * Memory Usage: 33.9 MB, less than 100.00% of Java online submissions for Number of Segments in a String.
     */
    public static int countSegments(String s) {
        int count = 0;
        boolean begin = true;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != ' ') {
                if (begin) {
                    count++;
                    begin = false;
                }
            } else {
                begin = true;

            }
        }
        return count;
    }
}
