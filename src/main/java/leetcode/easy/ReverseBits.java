package leetcode.easy;

/**
 * 190. Reverse Bits
 *
 * @link https://leetcode.com/problems/reverse-bits/
 * @tags Bit Manipulation.
 */
public class ReverseBits {
    /**
     * Some bit magic. Not mine solution.
     * <p>
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Reverse Bits.
     * Memory Usage: 30.7 MB, less than 7.32% of Java online submissions for Reverse Bits.
     */
    public static int reverseBits(int n) {
        if (n == 0) return 0;

        int result = 0;
        for (int i = 0; i < 32; i++) {
            result <<= 1;
            if ((n & 1) == 1) result++;
            n >>= 1;
        }
        return result;
    }

    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Reverse Bits.
     * Memory Usage: 29.4 MB, less than 7.32% of Java online submissions for Reverse Bits.
     */
    public static int reverseBitsBuiltIn(int n) {
        return Integer.reverse(n);
    }
}
