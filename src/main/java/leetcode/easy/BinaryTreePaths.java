package leetcode.easy;

import leetcode.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * 257. Binary Tree Paths
 *
 * @link https://leetcode.com/problems/binary-tree-paths/
 * @tags Tree, Depth-first search.
 */
public class BinaryTreePaths {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Binary Tree Paths.
     * Memory Usage: 36.4 MB, less than 100.00% of Java online submissions for Binary Tree Paths.
     */
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> result = new ArrayList<>();
        if (root != null) {
            String path = String.valueOf(root.val);
            addPath(path, result, root);
        }
        return result;
    }

    private void binaryTreePaths(String path, List<String> result, TreeNode root) {
        path = path + "->" + root.val;
        addPath(path, result, root);
    }

    private void addPath(String path, List<String> result, TreeNode root) {
        if (root.left == null && root.right == null) {
            result.add(path);
        }
        if (root.left != null) {
            binaryTreePaths(path, result, root.left);
        }
        if (root.right != null) {
            binaryTreePaths(path, result, root.right);
        }
    }
}
