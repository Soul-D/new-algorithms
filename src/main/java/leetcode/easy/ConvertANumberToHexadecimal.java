package leetcode.easy;

/**
 * 405. Convert a Number to Hexadecimal
 *
 * @link https://leetcode.com/problems/convert-a-number-to-hexadecimal/
 * @tags Bit Manipulation.
 */
public class ConvertANumberToHexadecimal {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Convert a Number to Hexadecimal.
     * Memory Usage: 33.7 MB, less than 100.00% of Java online submissions for Convert a Number to Hexadecimal.
     */
    public static String toHex(int num) {
        char[] map = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

        if (num == 0) return "0";
        StringBuilder result = new StringBuilder();
        while (num != 0) {
            result.append(map[(num & 15)]);
            num = (num >>> 4);
        }
        return result.reverse().toString();
    }
}
