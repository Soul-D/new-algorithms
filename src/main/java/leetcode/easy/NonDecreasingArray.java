package leetcode.easy;

/**
 * 665. Non-decreasing Array
 *
 * @link https://leetcode.com/problems/non-decreasing-array/
 * @tags Array.
 */
public class NonDecreasingArray {
    /**
     * Runtime: 1 ms, faster than 96.21% of Java online submissions for Non-decreasing Array.
     * Memory Usage: 50 MB, less than 9.52% of Java online submissions for Non-decreasing Array.
     */
    public static boolean checkPossibility(int[] nums) {
        int cnt = 0;
        for (int i = 1; i < nums.length && cnt <= 1; i++) {
            if (nums[i - 1] <= nums[i]) {
                continue;
            }
            cnt++;
            if (i - 2 < 0 || nums[i - 2] <= nums[i]) {
                nums[i - 1] = nums[i];
            } else {
                nums[i] = nums[i - 1];
            }
        }
        return cnt <= 1;
    }
}
