package leetcode.easy;

/**
 * 58. Length of Last Word
 *
 * @link https://leetcode.com/problems/length-of-last-word/description/
 * @tags String.
 */
public class LengthOfLastWord {
    /**
     * 100%
     * Runtime: 0 ms
     * Memory Usage: 35.6 MB
     */
    public int lengthOfLastWord(String s) {
        int length = 0;
        boolean hasWord = false;
        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) == ' ') {
                if (hasWord) {
                    break;
                }
            } else {
                hasWord = true;
                length++;
            }
        }
        return length;
    }
}
