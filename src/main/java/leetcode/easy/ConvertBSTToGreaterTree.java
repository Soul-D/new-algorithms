package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 538. Convert BST to Greater Tree
 *
 * @link https://leetcode.com/problems/convert-bst-to-greater-tree/
 * @tags Tree.
 */
public class ConvertBSTToGreaterTree {
    private static long sum;

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Convert BST to Greater Tree.
     * Memory Usage: 41.8 MB, less than 40.63% of Java online submissions for Convert BST to Greater Tree.
     */
    public static TreeNode convertBST(TreeNode root) {
        if (root == null) {
            return null;
        }
        sum = Long.MIN_VALUE;
        doWork(root);
        return root;
    }

    private static void doWork(TreeNode root) {
        if (root.right != null) {
            doWork(root.right);
        }
        if (sum == Long.MIN_VALUE) {
            sum = root.val;
        } else {
            root.val += sum;
            sum = root.val;
        }
        if (root.left != null) {
            doWork(root.left);
        }
    }
}
