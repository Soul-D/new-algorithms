package leetcode.easy;

/**
 * 66. Plus One
 *
 * @link https://leetcode.com/problems/plus-one/description/
 * @tags Array.
 */
public class PlusOne {
    /**
     * 100% time, 97,58% memory.
     * Runtime: 0 ms
     * Memory Usage: 35.2 MB
     */
    public int[] plusOne(int[] digits) {
        int add = 1;
        for (int i = digits.length - 1; i >= 0; i--) {
            int sum = digits[i] + add;
            add = sum / 10;
            digits[i] = sum % 10;
        }
        if (add == 1) {
            int[] res = new int[digits.length + 1];
            res[0] = 1;
            System.arraycopy(digits, 0, res, 1, digits.length);
            return res;
        }
        return digits;
    }
}
