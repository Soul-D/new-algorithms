package leetcode.easy;

/**
 * 88. Merge Sorted Array
 *
 * @link https://leetcode.com/problems/merge-sorted-array/description/
 * @tags Array, Two Pointers.
 */
public class MergeSortedArray {
    /**
     * Runtime: 0 ms
     * Memory Usage: 36.3 MB
     */
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int mInd = m, nInd = n;
        while (mInd > 0 && nInd > 0) {
            if (nums1[mInd - 1] > nums2[nInd - 1]) {
                nums1[mInd + nInd - 1] = nums1[mInd - 1];
                mInd--;
            } else {
                nums1[mInd + nInd - 1] = nums2[nInd - 1];
                nInd--;
            }
        }

        while (mInd > 0) {
            nums1[mInd - 1] = nums1[mInd - 1];
            mInd--;
        }

        while (nInd > 0) {
            nums1[nInd - 1] = nums2[nInd - 1];
            nInd--;
        }
    }
}
