package leetcode.easy;

/**
 * 121. Best Time to Buy and Sell Stock
 *
 * @link https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
 * @tags Array, Dynamic Programming.
 */
public class BestTimeToBuyAndSellStock {
    /**
     * Runtime: 1 ms, faster than 81.84% of Java online submissions for Best Time to Buy and Sell Stock.
     * Memory Usage: 36.2 MB, less than 100.00% of Java online submissions for Best Time to Buy and Sell Stock.
     */
    public static int maxProfit(int[] prices) {
        int profit = 0;
        int minPrice = Integer.MAX_VALUE;
        for (int i = 0; i < prices.length - 1; i++) {
            if (minPrice > prices[i]) {
                minPrice = prices[i];
            }
            int candidate = prices[i + 1] - minPrice;
            if (candidate > profit) {
                profit = candidate;
            }
        }

        return profit;
    }
}
