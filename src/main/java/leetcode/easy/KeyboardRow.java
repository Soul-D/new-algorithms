package leetcode.easy;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;

/**
 * 500. Keyboard Row
 *
 * @link https://leetcode.com/problems/keyboard-row/
 * @tags Hash Table.
 */
public class KeyboardRow {
    private final static Set<Set<Character>> rows = new HashSet<>();
    private static int[] lines = new int[]{1, 2, 2, 1, 0, 1, 1, 1, 0, 1, 1, 1, 2, 2, 0, 0, 0, 0, 1, 0, 0, 2, 0, 2, 0, 2};

    static {
        rows.add(new HashSet<>(asList('Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p')));
        rows.add(new HashSet<>(asList('A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l')));
        rows.add(new HashSet<>(asList('Z', 'X', 'C', 'V', 'B', 'N', 'M', 'z', 'x', 'c', 'v', 'b', 'n', 'm')));
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Keyboard Row.
     * Memory Usage: 34.7 MB, less than 100.00% of Java online submissions for Keyboard Row.
     */
    public static String[] findWordsFast(String[] words) {
        List<String> res = new ArrayList<>();
        for (String s : words) {
            boolean add = true;
            for (int i = 1; i < s.length(); i++) {
                if (lines[index(s.charAt(i))] != lines[index(s.charAt(0))]) {
                    add = false;
                    break;
                }
            }
            if (add) {
                res.add(s);
            }
        }
        return res.toArray(new String[0]);
    }

    private static boolean capital(char c) {
        return c >= 'A' && c <= 'Z';
    }

    private static int index(char c) {
        return capital(c) ? c - 'A' : c - 'a';
    }

    /**
     * Runtime: 1 ms, faster than 67.00% of Java online submissions for Keyboard Row.
     * Memory Usage: 34.6 MB, less than 100.00% of Java online submissions for Keyboard Row.
     */
    public static String[] findWords(String[] words) {
        int count = 0;
        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            Set<Character> row = getRow(word);
            for (int j = 1; j < word.length(); j++) {
                if (!row.contains(word.charAt(j))) {
                    words[i] = null;
                    break;
                }
            }
            if (words[i] != null) {
                count++;
            }
        }

        String[] result = new String[count];
        for (int i = words.length - 1; i >= 0; i--) {
            if (words[i] != null) {
                result[count-- - 1] = words[i];
            }
            if (count < 0) {
                break;
            }
        }
        return result;
    }

    private static Set<Character> getRow(String str) {
        for (Set<Character> row : rows) {
            if (row.contains(str.charAt(0))) {
                return row;
            }
        }
        throw new IllegalArgumentException("Probably corrupted string: " + str);
    }
}