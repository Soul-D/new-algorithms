package leetcode.easy;

/**
 * 67. Add Binary
 *
 * @link https://leetcode.com/problems/add-binary/description/
 * @tags Math, String.
 */
public class AddBinary {
    /**
     * 100% runtime
     * Runtime: 1 ms
     * Memory Usage: 36.1 MB
     */
    public static String addBinary(String a, String b) {
        int length = Math.min(a.length(), b.length());
        StringBuilder builder = new StringBuilder();
        int add = 0;
        for (int i = 0; i < length; i++) {
            int aCh = a.charAt(a.length() - 1 - i) - 48;
            int bCh = b.charAt(b.length() - 1 - i) - 48;
            int sum = aCh + bCh + add;
            add = sum / 2;
            builder.append(sum % 2);
        }
        for (int i = a.length() - 1 - length; i >= 0; i--) {
            int aCh = a.charAt(i) - 48;
            int sum = aCh + add;
            add = sum / 2;
            builder.append(sum % 2);
        }

        for (int i = b.length() - 1 - length; i >= 0; i--) {
            int bCh = b.charAt(i) - 48;
            int sum = bCh + add;
            add = sum / 2;
            builder.append(sum % 2);
        }

        if (add == 1) {
            builder.append(add);
        }

        return builder.reverse().toString();
    }
}
