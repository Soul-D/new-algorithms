package leetcode.easy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 506. Relative Ranks
 *
 * @link https://leetcode.com/problems/relative-ranks/
 */
public class RelativeRanks {
    /**
     * Runtime: 5 ms, faster than 88.77% of Java online submissions for Relative Ranks.
     * Memory Usage: 39.6 MB, less than 100.00% of Java online submissions for Relative Ranks.
     */
    public static String[] findRelativeRanks(int[] nums) {
        int[] sorted = Arrays.copyOf(nums, nums.length);
        Arrays.sort(sorted);
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < sorted.length; i++) {
            map.put(sorted[i], sorted.length - i);
        }
        String[] result = new String[nums.length];
        for (int i = 0; i < nums.length; i++) {
            result[i] = getPlace(nums[i], map);
        }

        return result;
    }

    private static String getPlace(int i, Map<Integer, Integer> map) {
        int place = map.get(i);
        if (place > 3) {
            return String.valueOf(place);
        }
        if (place == 1) {
            return "Gold Medal";
        } else if (place == 2) {
            return "Silver Medal";
        } else {
            return "Bronze Medal";
        }
    }
}
