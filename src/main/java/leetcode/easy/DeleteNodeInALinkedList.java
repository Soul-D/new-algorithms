package leetcode.easy;

import leetcode.model.ListNode;

/**
 * 237. Delete Node in a Linked List
 *
 * @link https://leetcode.com/problems/delete-node-in-a-linked-list/
 * @tags Linked List.
 */
public class DeleteNodeInALinkedList {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Delete Node in a Linked List.
     * Memory Usage: 36.6 MB, less than 100.00% of Java online submissions for Delete Node in a Linked List.
     */
    public void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }
}
