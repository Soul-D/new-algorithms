package leetcode.easy;

/**
 * 28. Implement strStr()
 *
 * @link https://leetcode.com/problems/implement-strstr/description/
 * @tags Two pointers, String
 */
public class ImplementStrStr {
    /**
     * 100%
     * Runtime: 0 ms
     * Memory Usage: 36 MB
     */
    public static int strStr1(String haystack, String needle) {
        return haystack.indexOf(needle);
    }

    /**
     * 67,60%
     * Runtime: 1 ms
     * Memory Usage: 37.9 MB
     */
    public static int strStr(String haystack, String needle) {
        for (int i = 0; i <= haystack.length() - needle.length(); i++) {
            if (haystack.substring(i).startsWith(needle)) {
                return i;
            }
        }
        return -1;
    }
}
