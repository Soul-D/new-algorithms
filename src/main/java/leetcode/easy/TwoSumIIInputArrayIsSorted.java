package leetcode.easy;

import java.util.Arrays;

/**
 * 167. Two Sum II - Input array is sorted
 *
 * @link https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/
 * @tags Array, Two Pointers, Binary Search.
 */
public class TwoSumIIInputArrayIsSorted {
    /**
     * Runtime: 1 ms, faster than 57.56% of Java online submissions for Two Sum II - Input array is sorted.
     * Memory Usage: 38.2 MB, less than 91.79% of Java online submissions for Two Sum II - Input array is sorted.
     */
    public int[] twoSum(int[] numbers, int target) {
        for (int i = 0; i < numbers.length; i++) {
            int toAdd = target - numbers[i];
            int ind = Arrays.binarySearch(numbers, toAdd);
            if (ind < 0) {
                continue;
            }
            if (ind != i) {
                return new int[]{i + 1, ind + 1};
            }
            if (ind < numbers.length - 1 && numbers[ind + 1] == toAdd) {
                return new int[]{i + 1, ind + 2};
            }
        }
        throw new IllegalArgumentException();
    }
}
