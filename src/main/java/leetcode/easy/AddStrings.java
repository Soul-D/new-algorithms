package leetcode.easy;

import java.util.Arrays;

/**
 * 415. Add Strings
 *
 * @link https://leetcode.com/problems/add-strings/
 * @tags String.
 */
public class AddStrings {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Add Strings.
     * Memory Usage: 36.2 MB, less than 100.00% of Java online submissions for Add Strings.
     */
    public static String addStrings(String num1, String num2) {
        int maxLength = Math.max(num1.length(), num2.length());
        int minLength = Math.min(num1.length(), num2.length());
        char[] chars = new char[maxLength + 1];
        int add = 0;
        for (int i = 0; i < minLength; i++) {
            int n = num1.charAt(num1.length() - 1 - i) + num2.charAt(num2.length() - 1 - i) - '0' - '0' + add;
            chars[maxLength - i] = (char) ((n % 10) + '0');
            add = n / 10;
        }

        for (int i = minLength; i < num1.length(); i++) {
            int n = num1.charAt(num1.length() - 1 - i) - '0' + add;
            chars[maxLength - i] = (char) ((n % 10) + '0');
            add = n / 10;
        }

        for (int i = minLength; i < num2.length(); i++) {
            int n = num2.charAt(num2.length() - 1 - i) - '0' + add;
            chars[maxLength - i] = (char) ((n % 10) + '0');
            add = n / 10;
        }
        char[] toConstr;
        if (add != 0) {
            chars[0] = (char) (add + '0');
            toConstr = chars;
        } else {
            toConstr = Arrays.copyOfRange(chars, 1, chars.length);
        }
        return new String(toConstr);
    }
}
