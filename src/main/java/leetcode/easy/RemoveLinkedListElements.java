package leetcode.easy;

import leetcode.model.ListNode;

/**
 * 203. Remove Linked List Elements
 *
 * @link https://leetcode.com/problems/remove-linked-list-elements/
 * @tags Linked List.
 */
public class RemoveLinkedListElements {
    /**
     * Runtime: 1 ms, faster than 98.81% of Java online submissions for Remove Linked List Elements.
     * Memory Usage: 39.9 MB, less than 100.00% of Java online submissions for Remove Linked List Elements.
     */
    public static ListNode removeElements(ListNode head, int val) {
        while (head != null && head.val == val) {
            head = head.next;
        }
        if (head == null) {
            return null;
        }

        ListNode prev = head;
        ListNode next = head.next;
        while (next != null) {
            if (next.val == val) {
                prev.next = next.next;
            } else {
                prev = prev.next;
            }

            next = next.next;
        }

        return head;
    }
}
