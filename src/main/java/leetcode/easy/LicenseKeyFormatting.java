package leetcode.easy;

/**
 * 482. License Key Formatting
 *
 * @link https://leetcode.com/problems/license-key-formatting/
 */
public class LicenseKeyFormatting {
    /**
     * Runtime: 11 ms, faster than 89.83% of Java online submissions for License Key Formatting.
     * Memory Usage: 38.1 MB, less than 100.00% of Java online submissions for License Key Formatting.
     */
    public static String licenseKeyFormatting(String S, int K) {
        StringBuilder builder = new StringBuilder();
        int l = K;
        for (int i = S.length() - 1; i >= 0; i--) {
            if (l == 0) {
                builder.append('-');
                l = K;
            }
            char ch = S.charAt(i);
            if (ch != '-') {
                builder.append(Character.toUpperCase(ch));
                l--;
            }
        }
        if (builder.length() != 0 && builder.charAt(builder.length() - 1) == '-') {
            builder.deleteCharAt(builder.length() - 1);
        }
        return builder.reverse().toString();
    }
}
