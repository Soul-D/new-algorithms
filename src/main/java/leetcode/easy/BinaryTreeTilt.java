package leetcode.easy;

import leetcode.model.TreeNode;

/**
 * 563. Binary Tree Tilt
 *
 * @link https://leetcode.com/problems/binary-tree-tilt/
 * @tags Tree.
 */
public class BinaryTreeTilt {
    private int tilts;

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Binary Tree Tilt.
     * Memory Usage: 39 MB, less than 63.64% of Java online submissions for Binary Tree Tilt.
     */
    public int findTiltFastest(TreeNode root) {
        if (root == null) {
            return 0;
        }
        getSum(root);
        return tilts;
    }

    private int getSum(TreeNode root) {
        if (root == null) {
            return 0;
        }

        int left = getSum(root.left);
        int right = getSum(root.right);
        tilts += Math.abs(left - right);
        return root.val + left + right;
    }

    /**
     * Runtime: 9 ms, faster than 15.79% of Java online submissions for Binary Tree Tilt.
     * Memory Usage: 37.4 MB, less than 100.00% of Java online submissions for Binary Tree Tilt.
     */
    public int findTiltSlow(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return (int) tilt(root);
    }

    private long sum(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return root.val + sum(root.left) + sum(root.right);
    }

    private long tilt(TreeNode root) {
        if (root == null) {
            return 0;
        }
        long left = sum(root.left);
        long right = sum(root.right);
        long currTilt = Math.abs(left - right);
        return currTilt + tilt(root.left) + tilt(root.right);
    }

    /**
     * Runtime: 1 ms, faster than 27.18% of Java online submissions for Binary Tree Tilt.
     * Memory Usage: 39.3 MB, less than 59.09% of Java online submissions for Binary Tree Tilt.
     */
    public int findTiltFaster(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return work(root).tilt;
    }

    private Result work(TreeNode root) {
        if (root == null) {
            return new Result();
        }
        Result left = work(root.left);
        Result right = work(root.right);
        int sum = root.val + left.sum + right.sum;
        int tilt = Math.abs(left.sum - right.sum);
        return new Result(sum, tilt + left.tilt + right.tilt);
    }

    private static class Result {
        public final int sum;
        public final int tilt;

        private Result(int sum, int tilt) {
            this.sum = sum;
            this.tilt = tilt;
        }

        private Result() {
            this.sum = 0;
            this.tilt = 0;
        }
    }
}
