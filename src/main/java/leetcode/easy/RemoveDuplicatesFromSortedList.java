package leetcode.easy;

import leetcode.model.ListNode;

/**
 * 83. Remove Duplicates from Sorted List
 *
 * @link https://leetcode.com/problems/remove-duplicates-from-sorted-list/description/
 * @tags Linked List.
 */
public class RemoveDuplicatesFromSortedList {
    /**
     * Runtime: 0 ms
     * Memory Usage: 36 MB
     */
    public static ListNode deleteDuplicates(ListNode head) {
        if (head == null) {
            return head;
        }
        ListNode result = head, prev = head;
        head = head.next;
        while (head != null) {
            if (prev.val == head.val) {
                prev.next = head.next;
            } else {
                prev = prev.next;
            }
            head = head.next;
        }

        return result;
    }
}
