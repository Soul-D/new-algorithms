package leetcode.easy;

/**
 * 374. Guess Number Higher or Lower
 *
 * @link https://leetcode.com/problems/guess-number-higher-or-lower/
 * @tags Binary Search.
 */
public class GuessNumberHigherOrLower {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Guess Number Higher or Lower.
     * Memory Usage: 32.9 MB, less than 100.00% of Java online submissions for Guess Number Higher or Lower.
     */
    public static int guessNumber(int n) {
        int low;
        int high;
        if (guess(n) == 0) {
            return n;
        } else if (guess(n) < 0) {
            low = 1;
            high = n;
        } else {
            low = n;
            high = Integer.MAX_VALUE;
        }

        while (low <= high) {
            int middle = (low + high) >>> 1;
            if (guess(middle) == 0) {
                return middle;
            }
            if (guess(middle) < 0) {
                high = middle - 1;
            } else {
                low = middle + 1;
            }
        }
        return low;
    }

    static int guess(int num) {
        int n = 6;
        return Integer.compare(n, num);
    }
}