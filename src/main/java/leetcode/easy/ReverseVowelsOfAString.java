package leetcode.easy;

/**
 * 345. Reverse Vowels of a String
 *
 * @link https://leetcode.com/problems/reverse-vowels-of-a-string/
 * @tags Two Pointers, String.
 */
public class ReverseVowelsOfAString {
    /**
     * Runtime: 2 ms, faster than 98.87% of Java online submissions for Reverse Vowels of a String.
     * Memory Usage: 37.1 MB, less than 100.00% of Java online submissions for Reverse Vowels of a String.
     */
    public static String reverseVowels(String s) {
        char[] chars = new char[s.length()];
        int start = 0;
        int end = s.length() - 1;
        char first = 0;
        char last = 0;
        while (start <= end) {
            char stCh = s.charAt(start);
            char eCh = s.charAt(end);
            if (isNotVowel(stCh)) {
                chars[start++] = stCh;
            } else {
                first = stCh;
            }
            if (isNotVowel(eCh)) {
                chars[end--] = eCh;
            } else {
                last = eCh;
            }

            if (first != 0 && last != 0) {
                chars[start] = last;
                chars[end] = first;
                first = last = 0;
                start++;
                end--;
            }
        }

        if (first != 0) {
            chars[start] = first;
        }
        if (last != 0) {
            chars[end] = last;
        }

        return new String(chars);
    }

    private static boolean isNotVowel(char ch) {
        return ch != 'a' && ch != 'e' && ch != 'u' && ch != 'o' && ch != 'i'
                && ch != 'A' && ch != 'E' && ch != 'U' && ch != 'O' && ch != 'I';
    }
}
