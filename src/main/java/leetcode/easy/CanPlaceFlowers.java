package leetcode.easy;

/**
 * 605. Can Place Flowers
 *
 * @link https://leetcode.com/problems/can-place-flowers/
 * @tags Array.
 */
public class CanPlaceFlowers {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Can Place Flowers.
     * Memory Usage: 39.5 MB, less than 96.43% of Java online submissions for Can Place Flowers.
     */
    public boolean canPlaceFlowers(int[] flowerbed, int n) {
        int prev = 0;
        for (int i = 0; i < flowerbed.length - 1; i++) {
            if (flowerbed[i] != 1 & prev == 0 && flowerbed[i + 1] == 0) {
                n--;
                prev = 1;
            } else {
                prev = flowerbed[i];
            }
        }
        if (prev == 0 && flowerbed[flowerbed.length - 1] == 0) {
            n--;
        }
        return n <= 0;
    }
}
