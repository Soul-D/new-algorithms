package leetcode.easy;

/**
 * 657. Robot Return to Origin
 *
 * @link https://leetcode.com/problems/robot-return-to-origin/
 * @tags String.
 */
public class RobotReturnToOrigin {
    /**
     * Runtime: 7 ms, faster than 63.40% of Java online submissions for Robot Return to Origin.
     * Memory Usage: 45.4 MB, less than 5.40% of Java online submissions for Robot Return to Origin.
     */
    public boolean judgeCircle(String moves) {
        int x = 0;
        int y = 0;
        for (int i = 0; i < moves.length(); i++) {
            switch (moves.charAt(i)) {
                case 'U':
                    y++;
                    break;
                case 'D':
                    y--;
                    break;
                case 'L':
                    x--;
                    break;
                case 'R':
                    x++;
                    break;
            }
        }
        return x == 0 && y == 0;
    }
}
