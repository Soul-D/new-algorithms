package leetcode.easy;

import java.util.Arrays;

/**
 * 532. K-diff Pairs in an Array
 *
 * @link https://leetcode.com/problems/k-diff-pairs-in-an-array/
 * @tags Arrays, Two Pointers.
 */
public class KDiffPairsInAnArray {
    /**
     * Runtime: 4 ms, faster than 99.49% of Java online submissions for K-diff Pairs in an Array.
     * Memory Usage: 39.5 MB, less than 94.74% of Java online submissions for K-diff Pairs in an Array.
     */
    public static int findPairsLittleFaster(int[] nums, int k) {
        if (k < 0) {
            return 0;
        }
        Arrays.sort(nums);
        int left = 0, right = 1, count = 0;
        while (right < nums.length) {
            if (nums[right] - nums[left] > k) {
                left++;
            } else if (nums[right] - nums[left] < k || right == left) {
                right++;
            } else {
                count++;
                left++;
                right++;
                while (right < nums.length && nums[right] == nums[right - 1]) {
                    right++;
                }
            }
        }
        return count;
    }

    /**
     * Runtime: 5 ms, faster than 94.50% of Java online submissions for K-diff Pairs in an Array.
     * Memory Usage: 40.1 MB, less than 89.47% of Java online submissions for K-diff Pairs in an Array.
     */
    public static int findPairs(int[] nums, int k) {
        if (k < 0 || nums.length == 0) {
            return 0;
        }
        Arrays.sort(nums);
        int count = 0;
        int prev = nums[0];

        if (Arrays.binarySearch(nums, prev + k) > 0) {
            count++;
        }

        boolean checked = count != 0;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == prev && (k != 0 || checked)) {
                continue;
            }

            int ind = Arrays.binarySearch(nums, nums[i] + k);
            if (ind >= 0 && ind != i) {
                count++;
            }
            checked = nums[i] == prev;
            prev = nums[i];
        }
        return count;
    }
}
