package leetcode.easy;

import java.util.ArrayList;
import java.util.List;

/**
 * 401. Binary Watch
 *
 * @link https://leetcode.com/problems/binary-watch/
 * @tags Backtracking, Bit Manipulation.
 */
public class BinaryWatch {
    /**
     * Runtime: 11 ms, faster than 39.10% of Java online submissions for Binary Watch.
     * Memory Usage: 36.7 MB, less than 100.00% of Java online submissions for Binary Watch
     */
    public List<String> readBinaryWatch(int num) {
        List<String> times = new ArrayList<>();
        for (int h = 0; h < 12; h++) {
            for (int m = 0; m < 60; m++) {
                if (Integer.bitCount(h * 64 + m) == num) {
                    times.add(String.format("%d:%02d", h, m));
                }
            }
        }
        return times;
    }
}
