package leetcode.easy;

/**
 * 371. Sum of Two Integers
 *
 * @link https://leetcode.com/problems/sum-of-two-integers/
 * @tags Bit Manipulation.
 */
public class SumOfTwoIntegers {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Sum of Two Integers.
     * Memory Usage: 33 MB, less than 6.67% of Java online submissions for Sum of Two Integers.
     *
     * @see https://stackoverflow.com/questions/38557464/sum-of-two-integers-without-using-operator-in-python
     */
    public static int getSum(int a, int b) {
        int oldA;
        while (b != 0) {
            oldA = a;
            a = a ^ b;
            b = (oldA & b) << 1;
        }
        return a;
    }
}
