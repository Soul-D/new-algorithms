package leetcode.easy;

/**
 * 453. Minimum Moves to Equal Array Elements
 *
 * @link https://leetcode.com/problems/minimum-moves-to-equal-array-elements/
 * @tags Math.
 */
public class MinimumMovesToEqualArrayElements {
    /**
     * Runtime: 2 ms, faster than 89.02% of Java online submissions for Minimum Moves to Equal Array Elements.
     * Memory Usage: 40.3 MB, less than 100.00% of Java online submissions for Minimum Moves to Equal Array Elements.
     */
    public static int minMoves(int[] nums) {
        if (nums == null || nums.length < 2) {
            return 0;
        }
        int min = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] < min) {
                min = nums[i];
            }
        }

        int result = 0;

        for (int num : nums) {
            result += num - min;
        }
        return result;
    }
}
