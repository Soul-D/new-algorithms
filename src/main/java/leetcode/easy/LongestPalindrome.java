package leetcode.easy;

/**
 * 409. Longest Palindrome
 *
 * @link https://leetcode.com/problems/longest-palindrome/
 * @tags Hash Table.
 */
public class LongestPalindrome {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Longest Palindrome.
     * Memory Usage: 34.9 MB, less than 100.00% of Java online submissions for Longest Palindrome.
     */
    public static int longestPalindrome(String s) {
        int[] counts = new int[58];
        for (int i = 0; i < s.length(); i++) {
            counts[s.charAt(i) - 'A']++;
        }
        int length = 0;
        boolean isOddNotCounted = true;
        for (int i = counts.length - 1; i >= 0; i--) {
            if (counts[i] % 2 == 0) {
                length += counts[i];
            } else {
                if (isOddNotCounted) {
                    length += counts[i];
                    isOddNotCounted = false;
                } else {
                    length += (counts[i] - 1);
                }
            }
        }
        return length;
    }
}
