package leetcode.easy;

/**
 * 598. Range Addition II
 *
 * @link https://leetcode.com/problems/range-addition-ii/
 * @tags Math.
 */
public class RangeAdditionII {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Range Addition II.
     * Memory Usage: 37.9 MB, less than 20.00% of Java online submissions for Range Addition II.
     */
    public static int maxCount(int m, int n, int[][] ops) {
        if (ops.length == 0 || ops[0].length == 0) {
            return m * n;
        }
        int mixM = Integer.MAX_VALUE;
        int mixN = Integer.MAX_VALUE;
        for (int[] op : ops) {
            if (op[0] < mixM) {
                mixM = op[0];
            }
            if (op[1] < mixN) {
                mixN = op[1];
            }
        }

        return mixM * mixN;
    }
}
