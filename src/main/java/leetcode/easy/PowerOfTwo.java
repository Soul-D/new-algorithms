package leetcode.easy;

/**
 * 231. Power of Two
 *
 * @link https://leetcode.com/problems/power-of-two/
 * @tags Math, Bit Manipulation.
 */
public class PowerOfTwo {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Power of Two.
     * Memory Usage: 33.8 MB, less than 7.32% of Java online submissions for Power of Two.
     *
     * @see http://graphics.stanford.edu/~seander/bithacks.html
     */
    public boolean isPowerOfTwo(int n) {
        return n > 0 && (n & (n - 1)) == 0;
    }
}
