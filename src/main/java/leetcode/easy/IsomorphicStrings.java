package leetcode.easy;

import java.util.HashMap;
import java.util.Map;

/**
 * 205. Isomorphic Strings
 *
 * @link https://leetcode.com/problems/isomorphic-strings/
 * @tags Hash Table.
 */
public class IsomorphicStrings {

    /**
     * Runtime: 14 ms, faster than 29.31% of Java online submissions for Isomorphic Strings.
     * Memory Usage: 37.3 MB, less than 100.00% of Java online submissions for Isomorphic Strings.
     */
    public boolean isIsomorphic(String s, String t) {
        Map<Character, Integer> first = new HashMap<>();
        Map<Character, Integer> second = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            Integer sInd = first.put(s.charAt(i), i);
            Integer tInd = second.put(t.charAt(i), i);
            if (sInd == null && tInd == null) {
                continue;
            }
            if (sInd == null || !sInd.equals(tInd)) {
                return false;
            }

        }
        return true;
    }
}
