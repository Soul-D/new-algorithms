package leetcode.easy;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 202. Happy Number
 *
 * @link https://leetcode.com/problems/oneMoreCache-number/
 * @tags Hash Table, Math.
 */
public class HappyNumber {
    private final static Set<Integer> cache;

    static {
        cache = new HashSet<>();
        cache.add(4);
        cache.add(16);
        cache.add(17);
        cache.add(20);
        cache.add(25);
        cache.add(29);
        cache.add(37);
        cache.add(41);
        cache.add(42);
        cache.add(50);
        cache.add(58);
        cache.add(85);
        cache.add(89);
        cache.add(99);
        cache.add(145);
        cache.add(162);
    }

    /**
     * Runtime: 1 ms, faster than 97.68% of Java online submissions for Happy Number.
     * Memory Usage: 33.6 MB, less than 10.60% of Java online submissions for Happy Number.
     */
    public static boolean isHappyWithCache(int n) {
        while (!cache.contains(n)) {
            n = convert(n);
            if (n == 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * Runtime: 2 ms, faster than 66.94% of Java online submissions for Happy Number.
     * Memory Usage: 33.7 MB, less than 10.60% of Java online submissions for Happy Number.
     */
    public static boolean isHappySlow(int n) {
        Set<Integer> integers = new HashSet<>();
        while (integers.add(n)) {
            n = convert(n);
            if (n == 1) {
                return true;
            }
        }
        String collect = integers.stream().sorted().map(Objects::toString)
                .map(x -> "oneMoreCache[" + x + "] = true;")
                .collect(Collectors.joining("\n"));
        System.out.println(collect);
        return false;
    }

    private static int convert(int n) {
        int res = 0;
        while (n != 0) {
            int rest = n % 10;
            res += (rest * rest);
            n = n / 10;
        }
        return res;
    }
}
