package leetcode.easy;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * 496. Next Greater Element I
 *
 * @link https://leetcode.com/problems/next-greater-element-i/
 * @tags Stack.
 */
public class NextGreaterElementI {
    /**
     * Runtime: 3 ms, faster than 79.77% of Java online submissions for Next Greater Element I.
     * Memory Usage: 37.4 MB, less than 100.00% of Java online submissions for Next Greater Element I.
     */
    public static int[] nextGreaterElement(int[] nums1, int[] nums2) {
        Map<Integer, Integer> map = new HashMap<>();
        Stack<Integer> stack = new Stack<>();
        for (int num : nums2) {
            while (!stack.isEmpty() && stack.peek() < num) {
                map.put(stack.pop(), num);
            }
            stack.push(num);
        }
        for (int i = 0; i < nums1.length; i++) {
            nums1[i] = map.getOrDefault(nums1[i], -1);
        }
        return nums1;
    }
}
