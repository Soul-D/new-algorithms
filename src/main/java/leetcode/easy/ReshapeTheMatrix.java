package leetcode.easy;

/**
 * 566. Reshape the Matrix
 *
 * @link https://leetcode.com/problems/reshape-the-matrix/
 * @tags Array.
 */
public class ReshapeTheMatrix {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Reshape the Matrix.
     * Memory Usage: 38.1 MB, less than 100.00% of Java online submissions for Reshape the Matrix.
     */
    public static int[][] matrixReshape(int[][] nums, int r, int c) {
        if (r * c == 0 || r * c != nums.length * nums[0].length) {
            return nums;
        }
        int colSize = nums[0].length;
        int[][] result = new int[r][c];
        int currEl = 0;
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                int row = currEl / colSize;
                int col = currEl % colSize;
                result[i][j] = nums[row][col];
                currEl++;
            }
        }
        return result;
    }
}
