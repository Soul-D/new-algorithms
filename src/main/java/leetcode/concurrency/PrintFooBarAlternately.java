package leetcode.concurrency;

import java.util.concurrent.Semaphore;

/**
 * 1115. Print FooBar Alternately
 *
 * @level medium
 * @link https://leetcode.com/problems/print-foobar-alternately/description/
 */
public class PrintFooBarAlternately {

    /**
     * Runtime: 16 ms, faster than 88.55% of Java online submissions for Print FooBar Alternately.
     * Memory Usage: 36.6 MB, less than 100.00% of Java online submissions for Print FooBar Alternately.
     */
    static class FooBar {
        private Semaphore foo;
        private Semaphore bar;

        private int n;

        public FooBar(int n) {
            this.n = n;
            foo = new Semaphore(1);
            bar = new Semaphore(0);
        }

        public void foo(Runnable printFoo) throws InterruptedException {

            for (int i = 0; i < n; i++) {

                // printFoo.run() outputs "foo". Do not change or remove this line.
                foo.acquire();
                printFoo.run();
                bar.release();
            }
        }

        public void bar(Runnable printBar) throws InterruptedException {

            for (int i = 0; i < n; i++) {

                // printBar.run() outputs "bar". Do not change or remove this line.
                bar.acquire();
                printBar.run();
                foo.release();
            }

        }
    }
}