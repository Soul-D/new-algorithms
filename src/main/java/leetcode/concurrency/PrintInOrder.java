package leetcode.concurrency;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 1114. Print in Order
 *
 * @level easy
 * @link https://leetcode.com/problems/print-in-order/description/
 */
public class PrintInOrder {
    /**
     * Runtime: 10 ms, faster than 48.60% of Java online submissions for Print in Order.
     * Memory Usage: 36 MB, less than 100.00% of Java online submissions for Print in Order.
     */
    public static class Foo {
        private AtomicBoolean first;
        private AtomicBoolean second;

        public Foo() {
            first = new AtomicBoolean(false);
            second = new AtomicBoolean(false);
        }

        public void first(Runnable printFirst) {
            printFirst.run();
            first.set(true);
        }

        public void second(Runnable printSecond) throws InterruptedException {
            while (!first.get()) {
                Thread.sleep(20);
            }
            printSecond.run();
            second.set(true);
        }

        public void third(Runnable printThird) throws InterruptedException {
            while (!second.get()) {
                Thread.sleep(20);
            }
            printThird.run();
        }
    }
}
