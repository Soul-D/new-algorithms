package leetcode.hard;

/**
 * 65. Valid Number
 *
 * @link https://leetcode.com/problems/valid-number/
 * @tags Math, String.
 */
public class ValidNumber {
    /**
     * Runtime: 2 ms, faster than 80.31% of Java online submissions for Valid Number.
     * Memory Usage: 38.1 MB, less than 6.25% of Java online submissions for Valid Number.
     */
    public boolean isNumber(String s) {
        s = s.trim();

        boolean hasDot = false;
        boolean hasExp = false;
        boolean hasNumber = false;
        boolean afterExp = true;
        for (int i = 0; i < s.length(); i++) {
            if ('0' <= s.charAt(i) && s.charAt(i) <= '9') {
                hasNumber = true;
                afterExp = true;
            } else if (s.charAt(i) == '.') {
                if (hasExp || hasDot) {
                    return false;
                }
                hasDot = true;
            } else if (s.charAt(i) == 'e') {
                if (hasExp || !hasNumber) {
                    return false;
                }
                afterExp = false;
                hasExp = true;
            } else if (s.charAt(i) == '-' || s.charAt(i) == '+') {
                if (i != 0 && s.charAt(i - 1) != 'e') {
                    return false;
                }
            } else {
                return false;
            }
        }

        return hasNumber && afterExp;
    }
}
