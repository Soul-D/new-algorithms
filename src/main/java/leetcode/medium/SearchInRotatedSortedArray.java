package leetcode.medium;

import java.util.Arrays;

/**
 * 33. Search in Rotated Sorted Array
 *
 * @link https://leetcode.com/problems/search-in-rotated-sorted-array/
 * @tags Array, Binary Search.
 */
public class SearchInRotatedSortedArray {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Search in Rotated Sorted Array.
     * Memory Usage: 38.2 MB, less than 45.91% of Java online submissions for Search in Rotated Sorted Array.
     */
    public static int search(int[] nums, int target) {
        int length = nums.length - 1;
        int left = 0;
        int right = length;
        // find the index of the smallest value using binary search.
        while (left < right) {
            int mid = (left + right) / 2;
            if (nums[mid] > nums[right]) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }

        int iLeft = Arrays.binarySearch(nums, 0, left, target);
        if (iLeft < 0) {
            int iRight = Arrays.binarySearch(nums, left, nums.length, target);
            return iRight >= 0 ? iRight : -1;
        } else {
            return iLeft;
        }
    }
}
