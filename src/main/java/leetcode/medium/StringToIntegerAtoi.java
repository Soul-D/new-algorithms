package leetcode.medium;

/**
 * 8. String to Integer (atoi)
 *
 * @link https://leetcode.com/problems/string-to-integer-atoi/
 * @tags Math, String.
 */
public class StringToIntegerAtoi {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for String to Integer (atoi).
     * Memory Usage: 38.6 MB, less than 5.59% of Java online submissions for String to Integer (atoi).
     */
    public static int myAtoi(String str) {
        int i = 0;
        for (; i < str.length(); i++) {
            if (str.charAt(i) != ' ') {
                break;
            }
        }
        if (i == str.length()) {
            return 0;
        }
        int multiplier = 1;
        if (str.charAt(i) == '-') {
            multiplier = -1;
            i++;
        } else if (str.charAt(i) == '+') {
            i++;
        }

        long num = 0;
        while (i < str.length()) {
            int c = str.charAt(i++) - '0';
            if (c < 0 || c > 10) {
                break;
            }
            num = num * 10 + c;
            if (multiplier * num <= Integer.MIN_VALUE) {
                return Integer.MIN_VALUE;
            }
            if (multiplier * num >= Integer.MAX_VALUE) {
                return Integer.MAX_VALUE;
            }
        }

        return (int) (num * multiplier);
    }
}
