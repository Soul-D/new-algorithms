package leetcode.medium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 18. 4Sum
 *
 * @link https://leetcode.com/problems/4sum/
 * @tags Array, Hash Table, Two Pointers.
 */
public class FourSum {
    /**
     * Runtime: 2 ms, faster than 100.00% of Java online submissions for 4Sum.
     * Memory Usage: 39.7 MB, less than 68.12% of Java online submissions for 4Sum.
     */
    public static List<List<Integer>> fourSum(int[] nums, int target) {
        Arrays.sort(nums);
        List<List<Integer>> result = new ArrayList<>();
        for (int i = 0; i < nums.length - 3; i++) {
            if (nums[i] + nums[nums.length - 1] + nums[nums.length - 2] + nums[nums.length - 3] < target) {
                continue;
            }

            if (i > 0 && nums[i - 1] == nums[i]) {
                continue;
            }
            int currTarget = target - nums[i];
            for (int j = i + 1; j < nums.length - 2; j++) {
                if (nums[j] + nums[j + 1] + nums[j + 2] > currTarget) {
                    break;
                }

                if (j > i + 1 && nums[j - 1] == nums[j]) {
                    continue;
                }
                int distance = currTarget - nums[j];
                int left = j + 1, leftStart = j + 1;
                int right = nums.length - 1, rightStart = nums.length - 1;
                while (left < right) {
                    if (left > leftStart && nums[left - 1] == nums[left]) {
                        left++;
                        continue;
                    }
                    if (right < rightStart && nums[right] == nums[right + 1]) {
                        right--;
                        continue;
                    }

                    if (distance == nums[left] + nums[right]) {
                        result.add(Arrays.asList(nums[i], nums[j], nums[left], nums[right]));
                        left++;
                        right--;
                    } else if (distance > nums[left] + nums[right]) {
                        left++;
                    } else {
                        right--;
                    }
                }
            }
        }
        return result;
    }
}
