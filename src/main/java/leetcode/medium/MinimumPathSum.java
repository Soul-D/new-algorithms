package leetcode.medium;

/**
 * 64. Minimum Path Sum
 *
 * @link https://leetcode.com/problems/minimum-path-sum/
 * @tags Array, Dynamic Programming.
 */
public class MinimumPathSum {
    /**
     * Runtime: 2 ms, faster than 88.23% of Java online submissions for Minimum Path Sum.
     * Memory Usage: 42.2 MB, less than 83.78% of Java online submissions for Minimum Path Sum.
     */
    public int minPathSum(int[][] grid) {
        int rows = grid.length;
        int cols = grid[0].length;
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                if (row == 0 && col == 0) {
                    continue;
                }
                if (row == 0) {
                    grid[row][col] += grid[row][col - 1];
                } else if (col == 0) {
                    grid[row][col] += grid[row - 1][col];
                } else {
                    grid[row][col] += Math.min(grid[row][col - 1], grid[row - 1][col]);
                }
            }
        }
        return grid[rows - 1][cols - 1];
    }
}
