package leetcode.medium;

/**
 * 5. Longest Palindromic Substring
 *
 * @link https://leetcode.com/problems/longest-palindromic-substring/
 * @tags String, Dynamic Programming.
 */
public class LongestPalindromicSubstring {
    /**
     * Runtime: 22 ms, faster than 59.82% of Java online submissions for Longest Palindromic Substring.
     * Memory Usage: 37.4 MB, less than 72.98% of Java online submissions for Longest Palindromic Substring.
     */
    public static String longestPalindrome(String s) {
        if (s.length() < 2) {
            return s;
        }
        // container[0] - start, container[1] - max length
        int[] container = new int[2];
        for (int i = 0; i < s.length() - 1; i++) {
            doWork(s, i, i, container);
            doWork(s, i, i + 1, container);
        }

        return s.substring(container[0], container[0] + container[1]);
    }

    private static void doWork(String s, int left, int right, int[] container) {

        while (left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
            left--;
            right++;
        }
        if (right - left - 1 > container[1]) {
            container[0] = left + 1;
            container[1] = right - left - 1;
        }
    }

    /**
     * Time Limit Exceeded
     */
    public static String longestPalindromeSlow(String s) {
        if (s.length() < 2) {
            return s;
        }
        int max = 0;
        int start = 0;
        int end = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int next = i;
            while (next >= 0) {
                next = s.indexOf(c, next + 1);
                if (isPalindrome(s, i, next)) {
                    if (next - i + 1 > max) {
                        max = Math.max(max, next - i + 1);
                        start = i;
                        end = next;
                    }
                }
            }
        }
        return s.substring(start, end + 1);
    }

    private static boolean isPalindrome(String s, int left, int right) {
        while (left < right) {
            if (s.charAt(left++) != s.charAt(right--)) {
                return false;
            }
        }
        return true;
    }
}
