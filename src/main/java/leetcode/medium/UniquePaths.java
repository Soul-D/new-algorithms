package leetcode.medium;

/**
 * 62. Unique Paths
 *
 * @link https://leetcode.com/problems/unique-paths/
 * @tags Array, Dynamic Programming.
 */
public class UniquePaths {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Unique Paths.
     * Memory Usage: 36.7 MB, less than 5.10% of Java online submissions for Unique Paths.
     */
    public static int uniquePaths(int m, int n) {
        int totalMoves = n + m - 2;
        int moves = m - 1;
        double res = 1;
        for (int i = 1; i <= moves; i++) {
            res = res * (totalMoves - moves + i) / i;
        }
        return (int)res;
    }
}
