package leetcode.medium;

import leetcode.model.ListNode;

/**
 * 19. Remove Nth Node From End of List
 *
 * @link https://leetcode.com/problems/remove-nth-node-from-end-of-list/
 * @tags Linked List, Two Pointers.
 */
public class RemoveNthNodeFromEndOfList {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Remove Nth Node From End of List.
     * Memory Usage: 37.7 MB, less than 6.37% of Java online submissions for Remove Nth Node From End of List.
     */
    public static ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode first = head;
        while (n != 0 && first != null) {
            first = first.next;
            n--;
        }

        if (first == null) {
            return head.next;
        } else {
            first = first.next;
        }
        ListNode second = head;
        while (first != null) {
            first = first.next;
            second = second.next;
        }
        second.next = second.next.next;
        return head;
    }
}
