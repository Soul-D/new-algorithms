package leetcode.medium;

import leetcode.model.ListNode;

import java.util.Stack;

/**
 * 445. Add Two Numbers II
 *
 * @link https://leetcode.com/problems/add-two-numbers-ii/
 * @tags Linked List.
 */
public class AddTwoNumbersII {
    /**
     * Runtime: 3 ms, faster than 73.45% of Java online submissions for Add Two Numbers II.
     * Memory Usage: 40.9 MB, less than 79.41% of Java online submissions for Add Two Numbers II.
     */
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        Stack<Integer> stack1 = new Stack<>();
        Stack<Integer> stack2 = new Stack<>();
        while (l1 != null) {
            stack1.push(l1.val);
            l1 = l1.next;
        }
        while (l2 != null) {
            stack2.push(l2.val);
            l2 = l2.next;
        }
        ListNode head = null;
        int add = 0;
        while (!stack1.empty() && !stack2.empty()) {
            int val = stack1.pop() + stack2.pop() + add;
            ListNode curr = new ListNode(val % 10);
            add = val / 10;
            if (head != null) {
                curr.next = head;
            }
            head = curr;
        }

        while (!stack2.empty()) {
            int val = stack2.pop() + add;
            ListNode curr = new ListNode(val % 10);
            add = val / 10;
            if (head != null) {
                curr.next = head;
            }
            head = curr;
        }

        while (!stack1.empty()) {
            int val = stack1.pop() + add;
            ListNode curr = new ListNode(val % 10);
            add = val / 10;
            if (head != null) {
                curr.next = head;
            }
            head = curr;
        }

        if (add != 0) {
            ListNode curr = new ListNode(add);
            curr.next = head;
            head = curr;
        }
        return head;
    }
}
