package leetcode.medium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 47. Permutations II
 *
 * @link https://leetcode.com/problems/permutations-ii/
 * @tags Backtracking.
 */
public class PermutationsII {
    /**
     * Runtime: 1 ms, faster than 99.65% of Java online submissions for Permutations II.
     * Memory Usage: 41.3 MB, less than 13.43% of Java online submissions for Permutations II.
     */
    public static List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(result, new ArrayList<>(nums.length + 1), nums, new boolean[nums.length]);
        return result;
    }

    private static void backtrack(List<List<Integer>> result, List<Integer> list, int[] nums, boolean[] used) {
        if (list.size() == nums.length) {
            result.add(new ArrayList<>(list));
            return;
        }

        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if (used[i]) {
                continue;
            }
            if (i > 0 && nums[i - 1] == num && !used[i - 1]) {
                continue;
            }
            list.add(num);
            used[i] = true;
            backtrack(result, list, nums, used);
            used[i] = false;
            list.remove(list.size() - 1);
        }
    }
}
