package leetcode.medium;

import java.util.List;

/**
 * 139. Word Break
 *
 * @link https://leetcode.com/problems/word-break/
 * @tags Dynamic Programming.
 */
public class WordBreak {
    /**
     * Runtime: 1 ms, faster than 99.37% of Java online submissions for Word Break.
     * Memory Usage: 37.9 MB, less than 21.74% of Java online submissions for Word Break.
     */
    public static boolean wordBreak(String s, List<String> wordDict) {
        boolean[] track = new boolean[s.length() + 1];
        track[0] = true;
        for (int i = 0; i < s.length(); i++) {
            if (!track[i]) {
                continue;
            }
            for (String word : wordDict) {
                if (s.startsWith(word, i)) {
                    track[i + word.length()] = true;
                }
            }
        }
        return track[s.length()];
    }
}
