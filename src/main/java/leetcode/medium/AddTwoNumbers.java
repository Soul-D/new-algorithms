package leetcode.medium;

import leetcode.model.ListNode;

/**
 * 2. Add Two Numbers
 *
 * @link https://leetcode.com/problems/add-two-numbers/
 * @tags Linked List, Math.
 */
public class AddTwoNumbers {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Add Two Numbers.
     * Memory Usage: 41.1 MB, less than 91.22% of Java online submissions for Add Two Numbers.
     */
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int add = 0;
        ListNode head = null;
        ListNode tail = null;
        ListNode temp = null;
        while (l1 != null && l2 != null) {
            ListNode newHead = new ListNode((l1.val + l2.val + add) % 10);
            add = (l1.val + l2.val + add) / 10;
            if (head == null) {
                head = newHead;
                tail = head;
            } else {
                tail.next = newHead;
                tail = newHead;
            }
            l1 = l1.next;
            l2 = l2.next;
        }

        while (l1 != null) {
            ListNode newHead = new ListNode((l1.val + add) % 10);
            add = (l1.val + add) / 10;
            if (head == null) {
                head = newHead;
                tail = head;
            } else {
                tail.next = newHead;
                tail = newHead;
            }
            l1 = l1.next;
        }

        while (l2 != null) {
            ListNode newHead = new ListNode((l2.val + add) % 10);
            add = (l2.val + add) / 10;
            if (head == null) {
                head = newHead;
                tail = head;
            } else {
                tail.next = newHead;
                tail = newHead;
            }
            l2 = l2.next;
        }

        if (add != 0) {
            tail.next = new ListNode(add);
        }

        return head;
    }
}
