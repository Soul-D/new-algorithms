package leetcode.medium;

/**
 * 11. Container With Most Water
 *
 * @link https://leetcode.com/problems/container-with-most-water/
 * @tags Array, Two Pointers.
 */
public class ContainerWithMostWater {
    /**
     * Runtime: 2 ms, faster than 95.86% of Java online submissions for Container With Most Water.
     * Memory Usage: 41.2 MB, less than 7.69% of Java online submissions for Container With Most Water.
     */
    public static int maxArea(int[] height) {
        int left = 0;
        int right = height.length - 1;
        int maxArea = 0;

        while (left < right) {
            maxArea = Math.max(maxArea, Math.min(height[left], height[right]) * (right - left));
            if (height[left] < height[right]) {
                left++;
            } else {
                right--;
            }
        }

        return maxArea;
    }
}
