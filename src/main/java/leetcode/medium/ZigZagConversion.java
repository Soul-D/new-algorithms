package leetcode.medium;

/**
 * 6. ZigZag Conversion
 *
 * @link https://leetcode.com/problems/zigzag-conversion/
 * @tags String.
 */
public class ZigZagConversion {
    /**
     * Runtime: 4 ms, faster than 82.73% of Java online submissions for ZigZag Conversion.
     * Memory Usage: 41.3 MB, less than 22.34% of Java online submissions for ZigZag Conversion.
     */
    public static String convert(String s, int numRows) {
        StringBuilder[] builders = new StringBuilder[numRows];
        for (int i = 0; i < numRows; i++) {
            builders[i] = new StringBuilder();
        }

        for (int i = 0; i < s.length(); ) {
            for (int j = 0; j < numRows && i < s.length(); j++) {
                builders[j].append(s.charAt(i++));
            }
            for (int j = numRows - 2; j >= 1 && i < s.length(); j--) {
                builders[j].append(s.charAt(i++));
            }
        }
        for (int i = 1; i < builders.length; i++) {
            builders[0].append(builders[i]);
        }
        return builders[0].toString();
    }
}
