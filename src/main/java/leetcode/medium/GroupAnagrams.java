package leetcode.medium;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 49. Group Anagrams
 *
 * @link https://leetcode.com/problems/group-anagrams/
 * @tags Hash Table, String.
 */
public class GroupAnagrams {
    private static int[] primes = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29,
            31, 37, 41, 43, 47, 53, 59, 61, 67,
            71, 73, 79, 83, 89, 97, 101};

    /**
     * Runtime: 3 ms, faster than 100.00% of Java online submissions for Group Anagrams.
     * Memory Usage: 43.9 MB, less than 48.54% of Java online submissions for Group Anagrams.
     */
    public static List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> result = new ArrayList<>();
        if (strs.length == 0) {
            return result;
        }

        Map<Integer, List<String>> hashes = new HashMap<>();
        for (String str : strs) {
            int hash = hash(str);
            List<String> list = hashes.get(hash);
            if (list == null) {
                list = new ArrayList<>();
                hashes.put(hash, list);
                result.add(list);
            }
            list.add(str);
        }
        return result;
    }

    private static int hash(String s) {
        int i = 1;
        for (int j = 0; j < s.length(); j++) {
            i *= primes[s.charAt(j) - 'a'];
        }
        return i;
    }

    /**
     * Runtime: 816 ms, faster than 5.04% of Java online submissions for Group Anagrams.
     * Memory Usage: 43.6 MB, less than 61.99% of Java online submissions for Group Anagrams.
     */
    public static List<List<String>> groupAnagramsSlow(String[] strs) {
        boolean[] used = new boolean[strs.length];
        List<List<String>> result = new ArrayList<>();
        for (int i = 0; i < strs.length; i++) {
            if (used[i]) {
                continue;
            }
            String str = strs[i];
            List<String> curr = new ArrayList<>();
            curr.add(str);
            for (int j = i + 1; j < strs.length; j++) {
                if (used[j]) {
                    continue;
                }
                if (isAnagrams(str, strs[j])) {
                    curr.add(strs[j]);
                    used[j] = true;
                }
            }
            result.add(curr);
        }
        return result;
    }

    private static boolean isAnagrams(String a, String b) {
        if (a.length() != b.length()) {
            return false;
        }
        int[] counts = new int[26];
        for (int i = 0; i < a.length(); i++) {
            counts[a.charAt(i) - 'a']++;
            counts[b.charAt(i) - 'a']--;
        }
        for (int count : counts) {
            if (count != 0) {
                return false;
            }
        }
        return true;
    }
}
