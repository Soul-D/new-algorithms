package leetcode.medium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 56. Merge Intervals
 *
 * @link https://leetcode.com/problems/merge-intervals/
 * @tags Array, Sort.
 */
public class MergeIntervals {
    /**
     * Runtime: 5 ms, faster than 93.57% of Java online submissions for Merge Intervals.
     * Memory Usage: 42.2 MB, less than 49.28% of Java online submissions for Merge Intervals.
     */
    public static int[][] merge(int[][] intervals) {
        if (intervals.length == 0) {
            return new int[0][];
        }
        Comparator<int[]> c = (o1, o2) -> {
            if (o1[0] != o2[0]) {
                return o1[0] - o2[0];
            } else {
                return o1[1] - o2[1];
            }
        };
        Arrays.sort(intervals, c);
        List<int[]> result = new ArrayList<>();
        int start = intervals[0][0];
        int end = intervals[0][1];
        for (int i = 1; i < intervals.length; i++) {
            int mayBeStart = intervals[i][0];
            int mayBeEnd = intervals[i][1];
            if (mayBeStart == start) {
                end = mayBeEnd;
            } else if (mayBeStart <= end) {
                end = Math.max(mayBeEnd, end);
            } else {
                result.add(new int[]{start, end});
                start = mayBeStart;
                end = mayBeEnd;
            }
        }

        result.add(new int[]{start, end});

        return result.toArray(new int[0][]);
    }
}
