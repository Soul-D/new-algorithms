package leetcode.medium;

import java.util.Arrays;

/**
 * 31. Next Permutation
 *
 * @link https://leetcode.com/problems/next-permutation/
 * @tags Array.
 */
public class NextPermutation {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Next Permutation.
     * Memory Usage: 39.1 MB, less than 50.00% of Java online submissions for Next Permutation.
     */
    public static void nextPermutationFaster(int[] nums) {
        int i = nums.length - 2;
        while (i >= 0 && nums[i] >= nums[i + 1]) {
            i--;
        }
        if (i >= 0) {
            int j = nums.length - 1;
            while (j >= 0 && nums[j] <= nums[i]) {
                j--;
            }
            swap(nums, i, j);
        }
        reverse(nums, i + 1);
    }

    private static void reverse(int[] nums, int start) {
        int i = start, j = nums.length - 1;
        while (i < j) {
            swap(nums, i, j);
            i++;
            j--;
        }
    }

    /**
     * Runtime: 1 ms, faster than 58.37% of Java online submissions for Next Permutation.
     * Memory Usage: 39.7 MB, less than 48.00% of Java online submissions for Next Permutation.
     */
    public static void nextPermutation(int[] nums) {
        int i;
        for (i = nums.length - 1; i > 0; i--) {
            if (nums[i] > nums[i - 1]) {
                break;
            }
        }
        if (i == 0) {
            Arrays.sort(nums);
            return;
        }

        int x = nums[i - 1], min = i;
        for (int j = i + 1; j < nums.length; j++) {
            if (nums[j] > x && nums[j] < nums[min]) {
                min = j;
            }
        }

        swap(nums, i - 1, min);
        Arrays.sort(nums, i, nums.length);
    }

    private static void swap(int[] ints, int a, int b) {
        int tmp = ints[a];
        ints[a] = ints[b];
        ints[b] = tmp;
    }
}
