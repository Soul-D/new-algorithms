package leetcode.medium;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 60. Permutation Sequence
 *
 * @link https://leetcode.com/problems/permutation-sequence/
 * @tags Math, Backtracking.
 */
public class PermutationSequence {
    /**
     * Runtime: 1 ms, faster than 97.77% of Java online submissions for Permutation Sequence.
     * Memory Usage: 36.8 MB, less than 20.83% of Java online submissions for Permutation Sequence.
     */
    public static String getPermutation(int n, int k) {
        List<Integer> numbers = new ArrayList<>();
        StringBuilder sb = new StringBuilder();

        int[] factorial = factorials(n);

        for (int i = 1; i <= n; i++) {
            numbers.add(i);
        }

        k--;

        for (int i = 1; i <= n; i++) {
            int index = k / factorial[n - i];
            sb.append(numbers.get(index));
            numbers.remove(index);
            k -= index * factorial[n - i];
        }

        return sb.toString();
    }

    private static int[] factorials(int n) {
        int[] factorial = new int[n + 1];
        int sum = 1;
        factorial[0] = 1;
        for (int i = 1; i <= n; i++) {
            sum *= i;
            factorial[i] = sum;
        }
        return factorial;
    }

    /**
     * Time Limit Exceeded
     */
    public static String getPermutationSlow(int n, int k) {
        LinkedList<String> queue = new LinkedList<>();
        queue.add("");
        while (queue.peek().length() != n) {
            String remove = queue.remove();
            for (int i = 1; i <= n; i++) {
                if (remove.indexOf((char) (i + '0')) >= 0) {
                    continue;
                }
                queue.addLast(remove + i);
            }
        }
        return queue.get(k - 1);
    }
}
