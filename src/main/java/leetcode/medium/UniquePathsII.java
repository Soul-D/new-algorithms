package leetcode.medium;

/**
 * 63. Unique Paths II
 *
 * @link https://leetcode.com/problems/unique-paths-ii/
 * @tags Array, Dynamic Programming.
 */
public class UniquePathsII {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Unique Paths II.
     * Memory Usage: 37.8 MB, less than 100.00% of Java online submissions for Unique Paths II.
     */
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int width = obstacleGrid[0].length;
        int[] dp = new int[width];
        dp[0] = 1;
        for (int[] row : obstacleGrid) {
            for (int j = 0; j < width; j++) {
                if (row[j] == 1) {
                    dp[j] = 0;
                } else if (j > 0) {
                    dp[j] += dp[j - 1];
                }
            }
        }
        return dp[width - 1];
    }
}
