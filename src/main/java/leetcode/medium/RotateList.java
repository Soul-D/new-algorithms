package leetcode.medium;

import leetcode.model.ListNode;

/**
 * 61. Rotate List
 *
 * @list https://leetcode.com/problems/rotate-list/
 * @tags Linked List, Two Pointers.
 */
public class RotateList {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Rotate List.
     * Memory Usage: 38.8 MB, less than 46.55% of Java online submissions for Rotate List.
     */
    public static ListNode rotateRight(ListNode head, int k) {
        int depth = getDepth(head);
        if (depth < 2) {
            return head;
        }

        ListNode first = head;
        ListNode second = head;
        k = k % depth;

        if (k == 0) {
            return head;
        }

        while (k != 0) {
            first = first.next;
            k--;
        }

        while (first.next != null) {
            first = first.next;
            second = second.next;
        }
        ListNode newHead = second.next;
        second.next = null;
        first.next = head;
        return newHead;
    }

    private static int getDepth(ListNode head) {
        int depth = 0;
        while (head != null) {
            depth++;
            head = head.next;
        }
        return depth;
    }
}
