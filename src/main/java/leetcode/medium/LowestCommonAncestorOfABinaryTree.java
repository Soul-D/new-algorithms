package leetcode.medium;

import leetcode.model.TreeNode;

/**
 * 236. Lowest Common Ancestor of a Binary Tree
 *
 * @link https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/description/
 * @tags Tree.
 */
public class LowestCommonAncestorOfABinaryTree {
    /**
     * Runtime: 6 ms, faster than 76.20% of Java online submissions for Lowest Common Ancestor of a Binary Tree.
     * Memory Usage: 35.1 MB, less than 5.55% of Java online submissions for Lowest Common Ancestor of a Binary Tree.
     */
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) {
            return null;
        }
        if (root == p) {
            return root;
        }
        if (root == q) {
            return root;
        }
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);
        if (left != null && right != null) {
            return root;
        } else {
            return left == null ? right : left;
        }
    }
}
