package leetcode.medium;

/**
 * 75. Sort Colors
 *
 * @link https://leetcode.com/problems/sort-colors/
 * @tags Array, Two Pointers, Sort.
 */
public class SortColors {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Sort Colors.
     * Memory Usage: 38.3 MB, less than 5.51% of Java online submissions for Sort Colors.
     */
    public static void sortColorsCount(int[] nums) {
        int zeros = 0;
        int ones = 0;
        for (int num : nums) {
            if (num == 0) {
                zeros++;
            } else if (num == 1) {
                ones++;
            }
        }

        for (int i = 0; i < nums.length; i++) {
            if (zeros > 0) {
                nums[i] = 0;
                zeros--;
            } else if (ones > 0) {
                nums[i] = 1;
                ones--;
            } else {
                nums[i] = 2;
            }
        }
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Sort Colors.
     * Memory Usage: 38.2 MB, less than 5.51% of Java online submissions for Sort Colors.
     */
    public static void sortColorsTwoPointers(int[] nums) {
        int left = 0;
        int right = nums.length - 1;
        int i = 0;
        while (left <= right && i < nums.length) {
            if (nums[i] == 0 && i > left) {
                swap(left++, i, nums);
            } else if (nums[i] == 2 && i < right) {
                swap(right--, i, nums);
            } else {
                i++;
            }
        }
    }

    private static void swap(int a, int b, int[] ints) {
        int tmp = ints[a];
        ints[a] = ints[b];
        ints[b] = tmp;
    }
}
