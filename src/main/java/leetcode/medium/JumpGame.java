package leetcode.medium;

/**
 * 55. Jump Game
 *
 * @link https://leetcode.com/problems/jump-game/
 * @tags Array, Greedy.
 */
public class JumpGame {
    /**
     * Runtime: 1 ms, faster than 97.90% of Java online submissions for Jump Game.
     * Memory Usage: 41.2 MB, less than 45.30% of Java online submissions for Jump Game.
     */
    public static boolean canJump(int[] nums) {
        int max = 0;
        for (int i = 0; i < nums.length; i++) {
            if (i > max) {
                return false;
            }
            max = Math.max(nums[i] + i, max);
        }
        return true;
    }
}
