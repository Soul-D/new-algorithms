package leetcode.medium;

/**
 * 59. Spiral Matrix II
 *
 * @link https://leetcode.com/problems/spiral-matrix-ii/
 * @tags Array.
 */
public class SpiralMatrixII {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Spiral Matrix II.
     * Memory Usage: 37.7 MB, less than 8.33% of Java online submissions for Spiral Matrix II.
     */
    public static int[][] generateMatrix(int n) {
        int[][] result = initialize(n);
        int left = 0;
        int right = n - 1;
        int up = 0;
        int down = n - 1;
        int cell = 1;

        while (left <= right && up <= down) {
            for (int i = left; i <= right; i++) {
                result[up][i] = cell++;
            }
            up++;

            for (int i = up; i <= down; i++) {
                result[i][right] = cell++;
            }
            right--;

            if (up <= down) {
                for (int i = right; i >= left; i--) {
                    result[down][i] = cell++;
                }
            }
            down--;

            if (left <= right) {
                for (int i = down; i >= up; i--) {
                    result[i][left] = cell++;
                }
            }
            left++;
        }
        return result;
    }

    private static int[][] initialize(int n) {
        int[][] result = new int[n][];
        for (int i = 0; i < result.length; i++) {
            result[i] = new int[n];
        }
        return result;
    }
}
