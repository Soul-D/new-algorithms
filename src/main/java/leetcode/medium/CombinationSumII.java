package leetcode.medium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 40. Combination Sum II
 *
 * @link https://leetcode.com/problems/combination-sum-ii/
 * @tags Array, Backtracking.
 */
public class CombinationSumII {
    /**
     * Runtime: 4 ms, faster than 69.04% of Java online submissions for Combination Sum II.
     * Memory Usage: 39 MB, less than 68.42% of Java online submissions for Combination Sum II.
     */
    public static List<List<Integer>> combinationSum2(int[] candidates, int target) {
        Arrays.sort(candidates);
        List<List<Integer>> result = new ArrayList<>();
        combine(candidates, 0, target, new ArrayList<>(), result);
        return result;
    }

    private static void combine(int[] candidates, int index, int target, List<Integer> current, List<List<Integer>> result) {
        if (target == 0) {
            result.add(new ArrayList<>(current));
            return;
        }
        if (target < 0) {
            return;
        }
        for (int i = index; i < candidates.length; i++) {
            if (i > index && candidates[i] == candidates[i - 1]) {
                continue;
            }
            current.add(candidates[i]);
            combine(candidates, i + 1, target - candidates[i], current, result);
            current.remove(current.size() - 1);
        }
    }
}
