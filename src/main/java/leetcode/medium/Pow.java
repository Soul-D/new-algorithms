package leetcode.medium;

/**
 * 50. Pow(x, n)
 *
 * @link https://leetcode.com/problems/powx-n/
 * @tags Math, Binary Search.
 */
public class Pow {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Pow(x, n).
     * Memory Usage: 36.2 MB, less than 5.88% of Java online submissions for Pow(x, n).
     */
    public static double myPow(double x, int n) {
        if (n == 0) {
            return 1;
        }
        if (n < 0) {
            if (n == Integer.MIN_VALUE) {
                n = Integer.MAX_VALUE;
                x = 1 / (x * x);
            } else {
                n = -n;
                x = 1 / x;
            }
        }
        return (n % 2 == 0) ? myPow(x * x, n / 2) : x * myPow(x * x, n / 2);
    }
}
