package leetcode.medium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 78. Subsets
 *
 * @link https://leetcode.com/problems/subsets/
 * @tags Array, Backtracking, Bit manipulation.
 */
public class Subsets {
    /**
     * Runtime: 1 ms, faster than 57.60% of Java online submissions for Subsets.
     * Memory Usage: 39.2 MB, less than 5.74% of Java online submissions for Subsets.
     */
    public static List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        Arrays.sort(nums);
        work(result, new ArrayList<>(), 0, nums);
        return result;
    }

    private static void work(List<List<Integer>> result, List<Integer> current, int start, int[] ints) {
        result.add(new ArrayList<>(current));
        for (int i = start; i < ints.length; i++) {
            current.add(ints[i]);
            work(result, current, i + 1, ints);
            current.remove(current.size() - 1);
        }
    }
}
