package leetcode.medium;

import java.util.Arrays;

/**
 * 16. 3Sum Closest
 *
 * @link https://leetcode.com/problems/3sum-closest/
 * @tags Array, Two Pointers.
 */
public class ThreeSumClosest {
    /**
     * Runtime: 4 ms, faster than 91.25% of Java online submissions for 3Sum Closest.
     * Memory Usage: 38.8 MB, less than 6.67% of Java online submissions for 3Sum Closest.
     */
    public static int threeSumClosest(int[] nums, int target) {
        int currMinDistance = Integer.MAX_VALUE;
        int maxSum = 0;
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i++) {
            int left = i + 1, right = nums.length - 1;
            while (left < right) {
                int distance = Math.abs(target - nums[i] - nums[left] - nums[right]);
                int currSum = nums[i] + nums[left] + nums[right];
                if (distance == 0) {
                    return target;
                }
                if (distance < currMinDistance) {
                    currMinDistance = distance;
                    maxSum = currSum;
                }
                if (currSum > target) {
                    right--;
                } else {
                    left++;
                }
            }
        }
        return maxSum;
    }
}
