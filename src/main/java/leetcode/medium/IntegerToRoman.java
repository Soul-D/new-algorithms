package leetcode.medium;

/**
 * 12. Integer to Roman
 *
 * @link https://leetcode.com/problems/integer-to-roman/
 * @tags Math, String.
 */
public class IntegerToRoman {
    private static String[][] strings = new String[][]{
            new String[]{"M", "MM", "MMM"},
            new String[]{"C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"},
            new String[]{"X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"},
            new String[]{"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"}
    };

    /**
     * Runtime: 4 ms, faster than 86.69% of Java online submissions for Integer to Roman.
     * Memory Usage: 41.2 MB, less than 11.25% of Java online submissions for Integer to Roman.
     */
    public static String intToRomanOne(int num) {
        int multiplier = 0;
        int divider = 1000;
        StringBuilder builder = new StringBuilder();
        while (num != 0) {
            int d = (int) (divider / Math.pow(10, multiplier));
            int curr = num / d;
            if (curr != 0) {
                builder.append(strings[multiplier][curr - 1]);
            }
            num = num % d;
            multiplier++;
        }
        return builder.toString();
    }

    private static int[] values = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
    private static String[] strs = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

    /**
     * Runtime: 3 ms, faster than 100.00% of Java online submissions for Integer to Roman.
     * Memory Usage: 40.8 MB, less than 11.25% of Java online submissions for Integer to Roman.
     */
    public static String intToRomanTwo(int num) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < values.length; i++) {
            while (num >= values[i]) {
                num -= values[i];
                sb.append(strs[i]);
            }
        }
        return sb.toString();
    }
}
