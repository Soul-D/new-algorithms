package leetcode.medium;

import java.util.HashMap;
import java.util.Map;

/**
 * 3. Longest Substring Without Repeating Characters
 *
 * @link https://leetcode.com/problems/longest-substring-without-repeating-characters/
 * @tags Hash Table, Two Pointers, String, Sliding Window.
 */
public class LongestSubstringWithoutRepeatingCharacters {
    /**
     * Runtime: 3 ms, faster than 93.16% of Java online submissions for Longest Substring Without Repeating Characters.
     * Memory Usage: 38.9 MB, less than 27.37% of Java online submissions for Longest Substring Without Repeating Characters.
     */
    public static int lengthOfLongestSubstringFaster(String s) {
        if (s.length() == 0) {
            return 0;
        }
        int[] indexes = new int[128];
        int max = 0;
        int curr = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            curr = Math.max(curr, indexes[c]);
            max = Math.max(max, i - curr + 1);
            indexes[c] = i + 1;
        }
        return max;
    }

    /**
     * Runtime: 5 ms, faster than 88.14% of Java online submissions for Longest Substring Without Repeating Characters.
     * Memory Usage: 41.1 MB, less than 5.20% of Java online submissions for Longest Substring Without Repeating Characters.
     */
    public static int lengthOfLongestSubstring(String s) {
        if (s.length() < 2) {
            return s.length();
        }
        int max = 0;
        int start = 0;
        int currLength = 1;
        Map<Character, Integer> map = new HashMap<>();
        map.put(s.charAt(0), 0);
        for (int i = 1; i < s.length(); i++) {
            Integer index = map.put(s.charAt(i), i);
            if (index != null) {
                for (int j = start; j < index; j++) {
                    map.remove(s.charAt(j));
                }
                start = index + 1;
                max = Math.max(max, currLength);
                currLength = map.size();
            } else {
                currLength++;
            }
        }
        max = Math.max(max, currLength);
        return max;
    }
}
