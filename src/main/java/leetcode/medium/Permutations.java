package leetcode.medium;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 46. Permutations
 *
 * @link https://leetcode.com/problems/permutations/
 * @tags Backtracking.
 */
public class Permutations {
    /**
     * Runtime: 10 ms, faster than 5.01% of Java online submissions for Permutations.
     * Memory Usage: 42.4 MB, less than 5.68% of Java online submissions for Permutations.
     */
    public static List<List<Integer>> permuteTooSlow(int[] nums) {
        LinkedList<Map.Entry<List<Integer>, Set<Integer>>> result = new LinkedList<>();
        result.add(new AbstractMap.SimpleEntry<>(new ArrayList<>(), new HashSet<>()));
        while (result.peek().getKey().size() != nums.length) {
            Map.Entry<List<Integer>, Set<Integer>> remove = result.remove();
            for (int num : nums) {
                Set<Integer> set = new HashSet<>(remove.getValue());
                if (set.add(num)) {
                    List<Integer> integers = new ArrayList<>(remove.getKey());
                    integers.add(num);
                    result.add(new AbstractMap.SimpleEntry<>(integers, set));
                }
            }
        }
        return result.stream().map(Map.Entry::getKey).collect(Collectors.toList());
    }

    /**
     * Runtime: 3 ms, faster than 21.96% of Java online submissions for Permutations.
     * Memory Usage: 41.2 MB, less than 5.68% of Java online submissions for Permutations.
     */
    public static List<List<Integer>> permuteFaster(int[] nums) {
        LinkedList<List<Integer>> result = new LinkedList<>();
        result.add(new ArrayList<>());
        while (result.peek().size() != nums.length) {
            List<Integer> remove = result.remove();
            for (int num : nums) {
                List<Integer> copy = new ArrayList<>(remove);
                if (!copy.contains(num)) {
                    copy.add(num);
                    result.add(copy);
                }
            }
        }
        return result;
    }

    /**
     * Runtime: 1 ms, faster than 92.85% of Java online submissions for Permutations.
     * Memory Usage: 41.1 MB, less than 5.68% of Java online submissions for Permutations.
     */
    public static List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        backtrack(result, new ArrayList<>(nums.length + 1), nums);
        return result;
    }

    private static void backtrack(List<List<Integer>> result, List<Integer> list, int[] nums) {
        if (list.size() == nums.length) {
            result.add(new ArrayList<>(list));
            return;
        }

        for (int num : nums) {
            if (list.contains(num)) {
                continue;
            }
            list.add(num);
            backtrack(result, list, nums);
            list.remove(list.size() - 1);
        }
    }
}
