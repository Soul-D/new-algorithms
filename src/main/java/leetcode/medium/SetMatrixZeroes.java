package leetcode.medium;

import java.util.Arrays;

/**
 * 73. Set Matrix Zeroes
 *
 * @link https://leetcode.com/problems/set-matrix-zeroes/
 * @tags Array.
 */
public class SetMatrixZeroes {
    /**
     * Runtime: 1 ms, faster than 91.65% of Java online submissions for Set Matrix Zeroes.
     * Memory Usage: 42.5 MB, less than 95.71% of Java online submissions for Set Matrix Zeroes.
     */
    public static void setZeroes(int[][] matrix) {
        if (matrix.length == 0 || matrix[0].length == 0) {
            return;
        }
        boolean[] columns = new boolean[matrix[0].length];
        for (int[] ints : matrix) {
            boolean shouldBeZero = false;
            for (int i = 0; i < ints.length; i++) {
                if (ints[i] == 0) {
                    columns[i] = true;
                    shouldBeZero = true;
                }
            }
            if (shouldBeZero) {
                Arrays.fill(ints, 0);
            }
        }

        for (int i = 0; i < columns.length; i++) {
            if (columns[i]) {
                for (int j = 0; j < matrix.length; j++) {
                    matrix[j][i] = 0;
                }
            }
        }
    }
}
