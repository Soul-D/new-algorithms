package leetcode.medium;

import leetcode.model.ListNode;

/**
 * 24. Swap Nodes in Pairs
 *
 * @link https://leetcode.com/problems/swap-nodes-in-pairs/
 * @tags Linked List.
 */
public class SwapNodesInPairs {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Swap Nodes in Pairs.
     * Memory Usage: 37.5 MB, less than 5.50% of Java online submissions for Swap Nodes in Pairs.
     */
    public static ListNode swapPairs(ListNode head) {
        ListNode result;
        ListNode curr;
        ListNode prev;
        if (head != null && head.next != null) {
            curr = head.next.next;
            ListNode tmp = head.next;
            result = head.next;
            prev = head;
            head.next = head.next.next;
            tmp.next = head;

        } else {
            return head;
        }

        while (curr != null && curr.next != null) {
            head = curr;
            prev.next = curr.next;
            curr.next = curr.next.next;
            prev.next.next = curr;
            prev = head;
            curr = prev.next;
        }
        return result;
    }
}
