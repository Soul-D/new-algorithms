package leetcode.medium;

import java.util.Arrays;

/**
 * 74. Search a 2D Matrix
 *
 * @link https://leetcode.com/problems/search-a-2d-matrix/
 * @tags Array, Binary Search.
 */
public class SearchA2DMatrix {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Search a 2D Matrix.
     * Memory Usage: 41.8 MB, less than 6.06% of Java online submissions for Search a 2D Matrix.
     */
    public static boolean searchMatrix(int[][] matrix, int target) {
        if (matrix.length == 0 || matrix[0].length == 0) {
            return false;
        }
        int[] boards = new int[matrix.length * 2];
        for (int i = 0; i < matrix.length; i++) {
            int[] m = matrix[i];
            boards[i * 2] = m[0];
            boards[i * 2 + 1] = m[m.length - 1];
        }
        int search = Arrays.binarySearch(boards, target);
        if (search >= 0) {
            return true;
        }
        int ind = -(search + 1);
        if (ind % 2 == 0 || ind > boards.length - 1) {
            return false;
        }

        int targetArray = ind / 2;
        return Arrays.binarySearch(matrix[targetArray], target) >= 0;
    }
}
