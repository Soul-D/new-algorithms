package leetcode.medium;

import java.util.Stack;

/**
 * 71. Simplify Path
 *
 * @link https://leetcode.com/problems/simplify-path/
 * @tags String, Stack.
 */
public class SimplifyPath {
    /**
     * Runtime: 4 ms, faster than 94.14% of Java online submissions for Simplify Path.
     * Memory Usage: 39.5 MB, less than 13.33% of Java online submissions for Simplify Path.
     */
    public static String simplifyPath(String path) {
        StringBuilder builder = new StringBuilder();
        Stack<String> stack = new Stack<>();
        String[] split = path.split("/");
        int skip = 0;
        for (int i = split.length - 1; i >= 0; i--) {
            if (split[i].length() == 0 || split[i].equals(".")) {
                continue;
            }
            if (split[i].equals("..")) {
                skip++;
                continue;
            }
            if (skip != 0) {
                skip--;
            } else {
                stack.push(split[i]);
            }
        }

        while (!stack.empty()) {
            String pop = stack.pop();
            builder.append('/').append(pop);
        }

        if (builder.length() == 0) {
            return "/";
        } else {
            return builder.toString();
        }
    }
}
