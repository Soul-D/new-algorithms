package leetcode.medium;

/**
 * 29. Divide Two Integers
 *
 * @link https://leetcode.com/problems/divide-two-integers/
 * @tags Math, Binary Search.
 */
public class DivideTwoIntegers {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Divide Two Integers.
     * Memory Usage: 37.2 MB, less than 6.06% of Java online submissions for Divide Two Integers.
     */
    public int divide(int dividend, int divisor) {
        int sign = 1;
        if ((dividend > 0 && divisor < 0) || (dividend < 0 && divisor > 0)) {
            sign = -1;
        }
        long longDividend = Math.abs((long) dividend);
        long longDivisor = Math.abs((long) divisor);

        if (longDivisor == 0) {
            return Integer.MAX_VALUE;
        }

        if ((longDividend == 0) || (longDividend < longDivisor)) {
            return 0;
        }

        long longAns = longDivide(longDividend, longDivisor);

        int ans;
        if (longAns > Integer.MAX_VALUE) {
            ans = sign == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
        } else {
            ans = (int) (sign * longAns);
        }
        return ans;
    }

    private long longDivide(long longDividend, long longDivisor) {
        if (longDividend < longDivisor) {
            return 0;
        }

        long sum = longDivisor;
        long multiple = 1;
        while ((sum + sum) <= longDividend) {
            sum += sum;
            multiple += multiple;
        }
        return multiple + longDivide(longDividend - sum, longDivisor);
    }
}
