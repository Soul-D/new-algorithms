package leetcode.medium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 39. Combination Sum
 *
 * @link https://leetcode.com/problems/combination-sum/
 * @tags Array, Backtracking.
 */
public class CombinationSum {
    /**
     * Runtime: 2 ms, faster than 99.70% of Java online submissions for Combination Sum.
     * Memory Usage: 41.2 MB, less than 5.19% of Java online submissions for Combination Sum.
     */
    public static List<List<Integer>> combinationSum(int[] candidates, int target) {
        Arrays.sort(candidates);
        List<List<Integer>> result = new ArrayList<>();
        getResult(result, new ArrayList<>(), candidates, target, 0);

        return result;
    }

    private static void getResult(List<List<Integer>> result, List<Integer> cur, int[] candidates, int target, int start) {
        if (target == 0) {
            result.add(new ArrayList<>(cur));
            return;
        }
        for (int i = start; i < candidates.length && target >= candidates[i]; i++) {
            cur.add(candidates[i]);
            getResult(result, cur, candidates, target - candidates[i], i);
            cur.remove(cur.size() - 1);
        }
    }
}
