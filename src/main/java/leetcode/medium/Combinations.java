package leetcode.medium;

import java.util.ArrayList;
import java.util.List;

/**
 * 77. Combinations
 *
 * @link https://leetcode.com/problems/combinations/
 * @tags Backtracking.
 */
public class Combinations {
    /**
     * Runtime: 18 ms, faster than 62.84% of Java online submissions for Combinations.
     * Memory Usage: 42.9 MB, less than 6.52% of Java online submissions for Combinations.
     */
    public static List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> result = new ArrayList<>();
        work(result, new ArrayList<>(), 1, n, k);
        return result;
    }

    private static void work(List<List<Integer>> result, List<Integer> current, int start, int n, int k) {
        if (k == 0) {
            result.add(new ArrayList<>(current));
            return;
        }
        for (int i = start; i <= n; i++) {
            current.add(i);
            work(result, current, i + 1, n, k - 1);
            current.remove(current.size() - 1);
        }
    }
}
