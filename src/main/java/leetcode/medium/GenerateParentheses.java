package leetcode.medium;

import java.util.ArrayList;
import java.util.List;

/**
 * 22. Generate Parentheses
 *
 * @link https://leetcode.com/problems/generate-parentheses/
 * @tags String, Backtracking.
 */
public class GenerateParentheses {
    /**
     * Runtime: 1 ms, faster than 92.46% of Java online submissions for Generate Parentheses.
     * Memory Usage: 39.3 MB, less than 21.29% of Java online submissions for Generate Parentheses.
     */
    public static List<String> generateParenthesis(int n) {
        List<String> list = new ArrayList<>();
        generateParenthesis(list, "", 0, 0, n);
        return list;
    }

    private static void generateParenthesis(List<String> list, String str, int open, int close, int max) {
        if (str.length() == max * 2) {
            list.add(str);
            return;
        }

        if (open < max) {
            generateParenthesis(list, str + "(", open + 1, close, max);
        }
        if (close < open) {
            generateParenthesis(list, str + ")", open, close + 1, max);
        }
    }
}
