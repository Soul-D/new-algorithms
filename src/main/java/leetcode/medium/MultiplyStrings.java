package leetcode.medium;

/**
 * 43. Multiply Strings
 *
 * @link https://leetcode.com/problems/multiply-strings/
 * @tags Math, String.
 */
public class MultiplyStrings {
    /**
     * Runtime: 3 ms, faster than 96.03% of Java online submissions for Multiply Strings.
     * Memory Usage: 38.1 MB, less than 26.67% of Java online submissions for Multiply Strings.
     */
    public static String multiply(String num1, String num2) {
        if (num1.equals("0") || num2.equals("0")) {
            return "0";
        }
        int[] storage = new int[num1.length() + num2.length()];
        int add = 0;
        for (int i = num1.length() - 1; i >= 0; i--) {
            for (int j = num2.length() - 1; j >= 0; j--) {
                int currStorage = storage.length - 1 - (num2.length() - 1 - j) - (num1.length() - 1 - i);
                int a = num1.charAt(i) - '0';
                int b = num2.charAt(j) - '0';

                int c = storage[currStorage];

                int val = a * b + c;
                storage[currStorage] = (val + add) % 10;
                add = (val + add) / 10;
            }
            if (add != 0) {
                storage[storage.length - 1 - (num2.length()) - (num1.length() - 1 - i)] = add;
                add = 0;
            }
        }

        int index = 0;
        while (index < storage.length && storage[index] == 0) {
            index++;
        }

        char[] chars = new char[storage.length - index];
        int charIndex = 0;
        for (; index < storage.length; index++) {
            chars[charIndex++] = (char) (storage[index] + '0');
        }
        return new String(chars);
    }
}
