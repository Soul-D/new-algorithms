package leetcode.medium;

/**
 * 79. Word Search
 *
 * @link https://leetcode.com/problems/word-search/
 * @tags Array, Backtracking.
 */
public class WordSearch {
    /**
     * Runtime: 4 ms, faster than 98.61% of Java online submissions for Word Search.
     * Memory Usage: 42 MB, less than 23.47% of Java online submissions for Word Search.
     */
    public static boolean exist(char[][] board, String word) {
        char[] w = word.toCharArray();
        for (int row = 0; row < board.length; row++) {
            for (int col = 0; col < board[row].length; col++) {
                if (exist(board, row, col, w, 0)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean exist(char[][] board, int row, int col, char[] word, int index) {
        if (index == word.length) {
            return true;
        }
        if (row < 0 || col < 0 || row == board.length || col == board[row].length) {
            return false;
        }
        if (board[row][col] != word[index]) {
            return false;
        }
        char ch = board[row][col];
        board[row][col] = '*';
        boolean exist = exist(board, row, col + 1, word, index + 1)
                || exist(board, row, col - 1, word, index + 1)
                || exist(board, row + 1, col, word, index + 1)
                || exist(board, row - 1, col, word, index + 1);
        board[row][col] = ch;
        return exist;
    }
}
