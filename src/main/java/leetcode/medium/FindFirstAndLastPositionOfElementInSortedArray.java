package leetcode.medium;

/**
 * 34. Find First and Last Position of Element in Sorted Array
 *
 * @link https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/
 * @tags Array, Binary Search.
 */
public class FindFirstAndLastPositionOfElementInSortedArray {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Find First and Last Position of Element in Sorted Array.
     * Memory Usage: 42.7 MB, less than 100.00% of Java online submissions for Find First and Last Position of Element in Sorted Array.
     */
    public static int[] searchRange(int[] nums, int target) {
        if (nums.length < 1) {
            return new int[]{-1, -1};
        }
        int left = 0;
        int right = nums.length - 1;
        while (left < right) {
            int mid = (left + right) / 2;
            if (nums[mid] < target) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        if (nums[left] != target) {
            return new int[]{-1, -1};
        }
        int[] res = new int[2];
        res[0] = left;
        right = nums.length - 1;
        while (left < right) {
            int mid = (left + right) / 2 + 1;
            if (nums[mid] > target) {
                right = mid - 1;
            } else {
                left = mid;
            }
        }
        res[1] = right;
        return res;
    }


}
