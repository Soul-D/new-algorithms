package leetcode.medium;

/**
 * 36. Valid Sudoku
 *
 * @link https://leetcode.com/problems/valid-sudoku/
 * @tags Hash Table.
 */
public class ValidSudoku {
    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Valid Sudoku.
     * Memory Usage: 40.5 MB, less than 98.55% of Java online submissions for Valid Sudoku.
     */
    public static boolean isValidSudoku(char[][] board) {
        boolean[][] columns = initContainer();
        boolean[][] quadrants = initContainer();
        for (int row = 0; row < board.length; row++) {
            boolean[] rows = new boolean[10];
            for (int col = 0; col < board[row].length; col++) {
                char current = board[row][col];
                if (current == '.') {
                    continue;
                }
                int curr = current - '0';
                if (rows[curr]) {
                    return false;
                } else {
                    rows[curr] = true;
                }

                if (columns[col][curr]) {
                    return false;
                } else {
                    columns[col][curr] = true;
                }

                int quadrant = getQuadrant(row, col);
                if (quadrants[quadrant][curr]) {
                    return false;
                } else {
                    quadrants[quadrant][curr] = true;
                }
            }
        }
        return true;
    }

    private static boolean[][] initContainer() {
        boolean[][] columns = new boolean[9][];
        for (int i = 0; i < columns.length; i++) {
            columns[i] = new boolean[10];
        }
        return columns;
    }

    private static int getQuadrant(int row, int col) {
        if (row < 3 && col < 3) return 0;
        if (row < 3 && col < 6) return 1;
        if (row < 3) return 2;
        if (row < 6 && col < 3) return 3;
        if (row < 6 && col < 6) return 4;
        if (row < 6) return 5;
        if (col < 3) return 6;
        if (col < 6) return 7;
        return 8;
    }
}
