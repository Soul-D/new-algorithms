package leetcode.medium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 15. 3Sum
 *
 * @link https://leetcode.com/problems/3sum/
 * @tags Array, Two Pointers.
 */
public class ThreeSum {
    /**
     * Runtime: 20 ms, faster than 90.57% of Java online submissions for 3Sum.
     * Memory Usage: 45.2 MB, less than 97.88% of Java online submissions for 3Sum.
     */
    public static List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 2; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) {              // skip same result
                continue;
            }
            int left = i + 1, right = nums.length - 1;
            int target = -nums[i];
            while (left < right) {
                if (nums[left] + nums[right] == target) {
                    res.add(Arrays.asList(nums[i], nums[left], nums[right]));
                    left++;
                    right--;
                    while (left < right && nums[left] == nums[left - 1]) {
                        left++;  // skip same result
                    }
                    while (left < right && nums[right] == nums[right + 1]) {
                        right--;  // skip same result
                    }
                } else if (nums[left] + nums[right] > target) {
                    right--;
                } else {
                    left++;
                }
            }
        }
        return res;
    }
}
