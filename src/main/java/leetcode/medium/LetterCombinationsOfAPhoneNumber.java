package leetcode.medium;

import java.util.LinkedList;
import java.util.List;

/**
 * 17. Letter Combinations of a Phone Number
 *
 * @link https://leetcode.com/problems/letter-combinations-of-a-phone-number/
 * @tags String, Backtracking.
 */
public class LetterCombinationsOfAPhoneNumber {
    /**
     * Runtime: 5 ms, faster than 23.69% of Java online submissions for Letter Combinations of a Phone Number.
     * Memory Usage: 38.5 MB, less than 6.16% of Java online submissions for Letter Combinations of a Phone Number.
     */
    private static String[] numbers = new String[]{"0", "1", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};

    public static List<String> letterCombinations(String digits) {
        LinkedList<String> result = new LinkedList<>();
        if (digits.isEmpty()) {
            return result;
        }

        result.add("");
        while (result.peek().length() != digits.length()) {
            String remove = result.remove();
            String map = numbers[digits.charAt(remove.length()) - '0'];
            for (char c : map.toCharArray()) {
                result.addLast(remove + c);
            }
        }
        return result;
    }
}
