package year_2022.leetcode.medium;

import java.util.ArrayList;
import java.util.List;

/**
 * [medium]
 * 57. Insert Interval
 * <p>
 * You are given an array of non-overlapping intervals intervals where intervals[i] = [starti, endi] represent
 * the start and the end of the ith interval and intervals is sorted in ascending order by starti.
 * You are also given an interval newInterval = [start, end] that represents the start and end of another interval.
 * Insert newInterval into intervals such that intervals is still sorted in ascending order by starti and intervals
 * still does not have any overlapping intervals (merge overlapping intervals if necessary).
 * Return intervals after the insertion.
 * <p>
 * Constraints:
 * <p>
 * 0 <= intervals.length <= 10^4
 * intervals[i].length == 2
 * 0 <= starti <= endi <= 10^5
 * intervals is sorted by starti in ascending order.
 * newInterval.length == 2
 * 0 <= start <= end <= 10^5
 */
public class InsertInterval {
    /**
     * Runtime: 1 ms, faster than 99.75% of Java online submissions for Insert Interval.
     * Memory Usage: 48.2 MB, less than 12.18% of Java online submissions for Insert Interval.
     */
    public static int[][] insert(int[][] intervals, int[] newInterval) {
        List<int[]> result = new ArrayList<>();

        for (int[] i : intervals) {
            if (newInterval == null || i[1] < newInterval[0]) {
                result.add(i);
            } else if (i[0] > newInterval[1]) {
                result.add(newInterval);
                result.add(i);
                newInterval = null;
            } else {
                newInterval[0] = Math.min(newInterval[0], i[0]);
                newInterval[1] = Math.max(newInterval[1], i[1]);
            }
        }

        if (newInterval != null) {
            result.add(newInterval);
        }

        int[][] toReturn = new int[result.size()][];
        for (int i = 0; i < result.size(); i++) {
            toReturn[i] = result.get(i);
        }

        return toReturn;
    }
}
