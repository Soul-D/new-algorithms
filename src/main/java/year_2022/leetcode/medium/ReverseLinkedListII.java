package year_2022.leetcode.medium;

import year_2022.leetcode.model.ListNode;

/**
 * [medium]
 * 92. Reverse Linked List II
 * <p>
 * Given the head of a singly linked list and two integers left and right where left <= right,
 * reverse the nodes of the list from position left to position right, and return the reversed list.
 * <p>
 * Constraints:
 * The number of nodes in the list is n.
 * 1 <= n <= 500
 * -500 <= Node.val <= 500
 * 1 <= left <= right <= n
 */
public class ReverseLinkedListII {
    public static void main(String[] args) {
        reverseBetween(ListNode.of(1, 2, 3, 4, 5), 2, 4).printNode();
        reverseBetween(ListNode.of(5), 1, 1).printNode();
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Reverse Linked List II.
     * Memory Usage: 41.7 MB, less than 60.59% of Java online submissions for Reverse Linked List II.
     */
    public static ListNode reverseBetween(ListNode head, int left, int right) {
        if (head == null) {
            return null;
        }
        ListNode dummy = new ListNode(0); // create a dummy node to mark the head of this list
        dummy.next = head;
        ListNode preStar = dummy; // make a pointer preStar as a marker for the node before reversing
        for (int i = 0; i < left - 1; i++) {
            preStar = preStar.next;
        }

        ListNode start = preStar.next; // a pointer to the beginning of a sub-list that will be reversed
        ListNode next = start.next; // a pointer to a node that will be reversed

        // 1 - 2 -3 - 4 - 5 ; left=2; right=4 ---> preStar = 1, start = 2, next = 3
        // dummy-> 1 -> 2 -> 3 -> 4 -> 5

        for (int i = 0; i < right - left; i++) {
            start.next = next.next;
            next.next = preStar.next;
            preStar.next = next;
            next = start.next;
        }

        // first reversing : dummy->1 - 3 - 2 - 4 - 5; preStar = 1, start = 2, next = 4
        // second reversing: dummy->1 - 4 - 3 - 2 - 5; preStar = 1, start = 2, next = 5 (finish)

        return dummy.next;
    }
}
