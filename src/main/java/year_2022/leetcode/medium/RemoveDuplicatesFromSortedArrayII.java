package year_2022.leetcode.medium;

import java.util.Arrays;

/**
 * [medium]
 * 80. Remove Duplicates from Sorted Array II
 * <p>
 * Given an integer array nums sorted in non-decreasing order, remove some duplicates in-place such that each
 * unique element appears at most twice. The relative order of the elements should be kept the same.
 * Since it is impossible to change the length of the array in some languages, you must instead have
 * the result be placed in the first part of the array nums. More formally, if there are k elements after removing the duplicates,
 * then the first k elements of nums should hold the final result. It does not matter what you leave beyond the first k elements.
 * Return k after placing the final result in the first k slots of nums.
 * Do not allocate extra space for another array. You must do this by modifying the
 * input array in-place with O(1) extra memory.
 * <p>
 * Custom Judge:
 * The judge will test your solution with the following code:
 * int[] nums = [...]; // Input array
 * int[] expectedNums = [...]; // The expected answer with correct length
 * int k = removeDuplicates(nums); // Calls your implementation
 * assert k == expectedNums.length;
 * for (int i = 0; i < k; i++) {
 * assert nums[i] == expectedNums[i];
 * }
 * If all assertions pass, then your solution will be accepted.
 * <p>
 * Constraints:
 * 1 <= nums.length <= 3 * 10^4
 * -10^4 <= nums[i] <= 10^4
 * nums is sorted in non-decreasing order.
 */
public class RemoveDuplicatesFromSortedArrayII {
    public static void main(String[] args) {
//        int[] ints = new int[]{1, 1, 1, 2, 2, 3}; //5, nums = [1,1,2,2,3,_]
        int[] ints = new int[]{0, 0, 1, 1, 1, 1, 2, 3, 3}; //7, nums = [0,0,1,1,2,3,3,_,_]
        System.out.println(Arrays.toString(ints));
        int k = removeDuplicates(ints);
        System.out.println(k);
        printResult(k, ints);
    }

    /**
     * Runtime: 1 ms, faster than 87.45% of Java online submissions for Remove Duplicates from Sorted Array II.
     * Memory Usage: 44.6 MB, less than 60.19% of Java online submissions for Remove Duplicates from Sorted Array II.
     */
    public static int removeDuplicates(int[] nums) {
        if (nums.length < 2) {
            return nums.length;
        }

        int lastIndex = 1;
        boolean seenOnce = true;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == nums[i - 1]) {
                if (seenOnce) {
                    nums[lastIndex++] = nums[i];
                    seenOnce = false;
                }
            }

            if (nums[i] != nums[i - 1]) {
                nums[lastIndex++] = nums[i];
                seenOnce = true;
            }
        }

        return lastIndex;
    }

    private static void printResult(int k, int[] ints) {
        System.out.println(Arrays.toString(Arrays.copyOf(ints, k)));
    }
}
