package year_2022.leetcode.medium;

/**
 * [medium]
 * 97. Interleaving String
 * <p>
 * Given strings s1, s2, and s3, find whether s3 is formed by an interleaving of s1 and s2.
 * An interleaving of two strings s and t is a configuration where s and t are divided into n and m non-empty
 * substrings respectively, such that:
 * s = s1 + s2 + ... + sn
 * t = t1 + t2 + ... + tm
 * |n - m| <= 1
 * The interleaving is s1 + t1 + s2 + t2 + s3 + t3 + ... or t1 + s1 + t2 + s2 + t3 + s3 + ...
 * Note: a + b is the concatenation of strings a and b.
 * <p>
 * Constraints:
 * 0 <= s1.length, s2.length <= 100
 * 0 <= s3.length <= 200
 * s1, s2, and s3 consist of lowercase English letters.
 */
public class InterleavingString {
    public static void main(String[] args) {
        System.out.println(isInterleave("aabcc", "dbbca", "aadbbcbcac"));
        System.out.println(isInterleave("aabcc", "dbbca", "aadbbbaccc"));
        System.out.println(isInterleave("", "", ""));
        System.out.println(isInterleave("", "", "a"));
    }

    /**
     * Runtime: 2 ms, faster than 84.72% of Java online submissions for Interleaving String.
     * Memory Usage: 40.6 MB, less than 94.77% of Java online submissions for Interleaving String.
     */
    public static boolean isInterleave(String s1, String s2, String s3) {

        if ((s1.length() + s2.length()) != s3.length()) {
            return false;
        }

        boolean[][] matrix = new boolean[s2.length() + 1][s1.length() + 1];

        matrix[0][0] = true;

        for (int i = 1; i < matrix[0].length; i++) {
            matrix[0][i] = matrix[0][i - 1] && (s1.charAt(i - 1) == s3.charAt(i - 1));
        }

        for (int i = 1; i < matrix.length; i++) {
            matrix[i][0] = matrix[i - 1][0] && (s2.charAt(i - 1) == s3.charAt(i - 1));
        }

        for (int i = 1; i < matrix.length; i++) {
            for (int j = 1; j < matrix[0].length; j++) {
                matrix[i][j] = (matrix[i - 1][j] && (s2.charAt(i - 1) == s3.charAt(i + j - 1)))
                        || (matrix[i][j - 1] && (s1.charAt(j - 1) == s3.charAt(i + j - 1)));
            }
        }

        return matrix[s2.length()][s1.length()];

    }

    public static boolean isInterleaveTimeExceed(String s1, String s2, String s3) {
        if (s1.length() + s2.length() != s3.length()) {
            return false;
        }

        if (s1.isEmpty() && s2.isEmpty() && s3.isEmpty()) {
            return true;
        }
        if (s1.isEmpty()) {
            return s2.charAt(0) == s3.charAt(0) && isInterleaveTimeExceed(s1, s2.substring(1), s3.substring(1));
        }

        if (s2.isEmpty()) {
            return s1.charAt(0) == s3.charAt(0) && isInterleaveTimeExceed(s1.substring(1), s2, s3.substring(1));
        }

        if (s1.charAt(0) == s3.charAt(0) && s2.charAt(0) == s3.charAt(0)) {
            return isInterleaveTimeExceed(s1.substring(1), s2, s3.substring(1)) || isInterleaveTimeExceed(s1, s2.substring(1), s3.substring(1));
        } else if (s1.charAt(0) == s3.charAt(0)) {
            return isInterleaveTimeExceed(s1.substring(1), s2, s3.substring(1));
        } else if (s2.charAt(0) == s3.charAt(0)) {
            return isInterleaveTimeExceed(s1, s2.substring(1), s3.substring(1));
        } else {
            return false;
        }
    }
}
