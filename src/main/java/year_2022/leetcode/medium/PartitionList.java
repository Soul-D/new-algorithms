package year_2022.leetcode.medium;

import year_2022.leetcode.model.ListNode;

/**
 * [medium]
 * 86. Partition List
 * <p>
 * Given the head of a linked list and a value x, partition it such that all nodes less than x
 * come before nodes greater than or equal to x.
 * You should preserve the original relative order of the nodes in each of the two partitions.
 * <p>
 * Constraints:
 * The number of nodes in the list is in the range [0, 200].
 * -100 <= Node.val <= 100
 * -200 <= x <= 200
 */
public class PartitionList {
    public static void main(String[] args) {
        partition(ListNode.of(1, 4, 3, 2, 5, 2), 3).printNode();
        partition(ListNode.of(2, 1), 2).printNode();
    }

    /**
     * Runtime: 1 ms, faster than 69.96% of Java online submissions for Partition List.
     * Memory Usage: 42.2 MB, less than 78.48% of Java online submissions for Partition List.
     */
    public static ListNode partition(ListNode head, int x) {
        ListNode lowerDummy = new ListNode(0);
        ListNode lowerCursor = lowerDummy;
        ListNode greaterDummy = new ListNode(0);
        ListNode greaterCursor = greaterDummy;

        while (head != null) {
            ListNode listNode = new ListNode(head.val);
            if (head.val < x) {
                lowerCursor.next = listNode;
                lowerCursor = lowerCursor.next;
            } else {
                greaterCursor.next = listNode;
                greaterCursor = greaterCursor.next;
            }
            head = head.next;
        }
        ListNode result;
        if (lowerDummy.next != null) {
            result = lowerDummy.next;
            lowerCursor.next = greaterDummy.next;
        } else {
            result = greaterDummy.next;
        }
        return result;
    }
}
