package year_2022.leetcode.medium;

import year_2022.leetcode.model.ListNode;

/**
 * [medium]
 * 82. Remove Duplicates from Sorted List II
 * <p>
 * Given the head of a sorted linked list, delete all nodes that have duplicate numbers,
 * leaving only distinct numbers from the original list. Return the linked list sorted as well.
 * <p>
 * Constraints:
 * The number of nodes in the list is in the range [0, 300].
 * -100 <= Node.val <= 100
 * The list is guaranteed to be sorted in ascending order.
 */
public class RemoveDuplicatesFromSortedListII {
    public static void main(String[] args) {
        deleteDuplicates(ListNode.of(1, 2, 3, 3, 4, 4, 5)).printNode();
    }

    /**
     * Runtime: 1 ms, faster than 84.79% of Java online submissions for Remove Duplicates from Sorted List II.
     * Memory Usage: 44.1 MB, less than 25.48% of Java online submissions for Remove Duplicates from Sorted List II.
     */
    public static ListNode deleteDuplicates(ListNode head) {
        ListNode auxNode = new ListNode(0);
        ListNode cursor = auxNode;
        while (head != null) {
            if (head.next != null && head.val == head.next.val) {
                while (head.next != null && head.val == head.next.val) {
                    head = head.next;
                }
            } else {
                cursor.next = head;
                cursor = cursor.next;
            }
            head = head.next;
        }
        cursor.next = null;
        return auxNode.next;
    }
}
