package year_2022.leetcode.medium;

import year_2022.leetcode.model.ListNode;

/**
 * [medium]
 * 2. Add Two Numbers
 * <p>
 * You are given two non-empty linked lists representing two non-negative integers.
 * The digits are stored in reverse order, and each of their nodes contains a single digit.
 * Add the two numbers and return the sum as a linked list.
 * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 * <p>
 * Constraints:
 * The number of nodes in each linked list is in the range [1, 100].
 * 0 <= Node.val <= 9
 * It is guaranteed that the list represents a number that does not have leading zeros.
 */
public class AddTwoNumbers {
    public static void main(String[] args) {
        addTwoNumbers(ListNode.of(2, 4, 3), ListNode.of(5, 6, 4)).printNode();
        addTwoNumbers(ListNode.of(0), ListNode.of(0)).printNode();
        addTwoNumbers(ListNode.of(9, 9, 9, 9, 9, 9, 9), ListNode.of(9, 9, 9, 9)).printNode();
    }

    /**
     * Runtime: 3 ms, faster than 80.74% of Java online submissions for Add Two Numbers.
     * Memory Usage: 47.3 MB, less than 79.20% of Java online submissions for Add Two Numbers.
     */
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int remaining = 0;
        ListNode result = null;
        ListNode cursor = null;
        while (l1 != null || l2 != null) {
            int value = 0;
            if (l1 != null) {
                value += l1.val;
                l1 = l1.next;
            }
            if (l2 != null) {
                value += l2.val;
                l2 = l2.next;
            }
            value += remaining;
            ListNode t = new ListNode(value % 10);
            remaining = value / 10;
            if (result == null) {
                result = t;
            }
            if (cursor == null) {
                cursor = t;
            } else {
                cursor.next = t;
                cursor = cursor.next;
            }
        }

        if (remaining != 0) {
            cursor.next = new ListNode(remaining);
        }

        return result;
    }
}
