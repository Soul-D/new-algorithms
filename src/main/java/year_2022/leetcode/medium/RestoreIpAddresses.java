package year_2022.leetcode.medium;

import java.util.ArrayList;
import java.util.List;

/**
 * [medium]
 * 93. Restore IP Addresses
 * <p>
 * A valid IP address consists of exactly four integers separated by single dots.
 * Each integer is between 0 and 255 (inclusive) and cannot have leading zeros.
 * For example, "0.1.2.201" and "192.168.1.1" are valid IP addresses, but "0.011.255.245", "192.168.1.312" and "192.168@1.1"
 * are invalid IP addresses.
 * Given a string s containing only digits, return all possible valid IP addresses that can be formed by inserting dots into s.
 * You are not allowed to reorder or remove any digits in s. You may return the valid IP addresses in any order.
 * <p>
 * Constraints:
 * 1 <= s.length <= 20
 * s consists of digits only.
 */
public class RestoreIpAddresses {
    public static void main(String[] args) {
        System.out.println(restoreIpAddresses("25525511135")); //[255.255.11.135, 255.255.111.35]
        System.out.println(restoreIpAddresses("0000")); //[0.0.0.0]
        System.out.println(restoreIpAddresses("101023")); //[1.0.10.23, 1.0.102.3, 10.1.0.23, 10.10.2.3, 101.0.2.3]
    }

    /**
     * Runtime: 14 ms, faster than 22.46% of Java online submissions for Restore IP Addresses.
     * Memory Usage: 43.2 MB, less than 60.44% of Java online submissions for Restore IP Addresses.
     */
    public static List<String> restoreIpAddresses(String s) {
        List<String> result = new ArrayList<>();
        int length = s.length();
        for (int i = 1; i < 4 && i < length - 2; i++) {
            for (int j = i + 1; j < i + 4 && j < length - 1; j++) {
                for (int k = j + 1; k < j + 4 && k < length; k++) {
                    String term1 = s.substring(0, i);
                    String term2 = s.substring(i, j);
                    String term3 = s.substring(j, k);
                    String term4 = s.substring(k, length);
                    if (isTermValid(term1) && isTermValid(term2) && isTermValid(term3) && isTermValid(term4)) {
                        result.add(term1 + "." + term2 + "." + term3 + "." + term4);
                    }
                }
            }
        }
        return result;
    }

    public static boolean isTermValid(String term) {
        if (term.length() > 3 || term.length() == 0 || (term.charAt(0) == '0' && term.length() > 1) || Integer.parseInt(term) > 255) {
            return false;
        }
        return true;
    }
}
