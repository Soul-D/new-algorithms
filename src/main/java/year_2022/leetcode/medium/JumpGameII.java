package year_2022.leetcode.medium;

/**
 * [medium]
 * 45. Jump Game II
 * <p>
 * Given an array of non-negative integers nums, you are initially positioned at the first index of the array.
 * Each element in the array represents your maximum jump length at that position.
 * Your goal is to reach the last index in the minimum number of jumps.
 * You can assume that you can always reach the last index.
 * <p>
 * Constraints:
 * 1 <= nums.length <= 10^4
 * 0 <= nums[i] <= 1000
 */
public class JumpGameII {
    /**
     * Runtime: 3 ms, faster than 55.34% of Java online submissions for Jump Game II.
     * Memory Usage: 48.7 MB, less than 83.54% of Java online submissions for Jump Game II.
     */
    public static int jump(int[] nums) {
        int jumps = 0, farthest = 0;
        int left = 0, right = 0;
        while (right < nums.length - 1) {
            for (int i = left; i <= right; ++i) {
                farthest = Math.max(farthest, i + nums[i]);
            }
            left = right + 1;
            right = farthest;
            ++jumps;
        }
        return jumps;
    }
}
