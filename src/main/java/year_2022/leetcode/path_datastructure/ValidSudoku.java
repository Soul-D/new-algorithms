package year_2022.leetcode.path_datastructure;

/**
 * [medium]
 * 36. Valid Sudoku
 * <p>
 * Determine if a 9 x 9 Sudoku board is valid. Only the filled cells need to be validated according to the following rules:
 * Each row must contain the digits 1-9 without repetition.
 * Each column must contain the digits 1-9 without repetition.
 * Each of the nine 3 x 3 sub-boxes of the grid must contain the digits 1-9 without repetition.
 * <p>
 * Note:
 * A Sudoku board (partially filled) could be valid but is not necessarily solvable.
 * Only the filled cells need to be validated according to the mentioned rules.
 * <p>
 * Constraints:
 * board.length == 9
 * board[i].length == 9
 * board[i][j] is a digit 1-9 or '.'.
 */
public class ValidSudoku {
    public static void main(String[] args) {
        char[][] board = new char[][]{
                {'5', '3', '.', '.', '7', '.', '.', '.', '.'},
                {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
                {'.', '9', '8', '.', '.', '.', '.', '6', '.'},
                {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
                {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
                {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
                {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
                {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
                {'.', '.', '.', '.', '8', '.', '.', '7', '9'}
        };

        System.out.println(isValidSudoku(board));
    }

    /**
     * Runtime: 2 ms, faster than 97.40% of Java online submissions for Valid Sudoku.
     * Memory Usage: 45.8 MB, less than 74.87% of Java online submissions for Valid Sudoku.
     */
    public static boolean isValidSudoku(char[][] board) {
        for (int i = 0; i < 9; i++) {
            if (!isRowValid(i, board) || !isColValid(i, board)) {
                return false;
            }
        }

        for (int i = 0; i < 9; i += 3) {
            for (int j = 0; j < 9; j += 3) {
                if (!isSectorValid(i, j, board)) {
                    return false;
                }
            }
        }

        return true;
    }

    private static boolean isSectorValid(int row, int col, char[][] board) {
        int[] container = new int[10];
        for (int i = row; i < row + 3; i++) {
            for (int j = col; j < col + 3; j++) {
                char c = board[i][j];
                if (c != '.') {
                    container[c - 49]++;
                }
            }
        }
        for (int i : container) {
            if (i > 1) {
                return false;
            }
        }
        return true;
    }

    private static boolean isColValid(int col, char[][] board) {
        int[] container = new int[10];

        for (char[] chars : board) {
            char c = chars[col];
            if (c != '.') {
                container[c - 49]++;
            }
        }

        for (int i : container) {
            if (i > 1) {
                return false;
            }
        }
        return true;
    }

    private static boolean isRowValid(int row, char[][] board) {
        int[] container = new int[10];
        for (char c : board[row]) {
            if (c != '.') {
                container[c - 49]++;
            }
        }
        for (int i : container) {
            if (i > 1) {
                return false;
            }
        }
        return true;
    }
}
