package year_2022.leetcode.path_datastructure;

/**
 * [easy]
 * 566. Reshape the Matrix
 * <p>
 * In MATLAB, there is a handy function called reshape which can reshape an m x n matrix into a new one with a
 * different size r x c keeping its original data.
 * You are given an m x n matrix mat and two integers r and c representing the number of
 * rows and the number of columns of the wanted reshaped matrix.
 * The reshaped matrix should be filled with all the elements of the original matrix in the same row-traversing order as they were.
 * If the reshape operation with given parameters is possible and legal, output the new reshaped matrix; Otherwise, output the original matrix.
 * <p>
 * Constraints:
 * m == mat.length
 * n == mat[i].length
 * 1 <= m, n <= 100
 * -1000 <= mat[i][j] <= 1000
 * 1 <= r, c <= 300
 */
public class ReshapeTheMatrix {
    /**
     * Runtime: 1 ms, faster than 90.08% of Java online submissions for Reshape the Matrix.
     * Memory Usage: 51.1 MB, less than 18.42% of Java online submissions for Reshape the Matrix.
     */
    public static int[][] matrixReshape(int[][] mat, int r, int c) {
        if (mat.length == 0 || mat.length * mat[0].length != r * c) {
            return mat;
        }
        int[][] result = new int[r][];
        int currentRow = 0;
        int currentCol = 0;
        int[] row = new int[c];
        for (int[] ints : mat) {
            for (int j = 0; j < mat[0].length; j++) {
                if (currentCol < c) {
                    row[currentCol++] = ints[j];
                    if (currentCol == c) {
                        result[currentRow++] = row;
                        row = new int[c];
                        currentCol = 0;
                    }
                }
            }
        }

        return result;
    }
}
