package year_2022.leetcode.path_datastructure;

/**
 * [easy]
 * 383. Ransom Note
 * <p>
 * Given two strings ransomNote and magazine, return true if ransomNote can be constructed by using
 * the letters from magazine and false otherwise.
 * Each letter in magazine can only be used once in ransomNote.
 * <p>
 * Constraints:
 * 1 <= ransomNote.length, magazine.length <= 10^5
 * ransomNote and magazine consist of lowercase English letters.
 */
public class RansomNote {
    public static void main(String[] args) {
        System.out.println(canConstructOneMore("a", "b")); //f
        System.out.println(canConstructOneMore("aa", "ab")); //f
        System.out.println(canConstructOneMore("aa", "aab")); //t
    }

    /**
     * Runtime: 3 ms, faster than 91.57% of Java online submissions for Ransom Note.
     * Memory Usage: 46 MB, less than 62.88% of Java online submissions for Ransom Note.
     */
    public static boolean canConstruct(String ransomNote, String magazine) {
        if (ransomNote.length() > magazine.length()) {
            return false;
        }

        int[] ransomNoteCounter = new int[26];
        int[] magazineCounter = new int[26];

        for (char c : ransomNote.toCharArray()) {
            ransomNoteCounter[c - 'a']++;
        }
        for (char c : magazine.toCharArray()) {
            magazineCounter[c - 'a']++;
        }

        for (int i = 0; i < ransomNoteCounter.length; i++) {
            if (ransomNoteCounter[i] == 0) {
                continue;
            }
            if (ransomNoteCounter[i] > magazineCounter[i]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Runtime: 1 ms, faster than 99.99% of Java online submissions for Ransom Note.
     * Memory Usage: 45.4 MB, less than 78.60% of Java online submissions for Ransom Note.
     */
    public static boolean canConstructOneMore(String ransomNote, String magazine) {
        if (ransomNote.length() > magazine.length()) {
            return false;
        }

        int[] indexes = new int[26];
        for (char c : ransomNote.toCharArray()) {
            int index = magazine.indexOf(c, indexes[c - 'a']);
            if (index < 0) {
                return false;
            } else {
                indexes[c - 'a'] = index + 1;
            }
        }

        return true;
    }
}
