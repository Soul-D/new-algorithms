package year_2022.leetcode.path_datastructure;

import java.util.Arrays;

/**
 * [easy]
 * 88. Merge Sorted Array
 * <p>
 * You are given two integer arrays nums1 and nums2, sorted in non-decreasing order, and two integers m and n,
 * representing the number of elements in nums1 and nums2 respectively.
 * Merge nums1 and nums2 into a single array sorted in non-decreasing order.
 * The final sorted array should not be returned by the function, but instead be stored inside the array nums1.
 * To accommodate this, nums1 has a length of m + n, where the first m elements denote the elements that should be merged, and the last n elements are set to 0 and should be ignored. nums2 has a length of n.
 * <p>
 * Constraints:
 * nums1.length == m + n
 * nums2.length == n
 * 0 <= m, n <= 200
 * 1 <= m + n <= 200
 * -10^9 <= nums1[i], nums2[j] <= 10^9
 */
public class MergeSortedArray {
    public static void main(String[] args) {
        check(new int[]{1, 2, 3, 0, 0, 0}, 3, new int[]{2, 5, 6}, 3); // [1,2,2,3,5,6]
        check(new int[]{1}, 1, new int[]{}, 0); // [1]
        check(new int[]{0}, 0, new int[]{1}, 1); // [1]
        check(new int[]{1, 2, 3, 0, 0, 0}, 3, new int[]{2, 5, 6}, 3); // [1,2,2,3,5,6]
    }

    private static void check(int[] nums1, int m, int[] nums2, int n) {
        merge(nums1, m, nums2, n);
        System.out.println(Arrays.toString(nums1));
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Merge Sorted Array.
     * Memory Usage: 43 MB, less than 40.32% of Java online submissions for Merge Sorted Array.
     */
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int firstIndex = m - 1;
        int secondIndex = n - 1;
        for (int i = m + n - 1; i >= 0; i--) {
            if (firstIndex < 0) {
                nums1[i] = nums2[secondIndex--];
            } else if (secondIndex < 0) {
                nums1[i] = nums1[firstIndex--];
            } else {
                if (nums1[firstIndex] > nums2[secondIndex]) {
                    nums1[i] = nums1[firstIndex--];
                } else {
                    nums1[i] = nums2[secondIndex--];
                }
            }
        }
    }
}
