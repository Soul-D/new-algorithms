package year_2022.leetcode.path_datastructure;

import java.util.Arrays;

/**
 * [medium]
 * 74. Search a 2D Matrix
 * <p>
 * Write an efficient algorithm that searches for a value target in an m x n integer matrix matrix. This matrix has the following properties:
 * Integers in each row are sorted from left to right.
 * The first integer of each row is greater than the last integer of the previous row.
 * <p>
 * Constraints:
 * m == matrix.length
 * n == matrix[i].length
 * 1 <= m, n <= 100
 * -10^4 <= matrix[i][j], target <= 10^4
 */
public class SearchA2DMatrix {
    public static void main(String[] args) {
        int[][] matrix1 = new int[][]{
                {1, 3, 5, 7}, {10, 11, 16, 20}, {23, 30, 34, 60}
        };
        int[][] matrix2 = new int[][]{
                {1}, {3}
        };
        System.out.println(searchMatrix(matrix1, 3));
        System.out.println(searchMatrix(matrix2, 3));
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Search a 2D Matrix.
     * Memory Usage: 42.2 MB, less than 80.00% of Java online submissions for Search a 2D Matrix.
     */
    public static boolean searchMatrix(int[][] matrix, int target) {
        int row = -1;
        for (int i = 0; i < matrix.length; i++) {
            int el = matrix[i][0];
            if (target > el) {
                row = i;
            } else if (target == el) {
                return true;
            }
        }

        if (row < 0) {
            return false;
        }
        return Arrays.binarySearch(matrix[row], target) >= 0;
    }
}
