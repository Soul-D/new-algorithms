package year_2022.leetcode.path_datastructure;

/**
 * [easy]
 * 387. First Unique Character in a String
 * <p>
 * Given a string s, find the first non-repeating character in it and return its index. If it does not exist, return -1.
 * <p>
 * Constraints:
 * 1 <= s.length <= 10^5
 * s consists of only lowercase English letters.
 */
public class FirstUniqueCharacterInString {
    public static void main(String[] args) {
        System.out.println(firstUniqChar("leetcode"));//0
        System.out.println(firstUniqChar("loveleetcode"));//2
        System.out.println(firstUniqChar("aabb"));//-1
    }

    /**
     * Runtime: 6 ms, faster than 96.36% of Java online submissions for First Unique Character in a String.
     * Memory Usage: 49.9 MB, less than 51.45% of Java online submissions for First Unique Character in a String.
     */
    public static int firstUniqChar(String s) {
        char[] chars = s.toCharArray();
        int[] counter = new int[26];

        for (char c : chars) {
            counter[c - 'a']++;
        }

        for (int i = 0; i < chars.length; i++) {
            if (counter[chars[i] - 'a'] == 1) {
                return i;
            }
        }

        return -1;
    }
}
