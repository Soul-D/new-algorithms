package year_2022.leetcode.path_datastructure;

import java.util.Stack;

/**
 * [easy]
 * 20. Valid Parentheses
 * <p>
 * Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
 * An input string is valid if:
 * Open brackets must be closed by the same type of brackets.
 * Open brackets must be closed in the correct order.
 * <p>
 * Constraints:
 * 1 <= s.length <= 10^4
 * s consists of parentheses only '()[]{}'.
 */
public class ValidParentheses {
    public static void main(String[] args) {
        System.out.println(
                isValid("([)]{}")
        );
    }

    /**
     * Runtime: 2 ms, faster than 90.79% of Java online submissions for Valid Parentheses.
     * Memory Usage: 42 MB, less than 52.30% of Java online submissions for Valid Parentheses.
     */
    public static boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for (char c : s.toCharArray()) {
            switch (c) {
                case '(':
                case '[':
                case '{':
                    stack.push(c);
                    break;
                case ')':
                case ']':
                case '}':
                    if (stack.empty()) {
                        return false;
                    }
                    Character pop = stack.pop();
                    if (c == ')' && pop != '(') {
                        return false;
                    } else if (c == ']' && pop != '[') {
                        return false;
                    } else if (c == '}' && pop != '{') {
                        return false;
                    }
                    break;
            }
        }

        return stack.empty();
    }
}
