package year_2022.leetcode.path_datastructure;

import year_2022.leetcode.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * [easy]
 * 94. Binary Tree Inorder Traversal
 * <p>
 * Given the root of a binary tree, return the inorder traversal of its nodes' values.
 */
public class BinaryTreeInorderTraversal {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Binary Tree Inorder Traversal.
     * Memory Usage: 42.6 MB, less than 22.74% of Java online submissions for Binary Tree Inorder Traversal.
     */
    public static List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> values = new ArrayList<>();
        inorderTraversal(root, values);
        return values;
    }

    private static void inorderTraversal(TreeNode root, List<Integer> values) {
        if (root == null) {
            return;
        }
        inorderTraversal(root.left, values);
        values.add(root.val);
        inorderTraversal(root.right, values);
    }
}
