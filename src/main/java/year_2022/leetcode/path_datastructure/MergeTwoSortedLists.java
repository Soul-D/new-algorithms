package year_2022.leetcode.path_datastructure;

import year_2022.leetcode.model.ListNode;

/**
 * [easy]
 * 21. Merge Two Sorted Lists
 * <p>
 * You are given the heads of two sorted linked lists list1 and list2.
 * Merge the two lists in a one sorted list. The list should be made by splicing together the nodes of the first two lists.
 * Return the head of the merged linked list.
 * <p>
 * Constraints:
 * The number of nodes in both lists is in the range [0, 50].
 * -100 <= Node.val <= 100
 * Both list1 and list2 are sorted in non-decreasing order.
 */
public class MergeTwoSortedLists {
    public static void main(String[] args) {
        ListNode node11 = new ListNode(1);
        ListNode node12 = new ListNode(2);
        ListNode node13 = new ListNode(3);
        ListNode node14 = new ListNode(4);
        ListNode node15 = new ListNode(5);

        node11.next = node12;
        node12.next = node13;
        node13.next = node14;
        node14.next = node15;


        ListNode node21 = new ListNode(10);
        ListNode node22 = new ListNode(11);
        ListNode node23 = new ListNode(12);
        ListNode node24 = new ListNode(13);
        ListNode node25 = new ListNode(14);

        node21.next = node22;
        node22.next = node23;
        node23.next = node24;
        node24.next = node25;


        mergeTwoLists(node11, node21).printNode();
    }

    /**
     * Runtime: 1 ms, faster than 80.19% of Java online submissions for Merge Two Sorted Lists.
     * Memory Usage: 43 MB, less than 52.10% of Java online submissions for Merge Two Sorted Lists.
     */
    public static ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        if (list1 == null) {
            return list2;
        }
        if (list2 == null) {
            return list1;
        }

        ListNode head, cursor;
        if (list1.val < list2.val) {
            head = cursor = list1;
            list1 = list1.next;
        } else {
            head = cursor = list2;
            list2 = list2.next;
        }

        while (true) {
            if (list1 == null) {
                cursor.next = list2;
                break;
            }

            if (list2 == null) {
                cursor.next = list1;
                break;
            }

            if (list1.val < list2.val) {
                cursor.next = list1;
                cursor = cursor.next;
                list1 = list1.next;
            } else {
                cursor.next = list2;
                cursor = cursor.next;
                list2 = list2.next;
            }
        }

        return head;
    }
}
