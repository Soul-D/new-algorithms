package year_2022.leetcode.path_datastructure;

import java.util.Arrays;

/**
 * [easy]
 * 350. Intersection of Two Arrays II
 * <p>
 * Given two integer arrays nums1 and nums2, return an array of their intersection.
 * Each element in the result must appear as many times as it shows in both arrays and you may return the result in any order.
 * <p>
 * Constraints:
 * 1 <= nums1.length, nums2.length <= 1000
 * 0 <= nums1[i], nums2[i] <= 1000
 * <p>
 * Follow up:
 * What if the given array is already sorted? How would you optimize your algorithm?
 * What if nums1's size is small compared to nums2's size? Which algorithm is better?
 * What if elements of nums2 are stored on disk, and the memory is limited such that you cannot load all elements into the memory at once?
 */
public class IntersectionOfTwoArraysII {
    public static void main(String[] args) {
        int[] num1 = new int[]{4, 9, 5};
        int[] num2 = new int[]{9, 4, 9, 8, 4};
        System.out.println(Arrays.toString(intersect(num1, num2)));
    }

    /**
     * Runtime: 3 ms, faster than 87.10% of Java online submissions for Intersection of Two Arrays II.
     * Memory Usage: 43.6 MB, less than 74.37% of Java online submissions for Intersection of Two Arrays II.
     */
    public static int[] intersect(int[] nums1, int[] nums2) {
        int length = Math.min(nums1.length, nums2.length);

        int[] storage = new int[length];
        int count = 0;

        Arrays.sort(nums1);
        Arrays.sort(nums2);

        for (int a = 0, b = 0; a < nums1.length && b < nums2.length; ) {
            if (nums1[a] > nums2[b]) {
                b++;
            } else if (nums1[a] < nums2[b]) {
                a++;
            } else {
                storage[count++] = nums1[a++];
                b++;
            }
        }

        return Arrays.copyOf(storage, count);
    }
}
