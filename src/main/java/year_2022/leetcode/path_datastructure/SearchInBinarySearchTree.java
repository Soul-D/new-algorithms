package year_2022.leetcode.path_datastructure;

import year_2022.leetcode.model.TreeNode;

/**
 * [easy]
 * 700. Search in a Binary Search Tree
 * <p>
 * You are given the root of a binary search tree (BST) and an integer val.
 * Find the node in the BST that the node's value equals val and return the subtree rooted with that node.
 * If such a node does not exist, return null.
 */
public class SearchInBinarySearchTree {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Search in a Binary Search Tree.
     * Memory Usage: 51.5 MB, less than 46.21% of Java online submissions for Search in a Binary Search Tree.
     */
    public TreeNode searchBST(TreeNode root, int val) {
        if (root == null || root.val == val) {
            return root;
        }
        if (root.val > val) {
            return searchBST(root.left, val);
        } else {
            return searchBST(root.right, val);
        }
    }
}
