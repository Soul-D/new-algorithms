package year_2022.leetcode.path_datastructure;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * [easy]
 * 1. Two Sum
 * <p>
 * Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 * You can return the answer in any order.
 * <p>
 * Constraints:
 * 2 <= nums.length <= 10^4
 * -10^9 <= nums[i] <= 10^9
 * -10^9 <= target <= 10^9
 * Only one valid answer exists.
 */
public class TwoSum {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(twoSum(new int[]{2, 7, 11, 15}, 9))); //[0,1]
        System.out.println(Arrays.toString(twoSum(new int[]{3, 2, 4}, 6))); //[1,2]
        System.out.println(Arrays.toString(twoSum(new int[]{3, 3}, 6))); //[0,1]
    }

    /**
     * Runtime: 4 ms, faster than 77.28% of Java online submissions for Two Sum.
     * Memory Usage: 46.5 MB, less than 13.11% of Java online submissions for Two Sum.
     */
    public static int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        int[] result = new int[2];
        for (int i = 0; i < nums.length; i++) {
            Integer complimentIndex = map.get(nums[i]);
            if (complimentIndex != null) {
                result[0] = complimentIndex;
                result[1] = i;
                break;
            }

            int diff = target - nums[i];
            map.put(diff, i);
        }

        return result;
    }
}
