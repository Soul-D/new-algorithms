package year_2022.leetcode.path_datastructure;

import java.util.HashSet;
import java.util.Set;

/**
 * [easy]
 * 217. Contains Duplicate
 * Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct.
 * Constraints:
 * 1 <= nums.length <= 105
 * -10^9 <= nums[i] <= 10^9
 */
public class ContainsDuplicate {
    public static void main(String[] args) {
        int[] nums = new int[]{1, 1, 1, 3, 3, 4, 3, 2, 4, 2};
        System.out.println(containsDuplicate(nums));
    }

    /**
     * Runtime: 4 ms, faster than 99.88% of Java online submissions for Contains Duplicate.
     * Memory Usage: 50.3 MB, less than 99.54% of Java online submissions for Contains Duplicate.
     */
    public static boolean containsDuplicate(int[] nums) {
        Set<Integer> container = new HashSet<>();
        for (int num : nums) {
            if (!container.add(num)) {
                return true;
            }
        }
        return false;
    }
}
