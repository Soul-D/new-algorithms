package year_2022.leetcode.path_datastructure;

import year_2022.leetcode.model.TreeNode;

import java.util.HashSet;
import java.util.Set;

/**
 * [easy]
 * 653. Two Sum IV - Input is a BST
 * <p>
 * Given the root of a Binary Search Tree and a target number k,
 * return true if there exist two elements in the BST such that their sum is equal to the given target.
 * <p>
 * Constraints:
 * The number of nodes in the tree is in the range [1, 10^4].
 * -10^4 <= Node.val <= 10^4
 * root is guaranteed to be a valid binary search tree.
 * -10^5 <= k <= 10^5
 */
public class TwoSumInputisBST {
    /**
     * Runtime: 6 ms, faster than 70.11% of Java online submissions for Two Sum IV - Input is a BST.
     * Memory Usage: 52 MB, less than 27.44% of Java online submissions for Two Sum IV - Input is a BST.
     */
    public static boolean findTarget(TreeNode root, int k) {
        return findTarget(root, k, new HashSet<>());
    }

    public static boolean findTarget(TreeNode root, int k, Set<Integer> set) {
        if (root == null) {
            return false;
        }

        if (set.contains(root.val)) {
            return true;
        }

        set.add(k - root.val);
        return findTarget(root.left, k, set) || findTarget(root.right, k, set);
    }
}
