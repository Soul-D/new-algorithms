package year_2022.leetcode.path_datastructure;

import year_2022.leetcode.model.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * [easy]
 * 144. Binary Tree Preorder Traversal
 * <p>
 * Given the root of a binary tree, return the preorder traversal of its nodes' values.
 * <p>
 * Constraints:
 * The number of nodes in the tree is in the range [0, 100].
 * -100 <= Node.val <= 100
 * <p>
 * Follow up: Recursive solution is trivial, could you do it iteratively?
 */
public class BinaryTreePreorderTraversal {
    //    [6,2,8,0,4,7,9,null,null,3,5]
    /*
                6
            /        \
           2           8
         /   \       /   \
        0     4     7      9
             /  \
             3   5
     */
    public static void main(String[] args) {
        TreeNode node6 = new TreeNode(6);
        TreeNode node2 = new TreeNode(2);
        TreeNode node8 = new TreeNode(8);
        TreeNode node0 = new TreeNode(0);
        TreeNode node4 = new TreeNode(4);
        TreeNode node7 = new TreeNode(7);
        TreeNode node9 = new TreeNode(9);
        TreeNode node3 = new TreeNode(3);
        TreeNode node5 = new TreeNode(5);


        node6.left = node2;
        node6.right = node8;
        node2.left = node0;
        node2.right = node4;
        node4.left = node3;
        node4.right = node5;

        node8.left = node7;
        node8.right = node9;

        // [6, 2, 0, 4, 3, 5, 8, 7, 9]
        System.out.println(preorderTraversal(node6));
        System.out.println(preorderTraversalIterative(node6));
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Binary Tree Preorder Traversal.
     * Memory Usage: 40.5 MB, less than 93.35% of Java online submissions for Binary Tree Preorder Traversal.
     */
    public static List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> values = new ArrayList<>();
        preorderTraversal(root, values);
        return values;
    }

    private static void preorderTraversal(TreeNode root, List<Integer> values) {
        if (root == null) {
            return;
        }
        values.add(root.val);
        preorderTraversal(root.left, values);
        preorderTraversal(root.right, values);
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Binary Tree Preorder Traversal.
     * Memory Usage: 40.8 MB, less than 85.33% of Java online submissions for Binary Tree Preorder Traversal.
     */
    public static List<Integer> preorderTraversalIterative(TreeNode root) {
        List<Integer> values = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        while (root != null) {
            values.add(root.val);
            if (root.right != null) {
                stack.push(root.right);
            }
            root = root.left;
            if (root == null && !stack.empty()) {
                root = stack.pop();
            }
        }
        return values;
    }
}
