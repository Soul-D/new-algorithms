package year_2022.leetcode.path_datastructure;

import year_2022.leetcode.model.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * [medium]
 * 235. Lowest Common Ancestor of a Binary Search Tree
 * Given a binary search tree (BST), find the lowest common ancestor (LCA) node of two given nodes in the BST.
 * <p>
 * According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined between two nodes p and q
 * as the lowest node in T that has both p and q as descendants (where we allow a node to be a descendant of itself).”
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [2, 10^5].
 * -10^9 <= Node.val <= 10^9
 * All Node.val are unique.
 * p != q
 * p and q will exist in the BST.
 */
public class LowestCommonAncestorOfBinarySearchTree {
    /**
     * Runtime: 4 ms, faster than 100.00% of Java online submissions for Lowest Common Ancestor of a Binary Search Tree.
     * Memory Usage: 42.5 MB, less than 99.99% of Java online submissions for Lowest Common Ancestor of a Binary Search Tree.
     */
    public static TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        List<TreeNode> pPath = getPath(root, p);
        List<TreeNode> qPath = getPath(root, q);
        int size = Math.min(pPath.size(), qPath.size());
        TreeNode candidate = null;

        for (int i = 0; i < size; i++) {
            if (pPath.get(i) == qPath.get(i)) {
                candidate = pPath.get(i);
            }
        }

        return candidate;
    }

    private static List<TreeNode> getPath(TreeNode root, TreeNode target) {
        if (root == target) {
            return Collections.singletonList(target);
        }

        List<TreeNode> result = new ArrayList<>();
        result.add(root);
        do {
            if (target.val < root.val) {
                root = root.left;
            } else {
                root = root.right;
            }
            result.add(root);
        } while (root != target);
        return result;
    }
}
