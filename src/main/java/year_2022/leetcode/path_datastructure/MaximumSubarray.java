package year_2022.leetcode.path_datastructure;

/**
 * [medium]
 * 53. Maximum Subarray
 * <p>
 * Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.
 * A subarray is a contiguous part of an array.
 * <p>
 * Constraints:
 * 1 <= nums.length <= 105
 * -10^4 <= nums[i] <= 10^4
 */
public class MaximumSubarray {
    public static void main(String[] args) {
        int[] nums = new int[]{5, 4, -1, 7, 8};
        System.out.println(maxSubArray(nums));
    }

    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Maximum Subarray.
     * Memory Usage: 73.6 MB, less than 62.30% of Java online submissions for Maximum Subarray.
     */
    public static int maxSubArray(int[] nums) {
        int result = nums[0];
        int sum = 0;
        for (int num : nums) {
            sum += num;
            result = Math.max(result, sum);
            sum = Math.max(sum, 0);
        }
        return result;
    }
}
