package year_2022.leetcode.path_datastructure;

import year_2022.leetcode.model.TreeNode;

/**
 * [easy]
 * 101. Symmetric Tree
 * <p>
 * Given the root of a binary tree, check whether it is a mirror of itself (i.e., symmetric around its center).
 * <p>
 * Constraints:
 * The number of nodes in the tree is in the range [1, 1000].
 * -100 <= Node.val <= 100
 */
public class SymmetricTree {
    /*
             6
         /        \
        2           2
      /   \       /   \
     0     4     4     0
  */
    public static void main(String[] args) {
        TreeNode node6 = new TreeNode(6);
        TreeNode node2 = new TreeNode(2);
        TreeNode node8 = new TreeNode(2);
        TreeNode node0 = new TreeNode(0);
        TreeNode node4 = new TreeNode(4);
        TreeNode node7 = new TreeNode(4);
        TreeNode node9 = new TreeNode(0);


        node6.left = node2;
        node6.right = node8;
        node2.left = node0;
        node2.right = node4;

        node8.left = node7;
        node8.right = node9;

        System.out.println(isSymmetric(node6));
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Symmetric Tree.
     * Memory Usage: 40.4 MB, less than 93.11% of Java online submissions for Symmetric Tree.
     */
    public static boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        return isSymmetric(root.left, root.right);
    }

    private static boolean isSymmetric(TreeNode left, TreeNode right) {
        if (left == null && right == null) {
            return true;
        }
        if (left == null || right == null) {
            return false;
        }
        if (left.val != right.val) {
            return false;
        }
        return isSymmetric(left.left, right.right) && isSymmetric(left.right, right.left);
    }
}
