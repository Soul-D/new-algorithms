package year_2022.leetcode.path_datastructure;

import year_2022.leetcode.model.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * [medium]
 * 102. Binary Tree Level Order Traversal
 * <p>
 * Given the root of a binary tree, return the level order traversal of its nodes' values. (i.e., from left to right, level by level).
 * <p>
 * Constraints:
 * The number of nodes in the tree is in the range [0, 2000].
 * -1000 <= Node.val <= 1000
 */
public class BinaryTreeLevelOrderTraversal {
    /*
                6
            /        \
           2           8
         /   \       /   \
        0     4     7      9
             /  \
             3   5
     */
    public static void main(String[] args) {
        TreeNode node6 = new TreeNode(6);
        TreeNode node2 = new TreeNode(2);
        TreeNode node8 = new TreeNode(8);
        TreeNode node0 = new TreeNode(0);
        TreeNode node4 = new TreeNode(4);
        TreeNode node7 = new TreeNode(7);
        TreeNode node9 = new TreeNode(9);
        TreeNode node3 = new TreeNode(3);
        TreeNode node5 = new TreeNode(5);


        node6.left = node2;
        node6.right = node8;
        node2.left = node0;
        node2.right = node4;
        node4.left = node3;
        node4.right = node5;

        node8.left = node7;
        node8.right = node9;

        System.out.println(levelOrder(node6));
    }

    /**
     * Runtime: 1 ms, faster than 93.16% of Java online submissions for Binary Tree Level Order Traversal.
     * Memory Usage: 44.2 MB, less than 8.24% of Java online submissions for Binary Tree Level Order Traversal.
     */
    public static List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> values = new ArrayList<>();
        if (root == null) {
            return values;
        }
        Queue<TreeNode> currentLevel = new LinkedList<>();
        currentLevel.add(root);
        while (!currentLevel.isEmpty()) {
            int levelLength = currentLevel.size();
            List<Integer> levelValues = new ArrayList<>();
            for (int i = 0; i < levelLength; i++) {
                TreeNode node = currentLevel.poll();
                levelValues.add(node.val);
                if (node.left != null) {
                    currentLevel.add(node.left);
                }
                if (node.right != null) {
                    currentLevel.add(node.right);
                }
            }
            values.add(levelValues);
        }

        return values;
    }
}
