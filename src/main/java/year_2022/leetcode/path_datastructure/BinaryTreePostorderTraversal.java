package year_2022.leetcode.path_datastructure;

import year_2022.leetcode.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * [easy]
 * 145. Binary Tree Postorder Traversal
 * <p>
 * Given the root of a binary tree, return the postorder traversal of its nodes' values.
 */
public class BinaryTreePostorderTraversal {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Binary Tree Postorder Traversal.
     * Memory Usage: 42.6 MB, less than 22.82% of Java online submissions for Binary Tree Postorder Traversal.
     */
    public static List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> values = new ArrayList<>();
        postorderTraversal(root, values);
        return values;
    }

    private static void postorderTraversal(TreeNode root, List<Integer> values) {
        if (root == null) {
            return;
        }
        postorderTraversal(root.left, values);
        postorderTraversal(root.right, values);
        values.add(root.val);
    }
}
