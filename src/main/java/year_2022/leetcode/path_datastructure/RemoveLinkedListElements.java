package year_2022.leetcode.path_datastructure;

import year_2022.leetcode.model.ListNode;

/**
 * [easy]
 * 203. Remove Linked List Elements
 * <p>
 * Given the head of a linked list and an integer val, remove all the nodes of the linked
 * list that has Node.val == val, and return the new head.
 */
public class RemoveLinkedListElements {
    public static void main(String[] args) {
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(1);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(1);
        ListNode node5 = new ListNode(5);
        ListNode node6 = new ListNode(5);
        ListNode node7 = new ListNode(5);
        ListNode node8 = new ListNode(1);
        ListNode node9 = new ListNode(5);
        ListNode node10 = new ListNode(1);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;
        node6.next = node7;
        node7.next = node8;
        node8.next = node9;
        node9.next = node10;

        removeElements(node1, 1).printNode();
    }

    /**
     * Runtime: 1 ms, faster than 99.10% of Java online submissions for Remove Linked List Elements.
     * Memory Usage: 49.7 MB, less than 12.21% of Java online submissions for Remove Linked List Elements.
     */
    public static ListNode removeElements(ListNode head, int val) {
        ListNode prev = null;
        ListNode result = null;
        ListNode cursor = head;

        while (cursor != null) {
            if (cursor.val != val) {
                if (result == null) {
                    result = cursor;
                }

                if (prev == null) {
                    prev = cursor;
                } else {
                    prev.next = cursor;
                    prev = prev.next;
                }
            }
            cursor = cursor.next;
        }

        if (prev != null) {
            prev.next = null;
        }
        return result;
    }
}
