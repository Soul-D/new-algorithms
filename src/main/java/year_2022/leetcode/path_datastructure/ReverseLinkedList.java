package year_2022.leetcode.path_datastructure;

import year_2022.leetcode.model.ListNode;

/**
 * [easy]
 * 206. Reverse Linked List
 * <p>
 * Given the head of a singly linked list, reverse the list, and return the reversed list.
 * <p>
 * Constraints:
 * The number of nodes in the list is the range [0, 5000].
 * -5000 <= Node.val <= 5000
 */
public class ReverseLinkedList {
    public static void main(String[] args) {
        ListNode node = ListNode.of(1/*, 2, 3, 4*/);
        reverseList(node).printNode();
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Reverse Linked List.
     * Memory Usage: 43.4 MB, less than 16.21% of Java online submissions for Reverse Linked List.
     */
    public static ListNode reverseList(ListNode head) {
        if (head == null) {
            return head;
        }

        ListNode result = null;
        while (head != null) {
            ListNode curr = new ListNode(head.val);
            curr.next = result;
            result = curr;
            head = head.next;
        }

        return result;
    }
}
