package year_2022.leetcode.path_datastructure;

import year_2022.leetcode.model.ListNode;

/**
 * [easy]
 * 83. Remove Duplicates from Sorted List
 * <p>
 * Constraints:
 * The number of nodes in the list is in the range [0, 300].
 * -100 <= Node.val <= 100
 * The list is guaranteed to be sorted in ascending order.
 */
public class RemoveDuplicatesFromSortedList {
    public static void main(String[] args) {
        ListNode node = ListNode.of(1, 1, 2, 3, 3, 4, 7, 7);
        deleteDuplicates(node).printNode();
    }

    /**
     * Runtime: 1 ms, faster than 79.17% of Java online submissions for Remove Duplicates from Sorted List.
     * Memory Usage: 43.8 MB, less than 73.97% of Java online submissions for Remove Duplicates from Sorted List.
     */
    public static ListNode deleteDuplicates(ListNode head) {
        ListNode result = head;
        while (head != null && head.next != null) {
            if (head.val == head.next.val) {
                head.next = head.next.next;
            } else {
                head = head.next;
            }
        }

        return result;
    }
}
