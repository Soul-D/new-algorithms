package year_2022.leetcode.path_datastructure;

import java.util.Stack;

/**
 * [easy]
 * 232. Implement Queue using Stacks
 * <p>
 * Implement a first in first out (FIFO) queue using only two stacks. The implemented queue should support all the functions of a normal queue (push, peek, pop, and empty).
 * Implement the MyQueue class:
 * void push(int x) Pushes element x to the back of the queue.
 * int pop() Removes the element from the front of the queue and returns it.
 * int peek() Returns the element at the front of the queue.
 * boolean empty() Returns true if the queue is empty, false otherwise.
 * Notes:
 * <p>
 * You must use only standard operations of a stack, which means only push to top, peek/pop from top, size, and is empty operations are valid.
 * Depending on your language, the stack may not be supported natively. You may simulate a stack using a list
 * or deque (double-ended queue) as long as you use only a stack's standard operations.
 */
public class ImplementQueueUsingStacks {
    /**
     * Runtime: 1 ms, faster than 71.15% of Java online submissions for Implement Queue using Stacks.
     * Memory Usage: 41.8 MB, less than 51.71% of Java online submissions for Implement Queue using Stacks.
     */
    public static class MyQueue {
        private Stack<Integer> stack;
        private Stack<Integer> queue;

        public MyQueue() {
            stack = new Stack<>();
            queue = new Stack<>();
        }

        public void push(int x) {
            while (!queue.isEmpty()) {
                stack.push(queue.pop());
            }
            stack.push(x);
            while (!stack.isEmpty()) {
                queue.push(stack.pop());
            }
        }

        public int pop() {
            return queue.pop();
        }

        public int peek() {
            return queue.peek();
        }

        public boolean empty() {
            return queue.isEmpty();
        }
    }
}
