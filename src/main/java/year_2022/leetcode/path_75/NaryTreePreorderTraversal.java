package year_2022.leetcode.path_75;

import year_2022.leetcode.model.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * [easy]
 * 589. N-ary Tree Preorder Traversal
 * <p>
 * Given the root of an n-ary tree, return the preorder traversal of its nodes' values.
 * Nary-Tree input serialization is represented in their level order traversal.
 * Each group of children is separated by the null value (See examples)
 */
public class NaryTreePreorderTraversal {
    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for N-ary Tree Preorder Traversal.
     * Memory Usage: 43.4 MB, less than 86.71% of Java online submissions for N-ary Tree Preorder Traversal.
     */
    public static List<Integer> preorder(Node root) {
        List<Integer> result = new ArrayList<>();

        traverse(root, result);

        return result;
    }

    private static void traverse(Node root, List<Integer> result) {
        if (root == null) {
            return;
        }
        result.add(root.val);
        if (root.children != null) {
            for (Node child : root.children) {
                traverse(child, result);
            }
        }
    }
}
