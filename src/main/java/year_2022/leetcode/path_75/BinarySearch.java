package year_2022.leetcode.path_75;

/**
 * [easy]
 * 704. Binary Search
 * <p>
 * Given an array of integers nums which is sorted in ascending order, and an integer target,
 * write a function to search target in nums. If target exists, then return its index. Otherwise, return -1.
 * You must write an algorithm with O(log n) runtime complexity.
 * <p>
 * Constraints:
 * 1 <= nums.length <= 10^4
 * -10^4 < nums[i], target < 10^4
 * All the integers in nums are unique.
 * nums is sorted in ascending order.
 */
public class BinarySearch {
    public static void main(String[] args) {
        System.out.println(search(new int[]{-1, 0, 3, 5, 9, 12}, 9));
        System.out.println(search(new int[]{-1, 0, 3, 5, 9, 12}, 2));
        System.out.println(search(new int[]{5}, 5));
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Binary Search.
     * Memory Usage: 54.8 MB, less than 15.46% of Java online submissions for Binary Search.
     */
    public static int search(int[] nums, int target) {
        int result = -1;

        if (nums.length > 0) {
            if (target < nums[0] || target > nums[nums.length - 1]) {
                return result;
            }
        }


        int low = 0;
        int high = nums.length;

        while (low < high) {
            int middle = (high + low) / 2;
            if (nums[middle] == target) {
                result = middle;
                break;
            }

            if (nums[middle] > target) {
                high = middle;
            } else {
                low = middle + 1;
            }
        }

        return result;
    }
}
