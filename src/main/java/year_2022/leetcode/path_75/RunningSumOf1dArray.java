package year_2022.leetcode.path_75;

import java.util.Arrays;

/**
 * [easy]
 * 1480. Running Sum of 1d Array
 * <p>
 * Given an array nums. We define a running sum of an array as runningSum[i] = sum(nums[0]…nums[i]).
 * <p>
 * Return the running sum of nums.
 */
public class RunningSumOf1dArray {
    public static void main(String[] args) {
        int[] nums = new int[]{3, 1, 2, 10, 1};
        System.out.println(Arrays.toString(runningSumMutable(nums)));
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Running Sum of 1d Array.
     * Memory Usage: 43.4 MB, less than 48.96% of Java online submissions for Running Sum of 1d Array.
     */
    public static int[] runningSum(int[] nums) {
        int[] result = new int[nums.length];
        int accum = 0;
        for (int i = 0; i < nums.length; i++) {
            accum += nums[i];
            result[i] = accum;
        }
        return result;
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Running Sum of 1d Array.
     * Memory Usage: 42.3 MB, less than 89.18% of Java online submissions for Running Sum of 1d Array.
     */
    public static int[] runningSumMutable(int[] nums) {
        int accum = 0;
        for (int i = 0; i < nums.length; i++) {
            accum += nums[i];
            nums[i] = accum;
        }
        return nums;
    }
}
