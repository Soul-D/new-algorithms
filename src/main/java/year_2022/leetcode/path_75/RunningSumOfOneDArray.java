package year_2022.leetcode.path_75;

import java.util.Arrays;

/**
 * [easy]
 * 1480. Running Sum of 1d Array
 * <p>
 * Given an array nums. We define a running sum of an array as runningSum[i] = sum(nums[0]…nums[i]).
 * Return the running sum of nums.
 * <p>
 * Constraints:
 * 1 <= nums.length <= 1000
 * -10^6 <= nums[i] <= 10^6
 */
public class RunningSumOfOneDArray {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(runningSum(new int[]{1, 2, 3, 4}))); // [1,3,6,10]
        System.out.println(Arrays.toString(runningSum(new int[]{1, 1, 1, 1, 1}))); // [1,2,3,4,5]
        System.out.println(Arrays.toString(runningSum(new int[]{3, 1, 2, 10, 1}))); // [3,4,6,16,17]
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Running Sum of 1d Array.
     * Memory Usage: 43.6 MB, less than 21.43% of Java online submissions for Running Sum of 1d Array.
     */
    public static int[] runningSum(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            if (i == 0) {
                continue;
            }
            nums[i] = nums[i] + nums[i - 1];
        }
        return nums;
    }
}
