package year_2022.leetcode.path_75;

/**
 * [easy]
 * 724. Find Pivot Index
 * <p>
 * Given an array of integers nums, calculate the pivot index of this array.
 * The pivot index is the index where the sum of all the numbers strictly to the left of the index is equal to the sum of all the numbers strictly to the index's right.
 * If the index is on the left edge of the array, then the left sum is 0 because there are no elements to the left. This also applies to the right edge of the array.
 * <p>
 * Return the leftmost pivot index. If no such index exists, return -1.
 */
public class FindPivotIndex {
    public static void main(String[] args) {
        int[] nums = new int[]{2, 1, -1};
        System.out.println(pivotIndex(nums));
    }

    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Find Pivot Index.
     * Memory Usage: 42.9 MB, less than 97.36% of Java online submissions for Find Pivot Index.
     */
    public static int pivotIndex(int[] nums) {
        int rightSum = 0;
        for (int num : nums) {
            rightSum += num;
        }
        int leftSum = 0;
        for (int i = 0; i < nums.length; i++) {
            rightSum -= nums[i];
            if (leftSum == rightSum) {
                return i;
            }
            leftSum += nums[i];
        }
        return -1;
    }
}
