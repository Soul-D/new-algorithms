package year_2022.leetcode.path_75;

import java.util.HashMap;
import java.util.Map;

/**
 * [easy]
 * 205. Isomorphic Strings
 * Given two strings s and t, determine if they are isomorphic.
 * Two strings s and t are isomorphic if the characters in s can be replaced to get t.
 * All occurrences of a character must be replaced with another character while preserving the order of characters.
 * No two characters may map to the same character, but a character may map to itself.
 * <p>
 * <p>
 * Constraints:
 * 1 <= s.length <= 5 * 10^4
 * t.length == s.length
 * s and t consist of any valid ascii character.
 */
public class IsomorphicStrings {

    public static void main(String[] args) {
        System.out.println(isIsomorphic("egg", "add")); // true
        System.out.println(isIsomorphic("foo", "bar")); // false
        System.out.println(isIsomorphic("paper", "title")); // true
        System.out.println(isIsomorphic("badc", "baba")); // false
    }

    /**
     * Runtime: 9 ms, faster than 70.65% of Java online submissions for Isomorphic Strings.
     * Memory Usage: 43.3 MB, less than 28.94% of Java online submissions for Isomorphic Strings.
     */
    public static boolean isIsomorphic(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        Map<Character, Character> mapping = new HashMap<>();
        Map<Character, Character> reverseMapping = new HashMap<>();

        for (int i = 0; i < s.length(); i++) {
            char sChar = s.charAt(i);
            char tChar = t.charAt(i);
            Character tCharacter = mapping.get(sChar);
            Character sCharacter = reverseMapping.get(tChar);
            if (sCharacter == null && tCharacter == null) {
                mapping.put(sChar, tChar);
                reverseMapping.put(tChar, sChar);
                continue;
            }
            if (sCharacter == null || tCharacter == null) {
                return false;
            }
            if (sChar != sCharacter || tChar != tCharacter) {
                return false;
            }
        }
        return true;
    }
}
