package year_2022.leetcode.path_75;

import java.util.*;

/**
 * [medium]
 * 692. Top K Frequent Words
 * <p>
 * Given an array of strings words and an integer k, return the k most frequent strings.
 * Return the answer sorted by the frequency from highest to lowest. Sort the words with the same frequency by their lexicographical order.
 * <p>
 * Constraints:
 * 1 <= words.length <= 500
 * 1 <= words[i].length <= 10
 * words[i] consists of lowercase English letters.
 * k is in the range [1, The number of unique words[i]]
 * <p>
 * Follow-up: Could you solve it in O(n log(k)) time and O(n) extra space?
 */
public class TopKFrequentWords {
    public static void main(String[] args) {
        System.out.println(topKFrequent(new String[]{"i", "love", "leetcode", "i", "love", "coding"}, 2));
        System.out.println(topKFrequentPriorityQueue(new String[]{"i", "love", "leetcode", "i", "love", "coding"}, 2));
        System.out.println(topKFrequent(new String[]{"i", "love", "leetcode", "i", "love", "coding"}, 3));
        System.out.println(topKFrequentPriorityQueue(new String[]{"i", "love", "leetcode", "i", "love", "coding"}, 3));
        System.out.println(topKFrequent(new String[]{"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"}, 4));
        System.out.println(topKFrequentPriorityQueue(new String[]{"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"}, 4));
    }

    /**
     * Runtime: 13 ms, faster than 25.67% of Java online submissions for Top K Frequent Words.
     * Memory Usage: 44.7 MB, less than 82.86% of Java online submissions for Top K Frequent Words.
     */
    public static List<String> topKFrequentPriorityQueue(String[] words, int k) {

        List<String> result = new LinkedList<>();
        Map<String, Integer> map = new HashMap<>();
        for (String word : words) {
            if (map.containsKey(word)) {
                map.put(word, map.get(word) + 1);
            } else {
                map.put(word, 1);
            }
        }

        PriorityQueue<Map.Entry<String, Integer>> pq = new PriorityQueue<>(
                (a, b) -> a.getValue() == b.getValue() ? b.getKey().compareTo(a.getKey()) : a.getValue() - b.getValue()
        );

        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            pq.offer(entry);
            if (pq.size() > k) {
                pq.poll();
            }
        }

        while (!pq.isEmpty()) {
            result.add(0, pq.poll().getKey());
        }

        return result;
    }

    /**
     * Runtime: 12 ms, faster than 34.15% of Java online submissions for Top K Frequent Words.
     * Memory Usage: 45.2 MB, less than 54.15% of Java online submissions for Top K Frequent Words.
     */
    public static List<String> topKFrequent(String[] words, int k) {
        List<String> result = new ArrayList<>();

        Map<String, Integer> container = new HashMap<>();
        for (String word : words) {
            container.merge(word, 1, Integer::sum);
        }

        List<Map.Entry<String, Integer>> entries = new ArrayList<>(container.entrySet());

        Comparator<Map.Entry<String, Integer>> cmp = (o1, o2) -> {
            if (!o1.getValue().equals(o2.getValue())) {
                return o1.getValue().compareTo(o2.getValue());
            }

            return -o1.getKey().compareTo(o2.getKey());
        };

        entries.sort(cmp);

        for (int i = entries.size() - 1; i >= 0; i--) {
            if (k == 0) {
                break;
            }
            result.add(entries.get(i).getKey());
            k--;
        }

        return result;
    }
}
