package year_2022.leetcode.path_75;

/**
 * [easy]
 * 392. Is Subsequence
 * <p>
 * Given two strings s and t, return true if s is a subsequence of t, or false otherwise.
 * A subsequence of a string is a new string that is formed from the original string by deleting some (can be none)
 * of the characters without disturbing the relative positions of the remaining characters. (i.e., "ace" is a subsequence
 * of "abcde" while "aec" is not).
 * <p>
 * Constraints:
 * 0 <= s.length <= 100
 * 0 <= t.length <= 10^4
 * s and t consist only of lowercase English letters.
 * <p>
 * Follow up: Suppose there are lots of incoming s, say s1, s2, ..., sk where k >= 10^9,
 * and you want to check one by one to see if t has its subsequence. In this scenario, how would you change your code?
 */
public class IsSubsequence {
    public static void main(String[] args) {
        System.out.println(isSubsequence("abc", "ahbgdc")); // true
        System.out.println(isSubsequence("axc", "ahbgdc")); // false
        System.out.println(isSubsequence("", "ahbgdc")); // false
        System.out.println(isSubsequence("abc", "ahbgdc")); // true
    }

    /**
     * Runtime: 1 ms, faster than 92.94% of Java online submissions for Is Subsequence.
     * Memory Usage: 41.9 MB, less than 67.27% of Java online submissions for Is Subsequence.
     */
    public static boolean isSubsequence(String s, String t) {
        if (s.isEmpty()) {
            return true;
        }
        int currIndex = 0;
        for (int i = 0; i < t.length(); i++) {
            if (t.charAt(i) == s.charAt(currIndex)) {
                currIndex++;
                if (currIndex == s.length()) {
                    return true;
                }
            }
        }
        return false;
    }
}
