package year_2022.leetcode.path_75;

/**
 * [easy]
 * 70. Climbing Stairs
 * <p>
 * You are climbing a staircase. It takes n steps to reach the top.
 * Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?
 * <p>
 * Constraints:
 * 1 <= n <= 45
 */
public class ClimbingStairs {
    public static void main(String[] args) {
        System.out.println(climbStairs(2)); // 2
        System.out.println(climbStairs(3)); // 3
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Climbing Stairs.
     * Memory Usage: 41.1 MB, less than 33.86% of Java online submissions for Climbing Stairs.
     */
    public static int climbStairs(int n) {
        int[] cache = new int[2];

        cache[0] = 1;
        cache[1] = 2;

        for (int i = 2; i < n; i++) {
            cache[i % 2] = cache[0] + cache[1];
        }

        return cache[(n - 1) % 2];
    }
}
