package year_2022.leetcode.path_75;

import year_2022.leetcode.model.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * [easy]
 * 235. Lowest Common Ancestor of a Binary Search Tree
 * <p>
 * Given a binary search tree (BST), find the lowest common ancestor (LCA) node of two given nodes in the BST.
 * According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined between
 * two nodes p and q as the lowest node in T that has both p and q as descendants (where we allow a node to be a descendant of itself).”
 */
public class LowestCommonAncestorOfBinarySearchTree {
//    [6,2,8,0,4,7,9,null,null,3,5]
    /*
                6
            /        \
           2           8
         /   \       /   \
        0     4     7      9
             /  \
             3   5
     */

    public static void main(String[] args) {
        TreeNode node6 = new TreeNode(6);
        TreeNode node2 = new TreeNode(2);
        TreeNode node8 = new TreeNode(8);
        TreeNode node0 = new TreeNode(0);
        TreeNode node4 = new TreeNode(4);
        TreeNode node7 = new TreeNode(7);
        TreeNode node9 = new TreeNode(9);
        TreeNode node3 = new TreeNode(3);
        TreeNode node5 = new TreeNode(5);


        node6.left = node2;
        node6.right = node8;
        node2.left = node0;
        node2.right = node4;
        node4.left = node3;
        node4.right = node5;

        node8.left = node7;
        node8.right = node9;


        TreeNode node = lowestCommonAncestor(node6, node2, node4);
        System.out.println(node.val);
    }

    public static TreeNode lowestCommonAncestorOneMore(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) {
            return null;
        }

        if (q.val < p.val) {
            TreeNode temp = q;
            q = p;
            p = temp;
        }

        if (p.val <= root.val && root.val <= q.val) {
            return root;
        }

        if (root.val > q.val) {
            return lowestCommonAncestorOneMore(root.left, p, q);
        } else {
            return lowestCommonAncestorOneMore(root.right, p, q);
        }
    }

    /**
     * Runtime: 7 ms, faster than 64.36% of Java online submissions for Lowest Common Ancestor of a Binary Search Tree.
     * Memory Usage: 50.9 MB, less than 6.63% of Java online submissions for Lowest Common Ancestor of a Binary Search Tree.
     */
    public static TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        List<TreeNode> pPath = getPath(root, p);
        List<TreeNode> qPath = getPath(root, q);
        int size = Math.min(pPath.size(), qPath.size());

        TreeNode candidate = null;

        for (int i = 0; i < size; i++) {
            if (pPath.get(i) == qPath.get(i)) {
                candidate = pPath.get(i);
            }
        }

        return candidate;
    }

    private static List<TreeNode> getPath(TreeNode root, TreeNode target) {
        if (root == target) {
            return Collections.singletonList(target);
        }

        List<TreeNode> result = new ArrayList<>();
        result.add(root);
        do {
            if (target.val < root.val) {
                root = root.left;
            } else {
                root = root.right;
            }
            result.add(root);
        } while (root != target);
        return result;
    }
}
