package year_2022.leetcode.path_75;

import year_2022.leetcode.model.ListNode;

/**
 * [easy]
 * 206. Reverse Linked List
 * <p>
 * Given the head of a singly linked list, reverse the list, and return the reversed list.
 * <p>
 * Constraints:
 * The number of nodes in the list is the range [0, 5000].
 * -5000 <= Node.val <= 5000
 */
public class ReverseLinkedList {
    public static void main(String[] args) {
        ListNode listNode = ListNode.of(1, 4, 7);
        System.out.println(reverseList(listNode));
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Reverse Linked List.
     * Memory Usage: 44 MB, less than 5.15% of Java online submissions for Reverse Linked List.
     */
    public static ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode tail = new ListNode(head.val);
        head = head.next;
        while (head != null) {
            ListNode temp = new ListNode(head.val);
            temp.next = tail;
            tail = temp;
            head = head.next;
        }
        return tail;
    }
}
