package year_2022.leetcode.path_75;

/**
 * [easy]
 * 509. Fibonacci Number
 * <p>
 * The Fibonacci numbers, commonly denoted F(n) form a sequence, called the Fibonacci sequence,
 * such that each number is the sum of the two preceding ones, starting from 0 and 1. That is,
 * F(0) = 0, F(1) = 1
 * F(n) = F(n - 1) + F(n - 2), for n > 1.
 * Given n, calculate F(n).
 * <p>
 * Constraints:
 * 0 <= n <= 30
 */
public class FibonacciNumber {
    public static void main(String[] args) {
        System.out.println(fib(2)); // 1
        System.out.println(fib(3)); // 2
        System.out.println(fib(4)); // 3
        System.out.println(fib(1)); // 1
        System.out.println(fib(0)); // 0
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Fibonacci Number.
     * Memory Usage: 41.1 MB, less than 32.32% of Java online submissions for Fibonacci Number.
     */
    public static int fib(int n) {
        if (n == 0) {
            return 0;
        }

        int prev = 0;
        int curr = 1;

        for (int i = 1; i < n; i++) {
            int temp = curr + prev;
            prev = curr;
            curr = temp;
        }

        return curr;
    }
}
