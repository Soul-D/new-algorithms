package year_2022.leetcode.path_75;

/**
 * [medium]
 * 200. Number of Islands
 * <p>
 * Given an m x n 2D binary grid `grid` which represents a map of '1's (land) and '0's (water), return the number of islands.
 * An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically.
 * You may assume all four edges of the grid are all surrounded by water.
 * <p>
 * Constraints:
 * m == grid.length
 * n == grid[i].length
 * 1 <= m, n <= 300
 * grid[i][j] is '0' or '1'.
 */
public class NumberOfIslands {
    public static void main(String[] args) {
        char[][] grid = new char[][]{
                {'1', '1', '0', '0', '0'},
                {'1', '1', '0', '0', '0'},
                {'0', '0', '1', '0', '0'},
                {'0', '0', '0', '1', '1'}
        };

        System.out.println(numIslands(grid));
    }

    /**
     * Runtime: 4 ms, faster than 77.46% of Java online submissions for Number of Islands.
     * Memory Usage: 58.1 MB, less than 18.04% of Java online submissions for Number of Islands.
     */
    public static int numIslands(char[][] grid) {
        int count = 0;
        for (int row = 0; row < grid.length; row++) {
            for (int col = 0; col < grid[0].length; col++) {
                if (grid[row][col] == '1') {
                    remarkIsland(grid, row, col);
                    count++;
                }
            }
        }
        return count;
    }

    private static void remarkIsland(char[][] grid, int row, int col) {
        if (row < 0 || col < 0 || row >= grid.length || col >= grid[0].length || grid[row][col] != '1') return;
        grid[row][col] = 'V';
        remarkIsland(grid, row - 1, col);
        remarkIsland(grid, row + 1, col);
        remarkIsland(grid, row, col - 1);
        remarkIsland(grid, row, col + 1);
    }
}
