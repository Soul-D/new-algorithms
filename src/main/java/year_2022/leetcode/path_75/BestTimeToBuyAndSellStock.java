package year_2022.leetcode.path_75;

/**
 * [easy]
 * 121. Best Time to Buy and Sell Stock
 * <p>
 * You are given an array prices where prices[i] is the price of a given stock on the ith day.
 * You want to maximize your profit by choosing a single day to buy one stock and choosing a different
 * day in the future to sell that stock.
 * Return the maximum profit you can achieve from this transaction. If you cannot achieve any profit, return 0.
 * <p>
 * Constraints:
 * 1 <= prices.length <= 10^5
 * 0 <= prices[i] <= 10^4
 */
public class BestTimeToBuyAndSellStock {
    public static void main(String[] args) {
        System.out.println(maxProfit(new int[]{7, 1, 5, 3, 6, 4})); // 5
        System.out.println(maxProfit(new int[]{7, 6, 4, 3, 1})); // 0
    }

    /**
     * Runtime: 3 ms, faster than 69.64% of Java online submissions for Best Time to Buy and Sell Stock.
     * Memory Usage: 83.8 MB, less than 49.60% of Java online submissions for Best Time to Buy and Sell Stock.
     */
    public static int maxProfit(int[] prices) {
        int profit = 0;
        int minPrice = Integer.MAX_VALUE;
        for (int i = 0; i < prices.length - 1; i++) {
            minPrice = Math.min(minPrice, prices[i]);
            int diff = prices[i + 1] - minPrice;
            profit = Math.max(diff, profit);
        }
        return profit;
    }
}
