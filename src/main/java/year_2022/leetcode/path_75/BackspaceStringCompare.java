package year_2022.leetcode.path_75;

/**
 * [easy]
 * 844. Backspace String Compare
 * <p>
 * Given two strings s and t, return true if they are equal when both are typed into empty text editors. '#' means a backspace character.
 * Note that after backspacing an empty text, the text will continue empty.
 * <p>
 * Constraints:
 * 1 <= s.length, t.length <= 200
 * s and t only contain lowercase letters and '#' characters.
 */
public class BackspaceStringCompare {
    public static void main(String[] args) {
        System.out.println(backspaceCompare("ab#c", "ad#c"));
        System.out.println(backspaceCompare("a#c", "b"));
        System.out.println(backspaceCompare("ab##", "c#d#"));
    }

    /**
     * Runtime: 1 ms, faster than 95.21% of Java online submissions for Backspace String Compare.
     * Memory Usage: 42 MB, less than 73.01% of Java online submissions for Backspace String Compare.
     */
    public static boolean backspaceCompare(String s, String t) {
        int sCursor = s.length() - 1;
        int tCursor = t.length() - 1;
        int sCut = 0;
        int tCut = 0;
        while (true) {
            while (sCursor >= 0 && (sCut > 0 || s.charAt(sCursor) == '#')) {
                sCut += s.charAt(sCursor) == '#' ? 1 : -1;
                sCursor--;
            }
            while (tCursor >= 0 && (tCut > 0 || t.charAt(tCursor) == '#')) {
                tCut += t.charAt(tCursor) == '#' ? 1 : -1;
                tCursor--;
            }
            if (sCursor >= 0 && tCursor >= 0 && s.charAt(sCursor) == t.charAt(tCursor)) {
                sCursor--;
                tCursor--;
            } else {
                break;
            }
        }
        return sCursor == -1 && tCursor == -1;
    }
}
