package year_2022.leetcode.path_75;

import year_2022.leetcode.model.ListNode;

/**
 * [easy]
 * 21. Merge Two Sorted Lists
 * <p>
 * You are given the heads of two sorted linked lists list1 and list2.
 * Merge the two lists in a one sorted list. The list should be made by splicing together the nodes of the first two lists.
 * Return the head of the merged linked list.
 * <p>
 * Constraints:
 * The number of nodes in both lists is in the range [0, 50].
 * -100 <= Node.val <= 100
 * Both list1 and list2 are sorted in non-decreasing order.
 */
public class MergeTwoSortedLists {
    public static void main(String[] args) {
        ListNode listNode1 = ListNode.of(1, 2, 4, 6, 9, 12);
        ListNode listNode2 = ListNode.of(1, 4, 7);
        System.out.println(mergeTwoLists(listNode1, listNode2));
    }

    /**
     * Runtime: 1 ms, faster than 80.19% of Java online submissions for Merge Two Sorted Lists.
     * Memory Usage: 43 MB, less than 52.10% of Java online submissions for Merge Two Sorted Lists.
     */
    public static ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        if (list1 == null) {
            return list2;
        }
        if (list2 == null) {
            return list1;
        }


        ListNode result, cursor;

        if (list1.val < list2.val) {
            cursor = result = new ListNode(list1.val);
            list1 = list1.next;
        } else {
            cursor = result = new ListNode(list2.val);
            list2 = list2.next;
        }


        while (list1 != null || list2 != null) {
            if (list1 == null || (list2 != null && list2.val < list1.val)) {
                cursor.next = new ListNode(list2.val);
                cursor = cursor.next;
                list2 = list2.next;
            } else {
                cursor.next = new ListNode(list1.val);
                cursor = cursor.next;
                list1 = list1.next;
            }
        }

        return result;
    }
}
