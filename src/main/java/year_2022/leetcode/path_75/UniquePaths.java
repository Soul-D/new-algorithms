package year_2022.leetcode.path_75;

/**
 * [medium]
 * 62. Unique Paths
 * <p>
 * There is a robot on an m x n grid. The robot is initially located at the top-left corner (i.e., grid[0][0]).
 * The robot tries to move to the bottom-right corner (i.e., grid[m - 1][n - 1]).
 * The robot can only move either down or right at any point in time.
 * Given the two integers m and n, return the number of possible unique paths that the
 * robot can take to reach the bottom-right corner.
 * <p>
 * The test cases are generated so that the answer will be less than or equal to 2 * 10^9.
 * <p>
 * Constraints:
 * 1 <= m, n <= 100
 */
public class UniquePaths {
    public static void main(String[] args) {
        System.out.println(uniquePaths(3, 7)); // 28
        System.out.println(uniquePaths(3, 2)); // 3
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Unique Paths.
     * Memory Usage: 41.4 MB, less than 21.97% of Java online submissions for Unique Paths.
     */
    public static int uniquePaths(int m, int n) {
        int totalMoves = n + m - 2;
        int moves = m - 1;
        long res = 1;
        for (int i = 1; i <= moves; i++) {
            res = res * (totalMoves - moves + i) / i;
        }
        return (int) res;
    }
}
