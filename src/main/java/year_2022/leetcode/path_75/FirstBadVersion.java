package year_2022.leetcode.path_75;

/**
 * [easy]
 * 278. First Bad Version
 * <p>
 * You are a product manager and currently leading a team to develop a new product. Unfortunately,
 * the latest version of your product fails the quality check. Since each version is developed based on the previous version,
 * all the versions after a bad version are also bad.
 * Suppose you have n versions [1, 2, ..., n] and you want to find out the first bad one, which causes all the following ones to be bad.
 * You are given an API bool isBadVersion(version) which returns whether version is bad. Implement a function to find
 * the first bad version. You should minimize the number of calls to the API.
 * <p>
 * Constraints:
 * 1 <= bad <= n <= 2^31 - 1
 */
public class FirstBadVersion {
    private static final int bad = 4;

    public static void main(String[] args) {
        System.out.println(firstBadVersion(5));
    }

    public static boolean isBadVersion(int version) {
        return version >= bad;
    }

    /**
     * Runtime: 16 ms, faster than 84.32% of Java online submissions for First Bad Version.
     * Memory Usage: 39.3 MB, less than 86.06% of Java online submissions for First Bad Version.
     */
    public static int firstBadVersion(int n) {
        int start = 1;
        int end = n;
        while (start < end) {
            int mid = start + (end - start) / 2;
            if (!isBadVersion(mid)) {
                start = mid + 1;
            } else {
                end = mid;
            }
        }
        return start;
    }
}
