package year_2022.leetcode.path_75;

/**
 * [medium]
 * 394. Decode String
 * <p>
 * Given an encoded string, return its decoded string.
 * The encoding rule is: k[encoded_string], where the encoded_string inside the square brackets is being repeated exactly k times.
 * Note that k is guaranteed to be a positive integer.
 * You may assume that the input string is always valid; there are no extra white spaces,
 * square brackets are well-formed, etc. Furthermore, you may assume that the original data
 * does not contain any digits and that digits are only for those repeat numbers, k. For example, there will not be input like 3a or 2[4].
 * <p>
 * The test cases are generated so that the length of the output will never exceed 10^5.
 * <p>
 * Constraints:
 * 1 <= s.length <= 30
 * s consists of lowercase English letters, digits, and square brackets '[]'.
 * s is guaranteed to be a valid input.
 * All the integers in s are in the range [1, 300].
 */
public class DecodeString {
    /**
     * Runtime: 1 ms, faster than 89.41% of Java online submissions for Decode String.
     * Memory Usage: 42.4 MB, less than 29.12% of Java online submissions for Decode String.
     */
    int index = 0;

    public static void main(String[] args) {
        System.out.println(new DecodeString().decodeString("3[a]2[bc]")); //"aaabcbc"
        System.out.println(new DecodeString().decodeString("3[a2[c]]")); //"accaccacc"
        System.out.println(new DecodeString().decodeString("2[abc]3[cd]ef")); //"abcabccdcdcdef"
    }

    public String decodeString(String s) {
        StringBuilder result = new StringBuilder();
        while (index < s.length() && s.charAt(index) != ']') {
            if (!Character.isDigit(s.charAt(index))) {
                result.append(s.charAt(index++));
            } else {
                int k = 0;
                // build k while next character is a digit
                while (index < s.length() && Character.isDigit(s.charAt(index))) {
                    k = k * 10 + s.charAt(index++) - '0';
                }
                // ignore the opening bracket '['
                index++;
                String decodedString = decodeString(s);
                // ignore the closing bracket ']'
                index++;
                // build k[decodedString] and append to the result
                while (k-- > 0) {
                    result.append(decodedString);
                }
            }
        }
        return new String(result);

    }
}
