package year_2022.leetcode.path_75;

/**
 * [easy]
 * 409. Longest Palindrome
 * <p>
 * Given a string s which consists of lowercase or uppercase letters,
 * return the length of the longest palindrome that can be built with those letters.
 * Letters are case sensitive, for example, "Aa" is not considered a palindrome here.
 * <p>
 * Constraints:
 * 1 <= s.length <= 2000
 * s consists of lowercase and/or uppercase English letters only.
 */
public class LongestPalindrome {
    public static void main(String[] args) {
        System.out.println(longestPalindrome("abccccdd")); //7
        System.out.println(longestPalindrome("a")); // 1
    }

    /**
     * Runtime: 1 ms, faster than 100.00% of Java online submissions for Longest Palindrome.
     * Memory Usage: 42.2 MB, less than 69.37% of Java online submissions for Longest Palindrome.
     */
    public static int longestPalindrome(String s) {
        int length = 0;
        boolean oddIsNotCounted = true;
        int[] counter = new int[58];
        char[] chars = s.toCharArray();
        for (char aChar : chars) {
            counter[aChar - 65]++;
        }

        for (int i : counter) {
            if (i == 0) {
                continue;
            }
            if (i % 2 == 0) {
                length += i;
            } else {
                if (oddIsNotCounted) {
                    length += i;
                    oddIsNotCounted = false;
                } else {
                    length += (i - 1);
                }
            }
        }
        return length;
    }
}
