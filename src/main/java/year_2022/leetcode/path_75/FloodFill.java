package year_2022.leetcode.path_75;

/**
 * [easy]
 * 733. Flood Fill
 * <p>
 * An image is represented by an m x n integer grid image where image[i][j] represents the pixel value of the image.
 * You are also given three integers sr, sc, and color. You should perform a flood fill on the image starting from the pixel image[sr][sc].
 * To perform a flood fill, consider the starting pixel, plus any pixels connected 4-directionally to the
 * starting pixel of the same color as the starting pixel, plus any pixels connected 4-directionally to those pixels
 * (also with the same color), and so on. Replace the color of all of the aforementioned pixels with color.
 * Return the modified image after performing the flood fill.
 * <p>
 * Constraints:
 * m == image.length
 * n == image[i].length
 * 1 <= m, n <= 50
 * 0 <= image[i][j], color < 2^16
 * 0 <= sr < m
 * 0 <= sc < n
 */
public class FloodFill {
    /**
     * Runtime: 1 ms, faster than 90.58% of Java online submissions for Flood Fill.
     * Memory Usage: 48 MB, less than 50.05% of Java online submissions for Flood Fill.
     */
    public static int[][] floodFill(int[][] image, int sr, int sc, int color) {
        if (image[sr][sc] != color) {
            fill(image, sr, sc, image[sr][sc], color);
        }

        return image;
    }

    private static void fill(int[][] image, int i, int j, int oldColor, int newColor) {
        if (i < 0 || j < 0 || i >= image.length || j >= image[0].length || image[i][j] != oldColor) {
            return;
        }
        image[i][j] = newColor;
        fill(image, i, j - 1, oldColor, newColor);
        fill(image, i, j + 1, oldColor, newColor);
        fill(image, i - 1, j, oldColor, newColor);
        fill(image, i + 1, j, oldColor, newColor);
    }

}
