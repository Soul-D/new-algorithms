package year_2022.leetcode.path_75;

import java.util.Arrays;
import java.util.Collections;
import java.util.PriorityQueue;

/**
 * [easy]
 * 1046. Last Stone Weight
 * <p>
 * You are given an array of integers stones where stones[i] is the weight of the ith stone.
 * We are playing a game with the stones. On each turn, we choose the heaviest two stones and smash them together.
 * Suppose the heaviest two stones have weights x and y with x <= y. The result of this smash is:
 * If x == y, both stones are destroyed, and
 * If x != y, the stone of weight x is destroyed, and the stone of weight y has new weight y - x.
 * At the end of the game, there is at most one stone left.
 * Return the weight of the last remaining stone. If there are no stones left, return 0.
 * <p>
 * Constraints:
 * 1 <= stones.length <= 30
 * 1 <= stones[i] <= 1000
 */
public class LastStoneWeight {
    /**
     * Runtime: 2 ms, faster than 82.55% of Java online submissions for Last Stone Weight.
     * Memory Usage: 41.8 MB, less than 36.66% of Java online submissions for Last Stone Weight.
     */
    public static int lastStoneWeight(int[] stones) {
        Arrays.sort(stones);
        for (int i = stones.length - 1; i > 0; i--) {
            stones[i - 1] = stones[i] - stones[i - 1];
            Arrays.sort(stones);
        }
        return stones[0];
    }

    /**
     * Runtime: 3 ms, faster than 43.06% of Java online submissions for Last Stone Weight.
     * Memory Usage: 41.8 MB, less than 36.66% of Java online submissions for Last Stone Weight.
     */
    public static int lastStoneWeightQueue(int[] stones) {
        PriorityQueue<Integer> queue = new PriorityQueue<>(Collections.reverseOrder());
        for (int stone : stones) {
            queue.add(stone);
        }

        while (queue.size() > 1) {
            int heaviest = queue.poll();
            int preHeavies = queue.poll();
            if (heaviest > preHeavies) {
                queue.add(heaviest - preHeavies);
            }
        }

        return queue.isEmpty() ? 0 : queue.poll();
    }
}
