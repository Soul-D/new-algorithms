package year_2022.leetcode.path_75;

import year_2022.leetcode.model.ListNode;

/**
 * [easy]
 * 876. Middle of the Linked List
 * <p>
 * Given the head of a singly linked list, return the middle node of the linked list.
 * If there are two middle nodes, return the second middle node.
 * <p>
 * Constraints:
 * The number of nodes in the list is in the range [1, 100].
 * 1 <= Node.val <= 100
 */
public class MiddleOfTheLinkedList {
    public static void main(String[] args) {
        ListNode node = ListNode.of(1, 2, 3, 4);
        System.out.println(middleNode(node).val);
    }

    /**
     * Runtime: 0 ms, faster than 100.00% of Java online submissions for Middle of the Linked List.
     * Memory Usage: 41.4 MB, less than 64.30% of Java online submissions for Middle of the Linked List.
     */
    public static ListNode middleNode(ListNode head) {
        ListNode fast = head;

        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            head = head.next;
        }

        return head;
    }
}
