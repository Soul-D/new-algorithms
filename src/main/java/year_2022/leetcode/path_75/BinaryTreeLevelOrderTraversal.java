package year_2022.leetcode.path_75;

import year_2022.leetcode.model.TreeNode;

import java.util.*;

/**
 * [medium]
 * 102. Binary Tree Level Order Traversal
 * Given the root of a binary tree, return the level order traversal of its nodes' values. (i.e., from left to right, level by level).
 * <p>
 * Constraints:
 * The number of nodes in the tree is in the range [0, 2000].
 * -1000 <= Node.val <= 1000
 */
public class BinaryTreeLevelOrderTraversal {
    public static void main(String[] args) {
        TreeNode head = new TreeNode(3);
        TreeNode head9 = new TreeNode(9);
        TreeNode head20 = new TreeNode(20);
        TreeNode head15 = new TreeNode(15);
        TreeNode head7 = new TreeNode(7);
        head.left = head9;
        head.right = head20;
        head20.left = head15;
        head20.right = head7;

        System.out.println(levelOrder(head));
    }

    /**
     * Runtime: 2 ms, faster than 36.73% of Java online submissions for Binary Tree Level Order Traversal.
     * Memory Usage: 43.9 MB, less than 28.08% of Java online submissions for Binary Tree Level Order Traversal.
     */
    public static List<List<Integer>> levelOrder(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        List<List<Integer>> result = new ArrayList<>();
        if (root != null) {
            queue.add(root);
        }
        while (!queue.isEmpty()) {
            List<TreeNode> level = new ArrayList<>();
            while (!queue.isEmpty()) {
                TreeNode el = queue.poll();
                if (el != null) {
                    level.add(el);
                }
            }
            List<Integer> levelNums = new ArrayList<>(level.size());
            for (TreeNode treeNode : level) {
                levelNums.add(treeNode.val);
                if (treeNode.left != null) {
                    queue.add(treeNode.left);
                }
                if (treeNode.right != null) {
                    queue.add(treeNode.right);
                }
            }
            result.add(levelNums);
        }
        return result;
    }

    /**
     * Runtime: 1 ms, faster than 93.16% of Java online submissions for Binary Tree Level Order Traversal.
     * Memory Usage: 43.7 MB, less than 37.81% of Java online submissions for Binary Tree Level Order Traversal.
     */
    public static List<List<Integer>> levelOrderOneMore(TreeNode root) {
        Queue<List<TreeNode>> queue = new LinkedList<>();
        List<List<Integer>> result = new ArrayList<>();
        if (root != null) {
            queue.add(Collections.singletonList(root));
        }

        while (!queue.isEmpty()) {
            List<TreeNode> level = queue.poll();
            List<TreeNode> nextLevel = new ArrayList<>();
            List<Integer> levelNums = new ArrayList<>();

            for (TreeNode treeNode : level) {
                levelNums.add(treeNode.val);
                if (treeNode.left != null) {
                    nextLevel.add(treeNode.left);
                }
                if (treeNode.right != null) {
                    nextLevel.add(treeNode.right);
                }
            }

            if (!nextLevel.isEmpty()) {
                queue.add(nextLevel);
            }
            result.add(levelNums);
        }

        return result;
    }
}
