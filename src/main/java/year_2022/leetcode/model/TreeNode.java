package year_2022.leetcode.model;

public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(TreeNode left, int x, TreeNode right) {
        this.left = left;
        val = x;
        this.right = right;
    }

    public TreeNode(int x, TreeNode right) {
        this(null, x, right);
    }

    public TreeNode(TreeNode left, int x) {
        this(left, x, null);
    }

    public TreeNode(int x) {
        this(null, x, null);
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "val=" + val +
                ", left=" + left +
                ", right=" + right +
                '}';
    }
}
